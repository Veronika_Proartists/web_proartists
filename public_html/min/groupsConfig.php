<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
     'css' => array('//templates/proart/css/normalize.css', '//templates/proart/css/main.css', '//templates/proart/css/media.css', '//templates/proart/css/jquery.arcticmodal-0.3.css', '//templates/proart/slick/slick.css', '//templates/proart/colorbox/colorbox.css')
//     'js' => array('//templates/designer/js/nivo_slider/jquery.nivo.slider.pack.js','//templates/designer/js/nicescroll/jquery.nicescroll.min.js','//templates/designer/js/foxi.common.js','//templates/designer/js/modernizr.js','//templates/designer/js/main.js','//templates/designer/js/jquery.isotope.min.js','//templates/designer/js/pace.min.js','//templates/designer/js/jquery.arcticmodal/jquery.arcticmodal-0.3.min.js')
);

/*'//js/site/__common.js','//js/client/utilities.js','//js/client/basket.js','//js/site/basket.js','//js/site/forms.js','//js/site/message.js','//js/site/captcha.js','//js/jquery/jquery.cookie.js',*/
