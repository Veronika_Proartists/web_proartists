<?php
	@require CURRENT_WORKING_DIR . '/libs/config.php';

	$buffer = outputBuffer::current('HTTPOutputBuffer');
	$buffer->contentType('text/xml');
	$buffer->charset('utf-8');

	$cmsController = cmsController::getInstance();
	$currentDomain = $cmsController->getCurrentDomain();

	if (!$currentDomain instanceof domain) {
		throw new Exception('Cannot detect current domain');
	}

	$domainId = $currentDomain->getId();
	$host = $currentDomain->getHost();

	switch (true) {
		case (isset($_GET['id']) && $_GET['id'] === '') : {
			$sql = 'SELECT count(`id`) FROM `cms_sitemap` WHERE `domain_id`=%d';
			$sql = sprintf($sql, $domainId);
			$res = l_mysql_query($sql);
			$res = mysql_fetch_row($res);

			if (isset($res[0]) && (int) $res[0] > 50000) {
				$siteMap = genSiteIndex($domainId, $host);
				break;
			}

			$siteMap = genSiteMap($domainId);
			break;
		}
		case (isset($_GET['id']) && (int) $_GET['id'] >= 0 && (int) $_GET['id'] <= 16) : {
			$siteMap = genSiteMap($domainId, $_GET['id']);
			break;
		}
		default : {
			$siteMap = getEmptySitemap();
		}
	}

	$buffer->push($siteMap);
	$buffer->end();
	exit();

	/**
	 * Возвращает индексную страницу карты сайта с пагинацией
	 * @param int $domainId идентификатор текущего домена
	 * @param string $host текущий домен
	 * @return string
	 */
	function genSiteIndex($domainId, $host) {
		$siteMap = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		$sql = 'SELECT `sort`, MAX(  `dt` ) FROM `cms_sitemap` WHERE `domain_id`=%d GROUP BY `sort`';
		$sql = sprintf($sql, $domainId);
		$res = l_mysql_query($sql);
		while (list($sort, $dt) = mysql_fetch_row($res)) {
			$dom = new DOMDocument();
			$url = $dom->createElement('sitemap');
			$loc = $dom->createElement('loc', sprintf('%s://%s/sitemap%d.xml', getSelectedServerProtocol(), $host, $sort));
			$lastMod = $dom->createElement('lastmod', date('c', strtotime($dt)));
			$dom->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastMod);
			$siteMap .= $dom->saveXML($url);
		}
		$siteMap .= '</sitemapindex>';
		return $siteMap;
	}

	/**
	 * Возвращает карту сайта
	 * @param int $domainId идентификатор текущего домена
	 * @param bool|int $pageId номер страницы в рамках пагинации
	 * @return string
	 */
	function genSiteMap($domainId, $pageId = false) {
		$siteMap = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		$domainId = (int) $domainId;
		$sql = "SELECT `link`,`priority`,`dt` FROM `cms_sitemap` WHERE `domain_id` = $domainId";
		if ($pageId !== false) {
			$sql .= ' AND `sort`=%d';
			$sql = sprintf($sql, $pageId);
		}
		$sql .= ' ORDER BY `level`;';
		$res = l_mysql_query($sql);
		while (list($link, $pagePriority, $dt) = mysql_fetch_row($res)) {
			$dom = new DOMDocument();
			$url = $dom->createElement('url');
			$loc = $dom->createElement('loc', $link);
			$priority = $dom->createElement('priority', $pagePriority);
			$lastMod = $dom->createElement('lastmod', date('c', strtotime($dt)));
			$dom->appendChild($url);
			$url->appendChild($loc);
			$url->appendChild($lastMod);
			$url->appendChild($priority);
			$siteMap .= $dom->saveXML($url);
		}
		$siteMap .= '</urlset>';
		return $siteMap;
	}

	/**
	 * Возвращает пустую карту сайта
	 * @return string
	 */
	function getEmptySiteMap() {
		return '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>';
	}
?>