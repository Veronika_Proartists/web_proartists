<?php
	/**
	 * Интерфейс менеджера системных настроек
	 */
	interface iConfiguration {

		/**
		 * Возвращает экземпляр класса конкретного менеджера системных настроек
		 * @return iConfiguration
		 */
		public static function getInstance();

		/**
		 * Возвращает все системные настройки
		 * @return array|null
		 */
		public function getParsedIni();

		/**
		 * Возвращает значение настройки
		 * @param string $section группа настройки
		 * @param string $variable название настройки
		 * @return mixed
		 */
		public function get($section, $variable);

		/**
		 * Устанавливает значение настройки
		 * @param string $section группа настройки
		 * @param string $variable название настройки
		 * @param mixed $value значение настройки
		 * @return mixed
		 */
		public function set($section, $variable, $value);

		/**
		 * Возвращает все системные настройки по секции
		 * @param string $section группа настройки
		 * @return array|null
		 */
		public function getList($section);

		/**
		 * Возвращает значение настройки из группы "includes"
		 * с преобразованиями
		 * @param string $key название настройки
		 * @param array $params параметры преобразования
		 * @return string
		 */
		public function includeParam($key, array $params =  null);

		/**
		 * Деструктор
		 */
		public function __destruct();
	}
?>