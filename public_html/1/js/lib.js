﻿function ValidateFields(myform)
{
	for(i=0;i<myform.elements.length;i++)
	{
		var el = myform.elements[i];
		if(el && el.hasAttribute('required'))
		{
			var holder = el.getAttribute('placeholder');
			var match_mail = /[0-9a-z_]+@[0-9a-z_^\.]+\.[a-z]{2,}/;
			var match_phone = /^([+]?[0-9]{1,2}\s?)?([(\s]?[0-9]{3}[)\s])?([0-9]{1,}[\s-]?)*[0-9]+$/;
			
			if((el.value == null) || (el.value == '') || (el.value == holder)) {
				addClass(el, 'ValidationError');
				alert("Укажите пожалуйста «" + holder + "»");
				el.focus();
				return false;
			} else if ($(el).attr('name') && $(el).attr('name') == 'mail' && !match_mail.test($(el).val())) {
				addClass(el, 'ValidationError');
				alert("Введите почтовый адрес в правильном формате");
				el.focus();
				return false;
			} else {
				removeClass(el, 'ValidationError');
			}
		}
	}
	return true;
}

function hasClass(ele,cls)
{
	return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function addClass(ele,cls)
{
	if (!this.hasClass(ele,cls)) ele.className += " "+cls;
}

function removeClass(ele,cls)
{
	if (hasClass(ele,cls))
	{
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className=ele.className.replace(reg,' ');
	}
}

function setPlaceholdersToForm(myform)
{
	var f = document.getElementById(myform);
	if(!f) return;
	for(i=0;i<f.elements.length;i++)
	{
		var el = f.elements[i];
		if(el && el.hasAttribute('placeholder'))
		{
			el.onfocus = function(){
					var holder = this.getAttribute('placeholder');
					var deftext = '';
					if(this.hasAttribute('defaulttext'))
					{
						deftext = this.getAttribute('defaulttext');
					}
					if(this.value && (this.value == holder))
					{
						this.value = deftext;
						removeClass(this, 'PlaceholderText');
					}
				}
			el.onblur = function(){
					var holder = this.getAttribute('placeholder');
					var deftext = '';
					if(this.hasAttribute('defaulttext'))
					{
						deftext = this.getAttribute('defaulttext');
					}
					if(!this.value || (this.value == holder) || (this.value == deftext))
					{
						this.value = holder;
						addClass(this, 'PlaceholderText');
					}
				}
			el.value = el.getAttribute('placeholder');
			addClass(el, 'PlaceholderText');
		}
	}
}

function AddDropdownMenuBehavior() {
	$('.DropdownMenu').mouseenter(function() {
		$(this).find('.MenuLevel2').fadeIn(300);
		$(this).find('.MenuLevel2').mouseleave(function() {
			$(this).fadeOut(300);
		});
	}).mouseleave(function() {
		$(this).find('.MenuLevel2').fadeOut(300);
	});
;
}

function AddScrollUpButtonBefavior() {
	if($(this).scrollTop() > $(this).height()) {
		$('#UpButton').fadeIn(100);
	} else {
		$('#UpButton').fadeOut(100);
	}
	$(window).scroll(function() {
		if($(this).scrollTop() > $(this).height()) {
			$('#UpButton').fadeIn(400);
		} else {
			$('#UpButton').fadeOut(400);
		}
	});
	$('#UpButton').click(function() {
		$('html, body').animate({scrollTop : 0},500);
	});
}

function preloadImage(img_url)
{
	$("<img>").attr("src", img_url);
}

// Плавно прокрутить страницу к елементу с указанным id
// USAGE:
//		<a href="javascript: void(0);" onclick="javascript: ScrollTo('Anchor1');"></a>
//		<div id="Anchor1"></div>
function ScrollTo(element_id) {
	var top;
	top = $('#' + element_id).offset().top;
	$('html, body').animate({scrollTop : top}, 1000);
}

$(document).ready(function (){
	$('.Enlargable').click(function() {
		var html = '<div id="EnlargedImg" style="position: fixed; width: 0px; height: 0px; top: 50%; left: 50%; text-align: center; z-index: 99;">' +
			'<div id="EnlargedImgBg" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; background-color: black; opacity: 0.3; filter: alpha(opacity=30);"></div>' +
			'<div id="EnlargedImgBlock" style="position: relative; top: 50%;"><img src="' + $(this).attr('src') + '" style="width: auto; height: auto;" /></div>' +
			'</div>';
		$(this).after(html);
		$('#EnlargedImgBlock').css("margin-top", "-" + Math.ceil($('#EnlargedImg div img').height()/2) + "px");
		$('#EnlargedImg').animate({width: "100%", height: "100%", top: "0px", left: "0px"}, 200, "swing", function (){
			$('#EnlargedImgBlock').css("margin-top", "-" + Math.ceil($('#EnlargedImg div img').height()/2) + "px");
		});
		$('#EnlargedImg').click(function (){
			$(this).animate({width: "0", height: "0", top: "50%", left: "50%"}, 200, "swing", function() {$(this).remove();});
		});
	});
	$('.EnlargableNew').each(function() {
		preloadImage($(this).attr('src').replace('-mini',''));
	});
	$('.EnlargableNew').click(function() {
		var img_src = "";
		var html = '<div id="EnlargedImg" style="position: fixed; width: 0px; height: 0px; top: 50%; left: 50%; text-align: center; z-index: 99;">' +
			'<div id="EnlargedImgBg" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; background-color: black; opacity: 0.3; filter: alpha(opacity=30);"></div>' +
			'<div id="EnlargedImgBlock" style="position: relative; top: 50%;margin: 0 auto;background: url(images/waiting.png) white center no-repeat;text-align:center;"><img src="' + $(this).attr('src').replace('-mini','') + '" style="width: auto; height: auto; margin: 0 auto; float: none;" /></div>' +
			'</div>';
		$(this).after(html);
		var h = ($('#EnlargedImg div img').height() > $(window).height() ? $(window).height() - 40 : $('#EnlargedImg div img').height());
		h = (h < 100 ? $(window).height() - 40 : h);
		$('#EnlargedImg div img').css('height', h + 'px');
		$('#EnlargedImgBlock').css("margin-top", "-" + Math.ceil(h/2) + "px");
		$('#EnlargedImg').animate({width: "100%", height: "100%", top: "0px", left: "0px"}, 200, "swing", function (){
			$('#EnlargedImgBlock').css("margin-top", "-" + Math.ceil(h/2) + "px");
			$('#EnlargedImgBlock').css('width', $('#EnlargedImg div img').outerWidth());
			$('#EnlargedImgBlock').css('height', $('#EnlargedImg div img').outerHeight());
		});
		$('#EnlargedImgBg').click(function (){
			$(this).parent().animate({width: "0", height: "0", top: "50%", left: "50%"}, 200, "swing", function() {$(this).remove();});
		});
		img_src = $('#EnlargedImg div img').attr('src').replace('-mini','');
		$('#EnlargedImg div img').fadeOut(200, function() {$(this).attr('src', img_src);});
		$('#EnlargedImg div img').load(function() {
			if ($(this).attr('src') == img_src) {
				$('#EnlargedImg div img').css('height', 'auto');
				$('#EnlargedImg div img').css('width', 'auto');
				h = ($('#EnlargedImg div img').height() > $(window).height() ? $(window).height() - 40 : $('#EnlargedImg div img').height());
				h = (h < 100 ? $(window).height() - 40 : h);
				$('#EnlargedImg div img').css('height', h + 'px');
				$('#EnlargedImgBlock').animate(
												{
													width: $('#EnlargedImg div img').width(),
													height: h + 'px',
													marginTop: '-' + Math.ceil(h/2) + 'px'
												},
												200,
												"swing",
												function() {$('#EnlargedImg img').fadeIn(200);}
												);
			}
		});
		$('#EnlargedImg img').click(function (){
			var img = $(this).parent().parent().prev();
			img.next().next().after($(this).parent().parent());
			if(!img.next().attr('src')) {
				$('#EnlargedImg').remove();
				return;
			}
			img_src = img.next().attr('src').replace('-mini','');
			$(this).fadeOut(200, function() {$(this).attr('src', img_src);});
		});
	});

	AddDropdownMenuBehavior();

	AddScrollUpButtonBefavior();
	
	$('.material img').mouseenter(function() {
		$(this).parent().addClass('active');
		$(this).parent().stop(true, true).animate({height : "385px"}, 300, "swing", function() {
			$(this).find('.expand_block').css("display", "block");
		});
	});
	$('.material').mouseleave(function() {
		var obj = $(this);
		$(this).stop(true, true).find('.expand_block').css("display", "none");
		obj.stop(true, true).animate({height : "225px"}, 300, "swing", function() {
			obj.removeClass('active');
		});
	});
	
	var add_prod1 = $('#prod1 .addition').outerHeight();
	
	$('#prod1 .addition').css('height', '0px').find('.prod1_table').css('display', 'none');
	
	$('#expand_prod1').click(function() {
		if($(this).hasClass('Expanded')) {
			$(this).removeClass('Expanded');
			$(this).find('img').attr('src', 'images/button3.png');
			$('#prod1 .addition .prod1_table').fadeOut(200,
				function() {
					$('#prod1 .addition').animate({height : "0px"}, 300);
				}
			);
		} else {
			$(this).addClass('Expanded');
			$(this).find('img').attr('src', 'images/button5.png');
			$('#prod1 .addition').animate({height : add_prod1 + "px"}, 300,
				function() {
					$('#prod1 .addition .prod1_table').fadeIn(200);
				}
			);
		}
	});
	
	var add_prod2 = $('#prod2 .addition').outerHeight();
	
	$('#prod2 .addition').css('height', '0px').find('.prod1_table').css('display', 'none');
	
	$('#expand_prod2').click(function() {
		if($(this).hasClass('Expanded')) {
			$(this).removeClass('Expanded');
			$(this).find('img').attr('src', 'images/button3.png');
			$('#prod2 .addition .prod1_table').fadeOut(200,
				function() {
					$('#prod2 .addition').animate({height : "0px"}, 300);
				}
			);
		} else {
			$(this).addClass('Expanded');
			$(this).find('img').attr('src', 'images/button5.png');
			$('#prod2 .addition').animate({height : add_prod2 + "px"}, 300,
				function() {
					$('#prod2 .addition .prod1_table').fadeIn(200);
				}
			);
		}
	});
	
	var SliderBusy = 0;
	
	$('.prod1_cell a').click(function() {
		$('#parent_popup').fadeIn();
		$('#more1').fadeIn();
		$('#more1 .more_content *').remove();
		$('#more1 .more_content').load($(this).attr('id') + '.html', function(){
			$('#slices img').click(
				function() {
					$('#slices img').removeClass('active');
					$(this).addClass('active');
					$(this).parent().prev().fadeOut(200, function() {
						$(this).attr('src', $('#slices img.active').attr('src'));
						$(this).fadeIn(200);
						SliderBusy = 1;
						setTimeout(function() {SliderBusy = 0;}, 5000);
					});
				}
			);
			$('#more1 .button_buy img').click(
				function() {
					$('#parent_popup').fadeIn();
					$('#popup_dialog').fadeIn();
					$('#popup_dialog').removeClass('Call');
					$('#popup_dialog #material').val('Заявка на ' + $('#more1 h3').text());
					$('#popup_dialog h3').text('Для покупки «' + $('#more1 h3').text() + '» заполните поля ниже и нажмите кнопку «Заказать»');
					$('#popup_dialog .question').css('display', 'block');
					$('#more1').fadeOut();
				}
			);
		});
	});
	function StartSlider() {
		var active = $('#slices img.active');

		if (SliderBusy == 0) {
			$('#slices img').removeClass('active');
			if(active.length == 0) {
				active = $($('#slices img')[0]);
			}
			if(active.next().length > 0) {
				active.next().addClass('active');
			} else {
				$($('#slices img')[0]).addClass('active');
			}
			$('#slices img.active').parent().prev().fadeOut(200, function() {
				$(this).attr('src', $('#slices img.active').attr('src'));
				$(this).fadeIn(200);
			});
		}
		
		setTimeout(StartSlider, 3000);
	}
	//setTimeout(StartSlider, 3000);
	
	$('.RequestCall').click(
		function() {
			$('#parent_popup').fadeIn();
			$('#popup_dialog').fadeIn();
			$('#popup_dialog').addClass('Call');
			$('#popup_dialog #material').val('');
			$('#popup_dialog h3').html('Заявка на<br /> бесплатный обратный звонок');
			$('#popup_dialog .question').val('');
			$('#popup_dialog .question').css('display', 'none');
		}
	);
	$('.material img.buy').click(
		function() {
			$('#parent_popup').fadeIn();
			$('#popup_dialog').fadeIn();
			$('#popup_dialog').removeClass('Call');
			$('#popup_dialog #material').val('Заявка на ' + $(this).parent().prev().text());
			$('#popup_dialog h3').text('Для покупки «' + $(this).parent().prev().text() + '» заполните поля ниже и нажмите кнопку «Заказать»');
			$('#popup_dialog .question').css('display', 'block');
		}
	);

});