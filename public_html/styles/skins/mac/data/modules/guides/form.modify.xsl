<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="field[@type = 'string' and @name='color']" mode="form-modify">
        <script src="/styles/common/jpicker/js/colorpicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/styles/common/jpicker/css/colorpicker.css" media="screen" />
        <div class="field">
            <label for="{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span class="color">
                    <input type="text" name="{@input_name}" value="{.}" id="{generate-id()}" style="float:left;width:70%">
                        <xsl:apply-templates select="." mode="required_attr">
                            <xsl:with-param name="old_class" select="@type" />
                        </xsl:apply-templates>
                    </input>
                    <div class="colorpicker_submit" style="position:static; cursor:pointer; float:left;margin-top: -2px;"></div>
                    <div style="cursor:pointer; float:left; margin-left:10px;height:30px; width: 70px; border: 1px solid #d3d3d3; background-color:{.};margin-top: -10px;"></div>
                </span>
            </label>
        </div>
        <script>
            $(document).ready(function () {
                $('.color input:text').ColorPicker({
                        onSubmit: function (hsb, hex, rgb, el) {
                            $(el).val('#' + hex);
                            $(el).next().next().css({backgroundColor:hex});
                            $(el).ColorPickerHide();
                        },
                        onBeforeShow: function () {
                            $(this).ColorPickerSetColor(this.value);
                        }
                    }).bind('keyup', function () {
                        $(this).ColorPickerSetColor(this.value);
                    });
                });
        </script>
    </xsl:template>

</xsl:stylesheet>
