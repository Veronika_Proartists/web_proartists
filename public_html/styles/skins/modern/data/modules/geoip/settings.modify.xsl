<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/geoip">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="data[@type = 'settings' and @action = 'modify']">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location">
					<div class="save_size"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>

				<div class="layout">
					<div class="column">
						<form method="post" action="do/" enctype="multipart/form-data">
							<xsl:apply-templates select="." mode="settings.modify"/>
						</form>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(function() {
				if (typeof history.pushState == 'function') {
					history.pushState(null, document.title, location.href.replace(/do\/$/, ''));
				}
			});
		</script>
	</xsl:template>

	<xsl:template match="group[@name = 'global']" mode="settings.modify">
		<div class="panel-settings">
			<div class="title">
				<h3>
					<xsl:text>&header-geoip-info;</xsl:text>
				</h3>
			</div>
			<div class="content">
				<div class="row">
					<div class="col-md-4">
						<xsl:apply-templates select="option[@name = 'ip']" mode="title" />
					</div>
					<div class="col-md-4">
						<xsl:apply-templates select="option[@name = 'ip']" mode="field" />
					</div>
					<div class="col-md-4">
						<input type="submit" class="btn btn-small color-blue" value="&label-check;" />
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="option" mode="title">
		<span><xsl:value-of select="@label" /></span>
	</xsl:template>

	<xsl:template match="option" mode="field">
		<xsl:apply-templates select="." mode="settings.modify-option" />
	</xsl:template>

	<xsl:template match="group[@name = 'geoinfo']/option" mode="settings.modify-option">
		<span>
			<xsl:value-of select="value" />
		</span>
	</xsl:template>

</xsl:stylesheet>