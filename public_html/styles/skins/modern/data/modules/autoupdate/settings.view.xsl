<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/autoupdate">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="data[@type = 'settings' and @action = 'view']">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location">
					<div class="save_size"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>

				<div class="layout">
					<div class="column">
						<form method="post" action="do/" enctype="multipart/form-data">
							<xsl:apply-templates select="group" mode="settings.view"/>
						</form>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="group" mode="settings.view">
		<xsl:variable name="patches-disabled" select="option[@type = 'boolean' and @name = 'patches-disabled']" />

		<xsl:call-template name="autoupdate" />

		<xsl:choose>
			<xsl:when test="$patches-disabled = '1'">
				<div class="panel-settings">
					<div class="title" style="cursor:default">
						<h3><xsl:value-of select="@label" /></h3>
					</div>
					<div class="content">
						<table class="tableContent">
							<tbody>
								<xsl:apply-templates select="option" mode="settings.view" />
							</tbody>
						</table>
						<div class="buttons">
							<div class="pull-right">
								<xsl:choose>
									<xsl:when test="option[@name='disabled-by-host']">
										<p>
											&label-updates-disabled-by-host;
											<a href="http://{option[@name='disabled-by-host']}/admin/autoupdate/versions/">http://<xsl:value-of select="option[@name='disabled-by-host']"/></a>
										</p>
									</xsl:when>
									<xsl:otherwise>
										<input type="button" class="btn color-blue" value="&label-check-updates;" id="update" />
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</div>
					</div>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<table class="btable btable-striped btable-bordered">
					<tbody>
						<xsl:apply-templates select="option" mode="settings.view" />
					</tbody>
				</table>
				<div class="pull-right">
					<xsl:choose>
						<xsl:when test="option[@name='disabled-by-host']">
							<p>
								&label-updates-disabled-by-host;
								<a href="http://{option[@name='disabled-by-host']}/admin/autoupdate/versions/">
									http://<xsl:value-of select="option[@name='disabled-by-host']"/>
								</a>
							</p>
						</xsl:when>
						<xsl:otherwise>
							<input type="button" class="btn color-blue"
								   value="&label-check-updates;" id="update"/>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="option[@type = 'boolean' and @name = 'patches-disabled']" mode="settings.view" />
	
	<xsl:template match="option[@type = 'check' and @name = 'disabled-by-host']" mode="settings.view" />

	<xsl:template match="option[@type = 'boolean' and @name = 'disabled']" mode="settings.view">
		<!-- <tr>
			<td class="eq-col">
				<label>
					<xsl:text>&label-manual-update;</xsl:text>
				</label>
			</td>
			<td>
				<div class="buttons">
					<div>
						<input type="button" value="&label-check-updates;" onclick="window.location = '/smu/index.php';" />
						<span class="l" /><span class="r" />
					</div>
				</div>
			</td>
		</tr> -->
	</xsl:template>
	
	
	<xsl:template name="autoupdate">
		<script src="/styles/skins/modern/design/js/autoupdate.js" />
	</xsl:template>
	

	<xsl:template match="result[@module = 'autoupdate' and @method = 'patches']/data">
		<xsl:choose>
			<xsl:when test="/result/@demo">
				<script language="javascript">
					function get(type, repository, id, link) {
						jQuery.jGrowl('<p>&label-stop-in-demo;</p>', {
							'header': 'UMI.CMS',
							'life': 10000
						});
						return false;
					}
				</script>
			</xsl:when>
			<xsl:otherwise>
				<script language="javascript">
					function get(type, repository, id, link) {
						jQuery.ajax({
							type:		"GET",
							url:		"/admin/autoupdate/getDiff/.xml",
							data:		{type: type, repository: repository, id: id, link: link},
							dataType:	"xml",
							success:	function(data) {
								var response = data.getElementsByTagName('response')[0];
								jQuery.jGrowl('<p>'+response.getAttribute('message')+'</p>', {
									'header': 'UMI.CMS',
									'life': 10000
								});

								if (response.getAttribute('code') == "ok") {
									switch(type) {
										case "apply": {
											jQuery('#'+ id +'.apply').switchClass("apply", "revert");
											var action = "get('revert', '" + repository +"', '"+ id + "', '"+ link + "')";
											var onclick = document.getElementsByClassName(id)[0];
											jQuery('.' + id).attr("value", "&label-diff-revert;");
											onclick.setAttribute("onclick", action);
										}
										break;
										case "revert": {
											jQuery('#'+ id +'.revert').switchClass("revert", "apply");
											var action = "get('apply', '" + repository +"', '"+ id + "', '"+ link + "')";
											var onclick = document.getElementsByClassName(id)[0];
											jQuery('.' + id).attr("value", "&label-diff-apply;");
											onclick.setAttribute("onclick", action);
										}
										break;
									};
								}
							}
						});
						return false;
					}
				</script>
			</xsl:otherwise>
			</xsl:choose>
		<xsl:apply-templates select="items" mode="patches"/>
	</xsl:template>

	<xsl:template match="items" mode="patches">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location" xmlns:umi="http://www.umi-cms.ru/TR/umi">
					<div class="saveSize"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>


				<div class="layout">
					<div class="column">
						<p><xsl:apply-templates select="../caution" mode="patches"/></p>
						<table class="tablePatches btable btable-striped">
							<tbody>
								<tr class="glow">
									<td style="width:9%;"><strong>&label-diff-id;</strong></td>
									<td style="width:11%;"><strong>&label-diff-version;</strong></td>
									<td style="width:5%;"><strong>&label-diff-revision;</strong></td>
									<td style="width:5%;"><strong>&label-diff-repository;</strong></td>
									<td style="width:60%;"><strong>&label-diff-description;</strong></td>
									<td style="width:10%;"><strong></strong></td>
								</tr>
								<xsl:apply-templates select="item" mode="patches"/>
							</tbody>
						</table>
						<p>&label-diff-hub; <a href="http://hub.umi-cms.ru" target="_blank">UMI.Hub</a></p>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</xsl:template>

	<xsl:template match="items[not(item)]" mode="patches">
		<div class="tabs-content module">
			<div class="section selected">
				<div class="location" xmlns:umi="http://www.umi-cms.ru/TR/umi">
					<div class="saveSize"></div>
					<a class="btn-action loc-right infoblock-show">
						<i class="small-ico i-info"></i>
						<xsl:text>&help;</xsl:text>
					</a>
				</div>


				<div class="layout">
					<div class="column">
						<table class="tablePatches">
							<tbody>
								<tr>
									<td>&label-diff-noitems;</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="column">
						<div  class="infoblock">
							<h3>
								<xsl:text>&label-quick-help;</xsl:text>
							</h3>
							<div class="content" title="{$context-manul-url}">
							</div>
							<div class="infoblock-hide"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</xsl:template>

	<xsl:template match="item" mode="patches">
		<xsl:variable name="applied" select="contains(../../applied, id)" />

		<tr>
			<xsl:if test="(position() mod 2 != 1)">
				<xsl:attribute name="class">glow</xsl:attribute>
			</xsl:if>
			<td>#<strong><xsl:value-of select="id" /></strong></td>
			<td><xsl:value-of select="version" /></td>
			<td><xsl:value-of select="revision" /></td>
			<td><xsl:apply-templates select="repository" mode="patches" /></td>
			<td><xsl:value-of select="description" /></td>
			<td>
				<div class="buttons">
					<div>
						<xsl:choose>
							<xsl:when test="$applied">
								<xsl:attribute name="id"><xsl:value-of select="id" /></xsl:attribute>
								<xsl:attribute name="class">revert</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="id"><xsl:value-of select="id" /></xsl:attribute>
								<xsl:attribute name="class">apply</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<input type="button">
							<xsl:choose>
								<xsl:when test="$applied">
									<xsl:attribute name="class"><xsl:value-of select="id" /></xsl:attribute>
									<xsl:attribute name="value">&label-diff-revert;</xsl:attribute>
									<xsl:attribute name="onclick">get('revert', '<xsl:value-of select="repository" />', '<xsl:value-of select="id" />', '<xsl:value-of select="link" />'); return false;</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="class"><xsl:value-of select="id" /></xsl:attribute>
									<xsl:attribute name="value">&label-diff-apply;</xsl:attribute>
									<xsl:attribute name="onclick">get('apply', '<xsl:value-of select="repository" />', '<xsl:value-of select="id" />', '<xsl:value-of select="link" />'); return false;</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
						</input>
						<span class="l" />
						<span class="r" />
					</div>
				</div>
			</td>
		</tr>

	</xsl:template>

	<xsl:template match="repository[.='native']" mode="patches">
		&label-diff-from-native;
	</xsl:template>

	<xsl:template match="repository[.='community']" mode="patches">
		&label-diff-from-community;
	</xsl:template>

	<xsl:template match="caution[.='all']" mode="patches">
		&label-diff-caution-all;
	</xsl:template>

	<xsl:template match="caution" mode="patches">
		&label-diff-caution-normal;
	</xsl:template>

</xsl:stylesheet>
