<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/updatesrv">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="data[@type = 'form' and (@action = 'modify' or @action = 'create')]">
		<form method="post" action="do/" enctype="multipart/form-data">
			<input type="hidden" name="referer" value="{/result/@referer-uri}" id="form-referer" />
			<input type="hidden" name="domain" value="{$domain-floated}"/>
			
			<xsl:apply-templates mode="form-modify" />
			<xsl:apply-templates select="page" mode="permissions" />

			<xsl:if test="object[@type-id=652]">
				<xsl:apply-templates select="object" mode="update-history" />
				<xsl:apply-templates select="object" mode="install-demosite-history" />
			</xsl:if>
			
			<xsl:if test="@action = 'modify' and count(page) = 1">
				<xsl:apply-templates select="document(concat('udata://backup/backup_panel/', page/@id))/udata" />
			</xsl:if>
		</form>
	</xsl:template>

    <xsl:template match="properties" mode="form-modify">
        <xsl:apply-templates select="group" mode="form-modify">
            <xsl:with-param name="show-name-input">hide</xsl:with-param>
            <xsl:with-param name="show-type-input">hide</xsl:with-param>
        </xsl:apply-templates>
    </xsl:template>

	<xsl:template match="object" mode="install-demosite-history">
		<div class="panel properties-group">
			<div class="header">
				<span>
					<xsl:text>Установка демо-шаблонов</xsl:text>
				</span>
				<div class="l"></div>
				<div class="r"></div>
			</div>
			<div class="content">
				<xsl:apply-templates select="document(concat('udata://updatesrv/getInstallDemositesHistory/', @id, '/'))/udata" />
			</div>
		</div>
	</xsl:template>

	<xsl:template match="udata[@module = 'updatesrv'][@method = 'getInstallDemositesHistory']">
		<xsl:variable name="count" select="count(//item)" />
		<xsl:choose>
			<xsl:when test="$count=0">
				<xsl:text>Установка шаблонов не производилась.</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<table class="tableContent">
					<thead>
						<tr>
							<th><xsl:text>Дата установки</xsl:text></th>
							<th><xsl:text>Название шаблона</xsl:text></th>
							<th><xsl:text>Ограниченная установка</xsl:text></th>
						</tr>
					</thead>
					<tbody>
						<xsl:apply-templates select="//item" />
					</tbody>
				</table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="udata[@module = 'updatesrv' and @method = 'getInstallDemositesHistory']//item">
		<tr>
			<td><xsl:value-of select="@date_install" /></td>
			<td><xsl:value-of select="@demosite_codename" /></td>
			<td>
				<xsl:choose>
					<xsl:when test="@limited">
						<xsl:text>Да</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Нет</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="object" mode="update-history">
		<div class="panel properties-group">
			<div class="header">
				<span>
					<xsl:text>Обновления системы</xsl:text>
				</span>
				<div class="l"></div>
				<div class="r"></div>
			</div>
			<div class="content">
				<xsl:apply-templates select="document(concat('udata://updatesrv/getUpdateHistory/', @id, '/'))/udata" />
			</div>
		</div>
	</xsl:template>

	<xsl:template match="udata[@module = 'updatesrv'][@method = 'getUpdateHistory']">
		<xsl:variable name="count" select="count(//item)" />
		<xsl:choose>
			<xsl:when test="$count=0">
				<xsl:text>Обновления не производились.</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<table class="tableContent">
					<thead>
						<tr>
							<th><xsl:text>Дата обновления</xsl:text></th>
							<th><xsl:text>Предыдущая версия</xsl:text></th>
							<th><xsl:text>Новая версия</xsl:text></th>
						</tr>
					</thead>
					<tbody>
						<xsl:apply-templates select="//item" />
					</tbody>
				</table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

    <xsl:template match="udata[@module = 'updatesrv' and @method = 'getUpdateHistory']//item">
    	<tr>
    		<td><xsl:value-of select="@date" /></td>
    		<td><xsl:value-of select="@old_system" /></td>
    		<td><xsl:value-of select="@new_system" /></td>
    	</tr>
    </xsl:template>

</xsl:stylesheet>