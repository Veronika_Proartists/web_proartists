<meta charset="UTF-8"/>
<?php

include "standalone.php";
$id = getRequest('id');
$sel = new selector('pages');
$sel->types('hierarchy-type')->name('catalog', 'object');
$sel->where('only')->equals(1);

$result = $sel->result();
$hierarchy = umiHierarchy::getInstance();
$relation = umiImportRelations::getInstance();
for($i = 0; $i < sizeof($result); $i++) {
    $element = &$result[$i];
    if($id == $element->getId()) echo $element->getId()." - ".$element->getName()."<br/>";
        else echo $element->getId()." - ".$element->getName()." <a href='/restruct.php?id=".$element->getId()."'>++</a><br/>";
}
echo "<br/><br/><br/>";
//954 saliner 772
$hierarchy = umiHierarchy::getInstance();
$objects = umiObjectsCollection::getInstance();

if($id && $parentElement = $hierarchy->getElement($id)){
    $relation = umiImportRelations::getInstance();
    $import_id = $relation->addNewSource('restruct');

    $hierarchyTypes = umiHierarchyTypesCollection::getInstance();
    $objectTypes = umiObjectTypesCollection::getInstance();
    $cmsController = cmsController::getInstance();

    $alt_name = $parentElement->getAltName();
    $parentElement->setAltName($alt_name.'_old');
    $parentElement->commit();
    $name = $parentElement->getName();
    $parent_id = $parentElement->getParentId();
    $tpl_id		= $parentElement->getTplId();
    $domain_id	= $parentElement->getDomainId();
    $lang_id	= $parentElement->getLangId();

    if(!$rel_id = $relation->getNewIdRelation($import_id,$id)){
        $hierarchy_type_id = $hierarchyTypes->getTypeByName("catalog", "category")->getId();
        $object_type_id = $objectTypes->getBaseType("catalog", "category");

        $object_type = $objectTypes->getType($object_type_id);
        if($object_type->getHierarchyTypeId() != $hierarchy_type_id) {
            $this->errorNewMessage("Object type and hierarchy type doesn't match");
            $this->errorPanic();
        }

        $element_id = $hierarchy->addElement($parent_id, $hierarchy_type_id, $name, $alt_name, $object_type_id, $domain_id, $lang_id, $tpl_id);
        permissionsCollection::getInstance()->setDefaultPermissions($element_id);

        $element = $hierarchy->getElement($element_id, true);

        $element->setIsActive(true);
        $element->setIsVisible(true);
        $element->setName($name);


        $element->setValue('title',$parentElement->getValue('title'));
        $element->setValue('h1',$parentElement->getValue('h1'));
        $element->setValue('image',$parentElement->getValue('image'));
        $element->setValue('meta_keywords',$parentElement->getValue('meta_keywords'));
        $element->setValue('meta_descriptions',$parentElement->getValue('meta_descriptions'));
        $element->setValue('tags',$parentElement->getValue('tags'));
        $element->setValue('white_breadcrumbs',$parentElement->getValue('white_breadcrumbs'));
        $element->setValue('photo',$parentElement->getValue('photo'));
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;
        $element->setValue('descr',$parentElement->getValue('descr'));
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = false;
        $element->commit();

        echo "Создана категория для товара ".$name."<br/>";
        $relation->setIdRelation($import_id,$id,$element_id);
    }else{
        echo "Категория для этого товара уже создана id - ".$rel_id;
    }

    $element_id = $rel_id;
    $options = $parentElement->getValue('color');
    $hierarchy_type_id = $hierarchyTypes->getTypeByName("catalog", "object")->getId();
    $object_type_id = $objectTypes->getBaseType("catalog", "object");
    foreach($options as $option){
        $obj = $objects->getObject($option['rel']);
        if(!$obj) continue;
        $name = $obj->getName();

        $color_id = $hierarchy->addElement($element_id, $hierarchy_type_id, $name, $name, $object_type_id, $domain_id, $lang_id, $tpl_id);
        permissionsCollection::getInstance()->setDefaultPermissions($color_id);
        $color = $hierarchy->getElement($color_id, true);
        $color->setIsActive(true);
        $color->setIsVisible(true);
        $color->setName($name);

        $color->setValue('title',$name);
        $color->setValue('h1',$name);
        $color->setValue('tags',$parentElement->getValue('tags'));
        $color->setValue('descr_mini',$parentElement->getValue('descr_mini'));
        $color->setValue('photo',$parentElement->getValue('photo'));
        $color->setValue('properties',$parentElement->getValue('properties'));
        $color->setValue('popular',$parentElement->getValue('popular'));
        $color->setValue('novelty',$parentElement->getValue('novelty'));
        $color->setValue('vendor',$parentElement->getValue('vendor'));
        $color->setValue('article',$name);
        $color->setValue('color_code',$obj->getValue('color'));
        $color->setValue('price',$parentElement->getValue('price'));
        $color->setValue('common_quantity',$option['int']);
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = true;
        $color->setValue('descr',$parentElement->getValue('descr'));
        umiObjectProperty::$IGNORE_FILTER_INPUT_STRING = false;

        $color->commit();
        echo "Создана товар ".$name."<br/>";
    }


}else{
    echo "Не указан идентификатор для трансформации";
}



?>