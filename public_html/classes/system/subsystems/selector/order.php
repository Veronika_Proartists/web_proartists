<?php
	abstract class selectorOrderField {
		public $asc = true;
		
		public function asc() { $this->asc = true; }
		public function desc() { $this->asc = false; }
		public function rand() { $this->name = 'rand'; }

		public function __get($prop) { 
			if (isset($this->$prop)) {
				return $this->$prop; 
			}
			return false;
		}
	};

	class selectorOrderFieldProp extends selectorOrderField {
		protected $fieldId;
		
		public function __construct($fieldId) {
			$this->fieldId = $fieldId;
		}
	};
	
	class selectorOrderSysProp extends selectorOrderField {
		public $name;
		
		public function __construct($name) {
			$this->name = $name;
		}
	}
?>