<?php
	class redirects implements iRedirects {
		/* @var string $urlSuffix суффикс адресов страниц сайта */
		private $urlSuffix = '/';

		/**
			* Получить экземпляр коллекции
			* @return iRedirects экземпляр коллекции
		*/
		public static function getInstance() {
			static $instance;
			if(is_null($instance)) {
				$instance = new redirects;
			}
			return $instance;
		}

		/**
		 * Конструктор
		 */
		private function __construct() {
			$config = mainConfiguration::getInstance();

			if ((bool) $config->get('seo', 'url-suffix.add')) {
				$this->urlSuffix = (string) $config->get('seo', 'url-suffix');
			}
		}
		
		/**
			* Добавить новое перенаправление
			* @param String $source адрес страницы, с которой осуществляется перенаправление
			* @param String $target адрес целевой страницы
			* @param Integer $status = 301 статус перенаправления
			* @param bool $madeByUser = false, редирект сделан пользователем
		*/
		public function add($source, $target, $status = 301, $madeByUser = false) {
			if($source == $target) return;
			
			$source = l_mysql_real_escape_string($this->parseUri($source));
			$target = l_mysql_real_escape_string($this->parseUri($target));
			$status = (int) $status;
			$madeByUser = (int) $madeByUser;
			
			l_mysql_query("START TRANSACTION /* Adding new redirect records */");
			
			//Создать новые записи на тот случай, если у нас уже есть перенаправление на $target
			$sql = <<<SQL
INSERT INTO `cms3_redirects`
	(`source`, `target`, `status`, `made_by_user`)
	SELECT `source`, '{$target}', '{$status}', $madeByUser FROM `cms3_redirects`
		WHERE `target` = '{$source}'
SQL;
			l_mysql_query($sql);
			
			//Удалить старые записи
			$sql = <<<SQL
DELETE FROM `cms3_redirects` WHERE `target` = '{$source}'
SQL;
			l_mysql_query($sql);
			
			$result = l_mysql_query("SELECT * FROM `cms3_redirects` WHERE `source` = '{$source}' AND `target` = '{$target}'", true);
			if(mysql_num_rows($result)) {
				l_mysql_query("ROLLBACK");
				return;
			}
			
			//Добавляем новую запись для перенаправления
			$sql = <<<SQL
INSERT INTO `cms3_redirects`
	(`source`, `target`, `status`, `made_by_user`)
	VALUES
	('{$source}', '{$target}', '{$status}', $madeByUser)
SQL;
			l_mysql_query($sql);
			
			l_mysql_query("COMMIT");
		}
		
		
		/**
			* Получить список перенаправлений со страницы $source
			* @param String $source адрес страницы, с которой осуществляется перенаправление
		 	* @param bool $madeByUser = false, редирект сделан пользователем
			* @return Array массив перенаправлений
		*/
		public function getRedirectsIdBySource($source, $madeByUser = false) {
			$sourceSQL = l_mysql_real_escape_string($this->parseUri($source));
			$madeByUser = (int) $madeByUser;
			$redirects = array();
			
			$sql = "SELECT `id`, `target`, `status` FROM `cms3_redirects` WHERE `source` = '{$sourceSQL}' AND `made_by_user` = $madeByUser";
			$result = l_mysql_query($sql);
			while(list($id, $target, $status) = mysql_fetch_row($result)) {
				$redirects[$id] = Array($source, $target, (int) $status);
			}
			return $redirects;
		}
		
		
		/**
			* Получить перенаправление по целевому адресу
			* @param String $target адрес целевой страницы
		 	* @param bool $madeByUser = false, редирект сделан пользователем
			* @return массив перенаправления
		*/
		public function getRedirectIdByTarget($target, $madeByUser = false) {
			$targetSQL = l_mysql_real_escape_string($this->parseUri($target));
			$madeByUser = (int) $madeByUser;

			$sql = "SELECT `id`, `source`, `status` FROM `cms3_redirects` WHERE `target` = '{$targetSQL}' AND `made_by_user` = $madeByUser";
			$result = l_mysql_query($sql);
			if(list($id, $source, $status) = mysql_fetch_row($result)) {
				return Array($source, $target, (int) $status);
			} else {
				return false;
			}
		}
		
		
		/**
			* Удалить перенаправление
			* @param Integer $id id перенаправления
		*/
		public function del($id) {
			$id = (int) $id;
			
			$sql = <<<SQL
DELETE FROM `cms3_redirects` WHERE `id` = '{$id}'
SQL;
			l_mysql_query($sql);
		}
		
		
		/**
			* Сделать перенаправление, если url есть в таблице перенаправлений
			* @param String $currentUri url для поиска
		 	* @param bool $madeByUser = false, редирект сделан пользователем
		 	* @return void
		*/
		public function redirectIfRequired($currentUri, $madeByUser = false) {
			$currentUri = l_mysql_real_escape_string($this->parseUri($currentUri));
			$madeByUser = (int) $madeByUser;

			$sql = <<<SQL
SELECT `target`, `status` FROM `cms3_redirects`
	WHERE `source` = '{$currentUri}' AND `made_by_user` = $madeByUser
	ORDER BY `id` DESC LIMIT 1
SQL;
			$result = l_mysql_query($sql);
			if(mysql_num_rows($result)) {
				list($target, $status) = mysql_fetch_row($result);
				if (strpos($target, 'http') !== 0) {
					$target = '/' . $target;
				}
				return $this->redirect($target, (int) $status);
			}

			//Попробуем найти в перенаправление в подстраницах
			$uriParts = explode("/", trim($currentUri, "/"));
			do {
				array_pop($uriParts);
				$subUri = implode("/", $uriParts) . "/";
				$subUriSQL = l_mysql_real_escape_string($this->parseUri($subUri));
				
				if(!strlen($subUriSQL)) {
					if(count($uriParts)) continue;
					else break;
				}

				$sql = <<<SQL
SELECT `source`, `target`, `status` FROM `cms3_redirects`
	WHERE `source` = '{$subUriSQL}' AND `made_by_user` = $madeByUser
	ORDER BY `id` DESC LIMIT 1
SQL;

				$result = l_mysql_query($sql);
				if(mysql_num_rows($result)) {
					list($source, $target, $status) = mysql_fetch_row($result);
					
					$sourceUriSuffix = substr($currentUri, strlen($source));
					$target .= $sourceUriSuffix;
					if (strpos($target, 'http') !== 0) {
						$target = '/' . $target;
					}
					$this->redirect($target, $status);
				}

			} while (sizeof($uriParts) > 1);
		}


		/**
			* Инициализировать события
		*/
		public function init() {
			$config = mainConfiguration::getInstance();
			
			if($config->get('seo', 'watch-redirects-history')) {
				$listener = new umiEventListener("systemModifyElement", "content", "onModifyPageWatchRedirects");
				$listener = new umiEventListener("systemMoveElement", "content", "onModifyPageWatchRedirects");
			}
		}

		/**
		 * Удаляет все редиректы
		 */
		public function deleteAllRedirects() {
			$connection = ConnectionPool::getInstance()->getConnection();
			$connection->query('TRUNCATE TABLE `cms3_redirects`;');
		}
		
		protected function redirect($target, $status) {
			$statuses = array(
				300 => 'Multiple Choices',
				'Moved Permanently', 'Found', 'See Other',
				'Not Modified', 'Use Proxy', 'Switch Proxy', 'Temporary Redirect'
			);
			
			if(!isset($statuses[$status])) return false;
			$statusMessage = $statuses[$status];
			
			$buffer = outputBuffer::current();
			if($referer = getServer('HTTP_REFERER')) {
				$buffer->header('Referrer', $referer);
			}
			$buffer->status($status . ' ' . $statusMessage);
			$buffer->redirect($target);
			$buffer->end();
		}
		
		protected function parseUri($uri) {
			$uri = ltrim($uri, '/');

			if ($this->urlSuffix == '/') {
				return rtrim($uri, '/');
			}

			$suffix = addcslashes($this->urlSuffix, '\^.$|()[]*+?{},');
			$pattern = '/(' . $suffix . ')/';
			$cleanUri = preg_replace($pattern, '', $uri);
			return (is_null($cleanUri))? $uri : $cleanUri;
		}
	};
?>