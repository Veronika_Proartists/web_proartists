<?php
	interface iRedirects {
		public static function getInstance();
		public function add($source, $target, $status = 301, $madeByUser = false);
		public function getRedirectsIdBySource($source, $madeByUser = false);
		public function getRedirectIdByTarget($target, $madeByUser = false);
		public function del($id);
		public function redirectIfRequired($currentUri, $madeByUser = false);
		public function deleteAllRedirects();
		public function init();
	};
?>