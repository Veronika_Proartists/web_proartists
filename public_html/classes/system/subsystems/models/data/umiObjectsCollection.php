<?php
/**
 * Этот класс служит для управления/получения доступа к объектам.
 * Класс является синглтоном, экземпляр класса можно получить через статический метод getInstance()
 * TODO Check and format all PHPDoc's
 */
	class umiObjectsCollection extends singleton implements iSingleton, iUmiObjectsCollection {
		private $objects = array();
		private $updatedObjects = array();
		private $objectsGuidsToId = array();

		/**
		 * Конструктор
		 */
		protected function __construct() {
			$this->preLoadObjectsIdsByGuid();
		}

		/**
			* Получить экземпляр коллекции
			* @return umiObjectsCollection экземпляр класса
		*/
		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}


		/**
		 * Проверить, загружен ли в память объект с id $object_id
		 * @param Interger $object_id id объекта
		 * @throws coreException
		 * @return Boolean true, если объект загружен
		 */
		private function isLoaded($object_id) {
			if(gettype($object_id) == "object") {
			throw new coreException("Object given!");
			}
			return (bool) array_key_exists($object_id, $this->objects);
		}

		/**
		 * Существует ли объект с ID $objectId
		 * @param int $objectId ID объекта
		 * @param mixed $typeId ID типа данных объекта
		 * @return bool
		 */
		public function isExists($objectId, $typeId = false) {
			if (is_object($objectId) || is_object($typeId)) {
				return false;
			}

			$objectIdNum = (int) $objectId;
			$typeIdNum = (int) $typeId;

			if ($objectIdNum <= 0 || ($typeIdNum <= 0 && $typeId !== false)) {
				return false;
			}

			if ($this->isLoaded($objectIdNum) &&
			    ($this->getObject($objectId)->getTypeId() === $typeIdNum || $typeId === false)) {
				return true;
			}

			$typeConditionPart =  $typeId !== false ? "AND `type_id` = '${typeIdNum}'" : '';

			$query =  <<<QUERY
SELECT EXISTS (SELECT `id`
FROM   `cms3_objects`
WHERE  `id` = '${objectIdNum}'
	   ${typeConditionPart}
LIMIT  1);
QUERY;
			$dbConnection = ConnectionPool::getInstance()->getConnection('core');
			$result = $dbConnection->queryResult($query);

			if ($result instanceof IQueryResult) {
				$row = $result->fetch();
				return (bool) ($row[0]);
			}

			return false;
		}

		/**
		 * Является ли $object объектом UMI
		 * @param mixed $object
		 * @return bool
		 */
		public function isUmiObject($object) {
			return ($object instanceof iUmiObject);
		}

		/**
		 * Получить объект по его имени
		 * @param string $name имя или языковая метка имени объекта
		 * @param mixed $typeId ID типа данных объекта
		 * @return umiObject найденный объект или false, если объект не найден
		 */
		public function getObjectByName($name, $typeId = false) {
			if ($typeId !== false && !is_numeric($typeId)) {
				return false;
			}

			$label = is_null(getI18n($name)) ? $name : getI18n($name);
			$escapedLabel = l_mysql_real_escape_string($label, 'core');
			$escapedTypeId = l_mysql_real_escape_string($typeId);

			$typeConditionPart = $typeId !== false ? "AND `type_id` = '${escapedTypeId}'" : '';
			$query = <<<QUERY
SELECT id
FROM   `cms3_objects`
WHERE  name = '${escapedLabel}'
       ${typeConditionPart}
LIMIT  1
QUERY;
			$dbConnection = ConnectionPool::getInstance()->getConnection('core');
			$result = $dbConnection->queryResult($query);

			if ($result instanceof IQueryResult) {
				$result->setFetchType(IQueryResult::FETCH_ASSOC);
				$row = $result->fetch();
				if (isset($row['id']) && is_numeric($row['id'])) {
					return $this->getObject($row['id']);
				}

				return false;
			}

			return false;
		}

		/**
		 * Получить экземпляр объекта с id $object_id
		 * @param int $object_id id объекта
		 * @param bool $row
		 *
		 * @return bool|umiObject  экземпляр объекта $object_id, либо false в случае неудачи
		 */
		public function getObject($object_id, $row = false) {
			$object_id = (int) $object_id;

			if (!$object_id) {
				return false;
			}

			if ($this->isLoaded($object_id)) {
				return $this->objects[$object_id];
			}

			$cacheFrontend = cacheFrontend::getInstance();
			$object = $cacheFrontend->load($object_id, "object");

			if (!$object instanceof iUmiObject) {
				try {
					$object = new umiObject($object_id, $row);
					$cacheFrontend->save($object, "object");
				} catch (baseException $e) {
					return false;
				}
			}

			if (is_object($object)) {
				$this->objects[$object_id] = $object;
				return $this->objects[$object_id];
			} else {
				return false;
			}
		}

		/**
		 * Получить тип по его GUID
		 *
		 * @param mixed $guid Global Umi Identifier
		 * @return umiObject тип данных (класс umiObjectType), либо false
		*/
		public function getObjectByGUID($guid) {
			$id = $this->getObjectIdByGUID($guid);
			return $this->getObject($id);
		}

		/**
		 * Получить числовой идентификатор типа по его GUID
		 *
		 * @param string $guid Global Umi Identifier
		 * @return integer/boolean(false)
		*/
		public function getObjectIdByGUID($guid) {
			static $cache = array();

			if(!$guid) {
				return false;
			}

			if (isset($this->objectsGuidsToId[$guid])) {
				return $cache[$guid] = $this->objectsGuidsToId[$guid];
			}

			if(!cmsController::$IGNORE_MICROCACHE && isset($cache[$guid])) return $cache[$guid];

			$guid = l_mysql_real_escape_string($guid);
			$query = "SELECT `id` FROM `cms3_objects` WHERE `guid` = '{$guid}'";
			$result = l_mysql_query($query, true);

			if($error = l_mysql_error()) {
				throw new coreException($error);
			}

			if(list($id) = mysql_fetch_row($result)) {
				return $this->objectsGuidsToId[$guid] = $cache[$guid] = $id;
			} else {
				return $this->objectsGuidsToId[$guid] = $cache[$guid] = false;
			}
		}

		/**
			* Удалить объект с id $object_id. Если объект заблокирован, он не будет удален.
			* При удалении принудительно вызывается commit() на удаляемом объекте
			* Нельзя удалить пользователей с guid равными system-guest и system-supervisor, нельзя удалить группу супервайзеров.
			* @param Integer $object_id id объекта
			* @return Boolean true, если удаление удалось
		*/
		public function delObject($object_id) {

			$event = new umiEventPoint("collectionDeleteObject");
			$event->setParam("object_id", $object_id);
			$event->setMode("before");
			$event->call();

			if($this->isExists($object_id)) {
				$this->disableCache();

				$object_id = (int) $object_id;

				if(defined("SV_USER_ID")) {
					if($object_id == SV_USER_ID || $object_id == SV_GROUP_ID || $object_id == $this->getObjectIdByGUID('system-guest')) {
						throw new coreException("You are not allowed to delete object #{$object_id}. Never. Don't even try.");
					}
				}


				//Make sure, we don't will not try to commit it later
				$object = $this->getObject($object_id);
				$object->commit();

				$sql = "DELETE FROM cms3_objects WHERE id = '{$object_id}' AND is_locked='0'";
				l_mysql_query($sql);

				if($err = l_mysql_error()) {
					throw new coreException($err);
					return false;
				}

				if($this->isLoaded($object_id)) {
					unset($this->objects[$object_id]);
				}

				cacheFrontend::getInstance()->del($object_id, "object");

				$event->setMode("after");
				$event->call();

				return true;
			} else {
				return false;
			}
		}

		/**
			* Создать новый объект в БД
			* @param String $name название объекта.
			* @param Integer $type_id id типа данных (класс umiObjectType), которому будет принадлежать объект.
			* @param Boolean $is_locked=false Состояние блокировки по умолчанию. Рекоммендуем этот параметр не указывать.
			* @return Integer id созданного объекта, либо false в случае неудачи
		*/
		public function addObject($name, $type_id, $is_locked = false) {
			$this->disableCache();

			$type_id = (int) $type_id;

			if(!$type_id) {
				throw new coreException("Can't create object without object type id (null given)");
			}

			$sql = "INSERT INTO cms3_objects (type_id) VALUES('$type_id')";
			l_mysql_query($sql);

			if($err = l_mysql_error()) {
				throw new coreException($err);
				return false;
			}

			$object_id = l_mysql_insert_id();
			$object = new umiObject($object_id);

			$object->setName($name);
			$object->setIsLocked($is_locked);

			/* @var users|def_module $users */
			$users = cmsController::getInstance()->getModule("users");

			if ($users instanceof def_module) {
				if ($users->is_auth()) {
					$user_id = $users->user_id;
					$object->setOwnerId($user_id);
				}
			} else {
				$object->setOwnerId(NULL);
			}

			$this->resetObjectProperties($object_id);
			$maxOrder = $this->getMaxOrderByTypeId($object->getTypeId());

			$object->setOrder($maxOrder + 1);
			$object->commit();

			$this->objects[$object_id] = $object;
			return $object_id;
		}

		/**
		 * Сделать копию объекта и всех его свойств
		 *
		 * @param int $iObjectId Id копируемого объекта
		 * @throws coreException Mysql ошибки
		 * @return int Id объекта-копии
		 */
		public function cloneObject($iObjectId) {
			$iObjectId = (int) $iObjectId;
			$vResult = false;

			$oObject = $this->getObject($iObjectId);
			if ($oObject instanceof umiObject) {
				// clone object definition
				$sSql = "INSERT INTO cms3_objects (name, guid, is_locked, type_id, owner_id, ord, updatetime) SELECT name, guid, is_locked, type_id, owner_id, ord, updatetime FROM cms3_objects WHERE id = '{$iObjectId}'";
				l_mysql_query($sSql);

				if ($err = l_mysql_error()) {
					throw new coreException($err);
					return false;
				}

				$iNewObjectId = l_mysql_insert_id();

				// clone object content
				$sSql = "INSERT INTO cms3_object_content (obj_id, field_id, int_val, varchar_val, text_val, rel_val, tree_val,float_val)  SELECT '{$iNewObjectId}' as obj_id, field_id, int_val, varchar_val, text_val, rel_val, tree_val,float_val FROM cms3_object_content WHERE obj_id = '$iObjectId'";
				l_mysql_query($sSql);

				if ($err = l_mysql_error()) {
					throw new coreException($err);
					return false;
				}

				$vResult = $iNewObjectId;
			}

			return $vResult;
		}

		/**
			* Получить отсортированный по имени список всех объектов в справочнике $guide_id (id типа данных).
			* Равнозначно если бы мы хотели получить этот список для всех объектов определенного типа данных
			* @param id $guide_id справочника (id типа данных)
			* @return Array массив, где ключи это id объектов, а значения - названия объектов
		*/
		public function getGuidedItems($guide_id) {
			$res = Array();

			if (is_numeric($guide_id)) {
				$guide_id = (int) $guide_id;
			} else {
				$guide_id = l_mysql_real_escape_string($guide_id);
				$query = "SELECT `id` FROM `cms3_object_types` WHERE `guid`='". $guide_id ."' LIMIT 1";
				$result = l_mysql_query($query);
				if (0 < mysql_numrows($result)) {
					$guide_id = mysql_result($result, 0);
				} else {
					$guide_id = (int) $guide_id;
				}
			}

			$ignoreSorting = intval(regedit::getInstance()->getVal("//settings/ignore_guides_sort")) ? true : false;

			if($ignoreSorting)
				$sql = "SELECT id, name FROM cms3_objects WHERE type_id = '{$guide_id}' ORDER BY id ASC";
			else
				$sql = "SELECT id, name FROM cms3_objects WHERE type_id = '{$guide_id}' ORDER BY name ASC";


			$result = l_mysql_query($sql);

			if($err = l_mysql_error()) {
				throw new coreException($err);
				return false;
			}

			while(list($id, $name) = mysql_fetch_row($result)) {
				$res[$id] = $this->translateLabel($name);
			}

			if(!$ignoreSorting)
				natsort($res);

			return $res;
		}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 * @param unknown_type $typeId
		 */
		public function getCountByTypeId($typeId) {
			$count = false;
			$typeId = (int) $typeId;
			$connection = ConnectionPool::getInstance()->getConnection();
			$result = $connection->queryResult("SELECT COUNT(id) FROM cms3_objects WHERE type_id = '{$typeId}'");
			if($error = l_mysql_error()) {
				throw new databaseException($error);
			}
			list($count) = $result->fetch();
			return $count;
		}

		/**
			* Обнулить все свойства у объекта $object_id
			* @param Integer $object_id id объекта
		*/
		protected function resetObjectProperties($object_id) {
			$object_id = (int) $object_id;
			$object = $this->getObject($object_id);
			$object_type_id = $object->getTypeId();
			$object_type = umiObjectTypesCollection::getInstance()->getType($object_type_id);
			$tableName = umiBranch::getBranchedTableByTypeId($object_type_id);

			$query = "INSERT INTO {$tableName} (obj_id, field_id, int_val, varchar_val, text_val, rel_val, tree_val, float_val) VALUES ";

			$object_fields = $object_type->getAllFields();

			$vals = array();
			foreach($object_fields as $object_field) {
				$vals[] = "('{$object_id}', '{$object_field->getId()}', NULL, NULL, NULL, NULL, NULL, NULL)";
			}

			if(sizeof($object_fields) != 0) {
				l_mysql_query($query . implode($vals, ", "));
			} else {
				$sql = "INSERT INTO {$tableName} (obj_id, field_id) VALUES ('{$object_id}', NULL)";
				l_mysql_query($sql);
				if($err = l_mysql_error()) {
					throw new coreException($err);
				}
			}
		}

		/**
			* Выгрузить объект из коллекции
			* @param Integer $object_id id объекта
		*/
		public function unloadObject($object_id) {
			if($this->isLoaded($object_id)) {
				unset($this->objects[$object_id]);
				umiObjectProperty::unloadPropData($object_id);
			} else {
				return false;
			}
		}

		/**
			* Выгрузить объекты из коллекции
			* @param Integer $object_id id объекта
		*/
		public function unloadAllObjects() {

			foreach($this->objects as $object_id => $v)
			{
				unset($this->objects[$object_id]);
				umiObjectProperty::unloadPropData($object_id);
			}
		}

		/**
			* Получить id всех объектов, загруженных в коллекцию
			* @return Array массив, состоящий из id объектов
		*/
		public function getCollectedObjects() {
			return array_keys($this->objects);
		}

		/**
			* Указать, что $object_id был изменен во время сессии. Используется внутри ядра.
			* Явный вызов этого метода клиентским кодом не нужен.
			* @param Integer $object_id id объекта
		*/
		public function addUpdatedObjectId($object_id) {
			if(!in_array($object_id, $this->updatedObjects)) {
				$this->updatedObjects[] = $object_id;
			}
		}

		/**
			* Получить список измененных объектов за текущую сессию
			* @return Array массив, состоящий из id измененных значений
		*/
		public function getUpdatedObjects() {
			return $this->updatedObjects;
		}

		/**
		 * Возвращает максимальное время последней модификации объектов, загруженных в текущей сессии
		 * @return int Unix timestamp
		 */
		public function getObjectsLastUpdateTime() {
			$maxUpdateTime = 0;

			/** @var iUmiObject $object */
			foreach ($this->objects as $object) {
				if (!$object instanceof iUmiObject) {
					continue;
				}

				if ($maxUpdateTime < $object->getUpdateTime()) {
					$maxUpdateTime = $object->getUpdateTime();
				}
			}

			return $maxUpdateTime;
		}

		/**
			* Деструктор коллекции. Явно вызывать его не нужно никогда.
		*/
		public function __destruct() {
			if(sizeof($this->updatedObjects)) {
				if(function_exists("deleteObjectsRelatedPages")) {
					deleteObjectsRelatedPages();

				}
			}
		}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 */
		public function clearCache() {
			$keys = array_keys($this->objects);
			foreach($keys as $key) unset($this->objects[$key]);
			$this->objects = array();
		}

		/**
		 * Проверяет, не "сломан" ли объект,
		 * возвращает результат проверки.
		 * @param int $objectId ид объекта
		 * @return void
		 * @throws coreException
		 */
		public function checkObjectById($objectId) {
			static $res;
			if ($res !== null) {
				return $res;
			}

			if (!cacheFrontend::getInstance()->getIsConnected()) {
				return $res = true;
			}

			$objectId = intval($objectId);

			$sql = "SELECT id FROM cms3_objects WHERE id = $objectId";
			$result = l_mysql_query($sql);
			if ($err = l_mysql_error()) {
				throw new coreException($err);
			}
			$res = (bool) mysql_num_rows($result);
			if (!$res) {
				cacheFrontend::getInstance()->flush();
			}
			return $res;
		}

		/**
		 * Меняет индекс сортировки двух объектов
		 * @param iUmiObject $firstObject первый объект
		 * @param iUmiObject $secondObject второй объект
		 * @param string $mode режим изменения сортировки
		 * @return bool
		 */
		public function changeOrder(iUmiObject $firstObject, iUmiObject $secondObject, $mode) {
			/* @var iUmiObject|umiEntinty $firstObject */
			$firstObjectOrder = $firstObject->getOrder();

			switch ($mode) {
				case 'child':
				case 'after': {
					/* @var iUmiObject|umiEntinty $secondObject */
					$secondObject->setOrder($firstObjectOrder + 1);
					$secondObject->commit();

					$this->reBuildOrder($secondObject, 'greed');
					break;
				}
				case 'before': {
					$this->reBuildOrder($firstObject, 'greed');

					/* @var iUmiObject|umiEntinty $secondObject */
					$secondObject->setOrder($firstObjectOrder);
					$secondObject->commit();
					break;
				}
				default: {
					return false;
				}
			}

			return true;
		}

		/**
		 * Перестраивает индекс сортировки у объектов, чей
		 * индекс больше индекса первого объекта
		 * @param iUmiObject $firstObject первый объект
		 * @param string $mode режим перестраивания
		 * @return bool
		 * @throws coreException
		 */
		public function reBuildOrder(iUmiObject $firstObject, $mode) {
			/* @var iUmiObject|umiEntinty $firstObject */
			$firstObjectOrder = (int) $firstObject->getOrder();
			$objectTypeId = (int) $firstObject->getTypeId();
			$objectTypeId = $this->getOrderTargetTypeId($objectTypeId);

			switch (true) {
				case is_numeric($objectTypeId): {
					$typeCondition = "`type_id` = $objectTypeId";
					break;
				}
				case is_array($objectTypeId): {
					$objectTypeId = implode(', ', $objectTypeId);
					$typeCondition = "`type_id` IN ($objectTypeId)";
					break;
				}
				default: {
					throw new coreException('Получено неверное значение идентификатора типа');
				}
			}

			$connection = ConnectionPool::getInstance()->getConnection();

			switch ($mode) {
				case 'greed': {
					$sql = <<<SQL
UPDATE `cms3_objects`
SET `ord` = `ord` + 1
WHERE $typeCondition
AND `ord` >= $firstObjectOrder;
SQL;
					break;
				}
				case 'normal':
				default: {
					$sql = <<<SQL
UPDATE `cms3_objects`
SET `ord` = `ord` + 1
WHERE $typeCondition
AND `ord` > $firstObjectOrder;
SQL;
				}
			}

			$connection->query($sql);
			return true;
		}

		/**
		 * Возвращает максимальное значение индекса сортировки
		 * среди объекто заданного типа данных
		 * @param int $objectTypeId идентификатор объектного типа данных
		 * @return int|bool
		 * @throws coreException
		 */
		public function getMaxOrderByTypeId($objectTypeId) {
			$objectTypeId = $this->getOrderTargetTypeId($objectTypeId);
			$connection = ConnectionPool::getInstance()->getConnection();

			switch (true) {
				case is_numeric($objectTypeId): {
					$sql = <<<SQL
SELECT max(`ord`) as ord FROM `cms3_objects` WHERE `type_id` = $objectTypeId;
SQL;
					break;
				}
				case is_array($objectTypeId): {
					$objectTypeId = implode(', ', $objectTypeId);
					$sql = <<<SQL
SELECT max(`ord`) as ord FROM `cms3_objects` WHERE `type_id` IN ($objectTypeId);
SQL;
					break;
				}
				default: {
					throw new coreException('Получено неверное значение идентификатора типа');
				}
			}

			/* @var mysqlQueryResult $result */
			$result = $connection->queryResult($sql, true);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);
			$result = $result->getIterator();
			/* @var mysqlQueryResultIterator $result */
			$row = $result->current();

			return (int) $row['ord'];
		}

		/**
		 * Возвращает идентификатор объектного типа данных,
		 * среди объекто которого нужно произвести перестройку
		 * индекса сортировки.
		 * @param int $objectTypeId идентификатор объектного типа данных
		 * @return int|array
		 */
		private function getOrderTargetTypeId($objectTypeId) {
			$umiObjectsTypes = umiObjectTypesCollection::getInstance();
			$objectTypeId = (int) $objectTypeId;
			$parentObjectTypeId = $umiObjectsTypes->getParentClassId($objectTypeId);

			if (!is_numeric($parentObjectTypeId)) {
				return $objectTypeId;
			}

			$objectTypeChildren = $umiObjectsTypes->getChildClasses($objectTypeId);
			$parentObjectType = $umiObjectsTypes->getType($parentObjectTypeId);

			switch (true) {
				case !$parentObjectType instanceof iUmiObjectType && count($objectTypeChildren) == 0: {
					return $objectTypeId;
				}
				case !$parentObjectType instanceof iUmiObjectType: {
					$objectTypeChildren[] = $objectTypeId;
					return $objectTypeChildren;
				}
			}

			/* @var umiObjectType $parentObjectType */
			if ($parentObjectType->getGuid() == 'root-guides-type') {
				return $objectTypeId;
			}

			return $umiObjectsTypes->getChildClasses($parentObjectTypeId);
		}

		/**
		 * Загружает во внутренний кеш класса id объектов с определенным гуидами
		 * @return void
		 * @throws coreException
		 */
		private function preLoadObjectsIdsByGuid() {
			$mainConfigs = mainConfiguration::getInstance();
			$guids = $mainConfigs->get('kernel', 'objects-guids-preload');
			if (!is_array($guids) || count($guids) == 0) {
				return;
			}
			$guids = array_map('l_mysql_real_escape_string', $guids);
			$sql = "SELECT `id`, `guid` FROM `cms3_objects` WHERE `guid` in ('" . implode('\', \'', $guids) . "');";

			$result = l_mysql_query($sql);

			if ($error = l_mysql_error()) {
				throw new coreException($error);
			}

			$objectsGuidsToId = &$this->objectsGuidsToId;

			while ($row = mysql_fetch_assoc($result)) {
				$objectsGuidsToId[$row['guid']] = $row['id'];
			}
		}
	}
?>