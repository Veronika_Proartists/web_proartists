<?php
	/**
	 * Этот класс служит для управления полем объекта
	 * Обрабатывает тип поля "Строка".
	 */
	class umiObjectPropertyString extends umiObjectProperty {
		/**
		 * Загружает значение свойства из БД, если тип свойства "Строка"
		 * @return Array массив со значением String
		 */
		protected function loadValue() {
			$res = Array();
			$field_id = $this->field_id;

			if ($data = $this->getPropData()) {
				foreach ($data['varchar_val'] as $val) {
					if (is_null($val)) continue;
					$res[] = self::filterOutputString((string) $val);
				}
				return $res;
			}

			$sql = "SELECT varchar_val FROM {$this->tableName} WHERE obj_id = '{$this->object_id}' AND field_id = '{$field_id}' LIMIT 1";
			$result = l_mysql_query($sql, true);

			while (list($val) = mysql_fetch_row($result)) {
				if (is_null($val)) continue;
				$res[] = self::filterOutputString((string) $val);
			}

			return $res;
		}

		/**
		 * Сохраняет значение свойства в БД, если тип свойства "Строка"
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();

			$cnt = 0;
			foreach ($this->value as $val) {
				if (strlen($val) == 0) continue;

				$val = self::filterInputString($val);

				$sql = "INSERT INTO {$this->tableName} (obj_id, field_id, varchar_val) VALUES('{$this->object_id}', '{$this->field_id}', '{$val}')";
				l_mysql_query($sql);
				++$cnt;
			}

			if (!$cnt) {
				$this->fillNull();
			}
		}

		/**
		 * @inherit
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValue = $this->value;

			if (!isset($oldValue[0])) {
				$oldValue = '';
			} else {
				$oldValue = strval($oldValue[0]);
			}

			if (!isset($newValue[0])) {
				$newValue = '';
			} else {
				$newValue = strval($newValue[0]);
			}

			return ($oldValue === $newValue) ? false : true;
		}
	}
?>