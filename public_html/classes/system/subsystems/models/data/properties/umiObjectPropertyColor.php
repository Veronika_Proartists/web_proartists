<?php
	/**
	 * Этот класс служит для управления полем объекта
	 * Обрабатывает тип поля "Цвет".
	 */
	class umiObjectPropertyColor extends umiObjectPropertyString {
		/**
		 * @inherit
		 */
		protected function loadValue() {
			return parent::loadValue();
		}

		/**
		 * @inherit
		 */
		protected function saveValue() {
			return parent::saveValue();
		}

		/**
		 * @inherit
		 */
		protected function isNeedToSave(array $newValue) {
			return parent::isNeedToSave($newValue);
		}
	}
?>