<?php
	/**
	 * Этот класс служит для управления полем объекта.
	 * Обрабатывает тип поля "Набор изображений"
	 */
	class umiObjectPropertyMultipleImgFile extends umiObjectProperty {
		/**
		 * @const string TABLE_NAME имя таблицы, в которой хранятся значения поля типа "Набор изображений"
		 */
		CONST TABLE_NAME = 'cms3_object_images';

		/**
		 * @inherit
		 */
		protected function loadValue() {
			$result = array();
			$fieldId = (int) $this->field_id;
			$objectId = (int) $this->object_id;
			$tableName = l_mysql_real_escape_string(self::TABLE_NAME);

			$selection = <<<SQL
	SELECT `id`, `src`, `alt`, `ord`
	FROM `$tableName`
	WHERE `obj_id` = $objectId AND `field_id` = $fieldId
	ORDER BY `ord`;
SQL;

			$connection = ConnectionPool::getInstance()->getConnection();
			$selectionResult = $connection->queryResult($selection, true);
			$selectionResult->setFetchType(IQueryResult::FETCH_ASSOC);

			if ($selectionResult->length() == 0) {
				return $result;
			}

			$isAdminMode = (cmsController::getInstance()->getCurrentMode() == 'admin');

			foreach ($selectionResult as $row) {
				$src = self::unescapeFilePath($row['src']);
				$image = new umiImageFile($src);
				$image->setAlt($row['alt']);
				$image->setOrder($row['ord']);
				$image->setId($row['id']);

				if ($image->getIsBroken() && !$isAdminMode) {
					continue;
				}

				$result[$image->getFilePath()] = $image;
			}

			return $result;
		}

		/**
		 * @inherit
		 */
		protected function saveValue() {
			$this->deleteCurrentRows();

			if (!is_array($this->value)) {
				return;
			}

			$fieldId = (int) $this->field_id;
			$objectId = (int) $this->object_id;
			$tableName = l_mysql_real_escape_string(self::TABLE_NAME);
			$connection = ConnectionPool::getInstance()->getConnection();
			$insertionCounter = 0;

			foreach ($this->value as $key => $value) {
				if (!$value instanceof umiImageFile || $value->getIsBroken()) {
					continue;
				}

				$src = l_mysql_real_escape_string($value->getFilePath());
				$src = self::unescapeFilePath($src);
				$alt = l_mysql_real_escape_string($value->getAlt());
				$ord = (int) $value->getOrder();

				if ($ord === 0) {
					$ord = $this->getMaxOrder() + 1;
				}

				$insertion = <<<SQL
	INSERT INTO `$tableName` (`obj_id`, `field_id`, `src`, `alt`, `ord`)
	VALUES ($objectId, $fieldId, '$src', '$alt', $ord)
SQL;

				$connection->query($insertion);
				$insertionCounter++;
			}

			if ($insertionCounter === 0) {
				$this->fillNull();
			}
		}

		/**
		 * @inherit
		 */
		protected function deleteCurrentRows() {
			$objectId = (int) $this->object_id;
			$fieldId = (int) $this->field_id;
			$tableName = l_mysql_real_escape_string(self::TABLE_NAME);
			$connection = ConnectionPool::getInstance()->getConnection();

			$deletion = <<<SQL
			DELETE FROM $tableName WHERE obj_id = $objectId AND field_id = $fieldId;
SQL;
			$connection->query($deletion);
		}

		/**
		 * @inherit
		 */
		protected function fillNull() {
			$objectId = (int) $this->object_id;
			$fieldId = (int) $this->field_id;
			$tableName = l_mysql_real_escape_string(self::TABLE_NAME);
			$connection = ConnectionPool::getInstance()->getConnection();

			$selection = <<<SQL
	SELECT COUNT(*) as count FROM `$tableName` WHERE `obj_id` = $objectId AND `field_id` = $fieldId;
SQL;

			/* @var mysqlQueryResult $result */
			$result = $connection->queryResult($selection, true);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);
			$result = $result->getIterator();
			/* @var mysqlQueryResultIterator $result */
			$row = $result->current();

			if ((int) $row['count'] === 0) {
				$insertion = <<<SQL
	INSERT INTO `$tableName` (`obj_id`, `field_id`) VALUES ($objectId, $fieldId);
SQL;
				$connection->query($insertion);
			}
		}

		/**
		 * @inherit
		 */
		protected function isNeedToSave(array $newValue) {
			$oldValues = $this->value;

			if (count($newValue) !== count($oldValues)) {
				return true;
			}

			$oldFilesPath = array();

			/* @var umiImageFile $oldValue */
			foreach ($oldValues as $oldValue) {
				$oldFilesPath[$oldValue->getFilePath()] = $oldValue;
			}

			foreach ($newValue as $key => $value) {
				if (!$value instanceof umiImageFile) {
					continue;
				}

				/* @var umiImageFile $oldValue */
				switch (true) {
					case isset($oldFilesPath[$value->getFilePath()]): {
						$oldValue = $oldFilesPath[$value->getFilePath()];
						break;
					}
					default: {
						return true;
					}
				}

				if ($value->getFilePath() !== $oldValue->getFilePath()) {
					return true;
				}

				if ($value->getAlt() !== $oldValue->getAlt()) {
					return true;
				}

				if ($value->getOrder() !== $oldValue->getOrder()) {
					return true;
				}
			}

			return false;
 		}

		/**
		 * Возвращает максимальное значение индекса сортировки
		 * среди изображений для текущего поля объекта
		 * @return int
		 */
		public function getMaxOrder() {
			$objectId = (int) $this->object_id;
			$fieldId = (int) $this->field_id;
			$tableName = l_mysql_real_escape_string(self::TABLE_NAME);

			$connection = ConnectionPool::getInstance()->getConnection();
			$selection = <<<SQL
	SELECT max(`ord`) as ord FROM `$tableName` WHERE `obj_id` = $objectId AND `field_id` = $fieldId;
SQL;
			/* @var mysqlQueryResult $result */
			$result = $connection->queryResult($selection, true);
			$result->setFetchType(IQueryResult::FETCH_ASSOC);
			$result = $result->getIterator();
			/* @var mysqlQueryResultIterator $result */
			$row = $result->current();

			return (int) $row['ord'];
		}
	}
?>