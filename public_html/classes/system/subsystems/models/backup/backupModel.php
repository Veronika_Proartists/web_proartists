<?php
/**
 * Класс для управления резервными копиями страниц
 * TODO Check and format all PHPDoc's
 * TODO Sort methods (public first)
 */
	class backupModel extends singleton implements iBackupModel {

		protected function __construct() {}

		/**
		 * TODO PHPDoc
		 * Enter description here ...
		 * @param unknown_type $c
		 * @return backupModel экземпляр класса
		 */
		public static function getInstance($c = NULL) {
			return parent::getInstance(__CLASS__);
		}

		/**
		 * Получить список изменений для страницы $cparam
		 * @param Integer $cparam = false - id страницы
		 * @return Array список изменений
		 */
		public function getChanges($cparam = false) {

			$regedit = regedit::getInstance();

			if (!$regedit->getVal("modules/backup/enabled")) {
				return false;
			}

			$limit = (int) $regedit->getVal("//modules/backup/max_save_actions");
			$time_limit = (int) $regedit->getVal("//modules/backup/max_timelimit");
			$end_time = $time_limit * 3600 * 24;

			$cparam = (int) $cparam;

			$limit = ($limit > 2) ? $limit : 2;

			$sql = "SELECT id, ctime, changed_module, user_id, is_active FROM cms_backup WHERE param='" . $cparam . "' AND (" . time() . "-ctime)<" . $end_time . " ORDER BY ctime DESC LIMIT {$limit}";
			$result = l_mysql_query($sql);

			if (mysql_num_rows($result) < 2) {
				$sql = "SELECT id, ctime, changed_module, user_id, is_active FROM cms_backup WHERE param='" . $cparam . "' ORDER BY ctime DESC LIMIT 2";
				$result = l_mysql_query($sql);
			}

			$params = array();
			$rows = array();
			while(list($revision_id, $ctime, $changed_module, $user_id, $is_active) = mysql_fetch_row($result)) {

				$revision_info = $this->getChangeInfo($revision_id, $ctime, $changed_module, $cparam, $user_id, $is_active);
				if (count($revision_info)) $rows[] = $revision_info;
			}

			$params['nodes:revision'] = $rows;
			return $params;

		}
		
		/**
		 * Возвращает список просроченных изменений модуля "Резервирование" 
		 * @param int $daysToExpire Количество дней хранения событий
		 * @return array массив объектов класса backupChange
		 */
		public function getOverdueChanges($daysToExpire = 30) {
			
			if ($daysToExpire === 0) {
				return;
			}
			
			$secondsInDay = 24 * 3600;
			$maxSecondsLimit = $daysToExpire * $secondsInDay;
			
			$overdueChangesQuery = <<<QUERY
				SELECT *
				FROM   `cms_backup` 
				WHERE  `ctime` < unix_timestamp() - ${maxSecondsLimit};
QUERY;
			$result = l_mysql_query($overdueChangesQuery);
			
			$changes = array();
			
			while ($data = mysql_fetch_assoc($result)) {
				if (isset($data['id'])) {
					$changes[] = new backupChange(
						$data['id'], $data['ctime'], $data['changed_module'], 
						$data['changed_method'], $data['param'], $data['param0'], 
						$data['user_id'], $data['is_active']);
				}
			}
			
			return $changes;
		}
		
		/**
		 * Удаляет изменения модуля "Резервирование"
		 * @param array $changes массив объектов класса backupChange
		 * @return boolean true в случае удаления хотя бы одного изменения
		 * @throws coreException в случае ошибки MySQL
		 */
		public function deleteChanges($changes = array()) {
			
			if (!is_array($changes)) {
				return false;
			}
			
			$changesID = array();
			
			/** var backupChange $backupChange */
			foreach ($changes as $backupChange) {
				if ($backupChange instanceof backupChange) {
					$changesID[] = $backupChange->id;
				}
			}
			
			if (empty($changesID)) {
				return false;
			}
			
			$changesIDForDeleting = implode($changesID, ', ');
			$deletingChangesQuery = <<<QUERY
				DELETE 
				FROM   `cms_backup` 
				WHERE  `id` IN (${changesIDForDeleting})
QUERY;
			l_mysql_query($deletingChangesQuery);
			
			if (l_mysql_error() !== '') {
				throw new coreException(mysql_error());
			}
			
			return true;
		}

		/**
		 * Возвращает данные об изменениях в точке восстановления $revision_id
		 * @param Integer $revision_id - id Точки восстановления
		 * @param Timestamp $ctime - Время создания точки восстановления
		 * @param String $changed_module - Название модуля к которому относится точка восстановления
		 * @param Integer $cparam - Id страницы, к которой относится точка восстановления
		 * @param Integer $user_id - Id пользователя, создавшего точку восстановления
		 * @param Integer $is_active - активность точки восстановления
		 * @return Array - данные о точке восстановления, подготовленные для шаблонизатора
		 */
		protected function getChangeInfo($revision_id, $ctime, $changed_module, $cparam, $user_id, $is_active) {

			$hierarchy = umiHierarchy::getInstance();
			$cmsController = cmsController::getInstance();

			$revision_info = array();

			$element = $hierarchy->getElement($cparam);
			if ($element instanceof umiHierarchyElement) {

				$revision_info['attribute:changetime'] = $ctime;
				$revision_info['attribute:user-id'] = $user_id;
				if (strlen($changed_module) == 0) {
					$revision_info['attribute:is-void'] = true;
				}
				if ($is_active) {
					$revision_info['attribute:active'] = "active";
				}
				$revision_info['date'] = new umiDate($ctime);
				$revision_info['author'] = selector::get('object')->id($user_id);
				$revision_info['link'] = "/admin/backup/rollback/{$revision_id}/";

				$module_name = $element->getModule();
				$method_name = $element->getMethod();

				$module = $cmsController->getModule($module_name);
				if ($module instanceof def_module) {
					$links = $module->getEditLink($cparam, $method_name);
					if (isset($links[1])) {
						$revision_info['page'] = array();
						$revision_info['page']['attribute:name'] = $element->getName();
						$revision_info['page']['attribute:edit-link'] = $links[1];
						$revision_info['page']['attribute:link'] = $element->link;
					}
				}
			}

			return $revision_info;
		}

		/**
		 * Получить список изменений для всех страниц
		 * @return Array список изменений
		 */
		public function getAllChanges() {
			if (!regedit::getInstance()->getVal("modules/backup/enabled")) {
				return false;
			}

			$sql = "SELECT id, ctime, changed_module, param, user_id, is_active FROM cms_backup ORDER BY ctime DESC LIMIT 100";
			$result = l_mysql_query($sql);

			$params = array();
			$rows = array();

			while (list($revision_id, $ctime, $changed_module, $cparam, $user_id, $is_active) = mysql_fetch_row($result)) {
				$revision_info = $this->getChangeInfo($revision_id, $ctime, $changed_module, $cparam, $user_id, $is_active);
				if (count($revision_info)) {
					$rows = array_merge($rows, array($revision_info));
				}
			}

			$params['nodes:revision'] = $rows;
			return $params;
		}

		/**
		 * TODO PHPDoc
		 * TODO refactor
		 * Сохранить как точку восстановления текущие изменения для страницы $cparam
		 * @param Integer $cparam = false id страницы
		 * @param String $cmodule = "" не используется более
		 * @param String $cmethod = "" не используется более
		 * @return Boolean
		 */
		public function save($cparam = "", $cmodule = "", $cmethod = "") {

			if (!regedit::getInstance()->getVal("//modules/backup/enabled")) {
				return false;
			}
			if (getRequest('rollbacked')) {
				return false;
			}

			$this->restoreIncrement();

			$cmsController = cmsController::getInstance();
			if (!$cmodule) {
				$cmodule = $cmsController->getCurrentModule();
			}
			$cmethod = $cmsController->getCurrentMethod();

			$cuser_id = ($cmsController->getModule('users')) ? $cuser_id = $cmsController->getModule('users')->user_id : 0;

			$ctime = time();

			if(!$cmodule) {
				$cmodule = getRequest('module');
			}
			if(!$cmethod) {
				$cmethod = getRequest('method');
			}

			foreach ($_REQUEST as $cn => $cv) {
				if ($cn == "save-mode") {
					continue;
				}
				$_temp[$cn] = (!is_array($cv)) ? base64_encode($cv) : $cv;
			}

			if (isset($_temp['data']['new'])) {
				$element = umiHierarchy::getInstance()->getElement($cparam);
				if ($element instanceof umiHierarchyElement) {
					$_temp['data'][$element->getObjectId()] = $_temp['data']['new'];
					unset($_temp['data']['new']);
				}
			}

			$req = serialize($_temp);
			$req = l_mysql_real_escape_string($req);

			$cparam = l_mysql_real_escape_string($cparam);
			$cmodule = l_mysql_real_escape_string($cmodule);
			$cmethod = l_mysql_real_escape_string($cmethod);

			$sql = "UPDATE cms_backup SET is_active='0' WHERE param='" . $cparam . "'";
			l_mysql_query($sql);

			$sql = <<<SQL
INSERT INTO cms_backup (ctime, changed_module, changed_method, param, param0, user_id, is_active)
				VALUES('{$ctime}', '{$cmodule}', '{$cmethod}', '{$cparam}', '{$req}', '{$cuser_id}', '1')
SQL;
			l_mysql_query($sql);

			$limit = regedit::getInstance()->getVal("//modules/backup/max_save_actions");
			$sql = "SELECT COUNT(*) FROM cms_backup WHERE param='" . $cparam . "' ORDER BY ctime DESC";
			$result = l_mysql_query($sql);
			list($total_b) = mysql_fetch_row($result);

			$td = $total_b - $limit;
			if ($td < 0) {
				$td = 0;
			}

			$sql = "SELECT id FROM cms_backup WHERE param='" . $cparam . "' ORDER BY ctime DESC LIMIT 2";
			$result = l_mysql_query($sql);
			$backupIds = array();
			while (list($backupId) = mysql_fetch_row($result)) {
				$backupIds[] = $backupId;
			}
			$notId = "";
			if (count($backupIds)) {
				$notId = "AND id NOT IN (" . implode(", ", $backupIds) . ")";
			}

			$sql = "DELETE FROM cms_backup WHERE param='" . $cparam . "' {$notId} ORDER BY ctime ASC LIMIT " . ($td);
			l_mysql_query($sql);

			$time_limit = regedit::getInstance()->getVal("//modules/backup/max_timelimit");
			$end_time = $time_limit*3600*24;
			$sql="DELETE FROM cms_backup WHERE param='" . $cparam . "' AND (" . time() . "-ctime)>" . $end_time . " {$notId} ORDER BY ctime ASC";
			l_mysql_query($sql);

			return true;
		}

		/**
		 * TODO Исправить описание возвращаемого значения
		 * Восстановить данные из резервной точки $revision_id
		 * @param Integer $revision_id id резервное копии
		 * @return Boolean false, если восстановление невозможно
		 * @throws requreMoreAdminPermissionsException - если недостаточно прав на использования модуля,
		 * к которому относится точка восстановления
		 */
		public function rollback($revision_id) {
			if(!regedit::getInstance()->getVal("//modules/backup/enabled")) {
				return false;
			}

			$revision_id = (int) $revision_id;

			$sql = "SELECT param, param0, changed_module, changed_method FROM cms_backup WHERE id='$revision_id' LIMIT 1";
			$result = l_mysql_query($sql);

			if(list($element_id, $data, $changed_module, $changed_method) = mysql_fetch_row($result)) {
				$changed_param = $element_id;

				$sql = "UPDATE cms_backup SET is_active='0' WHERE param='" . $changed_param . "'";
				l_mysql_query($sql);

				$sql = "UPDATE cms_backup SET is_active='1' WHERE id='" . $revision_id . "'";
				l_mysql_query($sql);

				$_temp = unserialize($data);
				$_REQUEST = Array();

				foreach($_temp as $cn => $cv) {
					if(!is_array($cv)) {
						$cv = base64_decode($cv);
					} else {
						foreach($cv as $i => $v) {
							$cv[$i] = $v;
						}
					}
					$_REQUEST[$cn] = $cv;
					$_POST[$cn] = $cv;
				}
				$_REQUEST['rollbacked'] = true;
				$_REQUEST['save-mode'] = getLabel('label-save');

				if($changed_module_inst = cmsController::getInstance()->getModule($changed_module)) {
					$element = umiHierarchy::getInstance()->getElement($element_id);

					if($element instanceof umiHierarchyElement) {
						$links = $changed_module_inst->getEditLink($element_id, $element->getMethod());
						if(sizeof($links) >= 2) {
							$edit_link = $links[1];
							$_REQUEST['referer'] = $edit_link;

							$edit_link = trim($edit_link, "/") . "/do";

							if(preg_match("/admin\/[A-z]+\/([^\/]+)\//", $edit_link, $out)) {
								if(isset($out[1])) {
									$changed_method = $out[1];
								}
							}
							$_REQUEST['path'] = $edit_link;
							$_REQUEST['param0'] = $element_id;
							$_REQUEST['param1'] = "do";
						}
					}

					return $changed_module_inst->cms_callMethod($changed_method, Array());
				} else {
					throw new requreMoreAdminPermissionsException("You can't rollback this action. No permission to this module.");
				}
			}

		}

		/**
		 * TODO Добавить валидацию значения $elementId для поддержания целостности данных
		 * Добавить сообщение в список изменений страницы $elementId без занесения самих изменений
		 * @param Integer $elementId id страницы
		 * @return Boolean
		 */
		public function addLogMessage($elementId) {
			if(!regedit::getInstance()->getVal("//modules/backup/enabled")) {
				return false;
			}

			$this->restoreIncrement();

			$cmsController = cmsController::getInstance();
			$cuser_id = ($cmsController->getModule('users')) ? $cmsController->getModule('users')->user_id : 0;

			$time = time();
			$param = (int) $elementId;

			$sql = "INSERT INTO cms_backup (ctime, param, user_id, param0) VALUES('{$time}', '{$param}', '{$cuser_id}', '{$time}')";
			l_mysql_query($sql);
			return true;
		}

		/**
		 * TODO Saving arrays
		 * Создает точку восстановления страницы, используя данные страницы и объекта сохраненные в БД на текущий момент
		 * @param Integer $elementId - id страницы которая сохраняется
		 * @return Boolean
		 */
		public function fakeBackup($elementId) {
			/* TODO saving arrays - test it
			 *
			 * if(is_array($elementId)) {
			 * 	$result = true;
			 * 	foreach($elementId as $id) {
			 * 		$result = $result && $this->fakeBackup($id);
			 * 	}
			 * return $result;
			 * }
			 */
			$element = selector::get('page')->id($elementId);
			if (!($element instanceof umiEntinty)) {
				return false;
			}
			$originalRequest = $_REQUEST;

			$object = $element->getObject();
			$type = selector::get('object-type')->id($object->getTypeId());

			$_REQUEST['name'] = $element->name;
			$_REQUEST['alt-name'] = $element->altName;
			$_REQUEST['active'] = $element->isActive;
			foreach ($type->getAllFields() as $field) {
				$fieldName = $field->getName();
				$value = $this->fakeBackupValue($object, $field);
				if (is_null($value)) {
					continue;
				}
				$_REQUEST['data'][$object->id][$fieldName] = $value;
			}

			$this->save($elementId, $element->getModule());
			$_REQUEST = $originalRequest;
			return true;
		}

		/**
		 * Возврашает значение свойства объекта в том виде, в котором значения
		 * данного типа поля изначально передается в формах редактирования
		 * @param iUmiObject $object - Объект, свойство которого мы получаем
		 * @param iUmiField $field - Поле, значение которого мы хотим получить
		 * @return String Значение поля.
		 */
		protected function fakeBackupValue(iUmiObject $object, iUmiField $field) {
			$value = $object->getValue($field->getName());

			switch($field->getDataType()) {
				case 'file':
				case 'img_file':
				case 'swf_file':
					return ($value instanceof iUmiFile) ? $value->getFilePath() : '';

				case 'boolean':
					return $value ? '1' : '0';

				case 'date':
					return ($value instanceof umiDate) ? $value->getFormattedDate('U') : NULL;

				case 'tags':
					return is_array($value) ? implode(", ", $value) : NULL;

				default:
					return (string) $value;
			}
		}

		/**
		 * TODO Should return somethink
		 * Проверяет и при необходимости меняеи значение автоинкремента в таблице cms_backup
		 */
		protected function restoreIncrement() {

			$result1 = l_mysql_query("SELECT max( id ) FROM `cms_backup`");
			$row1 = mysql_fetch_row($result1);
			$incrementToBe = $row1[0] + 1;

			$result = l_mysql_query("SHOW TABLE STATUS LIKE 'cms_backup'");
			$row = mysql_fetch_array($result);
			$increment = isset($row['Auto_increment']) ? (int) $row['Auto_increment'] : false;
			if($increment !== false && $increment != $incrementToBe){
				l_mysql_query("ALTER TABLE `cms_backup` AUTO_INCREMENT={$incrementToBe}");
			}
		}
	};
?>