<?php
/**
 * Класс соединения с базой данных
 * Пример использования:
 *		$connection = new Connection('localhost', 'root', '', 'umi');
 *		$connetcion->open();
 *		$connection->query('SHOW TABLES');
 *		$connection->close();
 */
class mysqlConnection implements IConnection {
	private $host		= null;
	private $username	= null;
	private $password	= null;
	private $dbname		= null;
	private $port		= false;
	private $persistent = false;
	private $critical   = true;
	private $conn		= null;
	private $queryCache = array();
	private $isOpen     = false;
	private $queriesCount = 0;
	private $logger     = null;

	/**
	 * Конструктор соединения
	 * @param String  $host хост СУБД
	 * @param String  $login имя пользователя БД
	 * @param String  $password пароль к БД
	 * @param String  $dbname имя БД
	 * @param Integer $port порт
	 * @param Boolean $persistent true - для сохранения подключения открытым
	 * @param Boolean $critical true - если функционирование подключения критично для системы
	 */
	public function __construct($host, $login, $password, $dbname, $port = false, $persistent = false, $critical = true) {
		$this->host       = $host;
		$this->username   = $login;
		$this->password   = $password;
		$this->dbname     = $dbname;
		$this->port       = $port;
		$this->persistent = $persistent;
		$this->critical   = $critical;
	}

	/**
	 * Открывает соединение
	 * @return Boolean
	 */
	public function open() {
		if($this->isOpen) {
			return true;
		}

		try {
			$server = $this->host . ($this->port ? ':' . $this->port : '');
			if($this->persistent) {
				$this->conn = mysql_pconnect($server, $this->username, $this->password);
			} else {
				$this->conn = mysql_connect($server, $this->username, $this->password, true);
			}
			if($this->errorOccured()) throw new Exception();
			if(!mysql_select_db($this->dbname, $this->conn)) throw new Exception();


			$this->mysqlQuery("SET NAMES utf8", $this->conn);
			$this->mysqlQuery("SET CHARSET utf8", $this->conn);
			$this->mysqlQuery("SET CHARACTER SET utf8", $this->conn);
			$this->mysqlQuery("SET character_set_client = 'utf8'", $this->conn);
			$this->mysqlQuery("SET SESSION collation_connection = 'utf8_general_ci'", $this->conn);
			$this->mysqlQuery("SET SQL_BIG_SELECTS=1", $this->conn);
			$this->queriesCount += 6;
		} catch(Exception $e) {
			if($this->critical) {
				mysql_fatal();
			}
			else {
				return false;
			}
		}
		$this->isOpen = true;
		return true;
	}

	/**
	 * @inherit
	 */
	public function getQueriesCount() {
		return (int) $this->queriesCount;
	}

	/**
	 * Закрывает текущее соединение
	 */
	public function close() {
		if($this->isOpen) {
			mysql_close($this->conn);
			$this->isOpen = false;
		}
	}
	/**
	 * Выполняет запрос к БД
	 * @param String  $queryString строка запроса
	 * @param Boolean $noCache true - кэшировать результат, false - не кэшировать
	 * @return Resource результат выполнения запроса
	 */
	public function query($queryString, $noCache = false) {
		if(!$this->isOpen) {
			if (!$this->open()) {
				return false;
			}
		}

		$queryString = trim($queryString, " \t\n");

		if(defined('SQL_QUERY_DEBUG') && SQL_QUERY_DEBUG) {
			echo $queryString, "\r\n";
		}

		if(defined('MYSQL_DISABLE_CACHE') || stripos($queryString, 'SELECT') !== 0) {
			$result = $this->mysqlQuery($queryString, $this->conn);
			++$this->queriesCount;

			if($this->errorOccured()) {
				throw new databaseException($this->errorDescription($queryString));
			}

			return $result;
		}

		$hash = md5($queryString);
		if(isset($this->queryCache[$hash]) && !$noCache) {
			$result = $this->queryCache[$hash][0];
			if($this->queryCache[$hash][1]) {
				mysql_data_seek($result, 0);
			}
		} else {
			$result = $this->mysqlQuery($queryString, $this->conn);
			++$this->queriesCount;

			if($this->errorOccured()) {
				$this->queryCache[$hash] = false;
				throw new databaseException($this->errorDescription($queryString));
			} elseif(SQL_QUERY_CACHE) {
				$this->queryCache[$hash] = array($result, mysql_num_rows($result));
			}
		}
		return $result;
	}
	/**
	 * Выполняет запрос к БД
	 * @param String  $queryString строка запроса
	 * @param Boolean $noCache true - кэшировать результат, false - не кэшировать
	 * @return IQueryResult результат выполнения запроса
	 */
	public function queryResult($queryString, $noCache = false) {
		$result = $this->query($queryString, $noCache);
		return $result ? new mysqlQueryResult($result) : null;
	}
	/**
	 * Проверяет, успешно ли завершен последний запрос
	 * @return Boolean true в случае возникновения ошибки, иначе false
	 */
	public function errorOccured() {
		return mysql_error($this->conn) !== '';
	}
	/**
	 * Возвращает описание последней возникшей ошибки
	 * @return String
	 */
	public function errorDescription($sqlQuery = null) {
		$descr = mysql_error($this->conn);
		if($sqlQuery) {
			$descr .= " in query: " . $sqlQuery;
		}
		return $descr;
	}
	/**
	 * Возвращает признак открыто соединение или нет
	 * @return Boolean
	 */
	public function isOpen() {
		return $this->isOpen;
	}
	/**
	* Экранирует входящую строку
	* @param String $input строка для экранирования
	* @return экранированная строка
	*/
	public function escape($input) {
		if($this->isOpen) {
			return mysql_real_escape_string($input);
		} else {
			return addslashes($input);
		}
	}

	public function getConnectionInfo() {
		return array (
			'host' => $this->host,
			'port' => $this->port,
			'user' => $this->username,
			'password' => $this->password,
			'dbname' => $this->dbname,
			'link' => $this->conn
		);
	}
	
	/**
	 *  Очищает кэш
	 *  @return void
	 */
	public function clearCache() {
		$this->queryCache = array();
	}

	/**
	 * Устанавливает логгер запросов
	 * @param iMysqlLogger $mysqlLogger
	 */
	public function setLogger(iMysqlLogger $mysqlLogger) {
		$this->logger = $mysqlLogger;
	}

	/**
	 * Возвращает логгер запросов
	 * @return null|iMysqlLogger
	 */
	private function getLogger() {
		return $this->logger;
	}

	/**
	 * Запускает выполнение запроса,
	 * при необходимости логгирует запрос
	 * @param string $query текст запроса
	 * @param $resource подключение к бд
	 * @return resource
	 */
	private function mysqlQuery($query, $resource) {
		$logger = $this->getLogger();

		if ($logger === null) {
			return mysql_query($query, $resource);
		}

		if ($logger instanceof iMysqlLogger) {
			$logger->runTimer();
			$queryResult = mysql_query($query, $resource);
			$elapsedTime = $logger->getTimer();
			$message = $logger->getMessage($query, $elapsedTime);
			$logger->log($message);
			return $queryResult;
		}
	}
};
?>