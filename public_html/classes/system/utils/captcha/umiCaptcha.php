<?php
/**
	* Класс, контролирующий работу с капчей
*/
class umiCaptcha implements iUmiCaptcha {
	/** @const ID_PARAM имя параметра для идентификатора CAPTCHA */
	const ID_PARAM = 'captcha_id';
	/**
		* Генерирует код вызова капчи
		* @param String $template = "default" шаблон для генерации кода капчи
		* @param String $input_id = "sys_captcha" id инпута для капчи
		* @param String $captcha_hash = "" md5-хеш кода, который будет выведен на картинке для предворительно проверки на клиенте
	 	* @param String $captchaId идентификатор CAPTCHA
		* @return Array|String результат обработки в зависимости от текущего шаблонизатора
	*/
	public static function generateCaptcha($template="default", $input_id="sys_captcha", $captcha_hash="", $captchaId = '') {
		// check captcha
		if (!self::isNeedCaptha()) {
			return def_module::isXSLTResultMode() ? array(
				'comment:explain' => 'Captcha is not required for logged users'
			) : '';
		}

		if(!$template) $template = "default";
		if(!$input_id) $input_id = "sys_captcha";
		if(!$captcha_hash) $captcha_hash = "";
		$randomString = "?" . time();

		$block_arr = array();
		$block_arr['void:input_id'] = $input_id;
		$block_arr['attribute:captcha_hash'] = $captcha_hash;
		$block_arr['attribute:random_string'] = $randomString;
		$block_arr['url'] = array(
			'attribute:random-string'	=> $randomString,
			'node:value'				=> '/captcha.php',
		);

		if ($captchaId) {
			$block_arr['url']['attribute:id_param'] = self::ID_PARAM;
			$block_arr['url']['attribute:id'] = $captchaId;
		}

		list($template_captcha) = def_module::loadTemplates("captcha/".$template, "captcha");

		return def_module::parseTemplate($template_captcha, $block_arr);
	}

	/**
		* Свериться с системными настройками, чтобы выяснить, нужно ли использовать капчу.
		* @return Boolean true, если капча обязательна, false если капча отключена или пользователь уже авторизован
	*/
	public static function isNeedCaptha() {
		if (cmsController::getInstance()->getModule('users')->is_auth()) return false;
		if(getSession('umi_captcha') == md5(getCookie('user_captcha'))) {
			$_SESSION['is_human'] = 1;
		}

		return (getSession('is_human') != 1);
	}

	/**
	* Проверить была корректно введена капча в текущей сессии
	* @return Boolean если true, то пользователь определен как не робот, false - еще не определен
	*/
	public static function checkCaptcha() {
		$config = mainConfiguration::getInstance();
		if(!$config->get('anti-spam', 'captcha.enabled')) {
			return true;
		}

		$permissions = permissionsCollection::getInstance();
		if ($permissions->isAuth()) {
			return true;
		}

		if (!isset($_SESSION['umi_captcha'])) {

			if (isset($_SESSION['is_human'])) {
				unset($_SESSION['is_human']);
			}

			return false;
		}

		$captcha = $_SESSION['umi_captcha'];
		$userCaptchaCode = (string) getRequest('captcha');

		if (is_array($captcha)) {
			$captchaId = (string) getRequest(self::ID_PARAM);

			if (!isset($captcha[$captchaId])) {
				return false;
			}

			$captchaCode = $captcha[$captchaId];
			return self::compareCodes($captchaCode, $userCaptchaCode, $captchaId);
		} else {
			return self::compareCodes($captcha, $userCaptchaCode);
		}
	}

	/**
	 * Получить объект отрисовки капчи
	 * @return captchaDrawer объект отрисовки капчи
	 * @throws coreException
	 */
	public static function getDrawer() {
		static $drawer = null;
		if(!is_null($drawer)) return $drawer;

		$config = mainConfiguration::getInstance();
		$drawerName = $config->get('anti-spam', 'captcha.drawer');
		if(!$drawerName) {
			$drawerName = 'default';
		}
		$path = CURRENT_WORKING_DIR . '/classes/system/utils/captcha/drawers/' . $drawerName . '.php';
		if(!is_file($path)) {
			throw new coreException("Captcha image drawer named \"{$drawerName}\" not found");
		}
		require $path;

		$className = $drawerName . 'CaptchaDrawer';
		if(class_exists($className)) {
			return $drawer = new $className;
		} else {
			throw new coreException("Class \"{$className}\" not found in \"{$path}\"");
		}
	}

	/**
	 * Сравнивает коды CAPTCHA и возвращает результат сравнения
	 * @param string $captchaCode сохранный код CAPTCHA
	 * @param mixed $userCode код, отправленный пользователем
	 * @param string $captchaId идентификатор CAPTCHA
	 * @return bool
	 */
	private static function compareCodes($captchaCode, $userCode, $captchaId = '') {
		$cookieName = ($captchaId ? 'user_captcha_' . $captchaId : 'user_captcha');
		$codeHashFromCookie = md5(getCookie($cookieName));
		$codeHashFromUser = md5($userCode);

		if ($captchaCode == $codeHashFromCookie || $captchaCode == $codeHashFromUser) {

			if ($codeHashFromUser == $captchaCode) {
				setcookie($cookieName, $userCode, time() + 3600 * 24 * 31, '/');
			}

			$_SESSION['is_human'] = 1;
			return true;
		}

		unset($_SESSION['is_human']);
		return false;
	}

}

?>
