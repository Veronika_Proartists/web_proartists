<?php
	// В этом файле вы можете расширить существующую группу прав, или добавить свою
	$permissions = array(
		'tree' => array('colorField'),
		'view' => array('getcategorylistfull', 'getobjectslistfull', 'customSearch', 'getCatalogDefaultName', 'isCategory', 'getSpecialOffers')
	);
?>