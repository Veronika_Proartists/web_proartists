<?php
	abstract class __custom_comments {
		//TODO: Write here your own macroses

		/**
		* Установка чекбоксов комментарию
		*/
		public function onCommentPostSetCheckboxes(iUmiEventPoint $event) {
			if($event->getMode() == 'after') {
				$commentId = $event->getParam('message_id');
				$oCommentElement = umiHierarchy::getInstance()->getElement($commentId, true);
				$oCommentElement->robots_deny = true;
				$oCommentElement->is_unindexed = true;
				$oCommentElement->commit();
			}
		}

		/**
		* Установка оценки
		*/
		public function onCommentPostSetGrade(iUmiEventPoint $event) {
			if($event->getMode() == 'after') {
				$grade = (string) getRequest('grade');
				$commentId = $event->getParam('message_id');
				$oCommentElement = umiHierarchy::getInstance()->getElement($commentId, true);
				$oCommentElement->setValue("grade", $grade);
				$oCommentElement->commit();
			}
		}

        public function post_custom($parent_element_id = false) {
            $bNeedFinalPanic = false;

            $parent_element_id = (int) $parent_element_id;
            if(!isset($parent_element_id) || !$parent_element_id) {
                $parent_element_id = (int) getRequest('param0');
            }

            $title = trim(getRequest('title'));
            $content = trim(getRequest('comment'));
            $nick = htmlspecialchars(getRequest('author_nick'));
            $email = htmlspecialchars(getRequest('author_email'));

            $referer_url = getServer('HTTP_REFERER');
            $posttime = time();
            $ip = getServer('REMOTE_ADDR');

            if(!$referer_url) {
                $referer_url = umiHierarchy::getInstance()->getPathById($parent_element_id);
            }
            $this->errorRegisterFailPage($referer_url);

            if (!(strlen($title) || strlen($content))) {
                $this->errorNewMessage('%comments_empty%', false);
                $this->errorPanic();
            }

            // check captcha
            if (!is_null(getRequest('captcha'))) {
                $_SESSION['user_captcha'] = md5((int) getRequest('captcha'));
            }


            if (!umiCaptcha::checkCaptcha() || !$parent_element_id) {
                $this->errorNewMessage("%errors_wrong_captcha%");
                $this->errorPanic();
            }

            $user_id = permissionsCollection::getInstance()->getUserId();

            if(!$nick) {
                $nick = getRequest('nick');
            }

            if(!$email) {
                $email = getRequest('email');
            }


            if($nick) {
                $nick = htmlspecialchars($nick);
            }

            if($email) {
                $email = htmlspecialchars($email);
            }

            $is_sv = false;
            if($users_inst = cmsController::getInstance()->getModule("users")) {
                if($users_inst->is_auth()) {
                    $author_id = $users_inst->createAuthorUser($user_id);
                    $is_sv = permissionsCollection::getInstance()->isSv($user_id);
                } else {
                    if(!(regedit::getInstance()->getVal("//modules/comments/allow_guest"))) {
                        $this->errorNewMessage('%comments_not_allowed_post%', true);
                    }

                    $author_id = $users_inst->createAuthorGuest($nick, $email, $ip);
                }
            }
            $moderated = (int) regedit::getInstance()->getVal("//modules/comments/moderated");
            $is_active = ($moderated && !$is_sv) ? 0 : 1;

            if($is_active) {
                $is_active = antiSpamHelper::checkContent($content.$title.$nick.$email) ? 1 : 0;
            }

            if (!$is_active) {
                $this->errorNewMessage('%comments_posted_moderating%', false);
                $bNeedFinalPanic = true;
            }

            $object_type_id = umiObjectTypesCollection::getInstance()->getBaseType("comments", "comment");
            $hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("comments", "comment")->getId();

            $parentElement = umiHierarchy::getInstance()->getElement($parent_element_id);
            $tpl_id		= $parentElement->getTplId();
            $domain_id	= $parentElement->getDomainId();
            $lang_id	= $parentElement->getLangId();

            if (!strlen(trim($title)) && ($parentElement instanceof umiHierarchyElement)) {
                $title = "Re: ".$parentElement->getName();
            }

            $element_id = umiHierarchy::getInstance()->addElement($parent_element_id, $hierarchy_type_id, $title, $title, $object_type_id, $domain_id, $lang_id, $tpl_id);
            permissionsCollection::getInstance()->setDefaultPermissions($element_id);

            $element = umiHierarchy::getInstance()->getElement($element_id, true);

            $element->setIsActive($is_active);
            $element->setIsVisible(false);

            $element->setValue("message", $content);
            $element->setValue("publish_time", $posttime);

            $element->getObject()->setName($title);
            $element->setValue("h1", $title);

            $element->setValue("author_id", $author_id);

            $object_id = $element->getObject()->getId();
            $data_module = cmsController::getInstance()->getModule('data');
            $data_module->saveEditedObjectWithIgnorePermissions($object_id, true,true);

            // moderate
            $element->commit();
            $parentElement->commit();

            $oEventPoint = new umiEventPoint("comments_message_post_do");
            $oEventPoint->setMode("after");
            $oEventPoint->setParam("topic_id", $parent_element_id);
            $oEventPoint->setParam("message_id", $element_id);
            $this->setEventPoint($oEventPoint);

            // redirect with or without error messages
            if ($bNeedFinalPanic) {
                $this->errorPanic();
            } else {
                // validate url
                $referer_url = preg_replace("/_err=\d+/is", '', $referer_url);
                while (strpos($referer_url, '&&') !== false || strpos($referer_url, '??') !== false || strpos($referer_url, '?&') !== false) {
                    $referer_url = str_replace('&&', '&', $referer_url);
                    $referer_url = str_replace('??', '?', $referer_url);
                    $referer_url = str_replace('?&', '?', $referer_url);
                }
                if (strlen($referer_url) && (substr($referer_url, -1) === '?' || substr($referer_url, -1) === '&')) $referer_url = substr($referer_url, 0, strlen($referer_url)-1);

                $this->redirect($referer_url);

            }
        }
	};

?>