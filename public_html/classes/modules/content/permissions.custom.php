<?php
	$permissions = Array(
		'content' => 
				Array('activityControl', 'customSitemap', 'getTypeFields'),
		'sitetree' =>
				Array('adminsitetree', 'getTreeNodes', 'getInitialTree', 'treeAction', 'saveOptioned', 'getOptionedAndGuide')
	);
?>