<?php
abstract class __videoalbum extends baseModuleAdmin {

    public function config() {
        $regedit = regedit::getInstance();
        $params = array('config' => array('int:per_page' => NULL));
        $mode = getRequest("param0");

        if($mode == "do") {
            $params = $this->expectParams($params);
            $regedit->setVar("//modules/videoalbum/per_page", $params['config']['int:per_page']);
            $this->chooseRedirect();
        }

        $params['config']['int:per_page'] = (int) $regedit->getVal("//modules/videoalbum/per_page");

        $this->setDataType("settings");
        $this->setActionType("modify");

        $data = $this->prepareData($params, "settings");

        $this->setData($data);
        return $this->doData();
    }


    public function lists() {
        $this->setDataType("list");
        $this->setActionType("view");

        if($this->ifNotXmlMode()) return $this->doData();

        $limit = 20;
        $curr_page = getRequest('p');
        $offset = $curr_page * $limit;

        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('videoalbum', 'videos');
        $sel->types('hierarchy-type')->name('videoalbum', 'video');
        $sel->limit($offset, $limit);

        selectorHelper::detectFilters($sel);

        $data = $this->prepareData($sel->result, "pages");

        $this->setData($data, $sel->length);
        $this->setDataRangeByPerPage($limit, $curr_page);
        return $this->doData();
    }

    public function add() {
        $parent = $this->expectElement("param0");
        $type = (string) getRequest("param1");
        $mode = (string) getRequest("param2");

        $this->setHeaderLabel("header-videoalbum-add-" . $type);

        $inputData = Array(	"type" => $type,
        "parent" => $parent,
        'type-id' => getRequest('type-id'),
        "allowed-element-types" => Array('videos', 'video'));

        if($mode == "do") {
            $element_id = $this->saveAddedElementData($inputData);
            if($type == "video") {
                umiHierarchy::getInstance()->moveFirst($element_id, ($parent instanceof umiHierarchyElement)?$parent->getId():0);
                //$element->setValue("create_time", time());
            }
            $this->chooseRedirect();
        }

        $this->setDataType("form");
        $this->setActionType("create");

        $data = $this->prepareData($inputData, "page");

        $this->setData($data);
        return $this->doData();
    }


    public function edit() {
        $element = $this->expectElement('param0', true);
        $mode = (string) getRequest('param1');
        $type = $this->getObjectTypeMethod($element->getObject());
        $this->setHeaderLabel("header-videoalbum-edit-" . $type);

        $inputData = array(
            'element'				=> $element,
            'allowed-element-types'	=> array('videos', 'video')
        );

        if($mode == "do") {

            $this->saveEditedElementData($inputData);
            if($type == "video") {
                $this->uploadVideoPreview($element);
            }
            $this->chooseRedirect();
        }

        $this->setDataType("form");
        $this->setActionType("modify");

        $data = $this->prepareData($inputData, "page");

        $this->setData($data);
        return $this->doData();
    }


    public function del() {
        $elements = getRequest('element');
        if(!is_array($elements)) {
            $elements = array($elements);
        }

        foreach($elements as $elementId) {
            $element = $this->expectElement($elementId, false, true);

            $params = array(
                "element" => $element,
                "allowed-element-types" => Array('videos', 'video')
            );
            $this->deleteElement($params);
        }

        $this->setDataType("list");
        $this->setActionType("view");
        $data = $this->prepareData($elements, "pages");
        $this->setData($data);

        return $this->doData();
    }


    public function activity() {
        $elements = getRequest('element');
        if(!is_array($elements)) {
            $elements = array($elements);
        }
        $is_active = getRequest('active');

        foreach($elements as $elementId) {
            $element = $this->expectElement($elementId, false, true);

            $params = array(
                "element" => $element,
                "allowed-element-types" => Array('videos', 'video'),
                "activity" => $is_active
            );
            $this->switchActivity($params);
            $element->commit();
        }

        $this->setDataType("list");
        $this->setActionType("view");
        $data = $this->prepareData($elements, "pages");
        $this->setData($data);

        return $this->doData();
    }


    public function getDatasetConfiguration($param = '') {
        return array(
            'methods' => array(
                array('title'=>getLabel('smc-load'), 'forload'=>true, 'module'=>'videoalbum', '#__name'=>'lists'),
                array('title'=>getLabel('smc-delete'),'module'=>'videoalbum', '#__name'=>'del', 'aliases' => 'tree_delete_element,delete,del'),
                array('title'=>getLabel('smc-activity'),'module'=>'videoalbum', '#__name'=>'activity', 'aliases' => 'tree_set_activity,activity'),
                array('title'=>getLabel('smc-copy'), 'module'=>'content', '#__name'=>'tree_copy_element'),
                array('title'=>getLabel('smc-move'),'module'=>'content', '#__name'=>'tree_move_element'),
                array('title'=>getLabel('smc-change-template'), 'module'=>'content', '#__name'=>'change_template'),
                array('title'=>getLabel('smc-change-lang'), 'module'=>'content', '#__name'=>'move_to_lang')),
                'types' => array(
                array('common' => 'true', 'id' => 'video')
                ),
                'stoplist' => array(),
                'default' => 'image[140px]|url[180px]'
            );
    }

    public function uploadVideoPreview($element) {
        if(!$element->getValue('photo'))
            if($url = $element->getValue('url')){
                if (stripos($url,'youtube.com')){
                    $video = substr($url,strrpos($url,'/') + 1);
                    if(stripos($video,'watch') !== false){
                        $video = str_replace('watch?v=','',$video);
                        if(stripos($video,'&list=')) $video = substr($video,0,strrpos($video,'&list='));
                    }
                    $url = 'http://img.youtube.com/vi/'.$video.'/maxresdefault.jpg';
                    $path = './images/youtube/'.$video.'.jpg';
                    if (file_put_contents($path, file_get_contents($url))){
                        $img = new umiImageFile($path);
                        $element->setValue('photo',$img);
                        $element->commit();
                    }
                }
            }
    }
};
?>