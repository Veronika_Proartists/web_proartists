<?php

$i18n = Array(
    "module-videoalbum" => "Видеогалереи",

    "label-add-list" => "Добавить видеоальбом",
    "label-add-item" => "Добавить видео",

    "header-videoalbum-lists" => "Видеогалереи",
    "header-videoalbum-config" => "Настройки модуля",
    "header-videoalbum-add" => "Добавление",
    "header-videoalbum-edit" => "Редактирование",

    "header-videoalbum-add-videos"   => "Добавление видеоальбома",
    "header-videoalbum-add-video"     => "Добавление видео",

    "header-videoalbum-edit-videos"   => "Редактирование видеоальбома",
    "header-videoalbum-edit-video"     => "Редактирование видео",

    "header-videoalbum-activity" => "Изменение активности",

    'perms-videoalbum-view' => 'Просмотр видеогалерей',
    'perms-videoalbum-lists' => 'Управление видеогалереями'
);

?>