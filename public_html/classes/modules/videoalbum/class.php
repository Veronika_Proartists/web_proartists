<?php

class videoalbum extends def_module {
    public $per_page;

    public function __construct() {
        parent::__construct();

        if(cmsController::getInstance()->getCurrentMode() == "admin") {

            $configTabs = $this->getConfigTabs();
            if ($configTabs) {
                $configTabs->add("config");
            }

            $this->__loadLib("__admin.php");
            $this->__implement("__videoalbum");

        } else {

            $this->per_page = regedit::getInstance()->getVal("//modules/videoalbum/per_page");
        }

    }

    public function videos($path = "", $template = "default") {
        if($this->breakMe()) return;
        $element_id = cmsController::getInstance()->getCurrentElementId();

        templater::pushEditable("videoalbum", "videos", $element_id);
        return $this->group($element_id, $template) . $this->listElements($element_id, $template);
    }

    public function video() {
        if($this->breakMe()) return;
        $element_id = (int) cmsController::getInstance()->getCurrentElementId();
        return $this->view($element_id);
    }

    public function albums($template = "default",$album = 0,$limit = false,$ignore_paging = false, $i_need_deep = 1) {
        $parent_id = $this->analyzeRequiredPath($album);
        if(!$template) $template = "default";
        $oHierarchy = umiHierarchy::getInstance();

        list($template_block, $template_line) = $this->loadTemplates("./tpls/videoalbum/{$template}.tpl", "pages_block", "pages_block_line");

        if($limit) $per_page = $limit;
        else $per_page = $this->per_page;

        $curr_page = (int) getRequest('p');
        if($ignore_paging) $curr_page = 0;

        $block_arr = Array();
        $block_arr['id'] = $parent_id;

        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('videoalbum', 'videos');
        $sel->where('hierarchy')->page($parent_id)->childs($i_need_deep);
        $sel->limit($curr_page*$per_page, $per_page);

        $result = $sel->result();
        $total = $sel->length();

        $lines = Array();
        foreach ($result as $element) {
            $line_arr = Array();
            $element_id = $element->id;
            $line_arr['attribute:id'] = $element_id;
            $line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element_id);
            $line_arr['xlink:href'] = "upage://" . $element_id;
            $line_arr['void:header'] = $lines_arr['name'] = $element->getName();
            $line_arr['node:name'] = $element->getName();

            $lines[] = self::parseTemplate($template_line, $line_arr, $element_id);

            $this->pushEditable("videoalbum", "video", $element_id);
        }

        $block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;

        $block_arr['per_page'] = $per_page;
        $block_arr['total'] = $total;
        //return $block_arr;
        // парсим основной блок шаблона $block_arr и возвращаем результат
        return self::parseTemplate($template_block, $block_arr);
    }

    public function album($template = "default",$album = 0,$limit = false,$ignore_paging = false, $i_need_deep = 1) {
                $parent_id = $this->analyzeRequiredPath($album);
                if(!$template) $template = "default";
                $oHierarchy = umiHierarchy::getInstance();

                list($template_block, $template_line) = $this->loadTemplates("./tpls/videoalbum/{$template}.tpl", "pages_block", "pages_block_line");
                
                if($limit) $per_page = $limit;
                    else $per_page = $this->per_page;
                    
                $curr_page = (int) getRequest('p');
                if($ignore_paging) $curr_page = 0;

                $block_arr = Array();
                $block_arr['id'] = $parent_id;

                $sel = new selector('pages');
                $sel->types('hierarchy-type')->name('videoalbum', 'video');
                $sel->where('hierarchy')->page($parent_id)->childs($i_need_deep);
                $sel->limit($curr_page*$per_page, $per_page);

                $result = $sel->result();
                $total = $sel->length();
                
                $lines = Array();
                foreach ($result as $element) {
                        $line_arr = Array();
                        $element_id = $element->id;
                        $line_arr['attribute:id'] = $element_id;
                        $line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element_id);
                        $line_arr['attribute:url'] = $element->getValue('url');
                        $line_arr['attribute:autor_id'] = $element->getValue('autor');
                        $line_arr['xlink:href'] = "upage://" . $element_id;
                        $line_arr['void:header'] = $lines_arr['name'] = $element->getName();
                        $line_arr['node:name'] = $element->getName();

                        $lines[] = self::parseTemplate($template_line, $line_arr, $element_id);

                        $this->pushEditable("videoalbum", "video", $element_id);
                }

                $block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;

                $block_arr['per_page'] = $per_page;
                $block_arr['total'] = $total;
                //return $block_arr;
                // парсим основной блок шаблона $block_arr и возвращаем результат
                return self::parseTemplate($template_block, $block_arr);
            }

    public function getAllVideo() {
        $block_arr = Array();
        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('videoalbum', 'video');
        $result = $sel->result();
        $lines = Array();
        foreach ($result as $element) {
            $line_arr = Array();
            $element_id = $element->id;
            $line_arr['attribute:id'] = $element_id;
            $line_arr['node:name'] = $element->getName();

            $lines[] = $line_arr;
        }
        $block_arr['items']['nodes:item'] = $lines;
        return $block_arr;
    }

    public function group($elementPath = "", $template = "default", $per_page = false) {
        if($this->breakMe()) return;
        $hierarchy = umiHierarchy::getInstance();
        list($template_block) = def_module::loadTemplates("tpls/videoalbum/{$template}.tpl", "group");

        $elementId = $this->analyzeRequiredPath($elementPath);

        $element = $hierarchy->getElement($elementId);

        templater::pushEditable("videoalbum", "videos", $element->id);
        return self::parseTemplate($template_block, array('id' => $element->id), $element->id);

    }

    public function view($elementPath = "", $template = "default") {
        if($this->breakMe()) return;
        $hierarchy = umiHierarchy::getInstance();
        list($template_block) = def_module::loadTemplates("tpls/videoalbum/{$template}.tpl", "view");

        $elementId = $this->analyzeRequiredPath($elementPath);
        $element = $hierarchy->getElement($elementId);

        templater::pushEditable("videoalbum", "video", $element->id);
        return self::parseTemplate($template_block, array('id' => $element->id), $element->id);
    }

    public function listGroup($elementPath, $template = "default", $per_page = false, $ignore_paging = false) {
    // Код метода
    }


    public function listElements($elementPath, $template = "default", $per_page = false, $ignore_paging = false) {
    // Код метода
    }

    public function config() {
        return __videoalbum::config();
    }

    public function getEditLink($element_id, $element_type) {
        $element = umiHierarchy::getInstance()->getElement($element_id);
        $parent_id = $element->getParentId();

        switch($element_type) {
            case "videos": {
                $link_add = $this->pre_lang . "/admin/videoalbum/add/{$element_id}/video/";
                $link_edit = $this->pre_lang . "/admin/videoalbum/edit/{$element_id}/";

                return Array($link_add, $link_edit);
                break;
            }

            case "video": {
                $link_edit = $this->pre_lang . "/admin/videoalbum/edit/{$element_id}/";

                return Array(false, $link_edit);
                break;
            }

            default: {
                return false;
            }
        }
    }

};
?>