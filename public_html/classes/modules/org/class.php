<?php

class org extends def_module {
    public $info_obj;

    public function __construct() {
        parent::__construct();
        $id = regedit::getInstance()->getVal("//modules/org/info_id");
        if(!$id or $id == 0) $id = $this->createInfoPage();
        if(cmsController::getInstance()->getCurrentMode() == "admin") {

            $configTabs = $this->getConfigTabs();
            if ($configTabs) {
                $configTabs->add("config");
            }

            $this->__loadLib("__admin.php");
            $this->__implement("__org");

        } else {
            $this->info_obj = umiObjectsCollection::getInstance()->getObject($id);
        }

    }

    public function createInfoPage() {
        $hierarchyTypes = umiHierarchyTypesCollection::getInstance();
        $type = $hierarchyTypes->getTypeByName('org','info');
        if(!$type) {
            $typeId = $hierarchyTypes->addType('org','Информационная страница','info');
            $type = $hierarchyTypes->getType($typeId);
        }
        $typesCollection = umiObjectTypesCollection::getInstance();
        $objTypeId = $typesCollection->getBaseType('org', 'info');
        if(!$objTypeId){
            $objTypeId = $typesCollection->addType(false,'Информационная страница',true,true);
            $objType = $typesCollection->getType($objTypeId);
            $objType->setHierarchyTypeId($type->getId());
        }
        $info_obj_id = umiObjectsCollection::getInstance()->addObject('Информационная страница',$objTypeId);
        regedit::getInstance()->setVal("//modules/org/info_id",$info_obj_id);
        return $info_obj_id;
    }
    public function getter() {
        translatorWrapper::$showEmptyFields = true;
        return array('full:object' => $this->info_obj);
    }

    public function get($name) {
        return $this->info_obj->getValue($name);
    }


    public function view($elementPath = "", $template = "default") {
        if($this->breakMe()) return;
        $hierarchy = umiHierarchy::getInstance();
        list($template_block) = def_module::loadTemplates("tpls/org/{$template}.tpl", "view");

        $elementId = $this->analyzeRequiredPath($elementPath);
        $element = $hierarchy->getElement($elementId);

        templater::pushEditable("org", "info", $element->getId());
        return self::parseTemplate($template_block, array('id' => $element->getId()), $element->getId());
    }


    public function config() {
        return __org::config();
    }

};
?>