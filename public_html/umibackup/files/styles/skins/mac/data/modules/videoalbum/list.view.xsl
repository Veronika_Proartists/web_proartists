<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common/videoalbum">
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="data" priority="1">
        <div class="imgButtonWrapper" xmlns:umi="http://www.umi-cms.ru/TR/umi">
            <a id="addCategory" href="{$lang-prefix}/admin/videoalbum/add/{$param0}/videos/" class="type_select_gray"
               umi:type="videoalbum::videos">
                <xsl:text>&label-add-list;</xsl:text>
            </a>
            <a id="addObject" href="{$lang-prefix}/admin/videoalbum/add/{$param0}/video/" class="type_select_gray"
               umi:type="videoalbum::video">
                <xsl:text>&label-add-item;</xsl:text>
            </a>
        </div>
        <xsl:call-template name="ui-smc-table">
            <xsl:with-param name="js-add-buttons">
                <![CDATA[ createAddButton($('#addCategory')[0], oTable, '{$pre_lang}/admin/videoalbum/add/{id}/videos/',
          ['videos', true]); createAddButton($('#addObject')[0], oTable, '{$pre_lang}/admin/videoalbum/add/{id}/video/', ['videos']); ]]>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
</xsl:stylesheet>