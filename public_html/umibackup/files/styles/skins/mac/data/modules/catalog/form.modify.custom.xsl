<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common">

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <xsl:template match="field[@type = 'optioned']" mode="form-modify">
        <div class="field text">
            <label for="{generate-id()}">
                <span>
                    <xsl:call-template name="std-optioned-control">
                        <xsl:with-param name="type" select="'int'" />
                    </xsl:call-template>
                </span>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="data[@type='form' and @action='modify']//field[@type = 'optioned' and @name='color']" mode="form-modify">
        <div class="field text" id="color-option-field" data-id="{//data/page/@object-id}">
            <label for="{generate-id()}">
                <span>
                    <xsl:call-template name="std-optioned-control">
                        <xsl:with-param name="type" select="'int'" />
                    </xsl:call-template>
                </span>
            </label>
        </div>
        <script>
            var gquery;
            $(document).ready(function(){
                $('#color-option-field tbody input').bind('keyup',function(e){
                    e.preventDefault;
                   /* if(typeof(gquery)!=='undefined' || gquery){
                        gquery.abort()  ;
                    }*/
                    ajax_update('edit',$(this).closest('tr').index());
                });
                $('#color-option-field a.remove').bind('click',function(e){
                    if(typeof(gquery)!=='undefined' || gquery){
                        gquery.abort()  ;
                    }
                    window.setTimeout(ajax_update('del',$(this).closest('tr').index()), 1000);
                });
                $('#color-option-field a.add').bind('click',function(e){
                    if(typeof(gquery)!=='undefined' || gquery){
                        gquery.abort()  ;
                    }
                    window.setTimeout(ajax_update('add'), 1000);
                });
                function ajax_update(mode,pos){
                    console.log(pos);
                    if(pos === undefined) pos = $('#color-option-field tbody tr').length - 1;
                    console.log(pos);
                    console.log($('#color-option-field tbody tr').eq(pos));
                    var id =  $('#color-option-field').data('id')
                    if(typeof(gquery)!=='undefined' || gquery){
                        gquery.abort()  ;
                    }
                    var param='';
                    $('#color-option-field tbody tr').eq(pos).find('input').each(function(){
                        if(param != '') param+='&amp;';
                            param+=$(this).attr('name')+'='+$(this).val();
                    })
                    if(param != '') param+='&amp;';
                    param+='pos='+(parseInt(pos));

                    console.log(param);
                    gquery = $.post('/udata://catalog/colorField/'+id+'/'+mode+'.json',param,function(){

                    },'json');
                }
                $('form').bind('submit',function(){
                    $('#color-option-field tbody input').each(function(){
                       $(this).removeAttr('name');
                    })
                })
            })
        </script>
    </xsl:template>


    <xsl:template match="field/values/value" mode="field-optioned">
        <xsl:param name="input-name" />
        <xsl:param name="type"><xsl:text>float</xsl:text></xsl:param>
        <xsl:variable name="position" select="position()" />
        <tr data-pos="{$position}">
            <td>
                <xsl:value-of select="object/@name" />
                <input type="hidden" name="{$input-name}[{$position}][rel]" value="{object/@id}" />
            </td>
            <td class="center">
                <xsl:choose>
                    <xsl:when test="$type = 'int'">
                        <input type="text" umi:type="{$type}" name="{$input-name}[{$position}][{$type}]" value="{@int}" />
                        <input type="hidden" umi:type="float" name="{$input-name}[{$position}][float]" value="0" />
                    </xsl:when>
                    <xsl:when test="$type = 'varchar'">
                        <input type="text" umi:type="{$type}" name="{$input-name}[{$position}][{$type}]" value="{@varchar}" />
                        <input type="hidden" umi:type="int" name="{$input-name}[{$position}][int]" value="1" />
                    </xsl:when>
                    <xsl:otherwise>
                        <input type="text" umi:type="float" name="{$input-name}[{$position}][float]" value="{@float}" />
                        <input type="hidden" umi:type="int" name="{$input-name}[{$position}][int]" value="1" />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td class="center narrow">
                <a href="#" class="remove">
                    <img src="/images/cms/admin/mac/table/ico_del.gif" />
                </a>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="field[@type = 'symlink' and @name = 'video']" mode="form-modify">
        <div class="field relation" id="{generate-id()}">
            <xsl:if test="not(@required = 'required')">
                <xsl:attribute name="umi:empty"><xsl:text>empty</xsl:text></xsl:attribute>
            </xsl:if>
            <label for="relationSelect{generate-id()}">
                <span class="label">
                    <acronym>
                        <xsl:apply-templates select="." mode="sys-tips" />
                        <xsl:value-of select="@title" />
                    </acronym>
                    <xsl:apply-templates select="." mode="required_text" />
                </span>
                <span>
                    <select name="{@input_name}" id="relationSelect{generate-id()}">
                        <xsl:apply-templates select="." mode="required_attr" />
                        <xsl:if test="not(values/item)">
                            <option value=""></option>
                        </xsl:if>
                        <xsl:apply-templates select="document('udata://videoalbum/getAllVideo')/udata/items/item">
                            <xsl:with-param name="val_items" select="values/item"/>
                        </xsl:apply-templates>
                    </select>
                </span>
            </label>
            <input type="text" id="relationInput{generate-id()}" class="search_input" />
        </div>
    </xsl:template>

    <xsl:template match="udata[@method = 'getAllVideo']/items/item">
        <xsl:param name="val_items"/>
        <xsl:variable name="id" select="@id"/>
        <option value="{@id}">
            <xsl:if test="$val_items[@id = $id]">
                <xsl:attribute name="selected">selected</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="."/>
        </option>
    </xsl:template>

</xsl:stylesheet>
