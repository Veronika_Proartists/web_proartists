<?php
abstract class __content_custom_admin {

	/**
	 * Кастом для sitetree, т.к. из оригинального sitetree стоит редирект, чтобы пользователь не смог зайти в /admin
	 */
	public function adminsitetree() {
		$domains = domainsCollection::getInstance()->getList();
		$permissions = permissionsCollection::getInstance();
		$user_id = $permissions->getUserId();

		$this->setDataType("list");
		$this->setActionType("view");

		foreach($domains as $i => $domain) {
			$domain_id = $domain->getId();

			if(!$permissions->isAllowedDomain($user_id, $domain_id)) {
				unset($domains[$i]);
			}
		}

		$data = $this->prepareData($domains, "domains");

		$this->setData($data, sizeof($domains));
		return $this->doData();
	}

};
?>