<?php
	class payuPayment extends payment {
		private $api;

		public function __construct(iUmiObject $object, order $order = null) {
			parent::__construct($object, $order);
			objectProxyHelper::includeClass('emarket/classes/payment/api/', 'PayU');

            $merchant = $this->object->merchant;
            $secret_key = $this->object->secret_key;

            if(!strlen($merchant) || !strlen($secret_key))
                throw new publicException(getLabel('error-payment-wrong-settings'));

			$this->api = new PayU($merchant, $merchant, $secret_key);
		}

        public function validate() {
            return true;
        }

        public function process($template = 'default') {
            if(!isset($template))
                $template = 'default';

            $order = $this->order;

            $currency = $this->object->currency;
            $language = $this->object->language;

            if(!strlen($currency) || !strlen($language))
                throw new publicException(getLabel('error-payment-wrong-settings'));

            $arItemsName = array();
            $arItemsCode = array();
            $arItemsPrice = array();
            $arItemsQuantity = array();
            $arItemsVat = array();

            foreach($this->order->getItems() as $item) {
                if($element = $item->getItemElement()) {
                    $price = $item->getTotalActualPrice();
                    $amount = $item->getAmount();

                    $arItemsName[] = $item->getName();
                    $arItemsCode[] = $element->getId();
                    $arItemsPrice[] = $price / $amount;
                    $arItemsQuantity[] = $amount;
                    $arItemsVat[] = '0';
                }
            }

            $forSend = array (
                'ORDER_REF' => $order->getId(),
                'ORDER_DATE' => date("Y-m-d H:i:s"),
                'ORDER_PNAME' => $arItemsName,
                'ORDER_PCODE' => $arItemsCode,
                'ORDER_PRICE' => $arItemsPrice,
                'ORDER_QTY' => $arItemsQuantity,
                'ORDER_VAT' => $arItemsVat,
                'ORDER_SHIPPING' => $order->getDeliveryPrice(),
                'PRICES_CURRENCY' => $currency,
            );

            $customer = umiObjectsCollection::getInstance()->getObject($this->order->customer_id);
            if($customer instanceof umiObject){
                if($customer->fname) $forSend['BILL_FNAME'] = $customer->fname;
                if($customer->lname) $forSend['BILL_LNAME'] = $customer->lname;
                if($customer->email) $forSend['BILL_EMAIL'] = $customer->email;
                if($customer->getValue('e-mail')) $forSend['BILL_EMAIL'] = $customer->getValue('e-mail');
                if($customer->phone) $forSend['BILL_PHONE'] = $customer->phone;
                $forSend['BILL_COUNTRYCOD'] = 'RU';
            }
            if($this->object->test_order){
                $this->api->debug = 1;
                $forSend['TESTORDER'] = 'TRUE';
            }

            $forSend['LANGUAGE'] = $language;

            $formData = $this->api->initLiveUpdateFormData($forSend,'http://proartists.ru/emarket/checkPayU');
            $params['form'] = $this->api->generateUmiForm($formData);

            $order->order();
            $this->order->setPaymentStatus('initialized');

            list($form_block) = def_module::loadTemplates('emarket/payment/payu/' . $template, 'form_block');
            return def_module::parseTemplate($form_block, $params);
        }

        public static function getOrderId() {
            return (int) getRequest('REFNOEXT');
        }

        public function poll() {
            $buffer = outputBuffer::current();
            $buffer->clear();
            $buffer->contentType('text/plain');

            $result = $this->api->handleIpnRequest();
            $paymentStatusSuccess = array('PAYMENT_AUTHORIZED','COMPLETE','TEST');
            $paymentStatusFiled = array('REVERSED','REFUND');

            if (in_array(getRequest('ORDERSTATUS'), $paymentStatusSuccess)) {
                $this->order->setPaymentStatus('accepted');
            } elseif (in_array(getRequest('ORDERSTATUS'), $paymentStatusFiled)) {
                $this->order->setPaymentStatus('declined');
            } else {
                $error = 'Invalid Order Status';
                $emarket = cmsController::getInstance()->getModule('emarket');
                $emarket->errorNewMessage($error);
                $emarket->errorPanic();
            }

            $buffer->push($result);
            $buffer->end();
        }
	}
?>