<?php
abstract class __org extends baseModuleAdmin {


    public function editInfo() {
        $regedit = regedit::getInstance();
        $id = $regedit->getVal("//modules/org/info_id");
        $object = umiObjectsCollection::getInstance()->getObject($id);
        $mode = (string) getRequest('param0');

        if($mode == "do") {
            $this->saveEditedObjectData($object);
            $this->chooseRedirect();
        }

        $this->setDataType("form");
        $this->setActionType("modify");

        $data = $this->prepareData($object, "object");

        $this->setData($data);
        return $this->doData();
    }

};
?>