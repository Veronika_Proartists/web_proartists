<?php

$INFO = Array();

$INFO['name'] = "org";
$INFO['filename'] = "modules/org/class.php";
$INFO['config'] = "1";
$INFO['ico'] = "ico_org";
$INFO['default_method'] = "view";
$INFO['default_method_admin'] = "editInfo";
$INFO['is_indexed'] = "1";
$INFO['info_id'] = 0;

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/org/__admin.php";
$COMPONENTS[1] = "./classes/modules/org/class.php";
$COMPONENTS[2] = "./classes/modules/org/i18n.php";
$COMPONENTS[3] = "./classes/modules/org/lang.php";
$COMPONENTS[4] = "./classes/modules/org/permissions.php";


?>