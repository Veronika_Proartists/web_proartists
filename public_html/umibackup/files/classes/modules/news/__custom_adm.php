<?php
	abstract class __custom_adm_news {
		//TODO: Write here your own macroses (admin mode) р
		
		public function onAddNewsItem(iUmiEventPoint $event) {
			if ($event->getMode() === "after") {
				if($oElement = $event->getRef('element')) {
					$oDate = $oElement->getValue('publish_time');
					if (!$oDate) {
						$oElement->setValue('publish_time', $_SERVER['REQUEST_TIME']);
						$oElement->commit();
					}
				}
			}
			return true;
		}
		
		public function onAddNewsItemQuick(iUmiEventPoint $event) {
			if ($event->getMode() === "after") {
				$iElementId = $event->getParam('elementId');
				if($oElement = umiHierarchy::getInstance()->getElement($iElementId)) {
					$oDate = $oElement->getValue('publish_time');
					if (!$oDate) {
						$oElement->setValue('publish_time', $_SERVER['REQUEST_TIME']);
						$oElement->commit();
					}
				}
			}
			return true;
		}
	};
?>