<?php
	abstract class __custom_catalog {
		
		
	/**
	* Список всех категорий, в т.ч. неактивных
	*/
	public function getCategoryListFull($template = "default", $category_id = false, $limit = false, $ignore_paging = false, $i_need_deep = 0, $bWithInactive = false) {
		if(!$template) $template = "default";
		list($template_block, $template_block_empty, $template_line) = def_module::loadTemplates("catalog/".$template, "category_block", "category_block_empty", "category_block_line");

		if (!$i_need_deep) $i_need_deep = intval(getRequest('param4'));
		if (!$i_need_deep) $i_need_deep = 0;
		$i_need_deep = intval($i_need_deep);
		if ($i_need_deep === -1) $i_need_deep = 100;

		if((string) $category_id != '0') $category_id = $this->analyzeRequiredPath($category_id);

		$social_module = cmsController::getInstance()->getModule("social_networks");
		if($social_module) {
			$social = $social_module->getCurrentSocial();
		}
		else {
			$social = false;
		}

		if ($category_id === false) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $category_id));
		}

		$hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "category")->getId();

		$per_page = ($limit) ? $limit : $this->per_page;

		$curr_page = (int) getRequest('p');
		if($ignore_paging) $curr_page = 0;

		$sel1 = new umiSelection;
		$sel1->addElementType($hierarchy_type_id);
		$sel1->addHierarchyFilter($category_id, $i_need_deep);
		$sel1->addActiveFilter(true);
		$sel1->addLimit($per_page, $curr_page);

		$result1 = umiSelectionsParser::runSelection($sel1);
		$total1 = umiSelectionsParser::runSelectionCounts($sel1);
		
		$result2 = array();
		$total2 = 0;
		if ($bWithInactive) {
			$sel2 = new umiSelection;
			$sel2->addElementType($hierarchy_type_id);
			$sel2->addHierarchyFilter($category_id, $i_need_deep);
			$sel2->addActiveFilter(false);
			$sel2->addLimit($per_page, $curr_page);

			$result2 = umiSelectionsParser::runSelection($sel2);
			$total2 = umiSelectionsParser::runSelectionCounts($sel2);
		}
		
		$result = array_merge($result1, $result2);
		$total = $total1 + $total2;

		if(($sz = sizeof($result)) > 0) {
			$block_arr = array();

			$lines = array();
			for($i = 0; $i < $sz; $i++) {
				if ($i < $limit || $limit == false) {
					$element_id = $result[$i];
					if($social && !$social->isHierarchyAllowed($result[$i])) {
						continue;
					}
					$element = umiHierarchy::getInstance()->getElement($element_id);

					if(!$element) continue;

					$line_arr = Array();
					$line_arr['attribute:id'] = $element_id;
					$line_arr['attribute:is-active'] = $element->getIsActive();
					$line_arr['void:alt_name'] = $element->getAltName();
					$line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element_id);
					$line_arr['xlink:href'] = "upage://" . $element_id;
					$line_arr['node:text'] = $element->getName();

					$lines[] = def_module::parseTemplate($template_line, $line_arr, $element_id);
				}
			}

			$block_arr['attribute:category-id'] = $block_arr['void:category_id'] = $category_id;
			$block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			return def_module::parseTemplate($template_block, $block_arr, $category_id);
		} else {
			$block_arr = array();
			$block_arr['attribute:category-id'] = $block_arr['void:category_id'] = $category_id;
			return def_module::parseTemplate($template_block_empty, $block_arr, $category_id);
		}
	}


	/**
	* Список всех объектов каталога, в т.ч. и неактивных
	*/
	public function getObjectsListFull($template = "default", $path = false, $limit = false, $ignore_paging = false, $i_need_deep = 0, $bWithInactive = false) {
		if(!$template) $template = "default";

		if (!$i_need_deep) $i_need_deep = intval(getRequest('param4'));
		if (!$i_need_deep) $i_need_deep = 0;
		$i_need_deep = intval($i_need_deep);
		if ($i_need_deep === -1) $i_need_deep = 100;
		
		$hierarchy = umiHierarchy::getInstance();

		list($template_block, $template_block_empty, $template_block_search_empty, $template_line) = def_module::loadTemplates("catalog/".$template, "objects_block", "objects_block_empty", "objects_block_search_empty", "objects_block_line");

		$hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("catalog", "object")->getId();

		$category_id = $this->analyzeRequiredPath($path);
		
		if($category_id === false && $path != KEYWORD_GRAB_ALL) {
			throw new publicException(getLabel('error-page-does-not-exist', null, $path));
		}

		$category_element = $hierarchy->getElement($category_id);

		$per_page = ($limit) ? $limit : $this->per_page;
		$curr_page = getRequest('p');
		if($ignore_paging) $curr_page = 0;

		$hierarchy_type = umiHierarchyTypesCollection::getInstance()->getType($hierarchy_type_id);
		$type_id = umiObjectTypesCollection::getInstance()->getBaseType($hierarchy_type->getName(), $hierarchy_type->getExt());
		
		if($path === KEYWORD_GRAB_ALL) {
			$curr_category_id = cmsController::getInstance()->getCurrentElementId();
		} else {
			$curr_category_id = $category_id;
		}
		
		
		if($path != KEYWORD_GRAB_ALL) {
			$type_id = $hierarchy->getDominantTypeId($curr_category_id, $i_need_deep, $hierarchy_type_id);
		}
		
		if(!$type_id) {
			$type_id = umiObjectTypesCollection::getInstance()->getBaseType($hierarchy_type->getName(), $hierarchy_type->getExt());
		}

		$sel1 = new umiSelection;
		$sel1->setElementTypeFilter();
		$sel1->addElementType($hierarchy_type_id);
		$sel1->addActiveFilter(true);
		
		if($path != KEYWORD_GRAB_ALL) {
			$sel1->setHierarchyFilter();
			$sel1->addHierarchyFilter($category_id, $i_need_deep);
		}
		
		$sel1->setPermissionsFilter();
		$sel1->addPermissions();

		if($type_id) { 
			$this->autoDetectOrders($sel1, $type_id);
			$this->autoDetectFilters($sel1, $type_id);
			
			if($this->isSelectionFiltered) {
				$template_block_empty = $template_block_search_empty;
				$this->isSelectionFiltered = false;
			}
		} else {
			$sel1->setOrderFilter();
			$sel1->setOrderByName();
		}

		if($curr_page !== "all") {
			$curr_page = (int) $curr_page;
			$sel1->setLimitFilter();
			$sel1->addLimit($per_page, $curr_page);
		}

		$result1 = umiSelectionsParser::runSelection($sel1);
		$total1 = umiSelectionsParser::runSelectionCounts($sel1);
		
		/**************************************/
		
		$result2 = array();
		$total2 = 0;
		if ($bWithInactive) {
			$sel2 = new umiSelection;
			$sel2->setElementTypeFilter();
			$sel2->addElementType($hierarchy_type_id);
			$sel2->addActiveFilter(false);
			
			if($path != KEYWORD_GRAB_ALL) {
				$sel2->setHierarchyFilter();
				$sel2->addHierarchyFilter($category_id, $i_need_deep);
			}
			
			$sel2->setPermissionsFilter();
			$sel2->addPermissions();

			if($type_id) { 
				$this->autoDetectOrders($sel2, $type_id);
				$this->autoDetectFilters($sel2, $type_id);
				
				if($this->isSelectionFiltered) {
					$template_block_empty = $template_block_search_empty;
					$this->isSelectionFiltered = false;
				}
			} else {
				$sel2->setOrderFilter();
				$sel2->setOrderByName();
			}

			if($curr_page !== "all") {
				$curr_page = (int) $curr_page;
				$sel2->setLimitFilter();
				$sel2->addLimit($per_page, $curr_page);
			}

			$result2 = umiSelectionsParser::runSelection($sel2);
			$total2 = umiSelectionsParser::runSelectionCounts($sel2);
		}
		
		/***********************************/
		
		$result = array_merge($result1, $result2);
		$total = $total1 + $total2;
		
		if(($sz = sizeof($result)) > 0) {
			$block_arr = Array();

			$lines = Array();
			for($i = 0; $i < $sz; $i++) {
				$element_id = $result[$i];
				$element = umiHierarchy::getInstance()->getElement($element_id);

				if(!$element) continue;

				$line_arr = Array();
				$line_arr['attribute:id'] = $element_id;
				$line_arr['attribute:is-active'] = $element->getIsActive();
				$line_arr['attribute:alt_name'] = $element->getAltName();
				$line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element_id);
				$line_arr['xlink:href'] = "upage://" . $element_id;
				$line_arr['node:text'] = $element->getName();

				$lines[] = def_module::parseTemplate($template_line, $line_arr, $element_id);

				$this->pushEditable("catalog", "object", $element_id);
				umiHierarchy::getInstance()->unloadElement($element_id);
			}

			$block_arr['subnodes:lines'] = $lines;
			$block_arr['numpages'] = umiPagenum::generateNumPage($total, $per_page);
			$block_arr['total'] = $total;
			$block_arr['per_page'] = $per_page;
			$block_arr['category_id'] = $category_id;
			
			if($type_id) {
				$block_arr['type_id'] = $type_id;
			}

			return def_module::parseTemplate($template_block, $block_arr, $category_id);
		} else {
			$block_arr['numpages'] = umiPagenum::generateNumPage(0, 0);
			$block_arr['lines'] = "";
			$block_arr['total'] = 0;
			$block_arr['per_page'] = 0;
			$block_arr['category_id'] = $category_id;
			
			return def_module::parseTemplate($template_block_empty, $block_arr, $category_id);
		}

	}


	/**
	* Кастомный поиск
	*/
	public function customSearch($category_id = false, $group_names = "", $template = "default", $type_id = false) {
		if(!$template) $template = "default";

		if($type_id === false) {
			$category_id = $this->analyzeRequiredPath($category_id);
			if(!$category_id) return "";
		}
		
		
		list($template_block, $template_block_empty, $template_block_line, $template_block_line_text, 
			$template_block_line_relation, $template_block_line_item_relation, $template_block_line_item_relation_separator, 
			$template_block_line_price, $template_block_line_boolean, $template_block_line_symlink) =
			
			def_module::loadTemplates("catalog/".$template, "search_block", "search_block_empty",
			"search_block_line", "search_block_line_text", "search_block_line_relation", 
			"search_block_line_item_relation", "search_block_line_item_relation_separator",
			"search_block_line_price", "search_block_line_boolean", "search_block_line_symlink");

		$block_arr = Array();
		
		if($type_id === false) {
			$type_id = umiHierarchy::getInstance()->getDominantTypeId($category_id);
		}

		if(is_null($type_id)) return "";

		if(!($type = umiObjectTypesCollection::getInstance()->getType($type_id))) {
			trigger_error("Failed to load type", E_USER_WARNING);
			return "";
		}

		$fields = Array();
		$groups = Array();
		$lines = Array();

		$group_names = trim($group_names); 
		if($group_names) {
			$group_names_arr = explode(" ", $group_names); 
    			foreach($group_names_arr as $group_name) { 
				if(!($fields_group = $type->getFieldsGroupByName($group_name))) {
				} else {
					$groups[] = $fields_group;
				}
			}
		} else {
			$groups = $type->getFieldsGroupsList();
		}


		$lines_all = Array();
		$groups_arr = Array();

		foreach($groups as $fields_group) {
			$fields = $fields_group->getFields();
			

			$group_block = Array();
			$group_block['attribute:name'] = $fields_group->getName();
			$group_block['attribute:title'] = $fields_group->getTitle();

			$lines = Array();

			foreach($fields as $field_id => $field) {
				if(!$field->getIsVisible()) continue;
				if(!$field->getIsInFilter()) continue;

				$line_arr = Array();

				$field_type_id = $field->getFieldTypeId();
				$field_type = umiFieldTypesCollection::getInstance()->getFieldType($field_type_id);

				$data_type = $field_type->getDataType();

				$line = Array();
				switch($data_type) {
					case "relation": {
						$line = $this->custom_parseSearchRelation($field, $template_block_line_relation, $template_block_line_item_relation, $template_block_line_item_relation_separator, $category_id, $type_id);
						break;
					}

					case "text": {
						$line = $this->parseSearchText($field, $template_block_line_text);
						break;
					}
				
					case "date": {
						$line = $this->parseSearchDate($field, $template_block_line_text);
						break;
					}

					case "string": {
						$line = $this->parseSearchText($field, $template_block_line_text);
						break;
					}

					case "wysiwyg": {
						$line = $this->parseSearchText($field, $template_block_line_text);
						break;
					}
				
					case "float": 
					case "price": {
			    			$line = $this->parseSearchPrice($field, $template_block_line_price);
						break;
					}
				
					case "int": { 
						$line = $this->parseSearchInt($field, $template_block_line_text);
						break;
					}

					case "boolean": {
						$line = $this->parseSearchBoolean($field, $template_block_line_boolean);
						break;
					}

					case "symlink": {
						$line = $this->parseSearchSymlink($field, $template_block_line_symlink, $category_id);
						break;
					}

					default: {
						$line = "[search filter for \"{$data_type}\" not specified]";
						break;
					}
				}

				if (def_module::isXSLTResultMode()) {
					if (is_array($line)) {
						$line['attribute:data-type'] = $data_type;
					}
				}

				$line_arr['void:selector'] = $line;

				if (def_module::isXSLTResultMode()) {
					$lines[] = $line;
				} else {
					$lines[] = $tmp = def_module::parseTemplate($template_block_line, $line_arr);
					$lines_all[] = $tmp;
				}
			}
			
			if(empty($lines)) {
				continue;
			}
			
			$group_block['nodes:field'] = $lines;
			$groups_arr[] = $group_block;

		}

		$block_arr['void:items'] = $block_arr['void:lines'] = $lines_all;
		$block_arr['nodes:group'] = $groups_arr;
		$block_arr['attribute:category_id'] = $category_id;
		
		if(!$groups_arr && !$lines && !$this->isXSLTResultMode()) {
			return $template_block_empty;
		}

		return def_module::parseTemplate($template_block, $block_arr);
	}
	
	/**
	* Подборка вариантов в зависимости от наличия товара (есть активные страницы)
	*/
	public function custom_parseSearchRelation(umiField $field, $template, $template_item, $template_separator, $category_id, $type_id) { 
		return $this->getDataModule()->custom_parseSearchRelation($field, $template, $template_item, $template_separator, $category_id, $type_id);
	}

	/**
	* Получение название каталога по умолчанию при импорте
	*/
	public function getCatalogDefaultName() {
		$contentPageTypeId = 10;	//Это по умолчанию в ЮМИ и менять не следует!
		$arChildTypes = umiObjectTypesCollection::getInstance()->getChildClasses($contentPageTypeId);
		$infoPageType = $arChildTypes[0];

		$sel = new umiSelection();
		$sel->addObjectType($infoPageType);
		$sel->addLimit(1, 0);
		$result = umiSelectionsParser::runSelection($sel);
		
		$object = umiObjectsCollection::getInstance()->getObject($result[0]);
		
		$name = $object->getValue('catalog_import_default_name');
		$block_arr['name'] = $name;
		return def_module::parseTemplate(array(), $block_arr);
	}
	
	
	/**
	 * Является ли страница категорией и преобладают ли в ней товары, а не категории
	 * @param mixed $pageId
	 * @return mixed|string
	 */
	public function isCategory($pageId = 0) {
		$result = 0;
		
		$oElement = umiHierarchy::getInstance()->getElement($pageId);
		if ($oElement) {
			$sModule = $oElement->getModule();
			$sMethod = $oElement->getMethod();
			if ($sModule == 'catalog' && $sMethod == 'category') {
				$iDominantObjectType = umiHierarchy::getInstance()->getDominantTypeId($pageId);
				$iCategoryTypeId = umiObjectTypesCollection::getInstance()->getBaseType('catalog', 'category');
				if($iDominantObjectType != $iCategoryTypeId) {
					$result = 1;
				}
			}
		}
		
		return def_module::parseTemplate(array(), array('iscategory' => $result));
	}
	
	/*********** Some event handlers (thank you, edit-in-place)   *************/
	public function unsetCache($iCategoryId) {
		$sCacheFile = CURRENT_WORKING_DIR . '/sys-temp/runtime-cache/customCache';
		
		if (file_exists($sCacheFile)) {
			$arCache = file_get_contents($sCacheFile);
			$arCache = unserialize($arCache);
			if (is_array($arCache)) {
				if (isset($arCache['smartCatalogFilter'][$iCategoryId])) {
					unset($arCache['smartCatalogFilter'][$iCategoryId]);
					
					if(file_put_contents($sCacheFile, serialize($arCache))) {
						clearstatcache();
						@chmod($sCacheFile, 0777);
					}
				}
			}
		}
	}
	
	public function onDeleteCatalogObject(iUmiEventPoint $event) {
		if ($event->getMode() === "after") {
			if($oElement = $event->getRef('element')) {
				if ($oElement->getModule() == 'catalog') {
					$iParentId = $oElement->getParentId();
					
					$this->unsetCache($iParentId);

					if(method_exists('regedit', 'onController') && regedit::onController()) {
						umiHierarchy::getInstance()->removeDeletedElement($oElement->getId());
					}
				}
			}
		}
		
		return true;
	}
	
	public function onSaveCatalogObject(iUmiEventPoint $event) {
		if ($event->getMode() === "after") {

			$oElement = $event->getParam('obj');
			if($oElement instanceof umiHierarchyElement) {
				$iParentId = $oElement->getParentId();
				$this->unsetCache($iParentId);
			}
		}

		return true;
	}
	
	public function onSwitchActivityCatalogObjectUmi(iUmiEventPoint $event) {
		if ($event->getMode() === "after") {
			if($oElement = $event->getRef('element')) {
				$iParentId = $oElement->getParentId();
				
				$this->unsetCache($iParentId);
			}
		}
		
		return true;
	}
	
	public function onSwitchActivityCatalogObject(iUmiEventPoint $event) {
		if ($event->getMode() === "after") {
			if($oElement = $event->getRef('element')) {
				$iParentId = $oElement->getParentId();
				
				$this->unsetCache($iParentId);
			}
		}
		
		return true;
	}

	public function onAddCatalogObject(iUmiEventPoint $event) {
		if ($event->getMode() === "after") {
			if($oElement = $event->getRef('element')) {
				$iParentId = $oElement->getParentId();
				
				$this->unsetCache($iParentId);
			}
		}

		return true;
	}

	public function onAddCatalogObjectQuick(iUmiEventPoint $event) {
		if ($event->getMode() === "after") {
			$iElementId = $event->getParam('elementId');
			if($oElement = umiHierarchy::getInstance()->getElement($iElementId)) {
				$iParentId = $oElement->getParentId();
				
				$this->unsetCache($iParentId);
			}
		}

		return true;
	}

	public function onMoveCatalogObject(iUmiEventPoint $event) {
		if ($event->getMode() === "before") {
			$iElementId = $event->getParam('elementId');
			if($oElement = umiHierarchy::getInstance()->getElement($iElementId)) {
				$iParentId = $oElement->getParentId();
				
				$this->unsetCache($iParentId);
			}
		} elseif ($event->getMode() === "after") {
			$iElementId = $event->getParam('elementId');
			if($oElement = umiHierarchy::getInstance()->getElement($iElementId)) {
				$iParentId = $oElement->getParentId();
				
				$this->unsetCache($iParentId);
			}
		}

		return true;
	}

	public function onModifyCatalogObject(iUmiEventPoint $event) {
		if($oElement = $event->getRef('element')) {
			$iParentId = $oElement->getParentId();
			
			$this->unsetCache($iParentId);
		}
		
		return true;
	}

	public function getSpecialOffers() {
		
		$limit = getRequest('limit');
		
		$sel = new selector('pages');
		$sel->types('hierarchy-type')->name('catalog', 'object');
		$sel->where('special_offer')->equals('1');
		
		$res = $sel->result;
		$total = sizeof($res);
		
		$block_arr = Array();
		$block_arr['total'] = $total;
		
		if($total > 0) {
			$pages = Array();
			for($i = 0; $i < $total; $i++) {
				
				$element = $res[$i];
				
				if(!$element) continue;
				
				$element_id = $element->getId();
				
				$page = Array();
				$page['attribute:id'] = $element_id;
				$page['attribute:is-active'] = $element->getIsActive();
				$page['attribute:alt_name'] = $element->getAltName();
				$page['attribute:link'] = umiHierarchy::getInstance()->getPathById($element_id);
				$page['xlink:href'] = "upage://" . $element_id;
				$page['name'] = $element->getName();
				
				$pages[] = $page;
				
				umiHierarchy::getInstance()->unloadElement($element_id);
			}
			
			$block_arr['nodes:page'] = $pages;
		}
		return def_module::parseTemplate(array(), $block_arr);
	}

        public function colorField($id,$mode){
            $data = getRequest('data');
           /* foreach($data as $id=>$arr1){
                foreach($arr1 as $fieldName=>$arr2){
                    foreach($arr2 as $pos=>$arr3){
                        break 3;
                        foreach($arr3 as $var_name=>$val){

                        }
                    }
                }
            }*/
            $objects = umiObjectsCollection::getInstance();
            $object = $objects->getObject($id);
            $color = $object->getValue('color');
//                                           data[20244][color][345][rel]
//            return  array('int'=>getRequest('data['.$id.'][color]['.$pos.'][int]'),'rel'=>getRequest('data['.$id.'][color]['.$pos.'][rel]'),'pos'=>$pos,'id'=>$id,$data);
            $items = array();
            foreach($color as $pos=>$item){
                $items[(int)$item['rel']] = $pos;
            }
            $log = array();
            foreach ($data as $id => $arr1) {
                foreach ($arr1 as $fieldName => $arr2) {
                    foreach ($arr2 as $pos => $arr3) {
                        $item = array();
                        $item['@id'] = $id;
                        $item['@fieldName'] = $fieldName;
                        $item['@pos'] = $pos;
                        $item['val'] = $arr3;
                        $log[] = $item;
                        switch ($mode) {
                            case 'add': {
                                $color[] = $arr3;
                                //                    $color[] = array('int'=>getRequest('data['.$id.'][color]['.$pos.'][int]'),'rel'=>getRequest('data['.$id.'][color]['.$pos.'][rel]'));
                            }
                                break;
                            case 'del': {
                                if(isset($items[$arr3['rel']])) unset($color[$items[$arr3['rel']]]);
//                                unset($color[$pos - 1]);

                            }
                                break;
                            case 'edit': {
                                if(isset($items[$arr3['rel']])) $color[$items[$arr3['rel']]] = $arr3;
//                                $color[$pos - 1] = $arr3;
                                //                    $color[$pos-1] = array('int'=>getRequest('data['.$id.'][color]['.$pos.'][int]'),'rel'=>getRequest('data['.$id.'][color]['.$pos.'][rel]'));
                            }
                                break;
                        }
                    }
                }
            }
            $object->setValue('color', $color);
            $object->commit();
            return array('+item'=>$color);
        }


};

?>
