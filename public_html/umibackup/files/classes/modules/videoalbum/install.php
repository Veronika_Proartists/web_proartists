<?php

$INFO = Array();

$INFO['name'] = "videoalbum";
$INFO['filename'] = "modules/videoalbum/class.php";
$INFO['config'] = "1";
$INFO['ico'] = "ico_videoalbum";
$INFO['default_method'] = "show";
$INFO['default_method_admin'] = "lists";
$INFO['is_indexed'] = "1";
$INFO['per_page'] = "10";

$INFO['func_perms'] = "";

$COMPONENTS = array();

$COMPONENTS[0] = "./classes/modules/videoalbum/__admin.php";
$COMPONENTS[1] = "./classes/modules/videoalbum/class.php";
$COMPONENTS[2] = "./classes/modules/videoalbum/i18n.php";
$COMPONENTS[3] = "./classes/modules/videoalbum/lang.php";
$COMPONENTS[4] = "./classes/modules/videoalbum/permissions.php";


?>