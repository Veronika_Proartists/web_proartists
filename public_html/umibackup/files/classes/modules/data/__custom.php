<?php
	abstract class __custom_data {
        /**
        * @desc Format the number in desired way
        * @param Number  $_Number    Number to format
        * @param Integer $_Decimals	 Number of decimal signs
        * @param Char    $_DecPoint  Delimiter of the decimals
        * @param Char    $_Separator Separator between thousand groups
        * @return String Formatted number	
        */
        function numberformat($_Number, $_Decimals = 2, $_DecPoint = '.', $_Separator = ' ') {
            die( strval($_Number) );
            return number_format($_Number, $_Decimals, $_DecPoint, $_Separator);
        }

		public function custom_parseSearchRelation(umiField $field, $template, $template_item, $template_separator, $category_id, $type_id) {
			$block_arr = Array();

			$name = $field->getName();
			$title = $field->getTitle();

			$guide_id = $field->getGuideId();
			$guide_items = $this->getSmartCatalogGuidedItems($guide_id, $field, $category_id, $type_id);

			$fields_filter = getRequest('fields_filter');
			$value = getArrayKey($fields_filter, $name);

			$items = Array();
			$i = 0;
			$sz = sizeof($guide_items);

			$is_tpl = !def_module::isXSLTResultMode();
			if (!$is_tpl) $template_item = true;

			$unfilter_link = "";

			foreach($guide_items as $object_id => $object_name) {
				if(is_array($value)) {
					$selected = (in_array($object_id, $value)) ? "selected" : "";
				} else {
					$selected = ($object_id == $value) ? "selected" : "";
				}

				if($template_item) {
					$line_arr = Array();
					$line_arr['attribute:id'] = $line_arr['void:object_id'] = $object_id;
					$line_arr['node:object_name'] = $object_name;

					$params = $_GET;
					unset($params['path']);
					unset($params['p']);
					$params['fields_filter'][$name] = $object_id;
					$filter_link = "?" . http_build_query($params, '', '&amp;');

					unset($params['fields_filter'][$name]);
					$unfilter_link = "?" . http_build_query($params, '', '&amp;');

					$line_arr['attribute:filter_link'] = $filter_link;
					$line_arr['attribute:unfilter_link'] = $unfilter_link;

					if($selected) {
						$line_arr['attribute:selected'] = "selected";
					}

					$items[] = def_module::parseTemplate($template_item, $line_arr);

					if(++$i < $sz) {
						if ($is_tpl) {
							$items[] = $template_separator;
						}
					}
				} else {
					$items[] = "<option value=\"{$object_id}\" {$selected}>{$object_name}</option>";
				}
			}

			$block_arr['attribute:unfilter_link'] = $unfilter_link;
			$block_arr['attribute:name'] = $name;
			$block_arr['attribute:title'] = $title;
			$block_arr['subnodes:values'] = $block_arr['void:items'] = $items;
			$block_arr['void:selected'] = ($value) ? "" : "selected";
			return def_module::parseTemplate($template, $block_arr);
		}

		/**
		* Получение только тех элементов, для которых есть товары в каталоге
		* Структура кеша - это сериализованный массив вида:
		array(
			[id категории] => array(
				[get-запрос] => array(
					array(
						[timestamp] => 000000000,
						[items] => array(
							[guide id] => array(guide_item_id_1, guide_item_id_2, ...),
							...
						)
					)
				),
				...
			),
			...
		)
		*/
		public function getSmartCatalogGuidedItems($guide_id, umiField $field, $category_id, $type_id) {
			$sCacheFile = CURRENT_WORKING_DIR . '/sys-temp/runtime-cache/customCache';

			$sCurrentFiltersSet = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?'));
			$sCurrentFiltersSet = ($sCurrentFiltersSet == "") ? "?" : $sCurrentFiltersSet;

			if (file_exists($sCacheFile)) {
				$arCache = file_get_contents($sCacheFile);
				$arCache = unserialize($arCache);
				if (is_array($arCache)) {
					$iCacheLifeTime = 60 * 60 * 24; //24 часа
					if (isset($arCache['smartCatalogFilter'])
						&& isset($arCache['smartCatalogFilter'][$category_id])
						&& isset($arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet])
						&& isset($arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['items'])
						&& isset($arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['items'][$guide_id])) {
						//Проверка на время
						if (isset($arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['timestamp']) && ($arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['timestamp'] > ($_SERVER['REQUEST_TIME'] - $iCacheLifeTime))) {
							return $arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['items'][$guide_id];
						}
					}
				}
			}

			$result = $this->setSmartCatalogGuidedItems($guide_id, $field, $category_id, $type_id);

			return $result;
		}

		/**
		* Записать в кеш умный справочник для фильтров
		*/
		public function setSmartCatalogGuidedItems($guide_id, umiField $field, $category_id, $type_id) {
			$sCacheFile = CURRENT_WORKING_DIR . '/sys-temp/runtime-cache/customCache';
			$oHierarchyType = umiHierarchyTypesCollection::getInstance()->getTypeByName('catalog', 'object');
			$iHierarchyTypeId = $oHierarchyType->getId();

			$arCache = array();
			if (file_exists($sCacheFile)) {
				$arCache = file_get_contents($sCacheFile);
				$arCache = unserialize($arCache);
			}

			$sCurrentFiltersSet = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?'));
			$sCurrentFiltersSet = ($sCurrentFiltersSet == "") ? "?" : $sCurrentFiltersSet;

			if (!isset($arCache['smartCatalogFilter'])) $arCache['smartCatalogFilter'] = array();
			if (!isset($arCache['smartCatalogFilter'][$category_id])) $arCache['smartCatalogFilter'][$category_id] = array();
			if (!isset($arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet])) $arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet] = array();
			if (!isset($arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['items'])) $arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['items'] = array();

			$arSmartGuideItems = array();

			$arGuideValues = umiObjectsCollection::getInstance()->getGuidedItems($guide_id);

			foreach($arGuideValues as $iItemId => $sItemName) {
				$sel = new umiSelection;
				$sel->addHierarchyFilter($category_id);
				$sel->addElementType($iHierarchyTypeId);
				$sel->addObjectType($type_id);

				$sel->addPropertyFilterEqual($field->getId(), $iItemId);
				$this->autoDetectFilters($sel, $type_id);

				$arResult = umiSelectionsParser::runSelection($sel);

				if(count($arResult) > 0) {
					$arSmartGuideItems[$iItemId] = $sItemName;
				}
			}

			$arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['items'][$guide_id] = $arSmartGuideItems;
			$arCache['smartCatalogFilter'][$category_id][$sCurrentFiltersSet]['timestamp'] = $_SERVER['REQUEST_TIME'];

			if(file_put_contents($sCacheFile, serialize($arCache))) {
				clearstatcache();
				@chmod($sCacheFile, 0777);
			}

			return $arSmartGuideItems;
		}

		/**
		* Загрузка частичного списка файлов - для динамической подгрузки средствами ajax
		*/
		public function getfilelistPartial() {
			$this->flushAsXml('getfilelistPartial');
			$this->setupCwd();

			$param = array(
						array('delete', 'unlink', 1),
						array('copy',   'copy', 2),
						array('move',   'rename', 2)
						);


			for($i=0; $i<count($param); $i++) {
				if( $param!= 'copy' &&  is_demo()) {
					continue; // disable in demo
				}

				if(isset($_REQUEST[$param[$i][0]]) && !empty($_REQUEST[$param[$i][0]])) {
					foreach($_REQUEST[$param[$i][0]] as $item) {
						$item = CURRENT_WORKING_DIR . base64_decode($item);
						$arguments = array($item);
						if($param[$i][2] > 1) {
							$arguments[] = $this->cwd . '/' . basename($item);
						}
						@call_user_func_array($param[$i][1], $arguments);
					}
				}
			}

			$imageExt = array("jpg", "jpeg", "gif", "png");
			$sizeMeasure = array("b", "Kb", "Mb", "Gb", "Tb");
			$allowedExt = true;
			if(isset($_REQUEST['showOnlyImages'])) {
				$allowedExt = $imageExt;
			} else if(isset($_REQUEST['showOnlyVideos'])) {
				$allowedExt = array("flv", "mp4");
			} else if(isset($_REQUEST['showOnlyMedia'])) {
				$allowedExt = array("swf","flv","dcr","mov","qt","mpg","mp3","mp4","mpeg","avi","wmv","wm","asf","asx","wmx","wvx","rm","ra","ram");
			}

			$directory = new DirectoryIterator($this->cwd);

			$cwd = substr($this->cwd, strlen(CURRENT_WORKING_DIR));

			$warning = false;
			$filesData = array();
			foreach($directory as $file) {
				if($file->isDir()) continue;
				if($file->isDot()) continue;
				$name = $file->getFilename();
				$ext = strtolower( substr($name, strrpos($name, ".")+1) );
				if($allowedExt !== true && !in_array($ext, $allowedExt)) continue;

				$ts   = $file->getCTime();
				$time = date('G:i, d.m.Y' , $ts );
				$size = $file->getSize();

				$img = $file;

				$sCharset = detectCharset($name);
				if (function_exists('iconv') && $sCharset !== 'UTF-8') {
					$warning = 'Error: Присутствуют файлы с недопустимыми названиями! Ошибка: http://errors.umi-cms.ru/13050/';
					continue;
				}

				if(!empty($ext)) {
					$sCharset = detectCharset($ext);
					if (function_exists('iconv') && $sCharset !== 'UTF-8') {
						continue;
						$textConverted = @iconv('windows-1251', 'UTF-8', $ext);
						if ($textConverted) $ext = $textConverted;
					}
				}

				$file = array('attribute:name' => $name,
							  'attribute:type' => $ext,
							  'attribute:size' => $size,
							  'attribute:ctime'     => $time,
							  'attribute:timestamp' => $ts);

				$i = 0;
				while($size > 1024.0) {
					$size /= 1024;
					$i++;
				}
				$convertedSize = (int)round($size);
				if($convertedSize == 1 && (int)floor($size) != $convertedSize) {
					$i++;
				}
				$file['attribute:converted-size'] = $convertedSize.$sizeMeasure[$i];
				if(in_array($ext, $imageExt) && $info = @getimagesize($img->getPath() . "/" . $img->getFilename())) {
					$file['attribute:mime']   = $info['mime'];
					$file['attribute:width']  = $info[0];
					$file['attribute:height'] = $info[1];
				}
				$filesData[] = $file;

			}

			$arResult = array(
				'attribute:folder'	=> $cwd,
				'data'				=> array(
				'list'				=> array(
					'files' => array('nodes:file' => $filesData)
				))
			);

			if($warning!='') {
				$arResult['data']['warning'] = $warning;
			}


			$iLimit = isset($_REQUEST['limit']) ? intval($_REQUEST['limit']) : 0;
			$iOffset = isset($_REQUEST['offset']) ? intval($_REQUEST['offset']) : 0;

			if($iLimit > 0) {
			   $arFilesListFull = $arResult['data']['list']['files']['nodes:file'];
			   $iTotal = count($arFilesListFull);

			   $arFilesListPortion = array_slice($arFilesListFull, $iOffset, $iLimit);

			   $arResult['data']['list']['files']['nodes:file'] = $arFilesListPortion;
			   $arResult['data']['total'] = $iTotal;
			   $arResult['data']['offset'] = $iOffset;
			   $arResult['data']['loaded'] = count($arFilesListPortion);
			}

			return $arResult;
		}

		/**
		* Кастомизация нового файлового менеджера
		* @param string $needInfo - если указан как getSystemInfo, то макрос отдаёт json с необходимыми для файлового менеджера параметрами
		*/
		public function elfinder_connector_custom($needInfo = false) {
			$needInfo = (!$needInfo) ? getRequest('param0') : $needInfo;
			if($needInfo == "getSystemInfo") {
				$arData = array(
					'maxFilesCount' => ini_get("max_file_uploads") ? ini_get("max_file_uploads") : 20
				);
				return def_module::flush(json_encode($arData), "text/javascript");
			}
			$elfClasses = CURRENT_WORKING_DIR . "/styles/common/other/elfinder/php/";
			require_once $elfClasses . 'elFinderConnector.class.php';
			//require_once $elfClasses . 'elFinder.class.php';
			require_once $elfClasses . 'elFinder.umiru.class.php';
			require_once $elfClasses . 'elFinderVolumeDriver.class.php';
			require_once $elfClasses . 'elFinderVolumeLocalFileSystem.class.php';
			require_once $elfClasses . 'elFinderVolumeUmiLocalFileSystem.class.php';
			require_once $elfClasses . 'elFinderVolumeUmiruLocalFileSystem.class.php';

			// full access mode for filemanager module (?full-access=1)
			$isFullAccess = (bool) getRequest('full-access');
			function elfinder_full_access($attr, $path, $data, $volume) {
				 return strpos(basename($path), '.') === 0  ? !($attr == 'read' || $attr == 'write') : ($attr == 'read' || $attr == 'write');
			}

			function elfinder_access($attr, $path, $data, $volume) {

				if (strpos(basename($path), '.') === 0) {
					return !($attr == 'read' || $attr == 'write');
				} else {
					if (defined("CURRENT_VERSION_LINE") && CURRENT_VERSION_LINE == "demo") {
						return !($attr == 'write' || $attr == 'hidden');
					}
					return ($attr == 'read' || $attr == 'write');
				}
			}

			/**
			* Проверка имени файла/папки
			*
			* @param mixed $sName
			* @return boolean
			*/
			function checkName($sName) {
				$bNotStartedWithDot = preg_match('/^[^\.]/', $sName);
				if(!$bNotStartedWithDot) return false;

				$bForbiddenExtension = preg_match('/^.*\.(php|pl|sh|exe|msi|com|bat|sql|js|ini|htaccess)$/', $sName);
				if($bForbiddenExtension) return false;

				return true;
			}

			$opts = array(
				//'debug' => true,
				'roots' => array(
					array(
						'id'			=> 'images',
						'driver'        => 'UmiruLocalFileSystem',
						'path'          => CURRENT_WORKING_DIR . '/images/cms/data',
						'startPath' 	=> CURRENT_WORKING_DIR . '/images/cms/data',
						'alias'			=> 'Изображения',
						'URL'           => '/images/cms/data/',
						'accessControl' => $isFullAccess ? 'elfinder_full_access' : 'elfinder_access',
						'acceptedName'	=> 'checkName',
					),
					array(
						'id'			=> 'files',
						'driver'        => 'UmiruLocalFileSystem',   // driver for accessing file system (REQUIRED)
						'path'          => CURRENT_WORKING_DIR . '/files/',         // path to files (REQUIRED)
						'alias'			=> 'Файлы',
						'URL'           => '/files/' , // URL to files (REQUIRED)
						'accessControl' => $isFullAccess ? 'elfinder_full_access' : 'elfinder_access',
						'acceptedName'	=> 'checkName',
					),
				)
			);

			// run elFinder
			$connector = new elFinderConnector(new elFinderUmiru($opts));
			$connector->run();
		}


		public function getElfinderHash($path) {
			if (!strlen($path)) return '';

			$path = str_replace('\\', '/', realpath("./" . trim($path, "./\\")));

			$permissions = permissionsCollection::getInstance();
			$userId = $permissions->getUserId();
			$user = umiObjectsCollection::getInstance()->getObject($userId);

			$source = "";

			if ($filemanagerDirectory = $user->getValue('filemanager_directory')) {
				$i = 1;
				$directories = explode(",", $filemanagerDirectory);
				foreach ($directories as $directory) {
					$directory = trim($directory);
					$directory = trim($directory, "/");
					if (!strlen($directory)) continue;
					$directoryPath = CURRENT_WORKING_DIR . "/" . $directory;
					if (strpos($directoryPath, CURRENT_WORKING_DIR) === false || !is_dir($directoryPath)) continue;
					if (strpos($path, $directory) !== false) {
						$source = "files" . $i;
						$path = trim(str_replace(CURRENT_WORKING_DIR . "/" . $directory, "", $path), "/");
						break;
					}
					$i++;
				}
			} else {
				$images_path = str_replace('\\', '/', realpath(CURRENT_WORKING_DIR . "/images/cms/data"));
				$files_path = str_replace('\\', '/', realpath(CURRENT_WORKING_DIR . "/files"));
				if (strpos($path, $images_path) === 0) {
					$path = trim(str_replace($images_path, "", $path), "/");
					$source = "images";
				} elseif (strpos($path, $files_path) === 0) {
					$path = trim(str_replace($files_path, "", $path), "/");
					$source = "files";
				}
			}

			$path = str_replace("/", DIRECTORY_SEPARATOR, $path);
			$hash = strtr(base64_encode($path), '+/=', '-_.');
			$hash = rtrim($hash, '.');

			return strlen($hash) ? "umiru" . $source . "_" . $hash : "";
		}

	};
?>