
site.utils = {};
site.utils.getObjectType = function(obj) {
	var toString = Object.prototype.toString,
		obj_type = false;
	switch (toString.call(obj)) {
		case "[object Array]": obj_type = 'array'; break;
		case "[object Object]": obj_type = 'object'; break;
		case "[object String]": obj_type = 'string'; break;
		case "[object Number]": obj_type = 'number'; break;
		case "[object Boolean]": obj_type = 'boolean'; break;
		case "[object Function]": obj_type = 'function'; break;
	}
	return obj_type;
};

site.utils.js = {};
site.utils.js.init = function(src) {
	switch (site.utils.getObjectType(src)) {
		case "array": for (i in src) this.init(src[i]); break;
		case "string": this.include(src); break;
	}
	return true;
};

site.utils.js.include = function(src) {
	jQuery("<script/>", {
		"charset" : 'utf-8',
		"type"    : 'text/javascript',
		"src"     : src
	}).appendTo("head");
};

site.utils.js.init([
    site.template_resources + 'js/basket.js',
    site.template_resources + 'js/foxi.basket.js',
    site.template_resources + 'js/foxi.order.js',
    site.template_resources + 'js/foxi.forms.js',
    site.template_resources + 'js/foxi.message.js',
    site.template_resources + 'js/foxi.captcha.js'
]);
if(site.module == 'catalog' && site.method == 'category')
    site.utils.js.init(site.template_resources + 'js/foxi.catalog.js');
if(site.module == 'catalog' && site.method == 'object')
    site.utils.js.init(site.template_resources + 'js/foxi.catalog-only.js');
if(site.page && site.page.alt_name =='reviews'){
    site.utils.js.init([
        site.template_resources + 'js/reviews.js'
    ]);
}

site.utils.replaceTerminate = function($num,$str,replacer){
    var $number = $num % 100,$terminate;
    if($number > 10 && $number < 15){
        $terminate  = replacer[2];
    } else {
        $number = $num % 10;
        if($number == 0) {$terminate = replacer[2];}
        if($number == 1 ) {$terminate = replacer[0];}
        if($number > 1 ) {$terminate = replacer[1];}
        if($number > 4 ) {$terminate = replacer[2];}
    }
    return $str.replace('%terminate%',$terminate);
};
site.utils.formatPrice = function($number,$decimal,$separator,$decpoint,replacer){
    var r = parseFloat($number), rr, b,
        exp10 = Math.pow(10,$decimal);// приводим к правильному множителю
    r = Math.round(r*exp10)/exp10;// округляем до необходимого числа знаков после запятой
    rr = Number(r).toFixed($decimal).toString().split('.');
    b = rr[0].replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1" + $separator);
    r = (rr[1]?b + $decpoint + rr[1]:b);
    return replacer.replace('%price%',r);
};

site.utils.dropImage = function(img, to){
    if(!img.length) return false;
    img.clone().css({
            'position' : 'absolute',
            'z-index' : '1001',
            'left' : img.offset().left,
            'top' : img.offset().top,
            'width' : img.width(),
            'height' : img.height(),
            'box-shadow' : '0 0 8px #5B5B5B'
        }).appendTo('body').animate({
            opacity: 0,
            left: to.offset().left,
            top: to.offset().top,
            width: to.height(),
            height: to.height()}, 900, function() {
                $(this).remove();
        });
};

site.utils.spinner = function(wrap){
    wrap.find('.spinner-wrp a').click(function (e) {
        e.preventDefault();
        var val = parseInt($(this).siblings('input').val());
        if (!val) val = 0;
        if ($(this).hasClass('left-button')) val = (val <= 0) ? 0 : val-1;
            else val++;
        $(this).siblings('input').val(val);
        $(this).siblings('input').change();
    });
};

site.utils.parseTemplate = function(template,params){
    for (var i in params)
        template = template.replace(new RegExp('#' + i,'g'),params[i]);
    return template;
};

site.utils.infinitescroll_start = function(wrapper, pathFunction, appendFunction){
    wrapper.infinitescroll({
            loading:{
                img: null
            },
            navSelector  : '.custom-14',
            nextSelector : '.custom-14',
            itemSelector : '.load_element',
            dataType: 'html',
            appendCallback: false,
            path: pathFunction
        },appendFunction
    );
};

jQuery(document).ready(function(){
    if(typeof $.colorbox != 'undefined') $('.comments_photo a, .offer-img-link').colorbox();
    if(site.module != 'catalog' || site.method != 'category'){
        $('[umi\\:call-point]').each(function(){
            var _ = $(this), call_point = _.attr('umi:call-point');
            if(_.attr('umi:button')){
                _.data('number',0).data('load',0);
                _.siblings('a').bind('click',function(e){
                    e.preventDefault();
                    if(_.data('load') == 0){
                        var number = _.data('number'), __ = $(this);
                        _.data('load',1).append('<span class="infinite-loader"/>');
                        $.get('/udata/custom/call_point/' + call_point + '?p=' + number,function(data){
                            if($(data).find('div').length != 0){
                                _.append(data).find('.infinite-loader').remove();
                                _.data('number',number + 1).data('load',0);
                            } else
                                __.hide();
                        },'html');
                    }
                });
            }else{
                site.utils.infinitescroll_start(_,function(number){
                    return '/udata/custom/call_point/' + call_point + '?p=' + (number-1);
                },function(newElements){
                    if(newElements.length == 0){
                        $(this).infinitescroll('destroy');
                    }
                        $(newElements).find('.comments_photo a').colorbox();
                    $(this).append(newElements);

                    console.log('reviews');
                });
            }
        });
    }

	jQuery('#on_edit_in_place').click(function() {
		uAdmin.eip.swapEditor();
		return false;
	});

});