site.catalog = {};
site.catalog.filter = {};
site.catalog.filter.form = jQuery('#foxiFormFilter');
site.catalog.filter.catalogWrapper = jQuery('#foxiCatalogWrapper');
site.catalog.filter.preloader = false;
site.catalog.filter.paginates = false;
site.catalog.filter.page = 0;
site.catalog.filter.url = '/catalog/getCatalog/' + site.pageId;

site.catalog.filter.init = function(param) {
    if(param.formSelector) this.form = jQuery(param.formSelector);
    if(param.catalogWrapperSelector) this.catalogWrapper = jQuery(param.catalogWrapperSelector);
    if(param.url) this.url = param.url;
    if(param.preloaderClass) this.preloader = '<span class="'+param.preloaderClass+'"></span>';
    if(param.paginateSelector) this.paginates = param.paginateSelector;
    var call_point = this.catalogWrapper.attr('umi:call-point');
    site.catalog.filter.url = '/udata/custom/call_point/' + call_point;
    $('[umi\\:call-point]').each(function(){
        var call_point = $(this).attr('umi:call-point');
        site.utils.infinitescroll_start($(this), function(number){
            return site.catalog.filter.url + '?' + site.catalog.filter.getParams() + '&p=' + (site.catalog.filter.page + 1);
        },function(newElements){
            site.catalog.filter.page++;
            if(newElements.length == 0){
                $(this).infinitescroll('pause');
                site.catalog.filter.page = 0;
            }
            $(this).append(newElements);
            site.catalog.installEvents(newElements);
        });
    });
    if(this.getParams() != ''){
        site.catalog.filter.start();
    }
    this.form.find('input',this.form).bind('change',function(){
        site.catalog.filter.page = 0;
        site.catalog.filter.catalogWrapper.infinitescroll('resume');
        site.catalog.filter.start();
    });
    if(this.paginates){
        jQuery(this.paginates).live('click',function(e){
            e.preventDefault();
            var href = $(this).attr('href'),
                arr = href.exec(/^\?p=(\d)\&/);
            if(arr[1]){
                site.catalog.filter.page = arr[1];
                site.catalog.filter.start();
            }
        });
    }
};
site.catalog.filter.start = function(){
    var params = this.getParams(),preloader = false;
    if(this.preloader) {
        preloader = jQuery(this.preloader);
        this.catalogWrapper.empty();
        preloader.appendTo(this.catalogWrapper);
    }
    jQuery.get(this.url,params,function(data){
        if(preloader) preloader.remove();
        site.catalog.filter.catalogWrapper.append(data);
        site.catalog.installEvents(site.catalog.filter.catalogWrapper);
    },'html');
};
site.catalog.filter.getParams = function(){
    return String(this.form.serialize());
};
site.catalog.installOrderEvent = function(wrapper){
    wrapper.find('a.order').bind('click',function(e){
        e.preventDefault();
        var _ = jQuery(this);
        jQuery.arcticmodal({
            type: 'ajax',
            url: _.attr('href'),
            afterLoadingOnShow: function(data, el) {
                site.forms.init(data.body);
                data.body.find('input[name = data\\[new\\]\\[offer_id\\]]').val(_.data('id'));
                data.body.find('input[name = data\\[new\\]\\[offer_name\\]]').val(_.data('name'));
            }
        });
    });
};
site.catalog.installEvents = function(wrapper){
    site.catalog.installOrderEvent(wrapper);
    wrapper.find('a.gray').bind('click',function(e){
        e.preventDefault();
        jQuery.arcticmodal({
            type: 'ajax',
            url: jQuery(this).attr('href'),
            afterLoading: function(data, el) {
                //alert('afterLoading');
            },
            afterLoadingOnShow: function(data, el) {
                data.body.find('select').selectbox();
                site.catalog.installOrderEvent(data.body);
                site.utils.spinner(data.body);
                site.basket.initModal(data.body);
                var nav_slider = data.body.find('.autoplay > div'),
                    big_slider = data.body.find('.autoplay-for > div');
                switch (nav_slider.length){
                    case 1:{
                        nav_slider.hide();
                        return false;
                    } break;
                    case 2:
                    case 3:{
                        nav_slider.parent().append(nav_slider.clone());
                        big_slider.parent().append(big_slider.clone());
                    }break;
                }
                data.wrap.find('.autoplay-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.autoplay'
                });
                data.wrap.find('.autoplay').slick({
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    asNavFor: '.autoplay-for',
                    focusOnSelect: true
                });
            }
        });
    });
};

site.catalog.init = function() {
    if(site.module == 'catalog' && site.method == 'category'){
        var wrapper = jQuery('.center > div[class^=catalog]');
        site.catalog.installEvents(wrapper);
        site.catalog.filter.init({
            formSelector: '.filter',
            catalogWrapperSelector: '[umi\\:call-point]',
            preloaderClass: 'infinite-loader'
        });
    }
};

jQuery(document).ready(function(){site.catalog.init();});

