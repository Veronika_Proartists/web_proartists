(function(){
    var app = {};


    app.el = {
        win : $(window),
        doc : $(document),
        card : $('.card'),
        arcticTrigger:  $('.arctic')
    };

    app.func = {
        modal : function(){
            app.el.arcticTrigger.on('click',function(e){
                e.preventDefault();
                var _ = jQuery(this);
                jQuery.arcticmodal({
                    type: 'ajax',
                    url: _.attr('href'),
                    afterLoadingOnShow: function(data, el) {
                        site.forms.init(data.body);
                        data.body.find('input[name = data\\[new\\]\\[offer_id\\]]').val(_.data('id'));
                        data.body.find('input[name = data\\[new\\]\\[offer_name\\]]').val(_.data('name'));
                        if($(window).width() > 992){
                            if($('.autoplay-for > div').length > 1){
                                $('.autoplay-for').slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    arrows: false,
                                    fade: true,
                                    asNavFor: '.autoplay'
                                });
                                $('.autoplay').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                    asNavFor: '.autoplay-for',
                                    dots: true,
                                    centerMode: true,
                                    focusOnSelect: true
                                });
                            } else {
                                $('.autoplay-for').slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    arrows: false,
                                    fade: true
                                });
                                $('.autoplay').hide();
                            }

                        }
                    }
                });

            });
        }
    };

    app.el.doc.ready(function(){
        app.func.modal();
        $('input[name*="phone"]').mask('+7 (000) 000-0000',
            {placeholder: "+7 (___) ___ - ____"});
    });
})();
jQuery(function($){
    $( '.menu-btn' ).click(function(){
        $('.responsive-menu').toggleClass('expand')
    })
});

jQuery(function($){
    $( '.responsive-menu-link' ).click(function(){
        $('.responsive-menu').removeClass('expand')
    })
});

$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top-60
            }, 1000);
            return false;
        }
    }
});