site.message = function(params) {
    jQuery.jGrowl(params.content,{
        'header': site.site_name,
        'life': 3000
    });
};
if(typeof getLabel == "undefined"){
    getLabel = function(str){
        switch (str){
            case "js-label-jgrowl-close-all": return 'Закрыть все';
        }
    };
}

