site.reviews={
    item_img : '<div class="item-img"><input type="file" name="data[new][img]" accept="image"/></div>',
    wrap : $('.file-uploader-block'),limit: 5,
    init: function(){
        site.reviews.onLoad(this.wrap.find('input:file'));
    },
    onLoad: function(em){
        em.bind('change',function(e){
            var file = e.target.files[0],itemWrap = $(this).closest('.item-img');
            itemWrap.find('div').remove();
            if(file.type.indexOf('image') != -1){
                $(this).hide();
                itemWrap.append('<div class="itemMessage">'+file.name+'</div>');
                itemWrap.addClass('ful');
                itemWrap.append('<div class="delete-img">x</div>');
                itemWrap.find('.delete-img').bind('click',function(){
                    $(this).closest('.item-img').remove();
                    site.reviews.checkLimit();
                });
                site.reviews.checkLimit();
            }else{
                $(this).remove();
                itemWrap.prepend('<input type="file" name="data[new][img]"/>');
                site.reviews.onLoad(itemWrap.find('input:file'));
                itemWrap.append('<div class="itemMessage">Загружать можно только картинки</div>');
            }
        })
    },
    checkLimit: function(){
        if(this.wrap.find('.item-img').length < 5 && this.wrap.find('.item-img').length == this.wrap.find('.ful').length ){
            var item = $(this.item_img);
            for(var i=0;i < this.limit;i++){
                if($('#image-item-'+(i+1)).length == 0){
                    item.find('input:file').attr('name','data[new][img_'+(i+1)+']').attr('id','image-item-'+(i+1));
                    site.reviews.onLoad(item.appendTo(site.reviews.wrap).find('input:file'));
                    break;
                }
            }
        }

    }
}

jQuery(document).ready(function(){
    if(site.page.alt_name =='reviews'){
        site.reviews.init();
    }
   
})

