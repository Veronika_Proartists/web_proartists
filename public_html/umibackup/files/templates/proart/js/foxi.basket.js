site.basket = {};
site.basket.wrap = jQuery('aside .basket');

site.basket.replace = function(id) {
	return function(e) {
		var item_total_price, cart_item, i, item, drop_image, price_template = '%price%',
            item_card, buy_button, buy_button_text, buy_button_selector = '.buttons .black',buy_button_more_selector = 'a.button-1',
			cart_summary = jQuery('#summary_price'),
			rem_item = true,
			detect_options = {};

		if (e.summary.amount > 0) {
            price_template = (e.summary.price.prefix||'') + ' %price% ' + (e.summary.price.suffix||'');
			for (i in e.items.item) {
				item = e.items.item[i];
				if (item.id == id) {
					rem_item = false;
					item_total_price = item["total-price"].actual;
				}
				if (item.page.id == id) {
					if (detect_options.amount) {
						detect_options.amount = detect_options.amount + item.amount;
					}
					else detect_options = {'id':id, 'amount':item.amount};
				}
			}
            if(site.method == 'object'){
                if(detect_options.amount && site.basket.modify.up){
                    site.message({content: i18n.basket_add_offer});
                    site.utils.dropImage(jQuery('.offer_' + id + ' .color'),site.basket.wrap);
                }else{
                    site.message({content: i18n.basket_del_offer});
                }
            }
			if (detect_options.amount && site.method == 'category') {
                site.message({content: i18n.basket_add_offer});
                item_card = jQuery('[umi\\:element-id=' + id + ']').not('img');
                buy_button = item_card.find(buy_button_selector);
                buy_button_text = i18n.basket_add_button(detect_options.amount,false);
                if(buy_button.get(0).tagName.toLowerCase() == 'input')
                    buy_button.val(buy_button_text);
                else
                    buy_button.text(buy_button_text);

                buy_button = item_card.find(buy_button_more_selector);
                if(buy_button.length)
                    buy_button.text(i18n.basket_add_button(detect_options.amount,true));
                if(item_card.length == 2){
                    drop_image = item_card.find('.autoplay-for .slick-active img, .block-0 > img');
                } else if(site.page && site.page.type_id == i18n.brush_type_id){
                    drop_image = item_card.find('img.custom-2');
                } else{
                    drop_image = item_card.find('img');
                }
                site.utils.dropImage(drop_image,site.basket.wrap)
			}
            //Допилить в корзине
            if(site.method == 'cart'){
                if (rem_item) {
                    if (cart_item = jQuery('.cart_item_' + id)) {
                        cart_item.animate({opacity:0},500,function(){
                            cart_item.css({display:'block'}).animate({height:0},300,function(){
                                cart_item.remove();
                            });
                        });
                    }
                } else {
                    jQuery('.cart_item_' + id + ' td').eq(3).text(site.utils.formatPrice(item_total_price,0,' ','.',price_template));
                }
                cart_summary.text(site.utils.formatPrice(e.summary.price.actual,0,' ','.',price_template));
            }
		} else {
            if(site.method == 'cart'){
                jQuery('.center .basket').slideUp(500,function(){
                    $(this).find('.breadcrumbs').siblings().remove();
                    $(this).append(i18n.basket_empty_html).slideDown(500);
                });
            }
		}
        site.basket.update(e.summary);
		site.basket.modify.complete = true;
	};
};

site.basket.update = function(summary){
    var text, price_template = (summary.price.prefix||'') + ' %price% ' + (summary.price.suffix||'');
    if (summary.amount > 0) {
        text = summary.price.actual;
        text = i18n.basket_items_total(summary.amount) + '<b>' + site.utils.formatPrice(text,0,' ','.',price_template) + '</b>';
        site.basket.wrap.removeClass('empty');
    }
    else {
        text = i18n.basket_empty;
        site.basket.wrap.addClass('empty');
    }
    site.basket.wrap.find('.hint p').html(text);
    site.basket.wrap.find('.wrp a span').html(summary.amount);
};

site.basket.add = function(id, form) {
	var e_name, amount_input, options = {};
	if (form) {
		var elements = jQuery('option:selected', form);
		for (var i = 0; i < elements.length; i++) {
            if(elements[i].value == 0) continue;
			e_name = elements[i].parentNode.name.replace(/^options\[/, '').replace(/\]$/, '');
			options[e_name] = elements[i].value;
		}
	}
    amount_input = jQuery('input[name=amount]', form);
    if(amount_input.length) options['amount'] = Math.abs(parseInt(amount_input.val()));
    basket.addElement(id, options, this.replace(id));
};

site.basket.put = function(id, amount_new, amount_old) {
    if(amount_new == 0) return basket.removeElement(id, this.replace(id));
    if(amount_new && amount_new.replace(/[\d]+/) == 'undefined' && amount_new != amount_old) {
        site.basket.modify.up = (amount_new > amount_old);
        basket.putElement(id, {amount:amount_new}, this.replace(id));
    }
    else this.modify.complete = true;

};

site.basket.modify = function(id, amount_new, amount_old) {
	if (amount_new.replace(/[\d]+/) == 'undefined' && amount_new != amount_old) {
		basket.modifyItem(id, {amount:amount_new}, this.replace(id));
	}
	else this.modify.complete = true;
};

site.basket.modify.complete = true;
site.basket.modify.up = true;

site.basket.remove = function(id) {
	if (id == 'all') basket.removeAll(this.replace(id));
	else basket.removeItem(id, this.replace(id));
};

site.basket.initModal = function(wrap){
    if(site.module == 'catalog' && site.method == 'category'){
        if(site.page && site.page.type_id == i18n.brush_type_id){
            wrap.find('a.button-1').bind('click',function(){
                var id = jQuery(this).attr('href').split('/').pop();
                site.basket.add(id, this.parentNode.parentNode);
                return false;
            });
        }else{
            wrap.find('a.button-1').bind('click',function(){
                var id = jQuery(this).attr('href').split('/').pop();
                basket.putElement(id, {},site.basket.replace(id));
                return false;
            });
        }
    }
};
site.basket.init = function() {
    if(site.module == 'catalog'){
        if(site.page && site.page.type_id == i18n.brush_type_id){
            jQuery('.catalog-2 form').live('submit',function(){
                var id = this.action.split('/').pop();
                site.basket.add(id, this);
                return false;
            });
        }else{
            jQuery('.catalog-1 a.black').live('click',function(){
                var id = jQuery(this).attr('href').split('/').pop();
                basket.putElement(id, {},site.basket.replace(id));
                return false;
            });
        }
        jQuery('.spinner-wrp input').bind('keyup', function() {
            if(0 > parseInt(this.value))
                this.value = Math.abs(this.value);
            if (site.basket.modify.complete) {
                site.basket.modify.complete = false;
                var amountThis = this;
                setTimeout(function() {
                    var id = parseInt(amountThis.parentNode.parentNode.parentNode.className.split('_').pop()),
                        e = jQuery(amountThis).next('input'),
                        old = e.val();
                    amountThis.value = Math.abs(parseInt(amountThis.value));
                    e.val(amountThis.value);
                    site.basket.put(id, amountThis.value, old);
                }, 500)
            }
        });
        jQuery('.spinner-wrp a').click(function(e){
            e.preventDefault();
            if (site.basket.modify.complete) {
                site.basket.modify.complete = false;
                var id = parseInt(this.parentNode.parentNode.parentNode.className.split('_').pop()),
                    e = ($(this).hasClass('left-button')) ? this.nextSibling.nextSibling : this.previousSibling,
                    old = e.value;
                var new_val = (parseInt(old) + (($(this).hasClass('left-button')) ? -1 : 1));
                if(new_val < 0) {
                    site.basket.modify.complete = true;
                    return false;
                }
                e.value = new_val;
                e.previousSibling.value = e.value;
                site.basket.put(id, e.value, old);
            }
        });
    } else if(site.module == 'emarket' && site.method == 'cart'){
        jQuery('.cart_item_close.del').click(function(){
            site.basket.remove(this.id.match(/\d+/).pop());
            return false;
        });
        jQuery('.spinner-wrp input').bind('keyup', function() {
            if(0 > parseInt(this.value))
                this.value = Math.abs(this.value);
            if (site.basket.modify.complete) {
                site.basket.modify.complete = false;
                var amountThis = this;
                setTimeout(function() {
                    var id = parseInt(amountThis.parentNode.parentNode.parentNode.className.split('_').pop()),
                        e = jQuery(amountThis).next('input'),
                        old = e.val();
                    e.val(amountThis.value);
                    site.basket.modify(id, amountThis.value, old);
                }, 500)
            }
        });
        jQuery('.spinner-wrp a').click(function(e){
            e.preventDefault();
            if (site.basket.modify.complete) {
                site.basket.modify.complete = false;
                var id = parseInt(this.parentNode.parentNode.parentNode.className.split('_').pop()),
                    e = ($(this).hasClass('left-button')) ? this.nextSibling.nextSibling : this.previousSibling,
                    old = e.value;
                e.value = (parseInt(old) + (($(this).hasClass('left-button')) ? -1 : 1));
                e.previousSibling.value = e.value;
                site.basket.modify(id, e.value, old);
            }
        });
    }
    site.basket.wrap.find('> .wrp > a').bind('click',function(e){
        if(site.basket.wrap.hasClass('empty')){
            e.preventDefault();
            site.message({content: i18n.basket_empty});
        }
    });
};

jQuery(document).ready(function(){site.basket.init()});