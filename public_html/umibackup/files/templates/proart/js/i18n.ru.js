/* lang: ru */
var i18n = {
        goods: ['товар','товара','товаров'],
        basket_items_total : function(amount){
            return site.utils.replaceTerminate(amount,'Спасибо за покупку! В вашей корзине ' + amount + ' %terminate% на сумму ',i18n.goods);
        },
		basket_empty : 'Ваша корзина пока пуста',
		basket_empty_html : '<h4 class="empty-content">ШАГ 1 - КОРЗИНА ТОВАРОВ</h4><p>Ваша корзина пока пуста. Вы можете <a href="/">вернуться к выбору товаров</a></p></div>',
		basket_options : 'Выбор опций',
		basket_add_button : function(amount,full){
            return ((!full) ? 'В' : 'Добавить в' ) + ' корзину (' + amount + ')';
        },
		basket_add_short : 'Добавить',
		basket_add_offer : 'Товар добавлен в корзину!',
		basket_del_offer : 'Товар удален из корзины',
		basket_add_color : 'Цвет добавлен в набор!',
        basket_amount : "Выбрано: #s шт.",
		forms_empty_field : "Поле обязательно для заполнения.",
		forms_short_login : "Слишком короткий логин. Логин должен состоять не менее, чем из 3х символов.",
		forms_long_login : "Слишком большой логин. Логин должен состоять не более, чем из 40 символов.",
		forms_short_pass : "Слишком короткий пароль. Пароль должен состоять не менее, чем из 3х символов.",
		forms_same_pass : "Пароль не должен совпадать с логином.",
		forms_confirm_pass : "Пароли должны совпадать.",
		forms_invalid_email : "Некорректный e-mail.",
		checkout : "Оформить",
		oneclick_checkout : "Быстрый заказ",
		finish_message_prefix : "Заказ ",
		finish_message_postfix : " успешно оформлен!<br/>Ожидайте звонка от менеджера магазина.",
		continue : "Продолжить",
		error : "Ошибка: ",
        brush_type_id : "130"
	};