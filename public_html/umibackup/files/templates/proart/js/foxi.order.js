site.order = {};
site.order.cache = jQuery('#cache_container');
site.order.delivery = jQuery('#delivery_box');

site.order.changeDelivery = function(delivery) {
    var data = String(delivery.data('payment')) ,payments = (data.indexOf(',') != -1) ? data.split(',') : [data];
    jQuery('#payment .row').hide();
    for(var i in payments){
        jQuery('#payment').find('input[value=' + payments[i] + ']').parent().show();
    }
    if(jQuery('#payment .row:visible input:checked').length == 0){
        jQuery('#payment .row:visible input').eq(0).attr('checked','checked');
    }
    switch(delivery.data('delivery')){
        case 'self': {
            if(site.order.cache.find('>.row:not(.address)').length != 0)
                this.delivery.prepend(site.order.cache.find('>.row:not(.address)'));
            if(this.delivery.parent().attr('id') != 'cache_container')
                this.delivery.slideUp(300).appendTo(this.cache);

        } break;
        case 'courier':
        case 'russianpost': {
            if(site.order.cache.find('>.row:not(.address)').length != 0)
                this.delivery.find('h5').after(site.order.cache.find('>.row:not(.address)').slideDown(300));
            if(this.delivery.parent().attr('id') == 'cache_container')
                this.delivery.appendTo(jQuery('#delivery')).slideDown(300);
        } break;
        case 'courier_piter': {
            if(this.delivery.parent().attr('id') == 'cache_container')
                this.delivery.appendTo(jQuery('#delivery')).slideDown(300);
            if(site.order.cache.find('>.row:not(.address)').length == 0)
                site.order.delivery.find('>.row:not(.address)').slideUp(300).appendTo(site.order.cache);

        } break;

    }
};

site.order.init = function() {
    if(site.module == 'emarket' && site.method == 'purchasing_one_step'){
        jQuery('*[name=data\\[new\\]\\[address\\]]').parent().addClass('address');
        site.order.changeDelivery(jQuery('#delivery_choose input:checked'));
        jQuery('#delivery_choose input').change(function(){
            site.order.changeDelivery(jQuery(this));
        });

    }
};

jQuery(document).ready(function(){site.order.init();});

