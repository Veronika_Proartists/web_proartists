site.forms = {};

site.forms.init = function (wrap) {
	var context = wrap.find('.required').closest("form");
	/*jQuery('input:not([type=submit]), textarea, select', context).focusout(function() {
		site.forms.errors.check(this);
	});*/
	site.forms.data.restore(wrap);
	site.forms.comments.init();
};

site.forms.data = {};

site.forms.data.save = function (form) {
	if (!form && !form.id) return false;
	var str = "", input, inputName, i, opt_str = "", o;
	for (i = 0; i < form.elements.length; i++) {
		input = form.elements[i];
		if (input.name) {
			inputName = input.name.replace(/([)\\])/g, "\\$1");
			switch (input.type) {
				case "password":break;
				case "text":
				case "textarea": str += 'TX,' + inputName + ',' + input.value; break;
				case "checkbox":
				case "radio": str += 'CH,' + input.id + ',' + (input.checked ? 1 : 0); break;
				case "select-one": str += 'SO,' + inputName + ',' + input.selectedIndex; break;
				case "select-multiple": {
					for (o = 0; o < input.options.length; o++) {
						if (input.options[o].selected) {
							opt_str += input.options[o].value;
							if (o < (input.options.length - 1)) opt_str += ":";
						}
					}
					str += 'SM,' + inputName + ',' + opt_str; break;
				}
			}
			if (i < (form.elements.length - 1)) str += "+";
		}
	}
	jQuery.cookie("frm" + form.id, str.replace(/([|\\])/g, "\\$1"));
	return true;
};

site.forms.data.restore = function (wrap) {
	var forms = wrap.find('form'), i, j, element, data;
	for (i = 0; i < forms.length; i++) {
		if (forms[i].id && (data = jQuery.cookie("frm" + forms[i].id))) {
			data = data.split('+');
			for (j = 0; j < data.length; j++) {
				element = data[j].split(',');
				if (!element) continue;
				switch (element[0]) {
					case "PW": break;
					case "TX": if(forms[i].elements[element[1]]) forms[i].elements[element[1]].value = element[2]; break;
					case "CH": document.getElementById(element[1]).checked = (element[2] == 1) ? true : false; break;
					case "SO": if(forms[i].elements[element[1]]) forms[i].elements[element[1]].selectedIndex = element[2]; break;
					case "SM":
						var options = forms[i].elements[element[1]].options;
						var opt_arr = element[2].split(":"), op, o;
						for (op = 0; op < options.length; op++)
							for (o = 0; o < opt_arr.length; o++)
								if (opt_arr[o] && (options[op].value == opt_arr[o]))
									options[op].selected = true;
						break;
				}
			}
		}
	}
	return true;
};

site.forms.comments = {};

site.forms.comments.init = function() {
	var blog_comm = jQuery('#comments');
	var blog_comm_arr, i;
	if (typeof blog_comm[0] == 'object') {
		blog_comm_arr = jQuery('a.comment_add_link', blog_comm[0]);
		for (i = 0; blog_comm_arr.length > i; i++) {
			blog_comm_arr[i].onclick = site.forms.comments.add(blog_comm_arr[i]);
		}
	}
};

site.forms.comments.add = function(element) {
	return (function() {site.forms.comments.setAction(element.id);});
};

site.forms.comments.setAction = function(comm_id) {
	var comment_add_form;
	if ((comment_add_form = jQuery('#comment_add_form'))) {
		comment_add_form[0].action = '/blogs20/commentAdd/' + comm_id;
		return true;
	}
	return false;
};

site.forms.data.check = function (form, num) {
    var r = num, elements_arr = jQuery(form).find('input.required, textarea.required , select.required').not('[type != checkbox]:hidden');
    ++r;
    for (var i = 0; elements_arr.length > i; i++) {
        if (typeof num != "undefined" && elements_arr[r] == elements_arr[i]) return false;
        if (!site.forms.errors.check(elements_arr[i], num)) return false;
    }
    return true;
};

site.forms.errors = {};

/**
 * Генерация ошибок
 *
 * @param {Object} element Проверяемый элемент формы
 * @param {Number} num Позиция элемента формы
 * @param {Boolean} bool Сообщение об ошибке
 * @return {Boolean} Результат корректности заполнения
 */
site.forms.errors.check = function(element, bool) {
	var _err, empty_err = i18n.forms_empty_field; 
	if(!$(element).hasClass('required')) return false;
    switch (element.type) {
        case "textarea" :
        case "text" : {
            switch (element.name) {
                case "login": {
                    switch (element.value.length) {
                        case 0: _err = empty_err; break;
                        case 1:
                        case 2: _err = i18n.forms_short_login; break;
                        default: {
                            if (element.value.length > 40) _err = i18n.forms_long_login;
                        }
                    }
                    break;
                }
                case "password": {
                    switch (element.value.length) {
                        case 0: _err = empty_err; break;
                        case 1:
                        case 2: _err = i18n.forms_short_pass; break;
                        default: {
                            if (element.form.elements['login'].value == element.value)
                                _err = i18n.forms_same_pass;
                        }
                    }
                    break;
                }
                case "password_confirm": {
                    if (element.value.length == 0) _err = empty_err;
                    else if (element.form.elements['password'].value !== element.value) {
                        _err = i18n.forms_confirm_pass;
                    }
                    break;
                }
                case "email": {
                    if (element.value.length == 0) _err = empty_err;
                    else if (!element.value.match(/.+@.+\..+/)) _err = i18n.forms_invalid_email;
                    break;
                }
                default: {
                    if (element.value.length == 0) _err = empty_err;
                    if (element.name.match(/^.*e.*mail.*$/) && element.name != 'email_to' && element.name != 'system_email_to')
                        if (!element.value.match(/.+@.+\..+/)) _err = i18n.forms_invalid_email;
                }
            }
        } break;
        case 'checkbox' : {
            if (jQuery(element).attr('checked') != 'checked') _err = empty_err;
        } break;
    }

	if (bool) {
		return !_err;
	} else {
		return site.forms.errors.write(_err, element);
	}
};

site.forms.errors.write = function (_err, element) {
    if(jQuery(element).hasClass('add_error')){
        var cont = element.parentNode;
        jQuery(cont).find('span.error').remove();
        if (_err) {
            $(element).addClass('error');
            var err_block = document.createElement('span');
            err_block.className = "error";
            err_block.innerHTML = _err;
            cont.appendChild(err_block);
            if (element.name == "password_confirm") element.value = "";
            element.focus();
            return false;
        }
        $(element).removeClass('error');
        $(element).addClass('success');
    } else {
        if (_err) {
            $(element).addClass('invalid');
            if(site.modal) window.top.site.message({content: i18n.error + _err});
            else site.message({content: i18n.error + _err});
            if (element.name == "password_confirm") element.value = "";
            element.focus();
            return false;
        }
        $(element).removeClass('invalid');
    }
	return true;
};
/**
 * Отображение формы по частям.
 */
site.forms.phone = {};
site.forms.phone.order = function(form) {
    jQuery.ajax({
        url: '/udata://webforms/ajax_send/.json',
        data: jQuery(form).serialize(),
        dataType: 'json',
        success: function(data){
            if(data.error){
               site.message({content: i18n.error + data.error})
            }
            if(data.reply){
                var parent = jQuery(form).parent();
                parent.html(data.reply);
            }
        }
    });
};
jQuery(document).ready(function(){site.forms.init(jQuery('html'))});