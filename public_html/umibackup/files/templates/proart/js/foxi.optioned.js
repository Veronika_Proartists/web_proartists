site.optioned = {};
site.optioned.wrap = jQuery('.colors > .slide-9 > .amount');

site.optioned.replace = function(optioned_id) {
	return function(e) {
        var summary_price = 0, amount = 0, item, rem_item = true ,i , j, option, item_id, item_name, option_item, cart_item, template, color, new_html,
            price_template = (e.summary.price.prefix||'') + ' %price% ' + (e.summary.price.suffix||'');

        for (i in e.items.item) {
            item = e.items.item[i];
            if(item.page.id == site.page.id){
                summary_price += parseInt(item["total-price"].actual);
                amount += parseInt(item.amount);
                if(item.options.option)
                    for (j in item.options.option) {
                        option = item.options.option[j];
                        if (option.id == optioned_id) {
                            rem_item = false;
                            item_id = item.id;
                            item_name = option.name;
                        }
                    }
            }
        }
        if(amount > 0){
            site.optioned.wrap.slideDown(500);
            site.optioned.wrap.find('.block-1 .custom-1').text(i18n.basket_amount.replace('#s',amount));
            site.optioned.wrap.find('.block-1 .custom-2 span').text(site.utils.formatPrice(summary_price,0,' ','.',price_template));
        } else {
            site.optioned.wrap.slideUp(500);
        }
        option_item = jQuery('[umi\\:object-id=' + optioned_id + ']');
        if(!rem_item){
            if(option_item.is(':visible')){
                site.utils.dropImage(option_item.find(' > a'),site.optioned.wrap.find('.block-2'));
                site.message({content: i18n.basket_add_color});
                option_item.fadeOut(500);
                template = jQuery('#item_template').text();
                color = option_item.find(' > a').css('backgroundColor');
                new_html = site.utils.parseTemplate(template,{id:item_id, option: optioned_id, name: item_name, amount: 1, color: color});
                cart_item = jQuery(new_html).css({display:'none'}).appendTo(site.optioned.wrap.find('.block-2 > div')).slideDown(500);
                site.optioned.initItemEvent(cart_item);
            }
        } else {
            jQuery('[data-option=' + optioned_id + ']').slideUp(500,function(){
                $(this).remove();
            });
            option_item.fadeIn(500);
        }
        site.basket.update(e.summary);
		site.optioned.modify = true;
	};
};

site.optioned.initItemEvent = function(wrap) {
    wrap.find('.close').bind('click',function(e){
        e.preventDefault();
        var item_id = jQuery(this).parent().data('id'),
            optioned_id = jQuery(this).parent().data('option');
        basket.removeItem(item_id, site.optioned.replace(optioned_id));
    });
    wrap.find('.spinner-wrp input[type=text]').bind('keyup', function() {
        if(0 > parseInt(this.value))
            this.value = Math.abs(this.value);
        if (site.optioned.modify) {
            site.optioned.modify = false;
            var amountThis = this;
            setTimeout(function() {
                var optioned_id = parseInt(jQuery(amountThis).parent().parent().data('option')),
                    e = jQuery(amountThis).next('input'),
                    old = e.val(),item_id = jQuery(amountThis).parent().parent().data('id');
                e.val(amountThis.value);
                basket.modifyItem(item_id, {amount:amountThis.value}, site.optioned.replace(optioned_id));
            }, 500)
        }
    });
    wrap.find('.spinner-wrp a').click(function(event){
        event.preventDefault();
        if (site.optioned.modify) {
            site.optioned.modify = false;
            var optioned_id = parseInt(jQuery(this).parent().parent().data('option')),
                e = jQuery(this).siblings('[type=hidden]'),
                old = e.val(),item_id = jQuery(this).parent().parent().data('id'),
                i = (!$(this).hasClass('left-button')) ? 1 : -1;
            e.val(parseInt(old) + i);
            e.prev().val( e.val());
            basket.modifyItem(item_id, {amount:e.val()}, site.optioned.replace(optioned_id));
        }
    });
};

site.optioned.modify = true;
site.optioned.init = function() {
    if(site.module == 'catalog' && site.method == 'object'){
        jQuery('.slide-9 li > a').bind('click',function(e){
            e.preventDefault();
            var optioned_id = jQuery(this).parent().attr('umi:object-id');
            site.optioned.modify = false;
            basket.addElement(site.page.id, {color:optioned_id}, site.optioned.replace(optioned_id));
        });
        site.optioned.initItemEvent(site.optioned.wrap);
    }
};

jQuery(document).ready(function(){site.optioned.init()});
