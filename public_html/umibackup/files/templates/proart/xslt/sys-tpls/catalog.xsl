<?xml version="1.0" encoding="utf-8"?>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">

	<fx:output encoding="utf-8" method="html" indent="yes"/>

    <fx:variable name="document-page-id" select="/udata/@category-id" />

	<fx:template match="/">
		<fx:apply-templates />
	</fx:template>

    <fx:include href="../modules/catalog/common.xsl"/>
    <fx:include href="../modules/emarket/price.xsl"/>
    <fx:include href="../library/numpages.xsl"/>

	
</fx:stylesheet>