<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">
 
<fx:stylesheet	version="1.0"
		xmlns:fx="http://www.w3.org/1999/XSL/Transform">
 
	<fx:output encoding="utf-8" method="html" indent="yes"/>
    <fx:param name="lang-prefix"/>

	<fx:template match="/" priority="1">
        <fx:choose>
            <fx:when test="udata/@method = 'add'">
                <div class="box-modal custom-2">
                    <div class="box-modal_close arcticmodal-close"></div>
                    <div class="wrp clearfix">
                        <div class="callback">
                            <h5>
                                <fx:value-of select="document(concat('utype://',udata/@form_id))/udata/type/@title"/>
                            </h5>
                            <fx:apply-templates select="udata"/>
                        </div>
                    </div>
                </div>
            </fx:when>
            <fx:otherwise>
                <fx:apply-templates/>
            </fx:otherwise>
        </fx:choose>
	</fx:template>

    <fx:include href="../library/captcha.xsl"/>
    <fx:include href="../modules/webforms/add.xsl"/>
 
</fx:stylesheet>