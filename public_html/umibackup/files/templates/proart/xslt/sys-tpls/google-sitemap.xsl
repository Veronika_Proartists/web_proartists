<?xml version="1.0" encoding="UTF-8"?>
 
<fx:stylesheet	version="1.0"
		xmlns:fx="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
		extension-element-prefixes="php"
		exclude-result-prefixes="php"
		xmlns:xlink="http://www.w3.org/TR/xlink"
		xmlns="http://www.google.com/schemas/sitemap/0.84">
 
	<fx:output encoding="utf-8" method="xml" indent="yes"/>
	<fx:param name="domain" />
 
 
	<fx:template match="/">
		<urlset xmlns="http://www.google.com/schemas/sitemap/0.84"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84
			http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">
			<fx:apply-templates select="//items"/>
		</urlset>
	</fx:template>
 
 
	<fx:template match="items">
		<fx:apply-templates select="item"/>
	</fx:template>
 
 
	<fx:template match="item">
		<fx:variable name="update-time" select="document(@xlink:href)/udata/page/@update-time" />
		<url>
			<loc>
				<fx:value-of select="concat('http://', $domain, @link)" />
			</loc>
			<lastmod>
				<fx:value-of select="document(concat('udata://system/convertDate/', $update-time, '/c/'))/udata" />
			</lastmod>
		</url>
	</fx:template>
 
</fx:stylesheet>