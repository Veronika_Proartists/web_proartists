<?xml version="1.0" encoding="UTF-8"?>
<fx:stylesheet xmlns:fx="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<fx:template name="videoplayer">
		<fx:param name="video-file" select="''" />
		<fx:param name="entity-id" select="''" />
		<fx:param name="field-name" select="''" />
		<fx:param name="width" select="640" />
		<fx:param name="height" select="360" />
		<fx:param name="autoload" select="false" />
		<fx:variable name="path" />

		<fx:choose>
			<fx:when test="$video-file">
				<fx:call-template name="videoplayer_code_template">
					<fx:with-param name="path" select="$video-file" />
					<fx:with-param name="width" select="$width" />
					<fx:with-param name="height" select="$height" />
					<fx:with-param name="autoload" select="$autoload" />
				</fx:call-template>
			</fx:when>
			<fx:otherwise>
				<fx:call-template name="videoplayer_code_template">
					<fx:with-param name="path" select="document(concat('udata://system/getVideoPlayer/',$entity-id,'/',$field-name,'/'))/udata/path/text()" />
					<fx:with-param name="width" select="$width" />
					<fx:with-param name="height" select="$height" />
					<fx:with-param name="autoload" select="$autoload" />
				</fx:call-template>
			</fx:otherwise>
		</fx:choose>

	</fx:template>

	<fx:template name="videoplayer_code_template">
		<fx:param name="path" />
		<fx:param name="width" select="640" />
		<fx:param name="height" select="360" />
		<fx:param name="autoload" select="false" />

		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
				id="UmiVideoPlayer" width="{$width}" height="{$height}"
				codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
				<param name="movie" value="/images/cms/video/UmiVideoPlayer.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#869ca7" />
				<param name="allowScriptAccess" value="sameDomain" />
				<param name="allowFullscreen" value="true" />
				<param name="flashVars" value="{concat('autoload=',$autoload,'&amp;video=',$path)}" />
				<embed src="/images/cms/video/UmiVideoPlayer.swf" quality="high" bgcolor="#869ca7"
					width="{$width}" height="{$height}" name="UmiVideoPlayer" align="middle"
					play="true"
					loop="false"
					allowScriptAccess="sameDomain"
					allowFullscreen="true"
					flashVars="{concat('autoload=',$autoload,'&amp;video=',$path)}"
					type="application/x-shockwave-flash"
					pluginspage="http://www.adobe.com/go/getflashplayer">
				</embed>
		</object>

	</fx:template>


	<fx:template match="property[@type = 'video_file']/value">
		<fx:call-template name="videoplayer">
			<fx:with-param name="video-file" select="." />
		</fx:call-template>
	</fx:template>

</fx:stylesheet>
