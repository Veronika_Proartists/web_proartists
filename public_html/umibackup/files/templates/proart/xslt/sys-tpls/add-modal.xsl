<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">
 
<fx:stylesheet	version="1.0"
		xmlns:fx="http://www.w3.org/1999/XSL/Transform">
 
	<fx:output encoding="utf-8" method="html" indent="yes"/>
    <fx:param name="lang-prefix"/>
    <fx:param name="template-resources" select="'/templates/proart/'"/>

	<fx:template match="/" priority="1">
        <fx:choose>
            <fx:when test="udata/@method = 'add'">
                <html>
                    <head>
                        <link type="text/css" rel="stylesheet" href="{$template-resources}css/normalize.css" />
                        <link type="text/css" rel="stylesheet" href="{$template-resources}css/main.css" />
                        <link type="text/css" rel="stylesheet" href="{$template-resources}css/media.css" />
                        <link type="text/css" rel="stylesheet" href="{$template-resources}css/jquery.arcticmodal-0.3.css" />
                        <fx:variable name="fancy">&lt;script type="text/javascript" charset="utf-8" src="/js/jquery/fancybox/</fx:variable>
                        <fx:value-of select="substring-before(document('udata://system/includeQuickEditJs')/udata,$fancy)" disable-output-escaping="yes"/>
                    </head>
                    <body style="background:none">
                        <div class="wrp clearfix">
                            <div class="callback">
                                <h5>
                                    <fx:value-of select="document(concat('utype://',udata/@form_id))/udata/type/@title"/>
                                </h5>
                                <div class="wrp custom-5">
                                    <fx:apply-templates select="udata">
                                        <fx:with-param name="ajax">no</fx:with-param>
                                    </fx:apply-templates>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript" src="{$template-resources}js/i18n.ru.js"></script>
                        <script type="text/javascript">
                            <![CDATA[]]>
                            window.top.$('#exampleModal-2 iframe').removeClass('preloader');
                            var site = {};
                            site.modal = true;
                            window.top.$('#exampleModal-2 iframe').height($('.wrp').height()+30);
                            <fx:for-each select="document('udata://system/listErrorMessages')/udata/items/item">
                                window.top.site.message({content: i18n.error + '<fx:value-of select="."/>'});
                            </fx:for-each>
                            <![CDATA[]]>
                        </script>
                        <script type="text/javascript" charset="utf-8" src="{$template-resources}js/foxi.forms.js"></script>
                        <script type="text/javascript" charset="utf-8" src="{$template-resources}js/foxi.captcha.js"></script>
                    </body>
                </html>
            </fx:when>
            <fx:otherwise>
                <fx:apply-templates/>
            </fx:otherwise>
        </fx:choose>
	</fx:template>

    <fx:include href="../library/captcha.xsl"/>
    <fx:include href="../modules/webforms/add.xsl"/>
 
</fx:stylesheet>