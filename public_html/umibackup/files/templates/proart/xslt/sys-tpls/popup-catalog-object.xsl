<?xml version="1.0" encoding="utf-8"?>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" >

    <fx:param name="lang-prefix"/>
    <fx:variable name="cart_items" select="document('udata://emarket/cart/')/udata/item/item"/>

    <fx:template match="/" priority="1">
        <fx:apply-templates select="udata/page" mode="ajax-view"/>
    </fx:template>

    <fx:include href="../library/common.xsl"/>
    <fx:include href="../modules/emarket/price.xsl"/>
    <fx:include href="../modules/catalog/ajax-view.xsl"/>

</fx:stylesheet>