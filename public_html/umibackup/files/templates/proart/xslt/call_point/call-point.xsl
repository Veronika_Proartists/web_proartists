<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"udata://custom/callPointInfo">

<fx:stylesheet xmlns:fx="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <fx:include href="../modules/&current_module;/common.xsl"/>
    <fx:include href="../modules/all-page.xsl"/>

    <fx:template match="udata[@module = '&current_module;' and @method = '&current_method;']" priority="1">
        <fx:choose>
            <fx:when test="'&current_mode;' = 'none'">
                <fx:apply-templates select="&current_uri;"/>
            </fx:when>
            <fx:otherwise>
                <fx:apply-templates select="&current_uri;" mode="&current_mode;"/>
            </fx:otherwise>
        </fx:choose>

    </fx:template>

    <fx:include href="../library/common.xsl"/>
</fx:stylesheet>
