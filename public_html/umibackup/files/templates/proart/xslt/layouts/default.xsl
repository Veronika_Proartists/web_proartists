<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
]>

<fx:stylesheet version="1.0"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:foxi="http://foxinet.ru">

    <fx:param name="template-resources"/>
	<fx:template match="/" mode="layout">

        <fx:text disable-output-escaping="yes">&lt;!doctype html&gt;</fx:text>
        <fx:comment>[if lt IE 7 ]&gt;&lt;html lang="en-gb" class="no-js lt-ie9 lt-ie8 lt-ie7"&gt; &lt;![endif]</fx:comment>
        <fx:comment>[if IE 7 ]&gt;&lt;html lang="en-gb" class="no-js lt-ie9 lt-ie8"&gt; &lt;![endif]</fx:comment>
        <fx:comment>[if IE 8 ]&gt;&lt;html lang="en-gb" class="no-js lt-ie9"&gt; &lt;![endif]</fx:comment>
        <fx:comment>[if gt IE 8 ]&gt;&lt;html lang="en-gb" class="no-js"&gt; &lt;![endif]</fx:comment>

			<head>

				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="keywords" content="{//meta/keywords}" />
				<meta name="description" content="{//meta/description}" />
                <!--<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1.0, user-scalable=no"/>-->
				<title><fx:value-of select="$document-title" /></title>
                <fx:choose>
                    <fx:when test="$user-type = 'sv'">
                        <fx:value-of select="document('udata://system/includeQuickEditJs')/udata" disable-output-escaping="yes" />
                    </fx:when>
                    <fx:otherwise>
                        <fx:variable name="fancy">&lt;script type="text/javascript" charset="utf-8" src="/js/jquery/fancybox/</fx:variable>
                        <fx:value-of select="substring-before(document('udata://system/includeQuickEditJs')/udata,$fancy)" disable-output-escaping="yes"/>
                    </fx:otherwise>
                </fx:choose>
                <meta http-equiv="Content-language" content="ru" />

                <link rel="shortcut icon" href="{foxi:thumbnail($org-prop[@name = 'favicon']/value,16,16,'fit')}"/>
                <link rel="apple-touch-icon" href="{foxi:thumbnail($org-prop[@name = 'favicon']/value,32,32,'fit')}"/>
                <link rel="apple-touch-icon" href="{foxi:thumbnail($org-prop[@name = 'favicon']/value,72,72,'fit')}" sizes="72x72"/>
                <link rel="apple-touch-icon" href="{foxi:thumbnail($org-prop[@name = 'favicon']/value,114,114,'fit')}" sizes="114x114"/>

                <!--<link rel="apple-touch-icon" href="{foxi:thumbnail($org-prop[@name = 'favicon']/value,114,114,'fit')}" sizes="114x114"/>-->
                <meta property="og:title" content="{/result/@title}"/>
                <meta property="og:type" content="website"/>
                <meta property="og:url" content="http://{/result/@domain}"/>
                <meta property="og:image" content="http://{/result/@domain}{$template-resources}images/proartists.jpg"/>
                <!--<meta property="og:image" content="http://{/result/@domain}{$org-prop[@name = 'logo']/value}"/>-->
                <meta property="og:site_name" content="{/result/@site-name}"/>
                <meta property="og:description" content="{//meta/description}"/>

                <!--<link type="text/css" rel="stylesheet" href="/min/g=css&amp;{/result/@system-build}" />-->
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/normalize.css" />
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/main.css?v=1" />
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/media.css" />
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/jquery.arcticmodal-0.3.css" />
                <link type="text/css" rel="stylesheet" href="{$template-resources}slick/slick.css"/>
                <link type="text/css" rel="stylesheet" href="{$template-resources}colorbox/colorbox.css"/>

                <fx:comment>[if IE 8]&gt;&lt;link rel="stylesheet" href="/templates/proart/css/ie8.css"&gt;&lt;![endif]</fx:comment>
                <fx:if test="result/user/@type != 'sv'">
                    <link type="text/css" rel="stylesheet" href="/js/jquery/jquery.jgrowl.css" />
                </fx:if>

			</head>
            <fx:choose>
                <fx:when test="result/@method != 'notfound'">
                    <body itemscope='' itemtype="http://schema.org/LocalBusiness">
                        <fx:comment>[if lt IE 7]&gt;&lt;p class="browsehappy"&gt;Вы используете &lt;strong&gt;устаревший&lt;/strong&gt; браузер. Пожалуйста, &lt;a rel="nofollow" href="http://browsehappy.com/"&gt;обновите Ваш браузер&lt;/a&gt;&lt;/p&gt;&lt;![endif]</fx:comment>
                        <a class="menu-trigger icon-list" href="#"></a>
                        <header class="clearfix">
                            <div class="block-1"></div>
                            <div class="block-2 clearfix">

                                <div class="slogan">
                                    <span><fx:apply-templates select="$org-prop[@name = 'title']" mode="org"/></span>
                                </div>
                                <div class="phone">
                                    <span class="custom-1">&header-phone-text;</span><br/>
                                    <span class="custom-2"><fx:apply-templates select="$org-prop[@name = 'phone']" mode="org"/></span><br/>
                                    <noindex>
                                        <a rel="nofollow" class="ajax-arctic" href="{$lang-prefix}/phone_modal/135">&order-phone;</a>
                                    </noindex>
                                </div>
                            </div>
                        </header>
                        <aside class="l-column clearfix">
                            <div class="wrapper">
                                <fx:element name="{foxi:if($is-default,'span','a')}">
                                    <fx:attribute name="class">logo</fx:attribute>
                                    <!--<fx:attribute name="style">background-image: url("<fx:value-of select="$org-prop[@name = 'logo']/value"/>");</fx:attribute>-->
                                    <fx:if test="not($is-default)"><fx:attribute name="href">/</fx:attribute></fx:if>
                                </fx:element>
                                <div class="alt-menu">
                                    <div class="wrp">
                                        <span>&menu-item-1;</span>
                                        <ul umi:element-id="0" umi:region="list" umi:module="catalog" umi:sortable="sortable" umi:button-position="top right">
                                            <fx:for-each select="document('udata://catalog/getCategoryList/temp/0/100')/udata/items/item">
                                                <li umi:element-id="{@id}" umi:region="row">
                                                    <a href="{@link}" umi:field-name="name" umi:empty="&empty-page-name;" umi:delete="delete">
                                                        <fx:value-of select="."/>
                                                    </a>
                                                    <p umi:field-name="vendor" umi:empty="&empty;">
                                                        <fx:value-of select="document(concat('upage://',@id,'.vendor'))/udata/property/value"/>
                                                    </p>
                                                </li>
                                            </fx:for-each>
                                        </ul>
                                    </div>
                                </div>
                                <ul class="menu clearfix">
                                    <li class="custom-1"><a href="#">&menu-item-1;</a></li>
                                    <li class="custom-2"><a href="/contacts/">&menu-item-2;</a></li>
                                    <li class="custom-3"><a href="/reviews/">&menu-item-3;</a></li>
                                </ul>
                                <div class="social">
                                    <fx:for-each select="document('udata://guides/getGuideItems/122/url,image')/udata/items/item">
                                        <a target="_blank" href="{.//property[@name = 'url']/value}" title="{@name}" rel="nofollow">
                                            <fx:attribute name="style">background: url("<fx:value-of select=".//property[@name = 'image']/value"/>") no-repeat scroll 0 0 rgba(0, 0, 0, 0);width: <fx:value-of select=".//property[@name = 'image']/value/@width"/>px;</fx:attribute>
                                        </a>
                                    </fx:for-each>
                                </div>
                            </div>
                            <noindex>
                                <fx:apply-templates select="document('udata://emarket/cart/')/udata" mode="basket"/>
                            </noindex>
                        </aside>
                        <div class="center">
                            <fx:apply-templates select="result"/>
                        </div>

                        <footer class="clearfix">
                            <div class="block-1">
                                <img>
                                    <fx:apply-templates select="$org-prop[@name = 'footer_logo']" mode="org"/>
                                </img>
                                <p>
                                    <span><fx:apply-templates select="$org-prop[@name = 'title']" mode="org"/></span><br/>
                                    <span><fx:apply-templates select="$org-prop[@name = 'address']" mode="org"/></span>
                                </p>
                            </div>
                            <div class="block-2">
                                <figure>
                                    <nav>
                                        <ul>
                                            <fx:for-each select="document('udata://menu/draw/info')/udata/item">
                                                <li>
                                                    <a href="{@link}"><fx:value-of select="@name"/></a>
                                                </li>
                                            </fx:for-each>
                                        </ul>
                                    </nav>
                                </figure>
                                <noindex>
                                    <p>&develop;</p>
                                </noindex>
                            </div>
                            <div class="block-3">
                                <p>&label-e-mail;: <a><fx:apply-templates select="$org-prop[@name = 'email']" mode="org"/></a><br/>
                                    &label-phone;: <span><fx:apply-templates select="$org-prop[@name = 'phone']" mode="org"/></span></p>
                                <noindex>
                                    <a rel="nofollow" class="ajax-arctic" href="{$lang-prefix}/phone_modal/135">&order-phone;</a>
                                </noindex>
                            </div>
                        </footer>
                        <fx:if test="result/user/@type != 'sv'">
                            <script type="text/javascript" charset="utf-8" src="/js/jquery/jquery.jgrowl_minimized.js"></script>
                        </fx:if>
                        <script type="text/javascript" src="{$template-resources}js/i18n.{result/@lang}.js"></script>
                        <fx:apply-templates select="result" mode="js-variable"/>
                        <fx:apply-templates select="result" mode="js-variable-additional"/>
                        <script type="text/javascript" charset="utf-8" src="{$template-resources}js/foxi.common.js?v=1"></script>
                        <fx:if test="result[@method = 'category'][count(/result/parents/page) &gt; 1]">
                            <script type="text/javascript" src="{$template-resources}js/catalog.js"/>
                        </fx:if>
                       
                        <script type="text/javascript" charset="utf-8" src="{$template-resources}js/plugins.js"></script>

                        <!-- Yandex.Metrika counter -->
                        <script type="text/javascript">
                            (function (d, w, c) {
                            (w[c] = w[c] || []).push(function() {
                            try {
                            w.yaCounter31419598 = new Ya.Metrika({
                            id:31419598,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                            });
                            } catch(e) { }
                            });

                            var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () { n.parentNode.insertBefore(s, n); };
                            s.type = "text/javascript";
                            s.async = true;
                            s.src = "https://mc.yandex.ru/metrika/watch.js";

                            if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f, false);
                            } else { f(); }
                            })(document, window, "yandex_metrika_callbacks");
                        </script>
                        <noscript><div><img src="https://mc.yandex.ru/watch/31419598" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                        <!-- /Yandex.Metrika counter -->

                    </body>
                </fx:when>
                <fx:otherwise>
                    <body id="p404">
                        <fx:apply-templates select="result"/>

                        <!-- Yandex.Metrika counter -->
                        <script type="text/javascript">
                            (function (d, w, c) {
                            (w[c] = w[c] || []).push(function() {
                            try {
                            w.yaCounter31419598 = new Ya.Metrika({
                            id:31419598,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                            });
                            } catch(e) { }
                            });

                            var n = d.getElementsByTagName("script")[0],
                            s = d.createElement("script"),
                            f = function () { n.parentNode.insertBefore(s, n); };
                            s.type = "text/javascript";
                            s.async = true;
                            s.src = "https://mc.yandex.ru/metrika/watch.js";

                            if (w.opera == "[object Opera]") {
                            d.addEventListener("DOMContentLoaded", f, false);
                            } else { f(); }
                            })(document, window, "yandex_metrika_callbacks");
                        </script>
                        <noscript><div><img src="https://mc.yandex.ru/watch/31419598" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                        <!-- /Yandex.Metrika counter -->

                    </body>
                </fx:otherwise>
            </fx:choose>

        <fx:text disable-output-escaping="yes">&lt;/html&gt;</fx:text>
	</fx:template>
</fx:stylesheet>