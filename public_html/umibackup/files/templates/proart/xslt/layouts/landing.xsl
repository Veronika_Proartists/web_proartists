<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
]>

<fx:stylesheet version="1.0"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:foxi="http://foxinet.ru">

    <fx:param name="template-resources"/>
	<fx:template match="/" mode="layout">

        <fx:text disable-output-escaping="yes">&lt;!doctype html&gt;</fx:text>
        <fx:comment>[if lt IE 7 ]&gt;&lt;html lang="en-gb" class="no-js lt-ie9 lt-ie8 lt-ie7"&gt; &lt;![endif]</fx:comment>
        <fx:comment>[if IE 7 ]&gt;&lt;html lang="en-gb" class="no-js lt-ie9 lt-ie8"&gt; &lt;![endif]</fx:comment>
        <fx:comment>[if IE 8 ]&gt;&lt;html lang="en-gb" class="no-js lt-ie9"&gt; &lt;![endif]</fx:comment>
        <fx:comment>[if gt IE 8 ]&gt;&lt;html lang="en-gb" class="no-js"&gt; &lt;![endif]</fx:comment>

			<head>

				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="keywords" content="{//meta/keywords}" />
				<meta name="description" content="{//meta/description}" />
                <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1"/>

                <meta http-equiv="x-ua-compatible" content="ie=edge"/>
				<title><fx:value-of select="$document-title" /></title>
                <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'/>
                <fx:choose>
                    <fx:when test="$user-type = 'sv'">
                        <fx:value-of select="document('udata://system/includeQuickEditJs')/udata" disable-output-escaping="yes" />
                    </fx:when>
                    <fx:otherwise>
                        <fx:variable name="fancy">&lt;script type="text/javascript" charset="utf-8" src="/js/jquery/fancybox/</fx:variable>
                        <fx:value-of select="substring-before(document('udata://system/includeQuickEditJs')/udata,$fancy)" disable-output-escaping="yes"/>
                    </fx:otherwise>
                </fx:choose>
                <meta http-equiv="Content-language" content="ru" />
                <meta property="og:title" content="{/result/@title}"/>
                <meta property="og:type" content="website"/>
                <meta property="og:url" content="http://{concat(result/@domain, result/@request-uri)}"/>
                <meta property="og:image" content="http://{/result/@domain}{$template-resources}images/proartists.jpg"/>
                <!--<meta property="og:image" content="http://{/result/@domain}{$org-prop[@name = 'logo']/value}"/>-->
                <meta property="og:site_name" content="{/result/@site-name}"/>
                <meta property="og:description" content="{//meta/description}"/>
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/normalize.css" />
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/landing/style.css?ver=28" />
                <!--<link type="text/css" rel="stylesheet" href="{$template-resources}css/jquery.arcticmodal-0.3.css" />-->
                <!--<link type="text/css" rel="stylesheet" href="{$template-resources}css/main.css" />-->
                <!--<link type="text/css" rel="stylesheet" href="{$template-resources}slick/slick.css"/>-->
                <fx:if test="result/user/@type != 'sv'">
                    <link type="text/css" rel="stylesheet" href="/js/jquery/jquery.jgrowl.css"/>
                </fx:if>
                <fx:comment>[if lt IE 9]&gt;&lt;script src="js/html5shiv.js"&gt;&lt;/script&gt;
                    &lt;script src="js/html5shiv-printshiv.js"&gt;&lt;/script&gt;&lt;![endif]</fx:comment>

                <fx:comment>[if lt IE 9]&gt;&lt;script src="js/html5shiv.js"&gt;&lt;/script&gt;
                    &lt;script src="js/html5shiv-printshiv.js"&gt;&lt;/script&gt;&lt;![endif]</fx:comment>

			</head>
            <body>
                <noindex>
                    <fx:comment>[if lt IE 8]&gt;&lt;p class="browsehappy"&gt;Вы используете &lt;strong&gt;устаревший&lt;/strong&gt; браузер. Пожалуйста, &lt;a rel="nofollow" href="http://browsehappy.com/"&gt;обновите Ваш браузер&lt;/a&gt;&lt;/p&gt;&lt;![endif]</fx:comment>
                </noindex>
                <fx:if test="$errors != ''">
                    <div class="errors" style="text-align: center; color: red;">
                        <h2>
                            <fx:text>Ошибка: </fx:text>
                            <fx:value-of select="$errors"/>
                        </h2>
                    </div>
                </fx:if>


                <fx:choose>
                    <fx:when test="result/page/@is-default = 1">
                        <fx:apply-templates select="result"/>
                    </fx:when>
                    <fx:otherwise>
                        <section class="appeal">
                            <div class="container">
                                <div class="row">
                                    <div class="fx-md-12">
                                        <div class="row">
                                            <div class="fx-sm-8">
                                                <div class="appeal__text">
                                                    <fx:apply-templates select="result"/>
                                                </div>
                                            </div>
                                            <div class="fx-sm-4">
                                                <img src="{document(concat('upage://',1049))//property[@name = 'photo']/value}"/>
                                                <span class="appeal__text">
                                                    <fx:value-of select="document(concat('upage://',1049))//property[@name = 'after_photo']/value" disable-output-escaping="yes"/>
                                                    <fx:if test="result/@method = 'notfound'">
                                                        <a href="tel:+79119228665">+7 (911) 922-86-65</a>
                                                    </fx:if>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fx:otherwise>
                </fx:choose>



                <section class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="footer__phone"><a href="tel:+79119228665">+7 (911) 922-86-65</a></div>
                            </div>
                        </div>
                    </div>
                </section>



                <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
                <!--&lt;!&ndash;<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
                <!--<script src="{$template-resources}js/landing/jquery.arcticmodal-0.3.min.js"></script>
                <script src="{$template-resources}js/landing/script.js"></script>&ndash;&gt;-->
                <fx:if test="result/user/@type != 'sv'">
                    <script type="text/javascript" charset="utf-8" src="/js/jquery/jquery.jgrowl_minimized.js"></script>
                </fx:if>

                <script type="text/javascript" src="{$template-resources}js/i18n.{result/@lang}.js"></script>
                <fx:apply-templates select="result" mode="js-variable"/>
                <script type="text/javascript" charset="utf-8" src="{$template-resources}js/foxi.common.js"></script>
                <script type="text/javascript" charset="utf-8" src="{$template-resources}js/plugins.js"></script>
                <script src="{$template-resources}js/landing/slick.min.js"></script>
                <script src="{$template-resources}js/landing/jquery.mask.min.js"></script>
                <script src="{$template-resources}js/landing/script.js?v=12"></script>
                </body>

        <fx:text disable-output-escaping="yes">&lt;/html&gt;</fx:text>
	</fx:template>


    <fx:template match="result[page/@is-default = 1]" priority="100">
        <!--Шапка-->

        <header class="header">

            <div class="mobile-nav">
                <div class="menu-btn" id="menu-btn">
                                    <span></span>
                    <span></span>
                    <span></span>
                </div>

                <div class="responsive-menu">
                    <ul class="responsive-menu-list">
                        <li class="responsive-menu-item"><a class="responsive-menu-link custom-6" href="#buy521">Наборы для живописи</a></li>
                        <li class="responsive-menu-item"><a class="responsive-menu-link custom-7" href="#buy523">Наборы для графики и рисования</a></li>

                        <li class="responsive-menu-item"><a class="responsive-menu-link custom-8" href="#review">Отзывы</a></li>
                        <li class="responsive-menu-item"><a class="responsive-menu-link custom-4" href="#contacts">Контакты</a></li>
                        <li class="responsive-menu-item"><a class="responsive-menu-link custom-5" href="#garant">Гарантии</a></li>
                    </ul>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="fx-md-12">
                        <div class="header__row">


                            <div class="header__cell">
                                         <a href="/" class="header__logo">
                                    <img src="{.//property[@name = 'logo']/value}"/>
                                </a>
                            </div>
                            <div class="header__cell hidden-sm hidden-ms hidden-xs">
                                <span class="header__text">
                                    <fx:value-of select=".//property[@name = 'anons_descr']/value" disable-output-escaping="yes"/>
                                </span>
                            </div>
                            <div class="header__cell">
                                <div class="phone">
                                    <span class="header-phone-text-premium">&header-phone-text;</span><br/>
                                    <span class="header-phone-number-premium"><fx:apply-templates select="$org-prop[@name = 'phone']" mode="org"/></span><br/>
                                    <a class="ajax-arctic" href="{$lang-prefix}/phone_modal/135">&order-phone;</a>
                                </div>
                                <!--<p class="header__phone">
                                    <fx:value-of select=".//property[@name = 'phone']/value" disable-output-escaping="yes"/>
                                </p>-->
                                <!--<p class="header__address">
                                    <fx:value-of select=".//property[@name = 'address']/value" disable-output-escaping="yes"/>
                                </p>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!--Слайдер-->
        <section class="sub">
            <div class="container">
                <div class="row">
                    <div class="fx-md-12">
                        <div class="sub__inner">
                            <h1 class="sub__title">
                                <fx:value-of select=".//property[@name = 'h1']/value" disable-output-escaping="yes"/>
                            </h1>
                            <h2 class="sub__subtitle">
                                <fx:value-of select=".//property[@name = 'h2_content']/value" disable-output-escaping="yes"/>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Преимущества-->
        <section class="benefit">
            <div class="container">
                <div class="row">
                    <div class="contacts" id="contacts">
                        <h3 class="contacts__title">Интернет магазин эксклюзивных товаров для рисования, живописи и графики с доставкой по всей России</h3>
                        <div class="contacts__wrap">
                            <p class="contacts__item">Адрес: г.Санкт-Петербург, Выборгское шоссе, д.15</p>
                            <p class="contacts__item">Телефон: +7 (911) 922-86-65</p>
                            <p class="contacts__item">Email: info@proartists.ru</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="container">
                <div class="row">
                    <fx:for-each select="document('udata://guides/getGuideItems/202/img,advantage?expire=3600')/udata/items/item">
                        <div class="fx-md-3 fx-sm-6">
                            <figure class="benefit__item">
                                <p class="benefit__img">
                                    <img src="{extended/properties/property[@name = 'img']/value}"/>
                                </p>
                                <figcaption class="benefit__text">
                                    <fx:value-of select="extended/properties/property[@name = 'advantage']/value" disable-output-escaping="yes"/>
                                </figcaption>
                            </figure>
                        </div>
                    </fx:for-each>
                </div>
            </div>

        </section>
        <section class="show">
            <div class="container">
            <div class="row">
                <div class="fx-md-8">
                    <div class="row">
                        <div class="fx-sm-6">
                            <div class="appeal__text">
                                <img src="{.//property[@name = 'photo']/value}"/>
                                <!--<span class="appeal__text black">
                                    <fx:value-of select=".//property[@name = 'after_photo']/value" disable-output-escaping="yes"/>
                                </span>-->

                            </div>
                        </div>
                        <div class="fx-sm-6">
                            <fx:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes"/>
                        </div>
                    </div>
                </div>

            </div>
            </div>
        </section>
        <!--Витрина-->
        <section id="buy" class="buy">
            <div class="buy__box">
                <div class="container">
                    <fx:apply-templates select=".//group[@name = 'showwindow']" mode="showwindow"/>
                </div>
            </div>
        </section>

        <!--Контент-контакт-->
        <section id="appeal" class="appeal">
            <div class="container hide">
                <div class="row">
                    <div class="fx-md-8">
                        <div class="row">
                            <div class="fx-sm-6">
                                <div class="appeal__text">
                                    <fx:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes"/>
                                </div>
                            </div>
                            <div class="fx-sm-6">
                                <img src="{.//property[@name = 'photo']/value}"/>
                                <span class="appeal__text">
                                    <fx:value-of select=".//property[@name = 'after_photo']/value" disable-output-escaping="yes"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="fx-md-4">
                        <section class="order">
                            <h4 class="order__title">Заказать набор</h4>
                            <fx:apply-templates select="document('udata://webforms/add/199?expire=3600')/udata" mode="premium_landing_form"/>
                            <p class="order__sub">Данные используются только для связи с вами в момент заказа и покупки.</p>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <!--Отзывы-->
        <section id="review" class="review">
            <div class="container">
                <div class="row">
                    <div class="fx-md-12">
                        <div class="review__title">Отзывы наших клиентов</div>
                    </div>
                </div>
                <div class="row">
                    <fx:for-each select="document('udata://guides/getGuideItems/200/fname_client,photo,rewiev?expire=3600')/udata/items/item">
                        <div class="fx-md-4">
                            <div class="review__item clearfix">
                                <div class="review__img">
                                    <img src="{extended/properties/property[@name = 'photo']/value}"/>
                                </div>
                                <p class="review__name">
                                    <fx:value-of select="extended/properties/property[@name = 'fname_client']/value"/>
                                </p>
                                <span class="review__text">
                                    <fx:value-of select="extended/properties/property[@name = 'rewiev']/value" disable-output-escaping="yes"/>
                                </span>
                            </div>
                        </div>
                    </fx:for-each>
                </div>
            </div>
        </section>

        <!--Гарантии-->
        <section id="garant" class="garant">
            <div class="container">
                <div class="row">
                    <div class="fx-md-12">
                        <div class="garant__title">Гарантируем:</div>
                    </div>
                </div>
                <div class="row">
                    <fx:for-each select="document('udata://guides/getGuideItems/201/guarantee,image,anons?expire=3600')/udata/items/item">
                        <div class="fx-sm-4">
                            <div class="garant__item">
                                <div class="garant__img">
                                    <img src="{extended/properties/property[@name = 'image']/value}"/>
                                </div>
                                <p class="garant__name">
                                    <fx:value-of select="extended/properties/property[@name = 'guarantee']/value" disable-output-escaping="yes"/>
                                </p>
                                <p class="garant__text">
                                    <fx:value-of select="extended/properties/property[@name = 'anons']/value" disable-output-escaping="yes"/>
                                </p>
                            </div>
                        </div>
                    </fx:for-each>
                </div>
                <div class="row">
                    <div class="fx-md-12">
                        <p class="garant__sub">
                            Персональные данные, переданные через данный сайт администрации сайта не будут переданы третьим
                            лицам, согласно закону об обработке персональных данных. Оставляя свои данные в контактной форме вы
                            автоматически подтвержаете согласие на сбор и обработку данных. Информация, представленная на сайте
                            не является договором-офертой, носит лишь ознакомительный и рекламный характер.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </fx:template>

    <!--Работа Витрины-->
    <fx:template match="group" mode="showwindow">
        <fx:apply-templates select="property" mode="property_showwindow"/>
    </fx:template>

    <fx:template match="property[position() mod 2 = 1]" mode="property_showwindow">
        <!--Вызываем заголовки-->
        <fx:apply-templates select="." mode="title_showwindow"/>
    </fx:template>

    <fx:template match="property[position() mod 2 = 0]" mode="property_showwindow">
        <!--Вызываем товары-->
        <fx:apply-templates select="." mode="objects_showwindow"/>
    </fx:template>

    <fx:template match="property" mode="title_showwindow">
        <div class="row">
            <div class="fx-md-12">
                <h3 class="buy__title" id="buy{@id}">
                    <fx:value-of select="value"/>
                </h3>
            </div>
        </div>
    </fx:template>

    <fx:template match="property" mode="objects_showwindow">
        <fx:apply-templates select="value/page[position() mod 3 = 1] " mode="item_mod3_premium"/>
    </fx:template>

    <fx:template match="page" mode="item_mod3_premium">
        <div class="row">
            <fx:apply-templates select="." mode="short_view_item_premium"/>
            <fx:apply-templates select="following-sibling::page[position() &lt; 3]" mode="short_view_item_premium"/>
        </div>
    </fx:template>

    <!--Сам товар-->
    <fx:template match="page" mode="short_view_item_premium">

            <div class="card">
                <div class="row">
                    <div class="fx-md-12">
                        <header class="card__header">
                            <fx:value-of select="name"/>
                        </header>
                    </div>
                </div>
                <div class="row">
                    <div class="fx-md-12">
                        <span class="card__descr">
                            <fx:choose>
                                <fx:when test="document(concat('upage://',@id,'.anons_descr_landing'))/udata/property/value != ''">
                                    <fx:value-of select="document(concat('upage://',@id,'.anons_descr_landing'))/udata/property/value" disable-output-escaping="yes"/>
                                </fx:when>
                                <fx:otherwise>
                                    <fx:value-of select="document(concat('upage://',@id,'.descr_mini'))/udata/property/value"/>
                                </fx:otherwise>
                            </fx:choose>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="fx-md-12">
                        <a class="card__img arctic" href="/offer_more_info/{@id}">
                            <fx:variable name="image" select="document(concat('upage://',@id,'.photo'))/udata/property/value"/>
                            <img src="{document(concat('udata://system/makeThumbnailFull/(.', $image, ')/302/236/default/0/1/'))/udata/src}"/>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="fx-md-12">
                        <span class="card__price">
                            <fx:value-of select="document(concat('upage://',@id,'.price'))/udata/property/value"/>
                            <fx:text> руб</fx:text>
                        </span>
                    </div>
                </div>
                <div class="card__btn clearfix">

                            <a href="/offer_more_info/{@id}" class="btn _gray arctic card__more">Подробнее</a>

                            <a href="/fast_basket_landing/{@id}" class="btn arctic _dark">Купить</a>

                </div>
            </div>
    </fx:template>

</fx:stylesheet>