<?xml version="1.0" encoding="utf-8"?>

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform">
	
	<fx:include href="menu.xsl" />
	<fx:include href="result.xsl" />
	<fx:include href="404.xsl" />
	<fx:include href="sitemap.xsl" />

</fx:stylesheet>