<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.custom.dtd:file">

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<fx:template match="/result[@method = 'notfound']" priority="1">
		<fx:variable name="error-page" select="document(concat('uhttp://', @domain, @pre-lang, '/notfound/.xml'))/result/page" />
        <table class="p404">
            <tbody>
                <tr>
                    <td umi:element-id="{$error-page/@id}" umi:field-name="content">
                        <img src="{$template-resources}&p404-image;"/>
                        <fx:value-of select="$error-page//property[@name = 'content']/value" disable-output-escaping="yes" />
                    </td>
                </tr>
            </tbody>
        </table>
	</fx:template>
</fx:stylesheet>