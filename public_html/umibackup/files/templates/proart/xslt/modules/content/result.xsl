<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
    xmlns:xlink="http://www.w3.org/TR/xlink">

    <fx:template match="result[@method = 'content' or @method = 'item']">
        <fx:apply-templates select="." mode="main-banner"/>
        <div class="content clearfix">
            <fx:apply-templates select=".//property[@name = 'quote']/value/item"/>
            <h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="{.//property[@name = 'h1']/title}">
                <fx:value-of select=".//property[@name = 'h1']/value"/>
            </h1>
            <div umi:field-name="content" umi:empty="&empty-page-content;">
                <fx:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes"/>
            </div>
        </div>
    </fx:template>


    <fx:template match="result[page/@is-default = '1']" priority="1">
        <fx:apply-templates select="." mode="main-banner"/>
        <div class="slide-2 clearfix">
            <img>
               <fx:apply-templates select="$org-prop[@name = 'slide2_pic']" mode="org"/>
            </img>
            <div class="block-1" umi:element-id="{@pageId}" umi:field-name="content" umi:empty="&empty-page-content;">
                <fx:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes"/>
            </div>
        </div>
        <fx:apply-templates select="document('usel://getAction')/udata/item" mode="action"/>
        <fx:apply-templates select="document('udata://news/lastlist/(news)/none/2')/udata"  mode="main-news" />
        <fx:apply-templates select="document('udata://photoalbum/albumRandom/(your_works)/none/8/1')/udata"  mode="main-album" />
    </fx:template>
	

</fx:stylesheet>