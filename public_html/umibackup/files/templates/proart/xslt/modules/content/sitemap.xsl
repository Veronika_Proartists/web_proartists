<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<fx:template match="result[@module = 'content'][@method = 'sitemap']" priority="1">
		<fx:apply-templates select="document('udata://content/sitemap')" />
	</fx:template>
	
	<fx:template match="udata[@method = 'sitemap']">
		<fx:apply-templates mode="sitemap" />
	</fx:template>
	
	<fx:template match="items" mode="sitemap">
		<ul umi:element-id="{parent::node()/@id}" class="test"
			umi:module="content" umi:method="sitemap" umi:region="list" umi:sortable="sortable">
			<fx:apply-templates mode="sitemap" />
		</ul>
	</fx:template>
	
	<fx:template match="item" mode="sitemap">
		<li umi:element-id="{@id}" umi:region="row">
			<a href="{@link}" umi:field-name="name" umi:delete="delete" umi:empty="&empty-section-name;">
				<fx:value-of select="@name" />
			</a>
			<fx:apply-templates mode="sitemap" />
		</li>
	</fx:template>

</fx:stylesheet>