<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi">


	<!-- Header menu -->
	<fx:template match="udata[@module = 'usel']" mode="info-pages-head">
		<ul umi:element-id="0" umi:module="content" umi:add-method="none" umi:region="list" umi:sortable="sortable">
			<fx:apply-templates select="page" mode="info-pages-head" />
		</ul>
	</fx:template>

	<fx:template match="udata[@module = 'usel']/page" mode="info-pages-head">
		<li umi:element-id="{@id}" umi:region="row">
			<a href="{@link}" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete">
				<fx:value-of select="name" />
			</a>
		</li>
	</fx:template>

	<fx:template match="udata[@module = 'usel']/page[@alt-name = 'help']" mode="info-pages-head">
		<li umi:element-id="{@id}" umi:region="row">
			<a href="{@link}" class="help" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete">
				<fx:value-of select="name" />
			</a>
		</li>
	</fx:template>

	<!-- Top menu -->
	<fx:template match="udata[@method = 'menu']" mode="top_menu">
		<div id="top_menu" umi:element-id="0" umi:module="content" umi:region="list" umi:sortable="sortable" umi:add-method="popup">
			<fx:apply-templates select="items/item" mode="top_menu" />
		</div>
	</fx:template>

	<fx:template match="item" mode="top_menu">
		<a href="{@link}" umi:element-id="{@id}" umi:region="row" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete">
			<fx:if test="position() = last()">
                <fx:attribute name="class">last</fx:attribute>
			</fx:if>
            <fx:value-of select="." />
		</a>
	</fx:template>


	<!-- Footer menu -->
	<fx:template match="udata" mode="info-pages-foot">
		<div class="links" umi:element-id="0" umi:module="content" umi:add-method="none" umi:region="list" umi:sortable="sortable">
			<a href="{document('udata://content/getMobileModesList/')//item[@status='active']/@link}">&mobile-mode-on;</a>
			<fx:apply-templates select="page" mode="info-pages-foot" />
		</div>
	</fx:template>

	<fx:template match="page" mode="info-pages-foot">
		<a href="{@link}" umi:element-id="{@id}" umi:field-name="name" umi:empty="&empty-section-name;" umi:delete="delete" umi:region="row">
			<fx:value-of select="name" />
		</a>
	</fx:template>
</fx:stylesheet>