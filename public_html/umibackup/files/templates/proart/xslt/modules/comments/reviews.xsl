<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                xmlns:umi="http://www.umi-cms.ru/TR/umi"
                xmlns:foxi="http://foxinet.ru"
				exclude-result-prefixes="fx foxi">

    <fx:template match="result[page/@alt-name = 'reviews']">
        <fx:apply-templates select="document(concat('udata://custom/comments/insert/',page/@id,'?extProps=author_id,message&amp;extGroups=author&amp;uri=items/item'))/udata"/>
    </fx:template>

    <fx:template match="udata[@module = 'usel' and @method = 'reviews']|udata[@module = 'comments' and @method = 'insert']">
        <fx:if test="action">
            <fx:apply-templates select="." mode="form"/>
        </fx:if>
        <script type="text/javascript">
            <![CDATA[]]>
            $(document).ready(function(){
            <fx:for-each select="document('udata://system/listErrorMessages')/udata/items/item">
                site.message({content: '<fx:value-of select="."/>'});
            </fx:for-each>
            });
            <![CDATA[]]>
        </script>
        <div class="reviews">
            <div class="block-1 clearfix">
                <h1>
                    <fx:value-of select="document(concat('upage://',foxi:if(action,$document-page-id,page/@parentId),'.h1'))/udata/property/value"/>
                    <fx:if test="action">
                       <fx:text> (</fx:text>
                        <fx:value-of select="document(concat('udata://comments/countComments/',$document-page-id))/udata"/>
                       <fx:text>)</fx:text>
                    </fx:if>
                </h1>
            </div>
            <div class="block-2 clearfix">
                <ul class="clearfix">
                    <fx:if test="@call_point">
                        <fx:attribute name="umi:call-point"><fx:value-of select="@call_point"/></fx:attribute>
                        <!--<fx:attribute name="umi:button">1</fx:attribute>-->
                    </fx:if>
                    <fx:apply-templates select="page"/>
                    <fx:apply-templates select="items/item"/>
                </ul>
                <fx:if test="@call_point">
                    <a href="/reviews/" class="custom-14">&comments-more;</a>
                </fx:if>
            </div>
        </div>

    </fx:template>



    <fx:template match="udata[@module = 'usel' and @method = 'reviews']/page|udata[@module = 'comments' and @method = 'insert']/items/item">
        <fx:variable name="author" select="document(concat('uobject://',.//property[@name = 'author_id']/value/item/@id))/udata"/>
        <fx:variable name="user" select="document(concat('uobject://',$author//property[@name = 'user_id']/value/item/@id))/udata"/>
        <fx:variable name="comment" select="document(concat('upage://',@id))/udata"/>
        <li class="clearfix load_element" umi:element-id="{@id}">
            <img>
                <fx:attribute name="src">
                    <fx:choose>
                        <fx:when test=".//property[@name = 'avatar']/value != ''">
                            <fx:value-of select="foxi:thumbnail(.//property[@name = 'avatar']/value,240,240,'fit')"/>
                        </fx:when>
                        <fx:when test="$user//property[@name = 'avatar']/value">
                            <fx:value-of select="foxi:thumbnail($user//property[@name = 'avatar']/value,240,240,'fit')"/>
                        </fx:when>
                        <fx:otherwise>&comment-no-photo;</fx:otherwise>
                    </fx:choose>
                </fx:attribute>
            </img>
            <div class="block-3">
                <h3>
                    <fx:choose>
                        <fx:when test=".//property[@name = 'author_name']/value != ''">
                            <fx:value-of select=".//property[@name = 'author_name']/value"/>
                        </fx:when>
                        <fx:when test="$author//property[@name = 'nickname']">
                            <fx:value-of select="$author//property[@name = 'nickname']/value"/>
                        </fx:when>
                        <fx:otherwise>
                            <fx:value-of select="$user//property[@name = 'fname']/value"/>
                            <fx:text> </fx:text>
                            <fx:value-of select="$user//property[@name = 'lname']/value"/>
                        </fx:otherwise>
                    </fx:choose>
                    </h3>
                    <p class="custom-1">
                        <fx:value-of select="foxi:upper-case(string(.//property[@name = 'profession']/value))"/>
                    </p>
                    <p>
                        <fx:value-of select=".//property[@name = 'message']/value" disable-output-escaping="yes"/>
                    </p>
                <fx:if test="$comment//group[@name='photos']/property">
                    <ul class="comments_photo">
                        <fx:apply-templates select="$comment//group[@name='photos']/property[@type='img_file' and string-length(value/node()) &gt; 0]" mode="comments_photo"/>
                    </ul>
                </fx:if>
                </div>
            </li>
    </fx:template>

    <fx:template match="property" mode="comments_photo">
        <li>
            <a href="{value}" rel="colorbox[{//udata/page/@id}]">
                <div class="hover">
                    <div class="box">
                       <!-- <p class="custom-1" umi:field-name="name" umi:empty="&empty;" umi:delete="delete">
                            <fx:value-of select="text()"/>
                        </p>
                        <p class="custom-2" umi:field-name="name" umi:empty="&empty;">
                            <fx:value-of select="document(concat('upage://',@id,'.autor'))/udata/property/value"/>
                        </p>-->
                    </div>
                </div>
                <fx:call-template name="catalog-thumbnail">
                    <fx:with-param name="element-id" select="//udata/page/@id" />
                    <fx:with-param name="field-name" select="@name" />
                    <fx:with-param name="empty"><fx:value-of select="$empty-photo"/></fx:with-param>
                    <fx:with-param name="width">220</fx:with-param>
                    <fx:with-param name="height">220</fx:with-param>
                    <fx:with-param name="no-fix">1</fx:with-param>
                    <fx:with-param name="mode">default</fx:with-param>
                </fx:call-template>
            </a>
        </li>
    </fx:template>

</fx:stylesheet>