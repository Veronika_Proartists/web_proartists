<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
]>

<fx:stylesheet	version="1.0"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				xmlns:xlink="http://www.w3.org/TR/xlink"
				xmlns:fb="http://www.facebook.com/2008/fbml"
				exclude-result-prefixes="fx umi xlink fb">


	<fx:template match="udata[@module = 'comments' and @method = 'insert']" mode="form">
        <div class="personal-information">
            <p class="custom-10">&comments-add;:</p>
            <form method="post" action="/comments/post_custom/{$document-page-id}" onsubmit="site.forms.data.save(this); return site.forms.data.check(this);" enctype="multipart/form-data">
                <fx:if test="$user-type = 'guest'">
                    <div class="row">
                        <label>
                            <p class="custom-9">&comment-you-name;:</p>
                            <input name="author_nick" class="txt required add_error" type="text"/>
                        </label>
                    </div>
                    <div class="row">
                        <fx:apply-templates select="document('udata://users/getLoginza')/udata"/>
                    </div>
                </fx:if>
                <div class="row">
                    <label>
                        <p class="custom-9">&comment-profession;:</p>
                        <input name="data[new][profession]" class="txt" type="text"/>
                    </label>
                </div>
                <div class="row">
                    <label>
                        <p class="custom-9">&comment-body;</p>
                        <textarea class="required add_error" name="comment"></textarea>
                    </label>
                </div>

                    <div class="row">
                        <p class="custom-9">Прикрепить фото:</p>
                        <div class="file-uploader-block">
                            <div class="item-img">
                                <input id="image-item-1" type="file" name="data[new][img_1]" accept="image"/>
                            </div>
                        </div>
                </div>
                   <!-- <div id="uploader">
                        <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
                    </div>-->

                <fx:apply-templates select="document('udata://system/captcha/')/udata" />
                <div class="row">
                    <p class="custom-9"></p>
                    <input type="submit" class="button-1" value="&comments-add;"/>
                </div>
            </form>
        </div>
	</fx:template>

    <fx:template match="udata[@method='getLoginza']">
        <script src="http://loginza.ru/js/widget.js" type="text/javascript"></script>
        <p class="custom-9">&comments-loginza;:</p>
        <div class="box">
            <a href="{widget_url}vkontakte" class="vk-1 loginza" alt="Войти через loginza"></a>
            <a href="{widget_url}facebook" class="fb-1 loginza" alt="Войти через loginza"></a>
            <a href="{widget_url}twitter" class="tw-1 loginza" alt="Войти через loginza"></a>
            <a href="{widget_url}odnoklassniki" class="od-1 loginza" alt="Войти через loginza"></a>
        </div>
    </fx:template>

</fx:stylesheet>