<?xml version="1.0" encoding="UTF-8"?>

<fx:stylesheet version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<fx:template match="result[@module = 'comments'][@method = 'comment']">
        <fx:value-of select="document(concat('udata://content/redirect/',page/@parentId))"/>
	</fx:template>

</fx:stylesheet>