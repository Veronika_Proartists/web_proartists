<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">

	<fx:include href="comments-list.xsl" />
	<fx:include href="comment-view.xsl" />

</fx:stylesheet>