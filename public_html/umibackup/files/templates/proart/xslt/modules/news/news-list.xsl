<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.custom.dtd:file">

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

	<fx:template match="/result[@module = 'news'][@method = 'rubric']">
        <fx:apply-templates select="." mode="main-banner"/>
        <fx:apply-templates select="document(concat('udata://custom/news/lastlist/',$document-page-id,'?uri=items/item&amp;mode=main-news'))"/>
	</fx:template>

    <fx:template match="udata[@module = 'news' and @method = 'lastlist']"/>

    <fx:template match="udata[@module = 'news' and @method = 'lastlist'][items]">
        <div class="slide-4 clearfix">
            <ul umi:element-id="{category_id}" umi:region="list" umi:sortable="none" umi:call-point="{@call_point}"
                umi:module="news" umi:method="lastlist" umi:button-position="top right">
                <fx:apply-templates select="items/item" mode="main-news" />
            </ul>
            <fx:if test="total &gt; per_page">&load-more;</fx:if>
        </div>
    </fx:template>

</fx:stylesheet>