<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
	xmlns:xlink="http://www.w3.org/TR/xlink"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str">

	<fx:template match="udata" mode="main-news" />

	<fx:template match="udata[items]" mode="main-news">
        <div class="slide-4 clearfix">
            <ul umi:element-id="{category_id}" umi:region="list" umi:sortable="none"
                umi:module="news" umi:method="lastlist" umi:button-position="top right">
                <fx:apply-templates select="items/item" mode="main-news" />
            </ul>
            <div style="text-align:center">
                <a class="custom-14" href="{document(concat('upage://',category_id))/udata/page/@link}">&news-all;</a>
            </div>
        </div>
	</fx:template>

	<fx:template match="item" mode="main-news">
        <li class="clearfix load_element" umi:element-id="{@id}" umi:region="row">
            <h3 umi:field-name="name" umi:delete="delete" umi:empty="&empty-page-name;">
                <fx:value-of select="." />
            </h3>
            <span class="date" umi:field-name="publish_time" umi:empty="&empty-page-date;">
                <fx:value-of select="document(concat('udata://custom/dateru/', @publish_time, '/(d%20F,%20Y)'))/udata" />
            </span>
            <fx:call-template name="catalog-thumbnail">
                <fx:with-param name="element-id" select="@id" />
                <fx:with-param name="field-name">anons_pic</fx:with-param>
                <fx:with-param name="empty">&empty-photo;</fx:with-param>
                <fx:with-param name="width">191</fx:with-param>
                <fx:with-param name="height">142</fx:with-param>
                <fx:with-param name="mode">default</fx:with-param>
                <fx:with-param name="alt">
                    <fx:value-of select="$document-title"/>
                </fx:with-param>
                <fx:with-param name="title">
                    <fx:value-of select="$document-title"/>
                </fx:with-param>
            </fx:call-template>
            <div class="description" umi:field-name="anons" umi:empty="&empty-page-anons;">
                <fx:variable name="anons" select="document(concat('upage://',@id,'.anons'))/udata/property/value"/>
                <fx:variable name="detimiler">&lt;/p&gt;</fx:variable>
                <fx:variable name="replace">&lt;a href="<fx:value-of select="@link"/>"&gt;&news-read-more;&lt;/a&gt;&lt;/p&gt;</fx:variable>
                <fx:for-each select="str:split($anons,$detimiler)">
                    <fx:choose>
                        <fx:when test="position() = last()">
                            <fx:value-of select="concat(.,$replace)" disable-output-escaping="yes"/>
                        </fx:when>
                        <fx:otherwise>
                            <fx:value-of select="concat(.,'&lt;/p&gt;')" disable-output-escaping="yes"/>
                        </fx:otherwise>
                    </fx:choose>
                </fx:for-each>
            </div>
        </li>
	</fx:template>
</fx:stylesheet>