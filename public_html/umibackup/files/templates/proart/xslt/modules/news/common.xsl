<?xml version="1.0" encoding="utf-8"?>
<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">
	<fx:include href="news-list.xsl" />
	<fx:include href="news-item.xsl" />
	<fx:include href="main-news.xsl" />
</fx:stylesheet>