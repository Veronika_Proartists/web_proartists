<?xml version="1.0" encoding="UTF-8"?>

<fx:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="fx date udt xlink">

	<fx:param name="template" />

	<fx:template match="result[@module = 'webforms'][@method = 'posted']">
		<div>
			<fx:apply-templates select="document(concat('udata://webforms/posted/', $template,'/'))/udata" />
		</div>
	</fx:template>

	<fx:template match="udata[@module = 'webforms'][@method = 'posted']">
		<fx:value-of select="." disable-output-escaping="yes" />
	</fx:template>

    <fx:template match="udata[@module = 'webforms'][@method = 'posted']" mode="iframe">
        <fx:param name="template-resources" select="'/templates/proart/'"/>
        <html>
            <head>
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/normalize.css" />
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/main.css" />
                <link type="text/css" rel="stylesheet" href="{$template-resources}css/media.css" />
                <fx:variable name="fancy">&lt;script type="text/javascript" charset="utf-8" src="/js/jquery/fancybox/</fx:variable>
                <fx:value-of select="substring-before(document('udata://system/includeQuickEditJs')/udata,$fancy)" disable-output-escaping="yes"/>
            </head>
            <body style="background:none">
                <div class="wrp clearfix">
                    <div class="callback">
                        <fx:value-of select="." disable-output-escaping="yes" />
                    </div>
                </div>
                <script type="text/javascript">

                    window.top.$('#exampleModal-2 iframe').height($('.callback').height()+30);
                </script>
            </body>
        </html>

	</fx:template>

</fx:stylesheet>