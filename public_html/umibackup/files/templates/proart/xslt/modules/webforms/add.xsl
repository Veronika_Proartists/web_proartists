<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="fx date udt xlink">


    <fx:template match="udata[@module = 'webforms'][@method = 'add']" mode="premium_landing_form">
        <fx:param name="ajax" select="'yes'"/>
        <form class="order__form" id="webform{/udata/@form_id}" method="post" action="{$lang-prefix}/webforms/send/" onsubmit="site.forms.data.save(this); return site.forms.data.check(this)" enctype="multipart/form-data">
            <fx:if test="$ajax = 'yes'">
                <fx:attribute name="onsubmit">site.forms.data.save(this); if(site.forms.data.check(this)) site.forms.phone.order(this);return false</fx:attribute>
            </fx:if>
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="/udata/custom/call_point/{document(concat('udata://custom/webforms/posted/',/udata/@form_id,'/?composite&amp;mode=iframe'))/udata/@call_point}" />
            <fx:apply-templates select="items" mode="address" />
            <fx:apply-templates select="groups/group" mode="webforms_landing"/>
            <!--<fx:apply-templates select="document('udata://system/captcha/')/udata"  mode="landing"/>-->
            <div class="order__label _btn">
                <input type="submit" class="order__btn" value="Заказать!"/>
            </div>
        </form>
    </fx:template>

    <fx:template match="group" mode="webforms_landing">
        <fx:apply-templates select="field" mode="webforms_landing" />
    </fx:template>

    <fx:template match="field" mode="webforms_landing">
        <label class="order__label">
            <span class="order__name">
                <fx:apply-templates select="." mode="webforms_required_landing_title" />
                <fx:value-of select="@title"/>
                <fx:text>: </fx:text>
            </span>
            <input type="text" name="{@input_name}" class="order__input">
                <fx:apply-templates select="." mode="webforms_required_landing_input" />
            </input>
            <fx:if test="@name = 'nabor'">
                <span class="order__hint">название фирмы и количество цветов</span>
            </fx:if>
        </label>
    </fx:template>

    <fx:template match="field" mode="webforms_required_landing_title" />
    <fx:template match="field[@required = 'required']" mode="webforms_required_landing_title">
        <fx:attribute name="class">order__name _required</fx:attribute>
    </fx:template>

    <fx:template match="field" mode="webforms_required_landing_input" />
    <fx:template match="field[@required = 'required']" mode="webforms_required_landing_input">
        <fx:attribute name="class">order__input required</fx:attribute>
    </fx:template>


	<fx:template match="udata[@module = 'webforms'][@method = 'add']">
        <fx:param name="ajax" select="'yes'"/>
        <form id="webform{/udata/@form_id}" method="post" action="{$lang-prefix}/webforms/send/" onsubmit="site.forms.data.save(this); return site.forms.data.check(this)" enctype="multipart/form-data">
            <fx:if test="$ajax = 'yes'">
                <fx:attribute name="onsubmit">site.forms.data.save(this); if(site.forms.data.check(this)) site.forms.phone.order(this);return false</fx:attribute>
            </fx:if>
            <input type="hidden" name="system_form_id" value="{/udata/@form_id}" />
            <input type="hidden" name="ref_onsuccess" value="/udata/custom/call_point/{document(concat('udata://custom/webforms/posted/',/udata/@form_id,'/?composite&amp;mode=iframe'))/udata/@call_point}" />
            <fx:apply-templates select="items" mode="address" />
            <fx:apply-templates select="groups/group" mode="webforms" />
            <fx:apply-templates select="document('udata://system/captcha/')/udata"  mode="form"/>
            <input type="submit" class="button-1" placeholder="Отправить"/>
        </form>
	</fx:template>

	<fx:template match="group" mode="webforms">
		<fx:apply-templates select="field" mode="webforms" />
	</fx:template>

    <fx:template match="field" mode="webforms">
        <div class="custom-{position()}"></div>
        <fx:apply-templates select="." mode="webforms_input_type" />
    </fx:template>

    <fx:template match="field[starts-with(@name,'offer')]" mode="webforms">
        <input type="hidden" name="{@input_name}"/>
    </fx:template>

	<fx:template match="field" mode="webforms_input_type">
		<input placeholder="{@title}" type="text" name="{@input_name}">
            <fx:apply-templates select="." mode="webforms_required" />
		</input>
	</fx:template>

	<fx:template match="field[@type = 'text' or @type='wysiwyg']" mode="webforms_input_type">
		<textarea placeholder="{@title}" name="{@input_name}">
            <fx:apply-templates select="." mode="webforms_required" />
		</textarea>
	</fx:template>

	<fx:template match="field[@type = 'password']" mode="webforms_input_type">
		<input placeholder="{@title}" type="password" name="{@input_name}" value="">
            <fx:apply-templates select="." mode="webforms_required" />
		</input>
	</fx:template>

	<fx:template match="field[@type = 'boolean']" mode="webforms_input_type">
		<input type="hidden" id="{@input_name}" name="{@input_name}" value="" />
		<input onclick="javascript:document.getElementById('{@input_name}').value = this.checked;" type="checkbox" value="1">
            <fx:apply-templates select="." mode="webforms_required" />
		</input>
	</fx:template>

	<fx:template match="field[@type = 'relation']" mode="webforms_input_type">
		<select name="{@input_name}">
			<fx:if test="@multiple">
				<fx:attribute name="multiple">
					<fx:text>multiple</fx:text>
				</fx:attribute>
			</fx:if>
			<option value=""></option>
			<fx:apply-templates select="values/item" mode="webforms_input_type" />
		</select>
	</fx:template>

	<fx:template match="field[@type = 'file' or @type = 'img_file' or @type = 'swf_file' or @type = 'video_file']" mode="webforms_input_type">
		<input type="file" name="{@input_name}" class="textinputs"/>
        <p class="file_text"><fx:text> &max-file-size; </fx:text><fx:value-of select="@maxsize" />Mb</p>
	</fx:template>

	<fx:template match="item" mode="webforms_input_type">
		<option value="{@id}"><fx:apply-templates /></option>
	</fx:template>

	<fx:template match="field" mode="webforms_required" />

	<fx:template match="field[@required = 'required']" mode="webforms_required">
        <fx:attribute name="class">required</fx:attribute>
    </fx:template>

	<fx:template match="items" mode="address">
		<fx:apply-templates select="item" mode="address" />
	</fx:template>

	<fx:template match="item" mode="address">
		<input type="hidden" name="system_email_to" value="{@id}" />
	</fx:template>

	<fx:template match="items[count(item) &gt; 1]" mode="address">
		<fx:choose>
			<fx:when test="count(item[@selected='selected']) != 1">
				<div class="form_element">
					<label class="required">
						<span><fx:text>Кому отправить:</fx:text></span>
						<select name="system_email_to">
							<option value=""></option>
							<fx:apply-templates select="item" mode="address_select" />
						</select>
					</label>
				</div>
			</fx:when>
			<fx:otherwise>
				<fx:apply-templates select="item[@selected='selected']" mode="address" />
			</fx:otherwise>
		</fx:choose>
	</fx:template>

	<fx:template match="item" mode="address_select">
		<option value="{@id}"><fx:apply-templates /></option>
	</fx:template>

</fx:stylesheet>
