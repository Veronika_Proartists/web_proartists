<?xml version="1.0" encoding="UTF-8"?>

<fx:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="fx date udt xlink">

	<fx:template match="result[@module = 'webforms'][@method = 'post']">
		<fx:apply-templates select="document('udata://webforms/post/')/udata" />
	</fx:template>

	<fx:template match="udata[@module = 'webforms'][@method = 'post']">
		<fx:value-of select="error" disable-output-escaping="yes" />
	</fx:template>

</fx:stylesheet>