<?xml version="1.0" encoding="UTF-8"?>

<fx:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="fx date udt xlink">

	<fx:include href="page.xsl" />
	<fx:include href="post.xsl" />
	<fx:include href="posted.xsl" />

</fx:stylesheet>