<?xml version="1.0" encoding="UTF-8"?>

<fx:stylesheet	version="1.0"
				xmlns="http://www.w3.org/1999/xhtml"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				exclude-result-prefixes="fx date udt xlink">

	<fx:template match="result[@module = 'webforms'][@method = 'page']">
		<fx:apply-templates select="document(concat('udata://webforms/add/', //property[@name = 'form_id']/value))/udata" />
	</fx:template>

</fx:stylesheet>