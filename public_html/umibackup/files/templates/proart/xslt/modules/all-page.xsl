<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
    xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <fx:template match="result" mode="header">
        <h1>
            <fx:value-of select="@header" />
        </h1>
    </fx:template>

    <fx:template match="result[@pageId]" mode="header">
        <h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="&empty-page-name;">
            <fx:value-of select="@header" />
        </h1>
    </fx:template>

    <fx:include href="org/common.xsl"/>
    <fx:include href="data/common.xsl"/>
    <fx:include href="emarket/price.xsl"/>
    <fx:include href="emarket/basket.xsl"/>
    <fx:include href="webforms/add.xsl"/>
    <fx:include href="news/main-news.xsl"/>
    <fx:include href="photoalbum/main-album.xsl"/>
    <fx:include href="comments/reviews.xsl"/>

</fx:stylesheet>