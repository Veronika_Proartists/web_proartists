<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                   xmlns:php="http://php.net/xsl"
                   extension-element-prefixes="php"
                   exclude-result-prefixes="php">

	<fx:template match="udata[@method = 'getCreateForm' or @method = 'getEditForm' or @method = 'getEditFormWithIgnorePermissions']">
		<fx:apply-templates select="group" mode="form" />
	</fx:template>
	
	<fx:template match="group" mode="form">
        <fx:param name="uniq_premium_landing"/>
		<h5>
			<fx:value-of select="@title" />
		</h5>
        <fx:choose>
            <fx:when test="$uniq_premium_landing = 1">
                <fx:apply-templates select="field" mode="form_landing"/>
            </fx:when>
            <fx:otherwise>
                <fx:apply-templates select="field" mode="form"/>
            </fx:otherwise>
        </fx:choose>
	</fx:template>

    <fx:template match="field[@name = 'lname']" mode="form_landing"/>
    <!--<fx:template match="field[@name = 'email']" mode="form_landing"/>-->
    <fx:template match="field[@name = 'father_name']" mode="form_landing"/>

    <fx:template match="field" mode="form_landing">
        <div class="row">
            <label for="{generate-id()}">
                <p>
                    <fx:value-of select="@title" />
                    <fx:apply-templates select="@required" mode="form" />
                    <fx:text>: </fx:text>
                </p>
            </label>
            <input type="text" name="{@input_name}" value="{.}" class="txt" id="{generate-id()}">
                <fx:apply-templates select="@required" mode="class" />
            </input>
        </div>
    </fx:template>

    <fx:template match="field" mode="form">
        <div class="row">
            <label for="{generate-id()}">
                <p>
                    <fx:value-of select="@title" />
                    <fx:apply-templates select="@required" mode="form" />
                    <fx:text>: </fx:text>
                </p>
            </label>
            <input type="text" name="{@input_name}" value="{.}" class="txt" id="{generate-id()}">
                <fx:apply-templates select="@required" mode="class" />
            </input>
        </div>
    </fx:template>

	<fx:template match="field[@type = 'relation']" mode="form">
        <div class="row">
            <label for="{generate-id()}">
                <p>
                    <fx:value-of select="@title" />
                    <fx:apply-templates select="@required" mode="form" />
                    <fx:text>: </fx:text>
                </p>
            </label>
            <select class="selectbox" type="text" name="{@input_name}" id="{generate-id()}">
                <fx:if test="@multiple = 'multiple'">
                    <fx:attribute name="multiple">multiple</fx:attribute>
                </fx:if>
                <fx:apply-templates select="@required" mode="class" />
                <fx:apply-templates select="values/item" mode="form" />
            </select>
        </div>
	</fx:template>
	
	<fx:template match="item" mode="form">
		<option value="{@id}">
			<fx:copy-of select="@selected" />
			<fx:value-of select="." />
		</option>
	</fx:template>

	<fx:template match="field[@type = 'boolean']" mode="form">
        <div class="row">
            <input type="hidden" name="{@input_name}" value="0" />
            <input type="checkbox" name="{@input_name}" value="1" id="{generate-id()}" class="css-checkbox">
                <fx:copy-of select="@checked" />
                <fx:apply-templates select="@required" mode="class" />
            </input>
            <label for="{generate-id()}" class="css-label">
                <p>
                    <fx:value-of select="@title" />
                    <fx:apply-templates select="@required" mode="form" />
                </p>
            </label>
        </div>
	</fx:template>

	<fx:template match="field[@type = 'text' or @type = 'wysiwyg']" mode="form">
        <div class="row">
            <label for="{generate-id()}">
                <p>
                    <fx:value-of select="@title" />
                    <fx:apply-templates select="@required" mode="form" />
                    <fx:text>: </fx:text>
                </p>
            </label>
            <textarea name="{@input_name}" class="textinputs" id="{generate-id()}">
                <fx:apply-templates select="@required" mode="class" />
                <fx:value-of select="." />
            </textarea>
        </div>
	</fx:template>

	<fx:template match="field[@type = 'file' or @type = 'img_file']" mode="form">
        <div class="row">
            <label for="{generate-id()}">
                <p>
                    <fx:value-of select="@title" />
                    <fx:apply-templates select="@required" mode="form" />
                    <fx:text>: </fx:text>
                </p>
            </label>
            <input type="file" name="{@input_name}" value="{.}" class="txt" id="{generate-id()}">
                <fx:apply-templates select="@required" mode="class" />
            </input>
        </div>
	</fx:template>
	
	<fx:template match="@required" mode="class">
		<fx:attribute name="class">add_error required</fx:attribute>
	</fx:template>

    <fx:template match="@required" mode="form">
        <span class="necessarily"> *</span>
	</fx:template>
</fx:stylesheet>