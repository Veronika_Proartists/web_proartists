<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">
	<fx:template match="result[@method = 'rss' or @method = 'atom']">
		<fx:value-of select="udata/error" />
	</fx:template>

</fx:stylesheet>