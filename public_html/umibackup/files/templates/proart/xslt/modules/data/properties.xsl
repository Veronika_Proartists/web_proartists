<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                   xmlns:umi="http://www.umi-cms.ru/TR/umi"
                   xmlns:php="http://php.net/xsl"
                   extension-element-prefixes="php"
                   exclude-result-prefixes="php">

	<fx:template match="property|fields/field//value">
		<fx:value-of select="value" />
	</fx:template>
	
	<fx:template match="property[@type = 'boolean'][value]">
		<fx:text>&no;</fx:text>
	</fx:template>

	<fx:template match="property[@type = 'boolean'][value = 1]|fields/field[@type = 'boolean']//value[.]">
		<fx:text>&yes;</fx:text>
	</fx:template>
	
	<fx:template match="property[@type = 'relation']|fields/field[@type = 'relation']//value">
		<fx:apply-templates select="value/item" />
	</fx:template>
	
	<fx:template match="property[@type = 'symlink']|fields/field[@type = 'symlink']//value">
		<fx:apply-templates select="value/page[1]/basetype" />
		<fx:apply-templates select="value/page" />
	</fx:template>

	<fx:template match="value/item">
		<fx:value-of select="concat(@name, ', ')" />
	</fx:template>

	<fx:template match="value/item[position() = last()]">
		<fx:value-of select="@name" />
	</fx:template>

	<fx:template match="value/page">
		<a href="{@link}">
			<fx:value-of select="name" />
		</a>
	</fx:template>

	<fx:template match="value/page/basetype">
		<fx:attribute name="umi:type">
			<fx:value-of select="@module" />
			<fx:text>::</fx:text>
			<fx:value-of select="@method" />
		</fx:attribute>
	</fx:template>

</fx:stylesheet>