<?xml version="1.0" encoding="utf-8"?>
<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:foxi="http://foxinet.ru">

    <fx:template match="udata[@method = 'price']">
        <fx:apply-templates select="price" />
    </fx:template>

    <fx:template match="udata[@method = 'price']" mode="psk">
        <fx:text>от </fx:text>
        <fx:value-of select="foxi:format-price(price/actual)"/>
        <fx:text> рубл</fx:text>
        <fx:value-of select="foxi:declension(floor(price/actual),'я','ей','ей')"/>
    </fx:template>

    <fx:template match="total-price">
        <fx:value-of select="@prefix"/>
        <fx:text> </fx:text>
        <fx:value-of select="foxi:format-price(actual)"/>
        <fx:text> </fx:text>
        <fx:value-of select="@suffix"/>
    </fx:template>

    <fx:template match="price">
        <fx:value-of select="@prefix"/>
        <fx:text> </fx:text>
        <fx:value-of select="foxi:format-price(original)"/>
        <fx:text> </fx:text>
        <fx:value-of select="@suffix"/>
    </fx:template>

    <fx:template match="price[not(original) or original = '']">
        <fx:value-of select="@prefix"/>
        <fx:text> </fx:text>
        <fx:value-of select="foxi:format-price(actual)"/>
        <fx:text> </fx:text>
        <fx:value-of select="@suffix"/>
    </fx:template>

    <fx:template match="@price">
        <fx:variable name="cart-price" select="document('udata://emarket/cart')/udata/summary/price"/>
        <fx:value-of select="$cart-price/@prefix"/>
        <fx:text> </fx:text>
        <fx:value-of select="foxi:format-price(.)"/>
        <fx:text> </fx:text>
        <fx:value-of select="$cart-price/@suffix"/>
    </fx:template>

</fx:stylesheet>
