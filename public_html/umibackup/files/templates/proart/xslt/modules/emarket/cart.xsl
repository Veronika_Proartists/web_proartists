<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                   xmlns:umi="http://www.umi-cms.ru/TR/umi"
                   xmlns:xlink="http://www.w3.org/TR/xlink">

	<fx:template match="result[@method = 'cart']">
        <div class="basket">
            <fx:apply-templates select="." mode="breadcrumbs"/>
            <fx:apply-templates select="document('udata://emarket/cart')/udata"/>

        </div>
	</fx:template>

	<fx:template match="udata[@method = 'cart']">
		<div class="basket_empty">
			<h4 class="empty-content">&emarket-step-1;</h4>
			<p>&return-to-catalog-custom;</p>
		</div>
	</fx:template>


	<fx:template match="udata[@method = 'cart'][count(items/item) &gt; 0]">
        <h1>&emarket-step-1;</h1>
        <div class="block-2">
            <table>
                <thead>
                    <tr>
                        <th>&basket-item;</th>
                        <th class="custom-block-1">&price;</th>
                        <th>&amount;</th>
                        <th>&sum;</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <fx:apply-templates select="items/item" />
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td colspan="4">
                            <fx:apply-templates select="summary" />
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <a class="button-1" href="{$lang-prefix}/emarket/purchasing_one_step">&begin-purchase;</a>

	</fx:template>

	<fx:template match="udata[@method = 'cart']//item">
        <tr class="cart_item_{@id}">
            <td>
                <fx:choose>
                    <fx:when test="options/option/@field-name = 'color'">
                        <fx:variable name="color" select="document(concat('uobject://',options/option/@id,'.color'))/udata/property/value"/>
                        <span class="cart-item-img">
                            <div class="color" style="background-color: {$color}"></div>
                        </span>
                        <div class="block-1">
                            <a class="cart-item-link" href="{page/@link}">
                                <fx:value-of select="options/option/@name"/>
                            </a>
                            <p>
                                <fx:value-of select="document(concat('upage://',page/@id,'.descr_mini'))/udata/property/value" />
                            </p>
                        </div>
                    </fx:when>
                    <fx:otherwise>
                        <span class="cart-item-img">
                            <fx:variable name="color" select="document(concat('upage://',page/@id,'.color_code'))/udata/property/value" />
                            <fx:choose>
                                <fx:when test="string-length($color) &gt; 0">
                                    <div class="color" style="background-color: {$color}"></div>
                                </fx:when>
                                <fx:otherwise>
                                    <fx:call-template name="catalog-thumbnail">
                                        <fx:with-param name="element-id" select="page/@id" />
                                        <fx:with-param name="field-name">photo</fx:with-param>
                                        <fx:with-param name="empty">&empty-photo;</fx:with-param>
                                        <fx:with-param name="width">76</fx:with-param>
                                        <fx:with-param name="height">59</fx:with-param>
                                        <fx:with-param name="alt" select="page/name"/>
                                        <fx:with-param name="title" select="page/name"/>
                                    </fx:call-template>
                                </fx:otherwise>
                            </fx:choose>
                        </span>
                        <div class="block-1">
                            <span class="cart-item-link">
                                <fx:value-of select="page/name"/>
                            </span>
                            <p>
                                <fx:value-of select="document(concat('upage://',page/@id,'.descr_mini'))/udata/property/value" />
                            </p>
                        </div>
                    </fx:otherwise>
                </fx:choose>
            </td>
            <td>
                <span class="price">
                    <fx:apply-templates select="price" />
                </span>
            </td>
            <td>
                <div class="spinner-wrp">
                    <a href="#" class="left-button">-</a>
                    <input type="text" value="{amount}" class="basket_count"/>
                    <input type="hidden" value="{amount}" />
                    <a href="#">+</a>
                </div>
            </td>
            <td>
                <div class="forever">
                    <span class="price">
                        <fx:apply-templates select="total-price" />
                    </span>
                </div>
            </td>
            <td>
                <a href="#" class="cart_item_close del" id="del_basket_{@id}"></a>
            </td>
        </tr>
	</fx:template>


	<fx:template match="udata[@method = 'cart']/summary">
        <p>&summary-price;:
            <span id="summary_price">
                <fx:apply-templates select="price"/>
            </span>
        </p>
	</fx:template>


</fx:stylesheet>