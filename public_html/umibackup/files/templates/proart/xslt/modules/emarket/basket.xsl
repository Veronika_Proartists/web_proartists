<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:foxi="http://foxinet.ru">

    <fx:template match="udata[@module = 'emarket' and @method = 'cart']" mode="basket">
        <div class="basket">
            <fx:if test="summary/amount = 0">
                <fx:attribute name="class">basket empty</fx:attribute>
            </fx:if>
            <div class="wrp">
                <a rel="nofollow" href="{$lang-prefix}/emarket/cart/">
                    &basket-order;
                    <span>
                        <fx:value-of select="summary/amount"/>
                    </span>
                </a>
                <div class="hint">
                    <div class="wrp">
                        <p><fx:apply-templates select="summary" mode="basket" /></p>
                        <a href="#" class="close">x</a>
                        <span class="corner"></span>
                    </div>
                </div>
            </div>

        </div>
    </fx:template>

    <fx:template match="summary" mode="basket">
        <fx:text>&basket-empty-custom;</fx:text>
    </fx:template>

    <fx:template match="summary[amount &gt; 0]" mode="basket">
        <fx:value-of select="foxi:string-replace(concat(amount,' ',foxi:declension(amount,'&offer-name-0;','&offer-name-1;','&offer-name-2;')),'&basket-text;')"/>
        <b>
            <fx:apply-templates select="price/@prefix" />
            <fx:value-of select="foxi:format-price(price/actual)"/>
            <fx:apply-templates select="price/@suffix" />
        </b>
    </fx:template>

    <fx:template match="summary[amount &gt; 0]/price/@prefix">
        <fx:value-of select="." />
        <fx:text>&#160;</fx:text>
    </fx:template>

    <fx:template match="summary[amount &gt; 0]/price/@suffix">
        <fx:text>&#160;</fx:text>
        <fx:value-of select="." />
    </fx:template>

</fx:stylesheet>