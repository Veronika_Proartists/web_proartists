<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
    xmlns:xlink="http://www.w3.org/TR/xlink">

	<fx:template match="result[@module = 'emarket' and @method = 'purchasing_one_step'][udata/onestep]">
        <div class="basket basket-2">
            <fx:apply-templates select="." mode="breadcrumbs"/>
            <h1>&emarket-step-2;</h1>
            <form action="/emarket/saveInfo" method="POST" onsubmit="site.forms.data.save(this); return site.forms.data.check(this);">
                <div class="block-3">
                    <div class="box">
                        <fx:apply-templates select="udata/onestep/customer/group" mode="form"/>
                    </div>
                </div>
                <div class="block-4" id="delivery_choose">
                    <div class="box">
                        <fx:apply-templates select="udata/onestep/delivery_choose"/>
                    </div>
                </div>
                <div class="block-3" id="delivery">
                    <div class="box" id="delivery_box">
                        <fx:apply-templates select="udata/onestep/delivery "/>
                    </div>
                </div>
                <div class="block-4" id="payment">
                    <fx:apply-templates select="udata/onestep/payment"/>
                    <div class="buttons clearfix">
                        <input type="submit" class="button-1" value="&begin-purchase;"/>
                        <a href="{$lang-prefix}/emarket/cart" class="gray">&emarket-back;</a>
                    </div>
                </div>
            </form>
            <div style="display:none" id="cache_container"></div>
        </div>
	</fx:template>

	
	<fx:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery">
        <input type="hidden" name="delivery-address" value="new"/>
        <fx:apply-templates select="document(@xlink:href)" />
	</fx:template>
	
	<fx:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery[items/item]">
        <input type="hidden" name="delivery-address" value="{items/item[1]/@id}"/>
        <fx:apply-templates select="document(concat('udata://data/getEditFormWithIgnorePermissions/',items/item[1]/@id,'/none///1'))/udata"/>
	</fx:template>
	
	<fx:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery_choose" />
	<fx:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/delivery_choose[items/item]">
		<div class="dychoose onestep">
			<h5>&delivery-agent;</h5>
			<fx:for-each select="items/item">
                <div class="row">
                    <input id="{generate-id()}" type="radio" class="css-checkbox {@type-name}" name="delivery-id" value="{@id}"
                           data-delivery="{document(concat('udata://emarket/getDeliveryType/',@id))/udata}">
                        <fx:if test="@active = 'active' or (not(../item[@active]) and position() = 1)">
                            <fx:attribute name="checked">
                                <fx:text>checked</fx:text>
                            </fx:attribute>
                        </fx:if>
                        <fx:attribute name="data-payment">
                            <fx:for-each select="document(concat('uobject://',@id,'.payments'))/udata/property/value/item">
                                <fx:value-of select="@id"/><fx:if test="position() != last()">,</fx:if>
                            </fx:for-each>
                        </fx:attribute>
                    </input>
                    <label for="{generate-id()}" class="css-label">
                        <fx:value-of select="@name" />
                        <fx:text> -</fx:text>
                        <fx:apply-templates select="@price"/>
                    </label>
                </div>
			</fx:for-each>
		</div>
	</fx:template>

    <fx:template match="@price[. = 'Невозможно автоматически определить стоимость']" priority="1">
        <span id="russian-post">&emarket-russian-post;</span>
    </fx:template>

	
	<fx:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/payment" />
	<fx:template match="result[@module = 'emarket' and @method = 'purchasing_one_step']/udata/onestep/payment[items/item]">
        <div id="payment_choose" class="payment onestep">
            <h5>&payment-type;</h5>
            <!--<script>
                <![CDATA[
					window.paymentId = null;
					jQuery('.basket-2 form').submit(function(){
						if (window.paymentId) {
							var checkPaymentReceipt = function(id) {
								if (jQuery(':radio:checked','#payment_choose').hasClass('receipt')) {
									var url = "/emarket/saveInfo";
									var win = window.open("", "_blank", "width=710,height=620,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no");
									win.document.write("<html><head><" + "script" + ">location.href = '" + url + "?payment-id=" + id + "'</" + "script" + "></head><body></body></html>");
									win.focus();
									return false;
								}
							}
							return checkPaymentReceipt(window.paymentId);
						}
						else return true;
					});
				]]>
            </script>-->
            <fx:apply-templates select="items/item" mode="payment" />
        </div>
	</fx:template>

    <fx:template match="item" mode="payment">
        <fx:if test="(position() = 1) and (@type-name = 'receipt')">
            <script>
                window.paymentId = <fx:value-of select="@id" />;
            </script>
        </fx:if>
        <div class="row">
            <input id="{generate-id()}" type="radio" class="css-checkbox {@type-name}" name="payment-id" value="{@id}" onclick="window.paymentId = {@id};">
                <fx:if test="@active = 'active' or (not(../item[@active]) and position() = 1)">
                    <fx:attribute name="checked">
                        <fx:text>checked</fx:text>
                    </fx:attribute>
                </fx:if>
            </input>
            <label for="{generate-id()}" class="css-label">
                <fx:value-of select="@name" />
            </label>
            <fx:variable name="img" select="document(concat('uobject://',@id,'.logos'))/udata/property/value"/>
            <fx:if test="$img">
                <fx:text>      </fx:text>
                <fx:call-template name="thumbnail">
                    <fx:with-param name="height">30</fx:with-param>
                    <fx:with-param name="mode">default</fx:with-param>
                    <fx:with-param name="src" select="$img"/>
                    <fx:with-param name="alt" select="@name"/>
                </fx:call-template>
            </fx:if>
        </div>
    </fx:template>

</fx:stylesheet>