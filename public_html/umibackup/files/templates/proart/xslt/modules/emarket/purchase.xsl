<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">


	<fx:template match="/result[@method = 'purchase']">
        <fx:apply-templates select="document('udata://emarket/purchase')" />
	</fx:template>
	
	<fx:template match="purchasing">
		<h5>
			<fx:text>Purchase is in progress: </fx:text>
			<fx:value-of select="concat(@stage, '::', @step, '()')" />
		</h5>
	</fx:template>
	
	<fx:template match="purchasing[@stage = 'result']">
        <div class="result-error">
            <h5 class="empty-content">
                <fx:text>&emarket-order-failed;</fx:text>
            </h5>
            <p style="margin-bottom: 35px;">
                <fx:text>&emarket-order-failed-text;</fx:text>
            </p>
            <div class="cart-buttons">
                <a href="{$lang-prefix}/" class="toCatalog">&continue-shopping;</a>
            </div>
        </div>
	</fx:template>
	
	<fx:template match="purchasing[@stage = 'result' and @step = 'successful']">
		<div class="result-successful">
            <h5 class="empty-content">
                <fx:text>&emarket-order-successful;</fx:text>
            </h5>
            <p style="margin-bottom: 35px;">
                <b><fx:value-of select="document(concat('uobject://', //order/@id))/udata/object/@name" /></b>
                <fx:text> </fx:text>
                &order-is-in-processing;
            </p>
            <div class="cart-buttons">
                <a href="{$lang-prefix}/" class="toCatalog">&continue-shopping;</a>
            </div>
		</div>
	</fx:template>

    <fx:include href="purchase/payment.xsl" />
</fx:stylesheet>