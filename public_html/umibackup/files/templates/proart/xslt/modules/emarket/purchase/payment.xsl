<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">

	<fx:template match="purchasing[@stage = 'payment'][@step = 'bonus']">
		<form id="bonus_payment" method="post" action="do/">
			<h4>
				<fx:text>&pay-by-bonuses;</fx:text>
			</h4>
			<div style="margin:0 15px;">
				<p>&can-use-bonuses;</p>
				<fx:text>&order-summ; </fx:text>
				<fx:value-of select="$currency-prefix" />
				<fx:text> </fx:text>
				<fx:value-of select="bonus/actual_total_price" />
				<fx:text> </fx:text>
				<fx:value-of select="$currency-suffix" />
				<fx:text>.</fx:text><br />
				<fx:text> &available-bonuses; </fx:text>
				<fx:value-of select="$currency-prefix" />
				<fx:text> </fx:text>
				<fx:value-of select="bonus/available_bonus" />
				<fx:text> </fx:text>
				<fx:value-of select="$currency-suffix" />
				<fx:text>.</fx:text>
			</div>
			<div><label>&spend-bonuses; <fx:value-of select="$currency-prefix" /><input type="text" name="bonus" /><fx:value-of select="$currency-suffix" /></label></div>
			<div><input type="submit" value="&continue;" class="button big" /></div>
		</form>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment']/bonus/@prefix">
		<fx:value-of select="." />
		<fx:text>&#160;</fx:text>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment']/bonus/@suffix">
		<fx:text>&#160;</fx:text>
		<fx:value-of select="." />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'choose']">
		<fx:apply-templates select="//steps" />
		<form id="payment_choose" method="post">
			<fx:attribute name="action"><fx:value-of select="submit_url" /></fx:attribute>
			<h4>
				<fx:text>&payment-type;:</fx:text>
			</h4>
			<script>
				<![CDATA[
					window.paymentId = null;
					jQuery('#payment_choose').submit(function(){
						if (window.paymentId) {
							var checkPaymentReceipt = function(id) {
								if (jQuery(':radio:checked','#payment_choose').attr('class') == 'receipt') {
									var url = "]]><fx:value-of select="submit_url" /><![CDATA[";
									var win = window.open("", "_blank", "width=710,height=620,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no");
									win.document.write("<html><head><" + "script" + ">location.href = '" + url + "?payment-id=" + id + "'</" + "script" + "></head><body></body></html>");
									win.focus();
									return false;
								}
							}
							return checkPaymentReceipt(window.paymentId);
						}
						else return true;
					});
				]]>
			</script>

			<fx:apply-templates select="items/item" mode="payment" />

			<div>
				<input type="submit" value="&continue;" class="button big" />
			</div>
		</form>
	</fx:template>


	<fx:template match="purchasing[@stage = 'payment'][@step = 'yandex30']">
		<form id="form_yandex30" action="{formAction}" method="post">
			<input type="hidden" name="shopId" value="{shopId}" />
			<input type="hidden" name="Sum" value="{Sum}" />
			<input type="hidden" name="BankId" value="{BankId}" />
			<input type="hidden" name="scid" value="{scid}" />
			<input type="hidden" name="CustomerNumber" value="{CustomerNumber}" />
			<input type="hidden" name="order-id" value="{orderId}" />
			<input type="hidden" name="PaymentType" value="" />
			<input type="hidden" name="PaymentTypeProvider" value="" />
			<div class="inline">
				<fx:apply-templates select="items/item" mode="payment-modes-yandex30" />
			</div>
			<div>
				<fx:text>&payment-redirect-text; Yandex.</fx:text>
			</div>
			<div>
				<input type="submit" value="Оплатить" class="button big" />
			</div>
		</form>
		<script>
			var elements = document.getElementById('form_yandex30').elements;
			elements.mode_type.value = '';
			function change(type, subtype) {
				elements.PaymentType.value = type;
				elements.PaymentTypeProvider.value = subtype;
			}
		</script>
	</fx:template>

    <fx:param name="payment_type"/>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'yandex30']" priority="1">
        <div class="basket basket-2">
            <h1>Оплата через Яндекс.Кассу</h1>
            <form name="ShopForm" method="POST" action="https://money.yandex.ru/eshop.xml">
                <input type="hidden" name="scid" value="{scid}"/>
                <input type="hidden" name="ShopID" value="{shopId}"/>
                <input type="hidden" name="CustomerNumber" value="{orderId}"/>
                <input type="hidden" name="Sum" value="{Sum}"/>
                <fx:variable name="order-info" select="document(concat('udata://emarket/order/',orderId))/udata"/>
                <input type="hidden" name="CustName" value="{concat($order-info//property[@name = 'lname']/value,' ',$order-info//property[@name = 'fname']/value,' ',$order-info//property[@name = 'father_name']/value)}"/>
                <fx:variable name="address">
                    <fx:if test="$order-info//property[@name = 'delivery_addresses']/value/item">
                        <fx:for-each select="document(concat('uobject://',$order-info//property[@name = 'delivery_addresses']/value/item/@id))//property">
                            <fx:value-of select="title"/>: <fx:apply-templates select="value"/>
                            <fx:if test="position() != last()">, </fx:if>
                        </fx:for-each>
                    </fx:if>
                </fx:variable>
                <fx:variable name="items">
                    <fx:for-each select="$order-info/items/item">
                        <fx:value-of select="@name"/> x <fx:value-of select="amount"/> шт. = <fx:apply-templates select="total-price"/>
                        <fx:if test="position() != last()">; </fx:if>
                    </fx:for-each>
                </fx:variable>
                <input type="hidden" name="CustAddr" value="{$address}"/>
                <input type="hidden" name="CustEMail" value="{$order-info//property[@name = 'email']/value}"/>
                <input type="hidden" name="OrderDetails" value="{$items}"/>
                <fx:choose>
                    <fx:when test="$payment_type">
                        <input type="hidden" value="{$payment_type}" name="paymentType"/>
                        <fx:call-template name="form-send" />
                    </fx:when>
                    <fx:otherwise>
                        <div class="block-4" id="payment">
                            <div class="row">
                                <input id="paymentTypePC" class="css-checkbox" type="radio" checked="checked" value="PC" name="paymentType"/>
                                <label class="css-label" for="paymentTypePC">Со счета в Яндекс.Деньгах</label>
                            </div>
                            <div class="row">
                                <input id="paymentTypeAC" class="css-checkbox" type="radio" value="AC" name="paymentType"/>
                                <label class="css-label" for="paymentTypeAC">С банковской карты</label>
                            </div>
                            <div class="row">
                                <input id="paymentTypeWM" class="css-checkbox" type="radio" value="WM" name="paymentType"/>
                                <label class="css-label" for="paymentTypeWM">Со счета WebMoney</label>
                            </div>
                            <div class="row">
                                <input id="paymentTypeGP" class="css-checkbox" type="radio" value="GP" name="paymentType"/>
                                <label class="css-label" for="paymentTypeGP">По коду через терминал</label>
                            </div>
                            <div class="buttons clearfix">
                                <input type="submit" class="button-1" value="Оплатить"/>
                            </div>
                        </div>
                    </fx:otherwise>
                </fx:choose>

            </form>
            <!---->
        </div>
    </fx:template>

	<fx:template match="item" mode="payment-modes-yandex30">
		<label><input type="radio" name="mode_type" value="{id}" onChange="javascript:change('{type}', '{subtype}');"/><fx:value-of select="label"/></label>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'acquiropay']">
		<form method="post" action="{formAction}">
			<input type="hidden" name="product_id" value="{product_id}" />
			<input type="hidden" name="amount" value="{amount}" />
			<input type="hidden" name="language" value="{language}" />
			<input type="hidden" name="cf" value="{order_id}" />
			<input type="hidden" name="ok_url" value="{ok_url}" />
			<input type="hidden" name="cb_url" value="{cb_url}" />
			<input type="hidden" name="ko_url" value="{ko_url}" />
			<input type="hidden" name="token" value="{token}" />
			<div>
				<fx:text>&payment-redirect-text; AcquiroPay.</fx:text>
			</div>
			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
		<fx:call-template name="form-send" />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'chronopay']">
		<form method="post" action="{formAction}">
			<input type="hidden" name="product_id" value="{product_id}" />
			<input type="hidden" name="product_name" value="{product_name}" />
			<input type="hidden" name="product_price" value="{product_price}" />
			<input type="hidden" name="language" value="{language}" />
			<input type="hidden" name="cs1" value="{cs1}" />
			<input type="hidden" name="cs2" value="{cs2}" />
			<input type="hidden" name="cs3" value="{cs3}" />
			<input type="hidden" name="cb_type" value="{cb_type}" />
			<input type="hidden" name="cb_url" value="{cb_url}" />
			<input type="hidden" name="decline_url" value="{decline_url}" />
			<input type="hidden" name="sign" value="{sign}" />

			<div>
				<fx:text>&payment-redirect-text; Chronopay.</fx:text>
			</div>

			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
		<fx:call-template name="form-send" />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'yandex']">
		<form action="{formAction}" method="post">
			<input type="hidden" name="shopId"	value="{shopId}" />
			<input type="hidden" name="Sum"		value="{Sum}" />
			<input type="hidden" name="BankId"	value="{BankId}" />
			<input type="hidden" name="scid"	value="{scid}" />
			<input type="hidden" name="CustomerNumber" value="{CustomerNumber}" />
			<input type="hidden" name="order-id" value="{orderId}" />

			<div>
				<fx:text>&payment-redirect-text; Yandex.</fx:text>
			</div>

			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
		<fx:call-template name="form-send" />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'payonline']">
		<form action="{formAction}" method="post">

			<input type="hidden" name="MerchantId" 	value="{MerchantId}" />
			<input type="hidden" name="OrderId" 	value="{OrderId}" />
			<input type="hidden" name="Currency" 	value="{Currency}" />
			<input type="hidden" name="SecurityKey" value="{SecurityKey}" />
			<input type="hidden" name="ReturnUrl" 	value="{ReturnUrl}" />
			<!-- NB! This field should exist for proper system working -->
			<input type="hidden" name="order-id"    value="{orderId}" />
			<input type="hidden" name="Amount" value="{Amount}" />

			<div>
				<fx:text>&payment-redirect-text; PayOnline.</fx:text>
			</div>

			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
		<fx:call-template name="form-send" />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'robox']">
		<form action="{formAction}" method="post">
			<input type="hidden" name="MrchLogin" value="{MrchLogin}" />
			<input type="hidden" name="OutSum"	  value="{OutSum}" />
			<input type="hidden" name="InvId"	  value="{InvId}" />
			<input type="hidden" name="Desc"	  value="{Desc}" />
			<input type="hidden" name="SignatureValue" value="{SignatureValue}" />
			<input type="hidden" name="IncCurrLabel"   value="{IncCurrLabel}" />
			<input type="hidden" name="Culture"   value="{Culture}" />
			<input type="hidden" name="shp_orderId" value="{shp_orderId}" />

			<div>
				<fx:text>&payment-redirect-text; Robox.</fx:text>
			</div>

			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
		<fx:call-template name="form-send" />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'rbk']">
		<form action="{formAction}" method="post">
			<input type="hidden" name="eshopId" value="{eshopId}" />
			<input type="hidden" name="orderId"	value="{orderId}" />
			<input type="hidden" name="recipientAmount"	value="{recipientAmount}" />
			<input type="hidden" name="recipientCurrency" value="{recipientCurrency}" />
			<input type="hidden" name="version" value="{version}" />

			<div>
				<fx:text>&payment-redirect-text; RBK Money.</fx:text>
			</div>

			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
		<fx:call-template name="form-send" />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'kupivkredit'][@action = 'widget']">
		<fx:choose>
			<fx:when test="./@test-mode">
				<script src="https://kupivkredit-test-fe.tcsbank.ru/widget/vkredit.js"></script>
			</fx:when>
			<fx:otherwise>
				<script src="https://www.kupivkredit.ru/widget/vkredit.js"></script>
			</fx:otherwise>
		</fx:choose>
		<script>
			jQuery(document).ready(function() {
			<![CDATA[
			function widgetOpen(order, sig, price) {
				vKredit = new VkreditWidget(1, price, {
					order: order,
					sig: sig,
					onClose: function() {
						window.location.assign("/emarket/purchase/payment/choose/");
					},
					onAccept: function(accepted) {
						if(accepted == 1) {
							window.location.assign("/emarket/purchase/payment/kupivkredit/?accepted=accepted");
						} else {
							window.location.assign("/emarket/purchase/payment/choose/");
					}

					}
				});

				vKredit.openWidget();
			}
		]]>
			widgetOpen("<fx:value-of select="order" />", "<fx:value-of select="sig" />", <fx:value-of select="totalPrice" />);
		});
		</script>
		<fx:apply-templates select="//steps" />
		<h4>&in-progress;</h4>
		<p>&credit-request;</p>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'kupivkredit'][@action = 'declined']">
		<fx:apply-templates select="//steps" />
		<h4>&error;</h4>
		<p>&credit-canceled;</p>
		<p><a href="/emarket/purchase/payment">&select-payment;</a></p>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'kupivkredit'][@action = 'error']">
		<fx:apply-templates select="//steps" />
		<h4>&error;</h4>
		<p>&order-error;</p>
		<p><a href="/emarket/purchase/payment">&select-payment;</a></p>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'payanyway']">
		<form action="{formAction}" method="post">
            <input type="hidden" name="MNT_ID" value="{mntId}" />
            <input type="hidden" name="MNT_TRANSACTION_ID" value="{mnTransactionId}" />
            <input type="hidden" name="MNT_CURRENCY_CODE" value="{mntCurrencyCode}" />
            <input type="hidden" name="MNT_AMOUNT" value="{mntAmount}" />
            <input type="hidden" name="MNT_TEST_MODE" value="{mntTestMode}" />
            <input type="hidden" name="MNT_SIGNATURE" value="{mntSignature}" />
            <input type="hidden" name="MNT_SUCCESS_URL" value="{mntSuccessUrl}" />
            <input type="hidden" name="MNT_FAIL_URL" value="{mntFailUrl}" />

			<div>
				<fx:text>&payment-redirect-text; PayAnyWay.</fx:text>
			</div>

			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
		<fx:call-template name="form-send" />
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'dengionline']">
		<fx:apply-templates select="//steps" />
		<form class="width_100" action="{formAction}" method="post">
			<h4>
				<fx:text>&payment-type;:</fx:text>
			</h4>
			<input type="hidden" name="project" value="{project}" />
			<input type="hidden" name="amount" value="{amount}" />
			<input type="hidden" name="nickname" value="{order_id}" />
			<input type="hidden" name="source" value="{source}" />
			<input type="hidden" name="order_id" value="{order_id}" />
			<input type="hidden" name="comment" value="{comment}" />
			<input type="hidden" name="paymentCurrency" value="{paymentCurrency}" />
			<div class="inline">
				<fx:apply-templates select="items/item[position() mod 3 = 1]" mode="payment-modes" />
			</div>
			<div class="inline">
				<fx:apply-templates select="items/item[position() mod 3 = 2]" mode="payment-modes" />
			</div>
			<div class="inline">
				<fx:apply-templates select="items/item[position() mod 3 = 0]" mode="payment-modes" />
			</div>
			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'paypal']">
		<fx:apply-templates select="//steps" />
		<form class="width_100" action="{formAction}" method="post">
			<input type="hidden" name="cmd" value="_xclick" />
			<input type="hidden" name="business" value="{paypalemail}" />
			<input type="hidden" name="item_name" value="Payment for order #{order_id}" />
			<input type="hidden" name="item_number" value="{order_id}" />
			<input type="hidden" name="amount" value="{total}" />
			<input type="hidden" name="no_shipping" value="1" />
			<input type="hidden" name="return" value="{return_success}" />
			<input type="hidden" name="rm" value="2" />
			<input type="hidden" name="cancel_return" value="{cancel_return}" />
			<input type="hidden" name="notify_url" value="{notify_url}" />
			<input type="hidden" name="currency_code" value="{currency}" />

			<div>
				<fx:text>&payment-redirect-text; PayPal.</fx:text>
			</div>

			<div>
				<input type="submit" value="&pay;" class="button big" />
			</div>
		</form>
	</fx:template>

	<fx:template match="item" mode="payment-modes">
		<label><input type="radio" name="mode_type" value="{id}"/><fx:value-of select="label"/></label>
	</fx:template>

	<fx:template match="purchasing[@stage = 'payment'][@step = 'invoice']" xmlns:xlink="http://www.w3.org/TR/xlink">
		<fx:apply-templates select="//steps" />
		<form id="invoice" method="post" action="do">
			<fx:apply-templates select="items" mode="legal-person" />
			<div>
				<input type="submit" value="&make-bill;" class="button big" />
			</div>
		</form>
		<script>
			jQuery('#invoice').submit(function(){
				var input = jQuery('input:radio:checked', this);
				if (typeof input.val() == 'undefined' || input.val() == 'new') {
					if (typeof input.val() == 'undefined') {
						jQuery('input:radio[value=new]', this).attr('checked','checked');
					}
					return site.forms.data.check(this);
				}
			});
		</script>
	</fx:template>

	<fx:template match="items" mode="legal-person" xmlns:xlink="http://www.w3.org/TR/xlink">
		<input type="hidden" name="legal-person" value="new" />
		<fx:apply-templates select="document(../@xlink:href)//field" mode="form" />
	</fx:template>

	<fx:template match="items[count(item) &gt; 0]" mode="legal-person" xmlns:xlink="http://www.w3.org/TR/xlink">
		<h4>
			<fx:text>&choose-legal_person;:</fx:text>
		</h4>
		<fx:apply-templates select="item" mode="legal-person" />

		<div>
			<label>
				<input type="radio" name="legal-person" value="new" />
				<fx:text>&new-legal-person;</fx:text>
			</label>
		</div>

		<div id="new-legal-person">
			<fx:apply-templates select="document(../@xlink:href)//field" mode="form" />
		</div>

	</fx:template>

    <fx:template match="purchasing[@stage = 'payment'][@step = 'payu']">
        <fx:value-of select="form" disable-output-escaping="yes" />
        <fx:call-template name="form-send" />
    </fx:template>

	<fx:template match="item" mode="legal-person"  xmlns:xlink="http://www.w3.org/TR/xlink">
		<div class="form_element">
			<label>
				<input type="radio" name="legal-person" value="{@id}">
					<fx:if test="position() = 1">
						<fx:attribute name="checked">
							<fx:text>checked</fx:text>
						</fx:attribute>
					</fx:if>
				</input>
				<fx:value-of select="@name" />
			</label>
		</div>
	</fx:template>
	
	<fx:template name="form-send">
		<script>
			jQuery('body').hide(0, function() {
				jQuery(document).ready(function(){
					jQuery('.center form, .appeal form').eq(0).submit();
				});
			});
		</script>
	</fx:template>

</fx:stylesheet>
