<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				exclude-result-prefixes="fx date udt umi">

	<fx:template match="udata[@module = 'photoalbum'][@method = 'albumRandom']"  mode="main-album" />
	<fx:template match="udata[@module = 'photoalbum'][@method = 'albumRandom'][total]"  mode="main-album" >
		<div id="photoalbum" umi:method="album" umi:module="photoalbum" umi:element-id="{id}">
			<div class="descr" umi:element-id="{id}" umi:field-name="descr">
				<fx:value-of select="document(concat('upage://',id,'.descr'))/udata/property/value" disable-output-escaping="yes" />
			</div>
			<div class="clear" />
		</div>
        <div class="slide-5">
            <div class="block-1 clearfix">
                <h1 umi:element-id="{id}" umi:field-name="h1">
                    <fx:value-of select="document(concat('upage://',id,'.h1'))/udata/property/value" disable-output-escaping="yes" />
                </h1>
            </div>
            <div class="block-2 clearfix">
                <ul umi:method="album" umi:module="photoalbum" umi:element-id="{id}" umi:region="list">
                    <fx:apply-templates select="items/item"  mode="main-album"/>
                </ul>
            </div>
        </div>
        <div class="slide-6 clearfix">
            <h1><a id="add_your_photo" class="custom-13" onclick="$('#exampleModal-2').arcticmodal();return false;">&photoalbum-add;</a></h1>
        </div>
        <div style="display: none;">
            <div class="box-modal custom-2" id="exampleModal-2">
                <div class="box-modal_close arcticmodal-close"></div>
                <iframe class="preloader" src="{$lang-prefix}/add_modal/139" width="100%" height="350px" style="border:none"></iframe>
            </div>
        </div>
	</fx:template>

	<fx:template match="udata[@module = 'photoalbum' and @method = 'albumRandom']/items/item" mode="main-album" >
        <li umi:element-id="{@id}" umi:region="row">
            <a href="{document(concat('upage://',@id,'.photo'))/udata/property/value}" rel="colorbox[main]">
                <div class="hover">
                    <div class="box">
                        <p class="custom-1" umi:field-name="name" umi:empty="&empty;" umi:delete="delete">
                            <fx:value-of select="text()"/>
                        </p>
                        <p class="custom-2" umi:field-name="name" umi:empty="&empty;">
                            <fx:value-of select="document(concat('upage://',@id,'.autor'))/udata/property/value"/>
                        </p>
                    </div>
                </div>
                <fx:call-template name="catalog-thumbnail">
                    <fx:with-param name="element-id" select="@id" />
                    <fx:with-param name="field-name">photo</fx:with-param>
                    <fx:with-param name="empty"><fx:value-of select="$empty-photo"/></fx:with-param>
                    <fx:with-param name="width">403</fx:with-param>
                    <fx:with-param name="height">351</fx:with-param>
                    <fx:with-param name="no-fix">1</fx:with-param>
                    <fx:with-param name="mode">default</fx:with-param>
                </fx:call-template>
            </a>
        </li>
	</fx:template>

</fx:stylesheet>