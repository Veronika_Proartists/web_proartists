<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0"
				xmlns:fx="http://www.w3.org/1999/XSL/Transform"
				xmlns:date="http://exslt.org/dates-and-times"
				xmlns:udt="http://umi-cms.ru/2007/UData/templates"
				xmlns:umi="http://www.umi-cms.ru/TR/umi"
				exclude-result-prefixes="xsl date udt umi">

	<fx:template match="result[@module = 'photoalbum'][@method = 'album']">
		<fx:apply-templates select="document(concat('udata://photoalbum/album/',$document-page-id, '?extProps=h1,descr'))/udata" />
	</fx:template>

	<fx:template match="udata[@module = 'photoalbum'][@method = 'album']">
		<div id="photoalbum" umi:method="album" umi:module="photoalbum" umi:element-id="{id}">
			<div class="descr" umi:element-id="{id}" umi:field-name="descr">
				<fx:value-of select="document(concat('upage://',id,'.descr'))/udata/property/value" disable-output-escaping="yes" />
			</div>
			<div class="clear" />
		</div>
	</fx:template>

	<fx:template match="udata[@module = 'photoalbum'][@method = 'album'][total]">
		<div id="photoalbum" umi:method="album" umi:module="photoalbum" umi:element-id="{id}">
			<div class="descr" umi:element-id="{id}" umi:field-name="descr">
				<fx:value-of select="document(concat('upage://',id,'.descr'))/udata/property/value" disable-output-escaping="yes" />
			</div>
			<fx:apply-templates select="items/item" />
			<div class="clear" />
		</div>
		<fx:apply-templates select="total" />
	</fx:template>

	<fx:template match="udata[@module = 'photoalbum' and @method = 'album']/items/item">
		<div class="gray_block" umi:element-id="{@id}">
			<a href="{@link}">
				<fx:call-template name="catalog-thumbnail">
					<fx:with-param name="element-id" select="@id" />
					<fx:with-param name="field-name">photo</fx:with-param>
					<fx:with-param name="empty">&empty-photo;</fx:with-param>
					<fx:with-param name="width">154</fx:with-param>
					<fx:with-param name="height">110</fx:with-param>
				</fx:call-template>
			</a>
			<div class="descr">
				<h3>
					<a href="{@link}" umi:delete="delete" umi:element-id="{@id}" umi:field-name="h1">
						<fx:value-of select=".//property[@name = 'h1']/value" />
					</a>
				</h3>
				<div umi:element-id="{@id}" umi:field-name="descr">
					<fx:value-of select=".//property[@name = 'descr']/value" disable-output-escaping="yes" />
				</div>
			</div>
			<div class="clear" />
		</div>
	</fx:template>

</fx:stylesheet>