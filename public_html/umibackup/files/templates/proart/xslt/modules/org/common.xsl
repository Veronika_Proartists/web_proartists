<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform"
	xmlns:umi="http://www.umi-cms.ru/TR/umi"
    xmlns:php="http://php.net/xsl"
    extension-element-prefixes="php"
    exclude-result-prefixes="php">

	<fx:template match="udata[@module = 'org']//property" priority="1" mode="org">
        <fx:param name="no-id" select="false()"/>
        <fx:param name="no-value" select="false()"/>
        <fx:if test="not($no-id)">
            <fx:attribute name="umi:object-id">
                <fx:value-of select="ancestor::object/@id"/>
            </fx:attribute>
        </fx:if>
        <fx:attribute name="umi:field-name">
            <fx:value-of select="@name"/>
        </fx:attribute>
        <fx:attribute name="umi:empty">
            <fx:choose>
                <fx:when test="@type = 'img_file'">&empty-photo;</fx:when>
                <fx:otherwise><fx:value-of select="title"/></fx:otherwise>
            </fx:choose>
        </fx:attribute>
        <fx:if test="not($no-value)">
            <fx:choose>
                <fx:when test="@type = 'img_file'">
                    <fx:apply-templates select="." mode="org_img"/>
                </fx:when>
                <fx:when test="contains(@name,'email')">
                    <fx:apply-templates select="." mode="org_email"/>
                </fx:when>
                <fx:otherwise>
                    <fx:apply-templates select="."/>
                </fx:otherwise>
            </fx:choose>
        </fx:if>
	</fx:template>

    <fx:template match="property[@type = 'img_file']"  mode="org_img">
        <fx:attribute name="src">&empty-photo;</fx:attribute>
    </fx:template>
    
    <fx:template match="property[@type = 'img_file'][string-length(value) &gt; 0]" mode="org_img">
        <fx:attribute name="src">
            <fx:value-of select="value"/>
        </fx:attribute>
        <fx:if test="contains(@name,'logo')">
            <fx:attribute name="alt">
                <fx:value-of select="../property[@name = 'org_name']/value"/>
            </fx:attribute>
        </fx:if>
    </fx:template>

    <fx:template match="property" mode="org_email">
        <fx:variable name="value">
            <fx:choose>
                <fx:when test="string-length(value) &gt; 0"><fx:value-of select="value"/></fx:when>
                <fx:otherwise><fx:value-of select="concat('info@',$domain)"/></fx:otherwise>
            </fx:choose>
        </fx:variable>
        <fx:attribute name="href">
            mailto:<fx:value-of select="$value"/>
        </fx:attribute>
        <fx:value-of select="$value"/>
    </fx:template>

    <fx:template match="property[starts-with(@name,'script')]" priority="1" mode="org">
        <fx:if test="$user-type = 'sv'">
            <div class="eip_hidden" umi:object-id="{ancestor::object/@id}" umi:field-name="{@name}" umi:empty="{title}">
                <fx:value-of select="value" disable-output-escaping="yes" />
            </div>
        </fx:if>
        <fx:value-of select="php:function('htmlspecialchars_decode', string(value))" disable-output-escaping="yes" />
    </fx:template>
	

</fx:stylesheet>