<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
    <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
    %system;
    <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
    %custom;
]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <fx:template match="result[@module = 'catalog' and @method = 'category']">
        <fx:apply-templates select="." mode="main-banner"/>
        <fx:apply-templates select="." mode="object-list"/>
        <fx:apply-templates select=".//property[@name = 'video']"/>
        <fx:apply-templates select="document('udata://guides/getGuideItems/129/descr,image,class')/udata" mode="how_work"/>
        <fx:apply-templates select="document('usel://reviews')/udata"/>
    </fx:template>

    <fx:template match="result[@module = 'catalog' and @method = 'category']" mode="object-list">
        <div class="catalog-1">
            <div class="wrp clearfix">
                <h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="{.//property[@name = 'h1']/title}">
                    <fx:value-of select=".//property[@name = 'h1']/value"/>
                </h1>
                <h2>Поштучный выбор</h2>
                <ul class="clearfix">
                    <fx:apply-templates select="document(concat('udata://catalog/getCategoryOnly/',$document-page-id))/udata/items/item" mode="&short-view;_pcs"/>
                </ul>
                <fx:call-template name="layout-control"/>
                <fx:apply-templates select="document(concat('udata://custom/catalog/getObjectsListNotField/',$document-page-id,'/only?uri=items/item&amp;mode=short-view'))/udata"/>
            </div>
        </div>
    </fx:template>

    <fx:template match="result[@module = 'catalog' and @method = 'category' and page/@type-id = '&brush-type-id;']" mode="object-list">
        <div class="catalog-2">
            <div class="wrp clearfix">
                <h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="{.//property[@name = 'h1']/title}">
                    <fx:value-of select=".//property[@name = 'h1']/value"/>
                </h1>

                <fx:call-template name="layout-control"/>
                <fx:apply-templates select="document(concat('udata://custom/catalog/getObjectsList/notemp/',$document-page-id,'/?uri=lines/item&amp;mode=short-view-brush'))/udata"/>
            </div>
        </div>
    </fx:template>

    <fx:template match="udata[@module = 'catalog' and @method = 'getObjectsListNotField']">
        <h2>Наборы</h2>
        <ul class="clearfix" umi:call-point="{@call_point}">
            <fx:apply-templates select="items/item" mode="&short-view;"/>
        </ul>
        <fx:if test="total &gt; per_page">&load-more;</fx:if>


        <!--<fx:for-each select="document(concat('udata://catalog/getObjectsListField/',$document-page-id,'/only'))/udata/items/item" mode="&short-view;">
            <a class="button-1" href="{@link}">
                <fx:value-of select="."/>
            </a>
        </fx:for-each>-->
    </fx:template>

    <fx:template match="udata[@module = 'catalog' and @method = 'getObjectsList']">
        <ul class="clearfix" umi:call-point="{@call_point}">
            <fx:apply-templates select="lines/item" mode="short-view-brush"/>
        </ul>
        <fx:if test="total &gt; per_page">&load-more;</fx:if>
    </fx:template>

    <fx:template name="layout-control">
        <form class="filter clearfix">
            <div class="block-1">
                <span class="custom-6">&catalog-sort;:</span>
                <label class="popular">
                    <input type="checkbox" name="order_filter[popular]" value="desc"/>
                    <span>&popular;</span>
                </label>
                <label class="novelty">
                    <input type="checkbox" name="fields_filter[novelty]" value="1"/>
                    <span>&novelty;</span>
                </label>
            </div>
            <div class="block-2">
                <span class="custom-6">&price;:</span>
                <label class="desc">
                    <input type="radio" name="order_filter[price]" value="desc"/>
                    <span>&desc;</span>
                </label>
                <label class="asc">
                    <input type="radio" name="order_filter[price]" value="asc"/>
                    <span>&asc;</span>
                </label>
            </div>
            <fx:apply-templates select="document(concat('udata://catalog/customSearch/',$document-page-id))/udata" mode="search"/>
        </form>
    </fx:template>


</fx:stylesheet>