<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi" xmlns:foxi="http://foxinet.ru">

    <fx:template match="page|item" mode="short-view">
        <li umi:element-id="{@id}" class="load_element">
            <div class="block-1">
                <h3 umi:field-name="name" umi:delete="delete">
                    <fx:value-of select="name" />
                </h3>
                <p umi:field-name="descr_mini" umi:delete="delete">
                    <fx:value-of select="descr" />
                </p>
                <fx:call-template name="catalog-thumbnail">
                    <fx:with-param name="element-id" select="@id" />
                    <fx:with-param name="field-name">photo</fx:with-param>
                    <fx:with-param name="empty" select="$empty-photo"/>
                    <fx:with-param name="width">186</fx:with-param>
                    <fx:with-param name="height">189</fx:with-param>
                    <fx:with-param name="alt" select="name"/>
                    <fx:with-param name="title" select="name"/>
                </fx:call-template>
                <p class="price" umi:field-name="price">
                    <fx:apply-templates select="document(concat('udata://emarket/price/',@id))/udata" />
                </p>
                <div class="buttons clearfix">
                    <a href="{$lang-prefix}/offer_more_info/{@id}" class="gray">&catalog-show-more;</a>
                    <fx:choose>
                        <fx:when test="@not_available">
                            <a href="{$lang-prefix}/phone_modal/196" class="order" data-id="{@id}" data-name="{name}">&catalog-basket-order;</a>
                        </fx:when>
                        <fx:otherwise>
                            <a href="{$lang-prefix}/emarket/basket/put/element/{@id}" class="black">
                                <fx:variable name="page_id" select="@id"/>
                                <fx:choose>
                                    <fx:when test="$cart_items[page/@id = $page_id]">
                                        <fx:value-of select="foxi:string-replace($cart_items[page/@id = $page_id]/amount,'&catalog-basket-add;')"/>
                                    </fx:when>
                                    <fx:otherwise>&catalog-basket-add-empty;</fx:otherwise>
                                </fx:choose>
                            </a>
                        </fx:otherwise>
                    </fx:choose>

                </div>
            </div>
        </li>
    </fx:template>

    <fx:template match="page|item" mode="short-view_pcs">
        <li umi:element-id="{@id}" class="load_element">
            <div class="block-1">
                <h3 umi:field-name="name" umi:delete="delete">
                    <fx:value-of select="name" />
                </h3>
                <p umi:field-name="descr_mini" umi:delete="delete">
                    <fx:value-of select="descr" />
                </p>
                <fx:call-template name="catalog-thumbnail">
                    <fx:with-param name="element-id" select="@id" />
                    <fx:with-param name="field-name">photo</fx:with-param>
                    <fx:with-param name="empty" select="$empty-photo"/>
                    <fx:with-param name="width">186</fx:with-param>
                    <fx:with-param name="height">189</fx:with-param>
                    <fx:with-param name="alt" select="name"/>
                    <fx:with-param name="title" select="name"/>
                </fx:call-template>
                <p class="price" umi:field-name="price">
                    <fx:apply-templates select="document(concat('udata://emarket/price/',@id))/udata" mode="psk"/>
                </p>
                <div class="buttons clearfix">
                    <a href="{@link}" class="darkred">Выбрать цвет</a>

                </div>
            </div>
        </li>
    </fx:template>

    <fx:template match="page|item" mode="short-view-brush">
        <li class="clearfix load_element" umi:element-id="{@id}">
            <h3 umi:field-name="name" umi:delete="delete">
                <fx:value-of select="." />
            </h3>
            <div class="block-1 clearfix">
                <p class="custom-1" umi:field-name="descr_mini" umi:delete="delete">
                    <fx:value-of select="document(concat('upage://',@id,'.descr_mini'))/udata/property/value" />
                </p>
                <p class="custom-2">&catalog-price;
                    <span umi:field-name="price">
                        <fx:apply-templates select="document(concat('udata://emarket/price/',@id))/udata" />
                    </span>
                </p>
            </div>
            <div class="block-2 clearfix">
                <fx:call-template name="catalog-thumbnail">
                    <fx:with-param name="element-id" select="@id" />
                    <fx:with-param name="field-name">photo_brush</fx:with-param>
                    <fx:with-param name="empty" select="$empty-photo"/>
                    <fx:with-param name="width">116</fx:with-param>
                    <fx:with-param name="height">48</fx:with-param>
                    <fx:with-param name="alt" select="."/>
                    <fx:with-param name="title" select="."/>
                    <fx:with-param name="class">custom-1</fx:with-param>
                </fx:call-template>
                <fx:call-template name="catalog-thumbnail">
                    <fx:with-param name="element-id" select="@id" />
                    <fx:with-param name="field-name">photo</fx:with-param>
                    <fx:with-param name="empty" select="$empty-photo"/>
                    <fx:with-param name="width">591</fx:with-param>
                    <fx:with-param name="height">48</fx:with-param>
                    <fx:with-param name="alt" select="."/>
                    <fx:with-param name="title" select="."/>
                    <fx:with-param name="class">custom-2</fx:with-param>
                </fx:call-template>
            </div>
            <form action="{$lang-prefix}/emarket/basket/put/element/{@id}" type="post">
                <div class="block-3">
                    <fx:apply-templates select="document(concat('upage://',@id,'.number_brush'))/udata/property"/>
                    <span class="custom-6">&catalog-amount;</span>
                    <div class="spinner-wrp">
                        <a class="left-button" href="#">-</a>
                        <input type="text" name="amount" value="1"/>
                        <input type="hidden" value="1"/>
                        <a href="#">+</a>
                    </div>
                </div>
                <div class="block-4 buttons">
                    <a href="{$lang-prefix}/offer_more_info/{@id}" class="gray">&catalog-show-more;</a>
                    <fx:choose>
                        <fx:when test="@not_available">
                            <a href="{$lang-prefix}/phone_modal/196" class="order" data-id="{@id}" data-name="{name}">&catalog-basket-order;</a>
                        </fx:when>
                        <fx:otherwise>
                            <input type="submit" class="black">
                                <fx:attribute name="value">
                                    <fx:variable name="page_id" select="@id"/>
                                    <fx:choose>
                                        <fx:when test="$cart_items[page/@id = $page_id]">
                                            <fx:value-of select="foxi:string-replace($cart_items[page/@id = $page_id]/amount,'&catalog-basket-add;')"/>
                                        </fx:when>
                                        <fx:otherwise>&catalog-basket-add-empty;</fx:otherwise>
                                    </fx:choose>
                                </fx:attribute>
                            </input>
                        </fx:otherwise>
                    </fx:choose>
                </div>
            </form>
        </li>
    </fx:template>

    <fx:template match="property[@name = 'number_brush']" priority="1">
        <span class="custom-6"><fx:value-of select="title"/>:</span>
        <select class="selectbox" name="option[{@name}]">
            <fx:for-each select="value/option">
                <option value="{object/@id}">
                    <fx:value-of select="substring-before(substring-after(object/@name,'('),')')"/>
                </option>
            </fx:for-each>
        </select>
        <fx:text> </fx:text>
    </fx:template>

    <fx:template match="property[@name = 'number_brush' and not(value/option)]" priority="1"/>


</fx:stylesheet>