<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <fx:template match="onestep" mode="fast-purchase">
        <div class="box-modal custom-1" umi:element-id="{@id}">
            <div class="box-modal_close arcticmodal-close"></div>
            <div class="wrp clearfix">
                <div class="block-2">
                    <form class="form-buy" action="/emarket/saveInfoYandex" method="POST" onsubmit="site.forms.data.save(this); return site.forms.data.check(this);">
                        <div class="block-3">
                            <div class="box">
                                <fx:apply-templates select="customer/group" mode="form">
                                    <fx:with-param name="uniq_premium_landing" select="1"/>
                                </fx:apply-templates>
                            </div>
                        </div>
                        <hr/>
                        <!--<div class="block-4" id="delivery_choose">
                            <div class="box">
                                <fx:apply-templates select="delivery_choose"/>
                            </div>
                        </div>
                        <div class="block-3" id="delivery">
                            <div class="box" id="delivery_box">
                                <fx:apply-templates select="delivery "/>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block-4" id="payment">
                                    <!--Яндекс-->
                                    <input type="hidden" value="45050" name="payment-id"/>
                                    <!--Курьеру-->
                                    <!--<input type="hidden" value="570" name="payment-id"/>-->

                                    <label class="css-label" for="paymentTypePC">
                                        <input id="paymentTypePC" class="css-checkbox" type="radio" checked="checked" value="PC" name="paymentType"/>
                                        Со счета в Яндекс.Деньгах
                                    </label>

                                    <label class="css-label" for="paymentTypeAC">
                                        <input id="paymentTypeAC" class="css-checkbox" type="radio" value="AC" name="paymentType"/>
                                        С банковской карты
                                        <img class="cards-logo" src="http://proartists.ru/templates/proart/images/landing/cards.png"/>
                                    </label>


                                    <label class="css-label" for="paymentTypeWM">
                                        <input id="paymentTypeWM" class="css-checkbox" type="radio" value="WM" name="paymentType"/>
                                        Со счета WebMoney
                                    </label>

                                    <label class="css-label" for="paymentTypeGP">
                                        <input id="paymentTypeGP" class="css-checkbox" type="radio" value="GP" name="paymentType"/>
                                        По коду через терминал
                                    </label>

                                    <label class="css-label" for="paymentType570">
                                        <input id="paymentType570" class="css-checkbox" type="radio" value="570" name="paymentType"/>
                                        Наличными курьеру
                                    </label>

                                    <div class="buttons clearfix">
                                        <input type="submit" class="button-1" value="Сделать заказ"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
           <!--<p class="custom-3">&catalog-delivery;</p>-->
        </div>
    </fx:template>

    <!--<fx:template match="payment[items/item]" mode="fast-purchase">
        <fx:apply-templates select="items/item[position() = 3]" mode="payment-fast-purchase" />
    </fx:template>-->

    <!--<fx:template match="item" mode="payment-fast-purchase">-->
        <!--<form name="ShopForm" method="POST" action="https://money.yandex.ru/eshop.xml">
            <input type="hidden" name="scid" value="{scid}"/>
            <input type="hidden" name="ShopID" value="{shopId}"/>
            <input type="hidden" name="CustomerNumber" value="{orderId}"/>
            <input type="hidden" name="Sum" value="{Sum}"/>
            <fx:variable name="order-info" select="document(concat('udata://emarket/order/',orderId))/udata"/>
            <input type="hidden" name="CustName" value="{concat($order-info//property[@name = 'lname']/value,' ',$order-info//property[@name = 'fname']/value,' ',$order-info//property[@name = 'father_name']/value)}"/>
            <fx:variable name="address">
                <fx:if test="$order-info//property[@name = 'delivery_addresses']/value/item">
                    <fx:for-each select="document(concat('uobject://',$order-info//property[@name = 'delivery_addresses']/value/item/@id))//property">
                        <fx:value-of select="title"/>: <fx:apply-templates select="value"/>
                        <fx:if test="position() != last()">, </fx:if>
                    </fx:for-each>
                </fx:if>
            </fx:variable>
            <fx:variable name="items">
                <fx:for-each select="$order-info/items/item">
                    <fx:value-of select="@name"/> x <fx:value-of select="amount"/> шт. = <fx:apply-templates select="total-price"/>
                    <fx:if test="position() != last()">; </fx:if>
                </fx:for-each>
            </fx:variable>
            <input type="hidden" name="CustAddr" value="{$address}"/>
            <input type="hidden" name="CustEMail" value="{$order-info//property[@name = 'email']/value}"/>
            <input type="hidden" name="OrderDetails" value="{$items}"/>
            <div class="block-4" id="payment">
                <div class="row">
                    <input id="paymentTypePC" class="css-checkbox" type="radio" checked="checked" value="PC" name="paymentType"/>
                    <label class="css-label" for="paymentTypePC">Со счета в Яндекс.Деньгах</label>
                </div>
                <div class="row">
                    <input id="paymentTypeAC" class="css-checkbox" type="radio" value="AC" name="paymentType"/>
                    <label class="css-label" for="paymentTypeAC">С банковской карты</label>
                </div>
                <div class="row">
                    <input id="paymentTypeWM" class="css-checkbox" type="radio" value="WM" name="paymentType"/>
                    <label class="css-label" for="paymentTypeWM">Со счета WebMoney</label>
                </div>
                <div class="row">
                    <input id="paymentTypeGP" class="css-checkbox" type="radio" value="GP" name="paymentType"/>
                    <label class="css-label" for="paymentTypeGP">По коду через терминал</label>
                </div>
                <div class="buttons clearfix">
                    <input type="submit" class="button-1" value="Оплатить"/>
                </div>
            </div>
        </form>-->

        <!--<div class="row" style="display:none">
            <input id="{generate-id()}" type="radio" class="css-checkbox {@type-name}" name="payment-id" value="{@id}" onclick="window.paymentId = {@id};">
                <fx:if test="@active = 'active' or (not(../item[@active]) and position() = 1)">
                    <fx:attribute name="checked">
                        <fx:text>checked</fx:text>
                    </fx:attribute>
                </fx:if>
            </input>
            <label for="{generate-id()}" class="css-label">
                <fx:value-of select="@name" />
            </label>
        </div>-->
    <!--</fx:template>-->


    <fx:include href="../emarket/common.xsl"/>
    <fx:include href="../data/common.xsl"/>

</fx:stylesheet>