<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
    <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
    %system;
    <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
    %custom;
]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <fx:template match="result[@module = 'catalog' and @method = 'category'][count(parents/page) &gt; 0]" mode="js-variable-additional">
        <script>site.method = 'object';</script>
    </fx:template>

    <fx:template match="result[@module = 'catalog' and @method = 'category'][count(parents/page) &gt; 0]">
        <fx:apply-templates select="." mode="main-banner"/>
        <div class="basket catalog-only">
            <h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="{.//property[@name = 'h1']/title}">
                <fx:value-of select=".//property[@name = 'h1']/value"/>
            </h1>
            <div class="block-2">
                <form class="filter clearfix">
                    <fx:apply-templates select="." mode="object-list-only"/>
                </form>
            </div>
        </div>
        <fx:apply-templates select=".//property[@name = 'video']"/>
        <fx:apply-templates select="document('udata://guides/getGuideItems/129/descr,image,class')/udata" mode="how_work"/>
        <fx:apply-templates select="document('usel://reviews')/udata"/>
    </fx:template>


    <fx:template match="result" mode="object-list-only">
        <table class="table_apiece">
            <thead>
                <tr>
                    <fx:apply-templates select="." mode="object-list-legend"/>
                </tr>
            </thead>
            <fx:apply-templates select="document(concat('udata://custom/catalog/getObjectsOnly/',$document-page-id,'/?uri=items/item&amp;mode=&short-view;_only'))/udata"/>
            <!--<tfoot><tr>
                <td></td>
                <td colspan="4"><p>Итого:
                    <span id="summary_price"> 47 341 руб</span></p></td>
            </tr></tfoot>-->
        </table>
        <!--<a href="/emarket/purchasing_one_step" class="button-1">Оформить заказ</a>-->
    </fx:template>

    <fx:template match="result" mode="object-list-legend">
        <th style="width:25%;">Название</th>
        <th>Артикул</th>
        <th>Палитра</th>
        <th>Объем</th>
        <th>Серия</th>
        <th style="width:15%;">Цена</th>
        <th style="width:15%;">Количество</th>
    </fx:template>

    <fx:template match="result[@module = 'catalog' and @method = 'category']" mode="object-list-legend">
        <th style="width:25%;"><div><label class="nosc"><input type="radio" name="order_filter[article]" value="asc"/><span>Название</span></label></div></th>
        <th><div><label class="nosc"><input type="radio" name="order_filter[name]" value="asc"/><span>Артикул</span></label></div></th>
        <th><div><label class="nosc"><input type="radio" name="order_filter[color_code]" value="asc"/><span>Палитра</span></label></div></th>
        <th><div><label class="nosc"><input type="radio" name="order_filter[volume]" value="asc"/><span>Объем</span></label></div></th>
        <th><div><label class="nosc"><input type="radio" name="order_filter[series]" value="asc"/><span>Серия</span></label></div></th>
        <th style="width:15%;"><div><label class="nosc"><input type="radio" name="order_filter[price]" value="asc"/><span>Цена</span></label></div></th>
        <th style="width:15%;">Количество</th>
    </fx:template>

    <fx:template match="udata[@module = 'catalog' and @method = 'getObjectsOnly']">
        <tbody class="bmx" umi:call-point="{@call_point}">
            <fx:apply-templates select="items/item" mode="&short-view;_only"/>
        </tbody>
    </fx:template>

    <fx:template match="udata[@module = 'catalog' and @method = 'getObjectsOnly']/items/item" mode="&short-view;_only">
        <tr umi:element-id="{@id}" class="offer_{@id}">
            <td>
                <a href="{@link}" class="cart-item-img">
                    <fx:value-of select="@art"/>
                </a>
            </td>
            <td>
                <div class="block-1">
                    <a href="{@link}" class="cart-item-link" umi:field-name="name" umi:delete="delete">
                        <fx:value-of select="name"/>
                    </a>
                </div>
            </td>
            <td>
                <a href="{@link}" class="cart-item-img">
                    <div style="background-color: {@color}" class="color"></div>
                </a>
                <!--<div class="block-1">
                    <a href="{@link}" class="cart-item-link" umi:field-name="name" umi:delete="delete">
                        <fx:value-of select="name"/>
                    </a>
                </div>-->
            </td>
            <td>
                <fx:if test="@volume"><fx:value-of select="@volume"/><fx:text> мл</fx:text></fx:if>
            </td>
            <td>
                <fx:value-of select="@series"/>
            </td>
            <td>
                <span class="price">
                    <fx:apply-templates select="document(concat('udata://emarket/price/',@id))/udata" />
                </span>
            </td>
            <td>
                <fx:choose>
                    <fx:when test="@amount &gt; 0">
                        <div class="spinner-wrp">
                            <a class="left-button" href="#">-</a>
                            <fx:variable name="amount">
                                <fx:variable name="page_id" select="@id"/>
                                <fx:choose>
                                    <fx:when test="$cart_items[page/@id = $page_id]">
                                        <fx:value-of select="$cart_items[page/@id = $page_id]/amount"/>
                                    </fx:when>
                                    <fx:otherwise>0</fx:otherwise>
                                </fx:choose>
                            </fx:variable>
                            <input type="text" class="basket_count" value="{$amount}"/>
                            <input type="hidden" value="{$amount}"/>
                            <a href="#">+</a>
                        </div>
                    </fx:when>
                    <fx:otherwise>
                        <span class="not-available">Нет в наличии</span>
                    </fx:otherwise>
                </fx:choose>

            </td>
        </tr>
    </fx:template>


</fx:stylesheet>