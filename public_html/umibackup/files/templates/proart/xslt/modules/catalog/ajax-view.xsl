<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi">

    <fx:template match="page" mode="ajax-view">
        <div class="box-modal custom-1" umi:element-id="{@id}">
            <div class="box-modal_close arcticmodal-close"></div>
            <div class="wrp clearfix">
                <fx:choose>
                    <fx:when test="properties/group[@name = 'images']/property">
                        <div class="block-0">
                            <div class="slider autoplay-for">
                                <fx:for-each select="properties/group[@name = 'images']/property">
                                    <div>
                                        <fx:call-template name="catalog-thumbnail">
                                            <fx:with-param name="element-id" select="ancestor::page/@id" />
                                            <fx:with-param name="field-name" select="@name"/>
                                            <fx:with-param name="empty" select="$empty-photo"/>
                                            <fx:with-param name="width">339</fx:with-param>
                                            <fx:with-param name="height">356</fx:with-param>
                                            <fx:with-param name="alt" select="name"/>
                                            <fx:with-param name="title" select="name"/>
                                        </fx:call-template>
                                    </div>
                                </fx:for-each>
                            </div>
                            <div class="wrp">
                                <div class="slider autoplay">
                                    <fx:for-each select="properties/group[@name = 'images']/property">
                                        <div>
                                            <fx:call-template name="catalog-thumbnail">
                                                <fx:with-param name="element-id" select="ancestor::page/@id" />
                                                <fx:with-param name="field-name" select="@name"/>
                                                <fx:with-param name="empty" select="$empty-photo"/>
                                                <fx:with-param name="width">80</fx:with-param>
                                                <fx:with-param name="height">80</fx:with-param>
                                                <fx:with-param name="alt" select="name"/>
                                                <fx:with-param name="title" select="name"/>
                                            </fx:call-template>
                                        </div>
                                    </fx:for-each>
                                </div>
                            </div>
                        </div>
                    </fx:when>
                    <fx:otherwise>
                        <div class="block-0">
                            <fx:call-template name="catalog-thumbnail">
                                <fx:with-param name="element-id" select="@id" />
                                <fx:with-param name="field-name">photo</fx:with-param>
                                <fx:with-param name="empty" select="$empty-photo"/>
                                <fx:with-param name="width">339</fx:with-param>
                                <fx:with-param name="height">356</fx:with-param>
                                <fx:with-param name="alt" select="name"/>
                                <fx:with-param name="title" select="name"/>
                                <fx:with-param name="no-fix">1</fx:with-param>
                            </fx:call-template>
                        </div>
                    </fx:otherwise>
                </fx:choose>

                <div class="block-1">
                    <h2 umi:field-name="h1">
                        <fx:value-of select=".//property[@name = 'h1']/value" />
                    </h2>
                    <div umi:field-name="descr">
                        <fx:value-of select=".//property[@name = 'descr']/value" disable-output-escaping="yes"/>
                    </div>
                    <fx:if test=".//property[@name = 'properties']">
                        <h3><fx:value-of select=".//property[@name = 'properties']/title" />:</h3>
                        <div umi:field-name="properties">
                            <fx:value-of select=".//property[@name = 'properties']/value" disable-output-escaping="yes"/>
                        </div>
                    </fx:if>
                    <fx:if test="document(concat('udata://catalog/getTypeId/',@parentId))/udata = &brush-type-id;">
                        <div class="block-3">
                            <fx:apply-templates select=".//property[@name = 'number_brush']"/>
                            <span class="custom-6">&catalog-amount;</span>
                            <div class="spinner-wrp">
                                <a class="left-button" href="#">-</a>
                                <input type="text" name="amount" value="1"/>
                                <input type="hidden" value="1"/>
                                <a href="#">+</a>
                            </div>
                        </div>
                    </fx:if>
                    <div class="price">
                        <p>&catalog-price;</p>
                        <span umi:field-name="price">
                            <fx:apply-templates select="document(concat('udata://emarket/price/',@id))/udata" />
                        </span>
                        <fx:choose>
                            <fx:when test=".//property[@name = 'not_available']/value">
                                <a href="{$lang-prefix}/phone_modal/196" class="button-order order" data-id="{@id}" data-name="{name}">&catalog-basket-order;</a>
                            </fx:when>
                            <fx:otherwise>
                                <a href="{$lang-prefix}/emarket/basket/put/element/{@id}" class="button-1">
                                    <fx:variable name="page_id" select="@id"/>
                                    <fx:choose>
                                        <fx:when test="$cart_items[page/@id = $page_id]">
                                            &basket-add; (<fx:value-of select="$cart_items[page/@id = $page_id]/amount"/>)
                                        </fx:when>
                                        <fx:otherwise>&basket-add;</fx:otherwise>
                                    </fx:choose>
                                </a>
                            </fx:otherwise>
                        </fx:choose>
                    </div>
                </div>
            </div>
           <!--<p class="custom-3">&catalog-delivery;</p>-->
        </div>
    </fx:template>

    <fx:include href="short-view.xsl"/>

</fx:stylesheet>