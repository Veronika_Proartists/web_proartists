<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0"
                  xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                  xmlns:umi="http://www.umi-cms.ru/TR/umi"
                  xmlns:foxi="http://foxinet.ru">

    <fx:variable name="this-page-cart-items" select="$cart_items[page/@id = $document-page-id]"/>

    <fx:template match="result[@module = 'catalog' and @method = 'object']">
        <fx:attribute name="class">center</fx:attribute>
        <div class="basket catalog-only">
            <div class="object-only clearfix">
                <div class="fleft">
                    <fx:apply-templates select="." mode="breadcrumbs"/>
                    <div class="additional_breadcrumbs">
                        <a href="{parents/page[last()]/@link}">&lt;&lt; Ввернуться к</a> поштучному выбору товаров
                    </div>
                    <h1 umi:element-id="{@pageId}" umi:field-name="h1" umi:empty="{.//property[@name = 'h1']/title}">
                        <fx:value-of select=".//property[@name = 'h1']/value"/>
                    </h1>
                    <div class="descr">
                        <fx:value-of select=".//property[@name = 'descr']/value" disable-output-escaping="yes"/>
                    </div>
                </div>
                <div class="fright">
                    <fx:if test=".//property[@name = 'photo']/value">
                        <a href="{.//property[@name = 'photo']/value}" class="offer-img-link">
                            <img class="offer-img" src="{.//property[@name = 'photo']/value}"/>
                        </a>
                    </fx:if>
                </div>
            </div>
            <div class="block-2">
                <fx:apply-templates select="." mode="object-list-only"/>
            </div>
        </div>
        <fx:apply-templates select=".//property[@name = 'video']"/>
        <fx:apply-templates select="document('udata://guides/getGuideItems/129/descr,image,class')/udata" mode="how_work"/>
        <fx:apply-templates select="document('usel://reviews')/udata"/>
    </fx:template>


</fx:stylesheet>