<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
        ]>

<fx:stylesheet	version="1.0"
                  xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                  xmlns:umi="http://www.umi-cms.ru/TR/umi"
                  xmlns:foxi="http://foxinet.ru">

    <fx:variable name="this-page-cart-items" select="$cart_items[page/@id = $document-page-id]"/>

    <fx:template match="result[@module = 'catalog' and @method = 'object']">
        <fx:attribute name="class">center colors</fx:attribute>
        <fx:if test="not(.//property[@type = 'optioned'])">
            <fx:value-of select="document(concat('udata://content/redirect/',page/@parentId))"/>
        </fx:if>
        <fx:apply-templates select="." mode="main-banner"/>
        <div class="slide-9 clearfix">
            <fx:apply-templates select=".//property[@type = 'optioned']" mode="objects-options"/>
            <fx:apply-templates select="document('udata://emarket/cart/')/udata" mode="optioned-cart"/>
        </div>
    </fx:template>

    <fx:template match="property[@type = 'optioned']" mode="objects-options">
        <h1>
            <fx:value-of select="concat('&catalog-change; ',foxi:lower-case(title))"/>
        </h1>
        <fx:variable name="prop-name" select="@name"/>
        <ul class="clearfix">
            <fx:for-each select="document(concat('udata://catalog/getSortPropertyItems/',$document-page-id,'/',@name,'/'))/udata/items">
                <div class="box clearfix">
                    <fx:for-each select="item">
                        <fx:sort select="@name" order="ascending"/>
                        <fx:variable name="option-id" select="@id"/>
                        <li umi:object-id="{@id}">
                            <fx:if test="$this-page-cart-items[options/option/@id = $option-id]">
                                <fx:attribute name="style">display:none</fx:attribute>
                            </fx:if>
                            <p class="custom-4" umi:field-name="name" umi:empty="&empty;">
                                <fx:value-of select="document(concat('uobject://',@id,'.palitra'))/udata/property/value"/>
                            </p>
                            <fx:variable name="color" select="document(concat('uobject://',@id,'.color'))/udata/property/value"/>
                            <p class="custom-5" umi:field-name="palette_code" umi:empty="&empty;">
                                <fx:text># </fx:text><fx:value-of select="@name"/>
                            </p>
                            <a class="color" style="background-color: {$color}" href="{$lang-prefix}/emarket/basket/put/element/{$document-page-id}?option[{$prop-name}]={@id}">
                                <fx:variable name="color-shade" select="document(concat('udata://custom/colorShade/',substring-after(string($color),'#')))/udata"/>
                                <fx:if test="string($color-shade) = 'dark'">
                                    <fx:attribute name="class">color-2</fx:attribute>
                                </fx:if>
                            </a>
                        </li>
                    </fx:for-each>
                </div>
                <fx:if test="position() != last()"><hr/></fx:if>
            </fx:for-each>
        </ul>
    </fx:template>

    <fx:template match="udata[@module = 'emarket' and @method = 'cart']" mode="optioned-cart">
        <div class="amount clearfix">
            <fx:if test="count(items/item[options/option/@field-name = 'color']) = 0">
                <fx:attribute name="style">display: none</fx:attribute>
            </fx:if>
            <div class="block-1 clearfix">
                <span class="custom-1">
                    <fx:value-of select="foxi:string-replace(sum(items/item[options/option/@field-name = 'color']/amount),'&catalog-basket-amount;')"/>
                </span>
                <span class="custom-2">
                    &catalog-basket-summary;
                    <span>
                        <fx:value-of select="summary/price/@prefix"/>
                        <fx:value-of select="foxi:format-price(sum(items/item[options/option/@field-name = 'color']/total-price/actual))"/>
                        <fx:value-of select="summary/price/@suffix"/>
                    </span>
                </span>
            </div>
            <fx:variable name="template"><![CDATA[
                    <div class="row" data-id="#id" data-option="#option">
                        <p>#name</p>
                        <a href="#" class="color" style="background-color: #color"></a>
                        <div class="custom-9"></div>
                        <div class="spinner-wrp">
                            <a href="#" class="left-button">-</a>
                            <input type="text" value="#amount"/>
                            <input type="hidden" value="#amount" />
                            <a href="#">+</a>
                        </div>
                        <span>]]>&amount-prefix;<![CDATA[</span>
                        <a class="close" href="#"></a>
                    </div>
                ]]></fx:variable>
            <script type="text/template" id="item_template">
                <fx:value-of select="$template" disable-output-escaping="yes"/>
            </script>
            <div class="block-2 clearfix">
                <div>
                    <fx:for-each select="items/item[count(options/option[@field-name = 'color']) != 0]">
                        <fx:variable name="color" select="document(concat('uobject://',options/option/@id,'.color'))/udata/property/value"/>
                        <fx:call-template name="foxi:templater">
                            <fx:with-param name="template" select="$template"/>
                            <fx:with-param name="replace-xml">
                                <id><fx:value-of select="@id"/></id>
                                <option><fx:value-of select="options/option/@id"/></option>
                                <name><fx:value-of select="options/option/@name"/></name>
                                <amount><fx:value-of select="amount"/></amount>
                                <color><fx:value-of select="$color"/></color>
                            </fx:with-param>
                        </fx:call-template>
                    </fx:for-each>
                </div>
                <a href="{$lang-prefix}/emarket/cart/" class="button-1">&go-to-cart;</a>
            </div>
        </div>
    </fx:template>

</fx:stylesheet>