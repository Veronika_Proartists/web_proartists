<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet [
        <!ENTITY % system SYSTEM "ulang://i18n/constants.dtd:file">
        %system;
        <!ENTITY % custom  SYSTEM "ulang://i18n/constants.custom.dtd:file">
        %custom;
]>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform" xmlns:umi="http://www.umi-cms.ru/TR/umi" xmlns:foxi="http://foxinet.ru">

    <fx:template match="udata" mode="search">
        <fx:apply-templates select="//field" mode="search"/>
    </fx:template>

    <fx:template match="field" mode="search">
        <div class="block-search">
            <span class="custom-6"><fx:value-of select="@title"/>:</span>
            <fx:for-each select="values/item">
                <label class="label-search">
                    <input type="checkbox" name="fields_filter[{../../@name}][eq][]" value="{@id}"/>
                    <span><fx:value-of select="text()"/></span>
                </label>
            </fx:for-each>
        </div>
    </fx:template>

    <fx:template match="field[count(values/item) = 0]" priority="1" mode="search"/>


</fx:stylesheet>