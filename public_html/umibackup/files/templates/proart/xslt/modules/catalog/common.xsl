<?xml version="1.0" encoding="utf-8"?>

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">

    <fx:variable name="cart_items" select="document('udata://emarket/cart/')/udata/items/item"/>

	<fx:include href="category.xsl" />
	<fx:include href="category-only.xsl" />
	<fx:include href="object.xsl" />
	<fx:include href="short-view.xsl" />
	<fx:include href="search.xsl" />

</fx:stylesheet>