<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://i18n/constants.dtd:file">

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output encoding="utf-8" method="html" indent="yes" />

	<xsl:template match="status_notification">
        <xsl:choose>
            <xsl:when test="status = ' поступил в обработку'">
                <xsl:variable name="order" select="document(concat('uobject://',order_id))" />
                <strong><xsl:text>Спасибо за Ваш заказ! </xsl:text></strong>
                <br/>
                <br/>
                <xsl:text>Ваш заказ №</xsl:text>
                <strong><xsl:value-of select="order_number" /></strong>
                <br/>
                <br/>
                <xsl:text>Содержание заказа:</xsl:text>
                <br/>
                <br/>
                <table cellpadding="0" cellspacing="0" border="0" style="width:600px">
                    <tr>
                        <td style="text-align: left; font-weight: bold" width="50%">
                            <xsl:text>Название</xsl:text>
                        </td>
                        <td style="text-align: left; font-weight: bold" width="15%">
                            <xsl:text>Количество</xsl:text>
                        </td>
                    </tr>
                    <xsl:apply-templates select="$order//property[@name='order_items']/value/item" mode="order-item" />
                </table>
                <br/>
                <br/>
                <xsl:text>Мы свяжемся с Вами в ближайшее время и согласуем детали доставки.</xsl:text>
                <br/>
                <br/>
                <br/>
                <xsl:text>С уважением, команда Proartists.ru</xsl:text>
                <br/>
                <xsl:text>Телефон для связи: +7-911-922-86-65</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Ваш заказ №</xsl:text>
                <xsl:value-of select="order_number" />
                <xsl:text> </xsl:text>
                <xsl:value-of select="status" />
            </xsl:otherwise>
        </xsl:choose>
	</xsl:template>

    <xsl:template match="item" mode="order-item">
        <xsl:variable name="item" select="document(concat('uobject://',@id))" />
        <tr>
            <td style="text-align: left">
                <!--<a href="{document(concat('upage://',$item//property[@name='item_link']//value/page/@id))/udata/page/@link}">-->
                <xsl:value-of select="document(concat('upage://',$item//property[@name='item_link']//value/page/@id))/udata/page/name" disable-output-escaping="yes" />
                <!--</a>-->
            </td>
            <td style="text-align: left">
                <xsl:value-of select="$item//property[@name='item_amount']//value" />
            </td>
        </tr>
    </xsl:template>

	<xsl:template match="status_notification_receipt">
		<xsl:text>Ваш заказ #</xsl:text>
		<xsl:value-of select="order_number" />
		<xsl:text> </xsl:text>
		<xsl:value-of select="status" />
		<br/><br/>
<!--		<xsl:text>Посмотреть историю заказов вы можете в своем </xsl:text>
		<a href="http://{domain}/emarket/personal/">
			<xsl:text>личном кабинете</xsl:text>
		</a>.
		<br/><br/>-->
		<xsl:text>Квитанцию на оплату вы можете получить, перейдя по </xsl:text>
		<a href="http://{domain}/emarket/receipt/{order_id}/{receipt_signature}/">
			<xsl:text>этой ссылке</xsl:text>
		</a>.
	</xsl:template>

	<xsl:template match="neworder_notification">
		<xsl:text>Поступил новый заказ #</xsl:text>
		<xsl:value-of select="order_number" />
		<xsl:text> (</xsl:text>
		<a href="http://{domain}/admin/emarket/order_edit/{order_id}/">
			<xsl:text>Просмотр</xsl:text>
		</a>
		<xsl:text>)</xsl:text><br/><br/>
		<xsl:text>Способ оплаты: </xsl:text>
		<xsl:value-of select="payment_type" /><br/>
		<xsl:text>Статус оплаты: </xsl:text>
		<xsl:value-of select="payment_status" /><br/>
		<xsl:text>Сумма оплаты:  </xsl:text>
		<xsl:value-of select="price" /><br/>
	</xsl:template>

</xsl:stylesheet>