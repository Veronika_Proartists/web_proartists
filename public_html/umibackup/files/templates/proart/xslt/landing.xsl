<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"udata://custom/systemInfo">

<fx:stylesheet	version="1.0"
	xmlns:fx="http://www.w3.org/1999/XSL/Transform">

	<fx:output encoding="utf-8" method="html" indent="yes"/>

    <fx:include href="library/common.xsl" />
    <fx:include href="layouts/landing.xsl" />

    <fx:include href="modules/&current_module;/common.xsl" />
    <fx:include href="modules/all-page.xsl" />

</fx:stylesheet>