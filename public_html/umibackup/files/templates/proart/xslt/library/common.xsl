<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.custom.dtd:file">

<fx:stylesheet	version="1.0"
                   xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                   xmlns:php="http://php.net/xsl"
                   xmlns:math="http://exslt.org/math"
                   xmlns:foxi="http://foxinet.ru"
                   xmlns:func="http://exslt.org/functions"
                   xmlns:exsl="http://exslt.org/common"
                   extension-element-prefixes="php func exsl"
                   exclude-result-prefixes="php">

	<fx:include href="errors.xsl" />
	<fx:include href="date.xsl" />
	<fx:include href="captcha.xsl" />
	<fx:include href="numpages.xsl" />
	<fx:include href="thumbnails.xsl" />
	<fx:include href="templates.xsl" />
	<!--<fx:include href="premium_landing.xsl" />-->
	<fx:include href="variables.xsl" />

	<fx:template match="/">
		<fx:choose>
			<fx:when test="count(udata) = 0">
				<fx:apply-templates select="." mode="layout" />
			</fx:when>
			<fx:otherwise>
				<fx:apply-templates />
			</fx:otherwise>
		</fx:choose>
	</fx:template>
	
	<fx:template match="result">
		<p>
			<fx:text>Не найден шаблон для данной страницы. Возможно вы ошиблись адресом</fx:text>
		</p>
	</fx:template>

    <!-- *****************Перменные js****************** -->

    <fx:template match="result" mode="js-variable">
		<script type="text/javascript">
            <![CDATA[]]>
            var site = {};
            site = {<fx:apply-templates select="attribute::*[not(contains(name(),':'))]" mode="js-variable"/>};
            site.user = {<fx:apply-templates select="user/attribute::*[not(contains(name(),':'))]" mode="js-variable"/>};<fx:if test="page">
            site.page = {<fx:apply-templates select="page/attribute::*[not(contains(name(),':'))]" mode="js-variable"/>};</fx:if>
            site.template_resources = '<fx:value-of select="$template-resources"/>';
            <![CDATA[]]>
        </script>
	</fx:template>

    <fx:template match="result" mode="js-variable-additional"/>

    <fx:template match="attribute::*" mode="js-variable">
        <fx:variable name="q"><fx:text>&#39;</fx:text></fx:variable>
        <fx:value-of select="translate(name(),':-','__')"/> : '<fx:value-of select="translate(.,$q,'')"/>'<fx:if test="position() != last()">,</fx:if>
    </fx:template>

    <!-- *****************хлебные крошки****************** -->
    <fx:template match="result" mode="breadcrumbs">
        <div class="breadcrumbs">
            <fx:if test=".//property[@name = 'white_breadcrumbs']/value">
                <fx:attribute name="class">breadcrumbs custom-1</fx:attribute>
            </fx:if>
            <span class="custom-1">
                <a href="/">&main-page;</a>
            </span>
            <fx:apply-templates select="parents" mode="breadcrumbs"/>
            <fx:apply-templates select="udata" mode="breadcrumbs"/>
        </div>
    </fx:template>

    <fx:template match="parents" mode="breadcrumbs">
        <fx:apply-templates select="page" mode="breadcrumbs"/>
        <fx:apply-templates select="following-sibling::page" mode="breadcrumbs_end"/>
    </fx:template>

    <fx:template match="page" mode="breadcrumbs_end">
        <span>
            <fx:value-of select="name"/>
        </span>
    </fx:template>

    <fx:template match="page" mode="breadcrumbs">
        <span class="custom-1">
            <a href="{@link}">
                <fx:value-of select="name"/>
            </a>
        </span>
    </fx:template>

    <fx:template match="udata" mode="breadcrumbs">
        <span>
            <fx:value-of select="../@header"/>
        </span>
    </fx:template>
    <!-- ****************хлебные крошки******************* -->

    <func:function name="foxi:declension">
        <fx:param name="number"/>
        <fx:param name="f0"/>
        <fx:param name="f1"/>
        <fx:param name="f2"/>
        <func:result>
            <fx:call-template name="declension">
                <fx:with-param name="number" select="$number"/>
                <fx:with-param name="f0" select="$f0"/>
                <fx:with-param name="f1" select="$f1"/>
                <fx:with-param name="f2" select="$f2"/>
            </fx:call-template>
        </func:result>
    </func:function>

    <fx:template name="declension">
        <fx:param name="number" select="number"/>
        <fx:param name="f0" select="f0" />
        <fx:param name="f1" select="f1" />
        <fx:param name="f2" select="f2" />
        <fx:variable name="absnum" select="math:abs(number($number))"/>
        <fx:choose>
            <fx:when test="($absnum mod 10) = 1 and ($absnum mod 100) != 11">
                <fx:value-of select="$f0"/>
            </fx:when>
            <fx:when test="(($absnum mod 10) &gt;= 2) and (($absnum mod 10) &lt;= 4) and (($absnum mod 100 &lt; 10) or ($absnum mod 100 &gt;= 20))">
                <fx:value-of select="$f1"/>
            </fx:when>
            <fx:otherwise>
                <fx:value-of select="$f2"/>
            </fx:otherwise>
        </fx:choose>
    </fx:template>

    <func:function name="foxi:format-price">
        <fx:param name="price"/>
        <func:result>
            <fx:call-template name="format-price">
                <fx:with-param name="price" select="$price"/>
            </fx:call-template>
        </func:result>
    </func:function>

    <fx:template name="format-price">
        <fx:param name="price"/>
        <fx:if test="$price">
            <fx:value-of select="php:function('number_format',string($price),0,'',' ')" />
        </fx:if>
    </fx:template>

    <func:function name="foxi:lower-case">
        <fx:param name="str"/>
        <func:result>
            <fx:call-template name="lower-case">
                <fx:with-param name="str" select="$str"/>
            </fx:call-template>
        </func:result>
    </func:function>

    <fx:template name="lower-case">
        <fx:param name="str"/>
        <fx:if test="$str">
            <fx:value-of select="php:function('mb_strtolower',string($str),'UTF-8')" />
        </fx:if>
    </fx:template>

    <func:function name="foxi:upper-case">
        <fx:param name="str"/>
        <func:result>
            <fx:call-template name="upper-case">
                <fx:with-param name="str" select="$str"/>
            </fx:call-template>
        </func:result>
    </func:function>

    <fx:template name="upper-case">
        <fx:param name="str"/>
        <fx:if test="$str">
            <fx:value-of select="php:function('mb_strtoupper',string($str),'UTF-8')" />
        </fx:if>
    </fx:template>

    <func:function name="foxi:string-replace">
        <fx:param name="replace"></fx:param>
        <fx:param name="string"/>
        <fx:param name="template" select="string('#s')"/>
        <func:result>
            <fx:call-template name="string-replace">
                <fx:with-param name="replace" select="$replace"/>
                <fx:with-param name="string" select="$string"/>
                <fx:with-param name="template" select="$template"/>
            </fx:call-template>
        </func:result>
    </func:function>
    
    <fx:template name="string-replace">
        <fx:param name="replace"></fx:param>
        <fx:param name="string"/>
        <fx:param name="template" select="string('#s')"/>
        <fx:value-of select="php:function('str_replace',$template,string($replace),string($string))"/>
    </fx:template>

    <func:function name="foxi:if">
        <fx:param name="bool" select="false()"/>
        <fx:param name="val_true"/>
        <fx:param name="val_false"/>
        <fx:choose>
            <fx:when test="boolean($bool)">
                <func:result select="$val_true" />
            </fx:when>
            <fx:otherwise>
                <func:result select="$val_false" />
            </fx:otherwise>
        </fx:choose>
    </func:function>

    <fx:template name="foxi:templater">
        <fx:param name="template"/>
        <fx:param name="replace-xml"/>
        <fx:apply-templates select="exsl:node-set($replace-xml)/*[1]" mode="foxi-templater">
            <fx:with-param name="template" select="$template"/>
        </fx:apply-templates>
    </fx:template>

    <fx:template match="*" mode="foxi-templater">
        <fx:param name="template"/>
        <fx:variable name="new-template" select="foxi:string-replace(text(),$template,concat('#',name()))"/>
        <fx:choose>
            <fx:when test="following-sibling::*">
                <fx:apply-templates select="following-sibling::*[1]" mode="foxi-templater">
                    <fx:with-param name="template" select="$new-template"/>
                </fx:apply-templates>
            </fx:when>
            <fx:otherwise>
                <fx:value-of select="exsl:node-set($new-template)" disable-output-escaping="yes"/>
            </fx:otherwise>
        </fx:choose>
    </fx:template>

</fx:stylesheet>