<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM "ulang://i18n/constants.custom.dtd:file">

<fx:stylesheet version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">
	<fx:template match="udata[@module = 'system' and @method = 'captcha']" />
	<fx:template match="udata[@module = 'system' and @method = 'captcha' and count(url)]">
        <div class="row captcha">
            <label>
                <p class="custom-9">&enter-captcha;:</p>
                <input name="captcha" class="txt required add_error" type="text"/>
                <img src="{url}{url/@random-string}" id="captcha_img" />
                <a id="captcha_reset"></a>
            </label>
        </div>
	</fx:template>

    <fx:template match="udata[@module = 'system' and @method = 'captcha']"  mode="form"/>
    <fx:template match="udata[@module = 'system' and @method = 'captcha' and count(url)]" mode="form">
        <div>
            <a href="#" id="captcha_reset"></a>
        </div>
        <input type="text" class="captcha_code required " name="captcha" placeholder="&enter-captcha;"/>
        <div class="captcha_image">
            <img src="{url}{url/@random-string}" id="captcha_img" />
        </div>
	</fx:template>

    <fx:template match="udata[@module = 'system' and @method = 'captcha']"  mode="landing"/>
    <fx:template match="udata[@module = 'system' and @method = 'captcha' and count(url)]" mode="landing">
        <div class="order__label _captcha">
            <div class="row">
                <div class="fx-sm-6">
                    <input type="text" class="order__input required" name="captcha" placeholder="&enter-captcha;"/>
                </div>
                <div class="fx-sm-6">
                    <div class="captcha_image">
                        <img src="{url}{url/@random-string}" id="captcha_img" />
                        <a href="#" id="captcha_reset">Обновить код</a>
                    </div>
                </div>
            </div>
        </div>
    </fx:template>
</fx:stylesheet>