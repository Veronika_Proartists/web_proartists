<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">
	<fx:template match="udata[@module = 'system' and @method = 'listErrorMessages']" />

	<fx:template match="udata[@module = 'system' and @method = 'listErrorMessages'][count(items/item) &gt; 0]">
		<div class="errors">
			<h3>
				<fx:text>&errors;:</fx:text>
			</h3>
			<ul>
				<fx:apply-templates select="items/item" mode="error" />
			</ul>
		</div>
	</fx:template>
	
	<fx:template match="item" mode="error">
		<li>
			<fx:value-of select="." />
		</li>
	</fx:template>


	<fx:template match="error">
		<div class="errors">
			<h3>
				<fx:text>&errors;:</fx:text>
			</h3>
			<ul>
				<li>
					<fx:value-of select="." />
				</li>
			</ul>
		</div>
	</fx:template>
</fx:stylesheet>