<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">
	<fx:template match="total" />
	<fx:template match="total[. &gt; ../per_page]">
		<fx:apply-templates select="document(concat('udata://system/numpages/', ., '/', ../per_page))" />
	</fx:template>
	
	<fx:template match="udata[@method = 'numpages']" />
	<fx:template match="udata[@method = 'numpages'][count(items)]">
		<div class="numpages">
			<div class="links">
				<fx:if test="toprev_link and not(tonext_link)">
					<span>
						<fx:text>|</fx:text>
					</span>
				</fx:if>
				<fx:apply-templates select="toprev_link|tonext_link" />
			</div>
			<div class="pages">
				<fx:apply-templates select="items/item" mode="numpages" />
			</div>
		</div>
	</fx:template>
	
	<fx:template match="item" mode="numpages">
		<a href="{@link}">
			<fx:value-of select="." />
		</a>
	</fx:template>
	
	<fx:template match="item[@is-active = '1']" mode="numpages">
		<span>
			<fx:value-of select="." />
		</span>
	</fx:template>
	
	<fx:template match="toprev_link">
		<a class="prev" href="{.}">
			<fx:text>&previous-page;</fx:text>
		</a>
	</fx:template>
	
	<fx:template match="tonext_link">
		<span>
			<fx:text>|</fx:text>
		</span>
		<a class="next" href="{.}">
			<fx:text>&next-page;</fx:text>
		</a>
	</fx:template>

	<fx:template match="item" mode="slider">
		<fx:apply-templates select="preceding-sibling::item[1]" mode="slider_back" />
		<fx:apply-templates select="following-sibling::item[1]" mode="slider_next" />
	</fx:template>

	<fx:template match="item" mode="slider_back">
		<a href="{@link}" title="&previous-page;" class="back" />
	</fx:template>

	<fx:template match="item" mode="slider_next">
		<a href="{@link}" title="&next-page;" class="next" />
	</fx:template>

</fx:stylesheet>