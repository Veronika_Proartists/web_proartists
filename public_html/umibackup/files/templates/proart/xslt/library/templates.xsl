<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet version="1.0"
               xmlns:fx="http://www.w3.org/1999/XSL/Transform"
               xmlns:umi="http://www.umi-cms.ru/TR/umi"
               xmlns:xlink="http://www.w3.org/TR/xlink">

    <fx:template match="result" mode="main-banner">
        <div class="preview-slide" umi:element-id="{@pageId}">
            <fx:if test="@method = 'object'">
                <fx:attribute name="class">preview-slide custom-1</fx:attribute>
            </fx:if>
            <fx:if test=".//property[@name = 'image']/value">
                <img src="{.//property[@name = 'image']/value}" title="{$document-title}" alt="{$document-title}" umi:field-name="image" umi:empty="{$empty-photo}"/>
            </fx:if>
            <div class="block-1">
                <fx:apply-templates select="." mode="breadcrumbs"/>
                <div umi:field-name="descr" umi:empty="&empty-page-content;">
                    <fx:value-of select=".//property[@name = 'descr']/value" disable-output-escaping="yes"/>
                </div>
            </div>
        </div>
    </fx:template>

    <fx:template match="result[page/@is-default = '1']" mode="main-banner">
        <div class="slide-1" umi:element-id="{@pageId}">
            <img src="{.//property[@name = 'image']/value}" title="{$document-title}" alt="{$document-title}" umi:field-name="image" umi:empty="{$empty-photo}"/>
        </div>
    </fx:template>

    <fx:template match="udata[@module = 'usel' and @method = 'getAction']/item" mode="action">
        <div class="slide-3" umi:object-id="{@id}">
            <div class="block-1 clearfix">
                <h1 umi:field-name="name" umi:empty="&empty-page-name;">
                    <fx:value-of select="@name"/>
                </h1>
            </div>
            <div class="block-2 clearfix">
                <img src="{.//property[@name = 'image']/value}" title="{@name}" alt="{@name}"/>
                <div class="descr" umi:field-name="text" umi:empty="&empty-page-content;">
                    <fx:value-of select=".//property[@name = 'text']/value" disable-output-escaping="yes"/>
                </div>
                <div class="callback">
                    <fx:apply-templates select="document('udata://webforms/add/124')/udata"/>
                </div>
            </div>
        </div>
    </fx:template>

    <fx:template match="result[page/@alt-name = 'contacts']" priority="1">
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <div class="preview-slide contacts" umi:element-id="{@pageId}">
            <div id="map"></div>
            <div class="block-1">
                <fx:apply-templates select="." mode="breadcrumbs"/>
                <h4 umi:field-name="h1" umi:empty="&empty-page-name;">
                    <fx:value-of select=".//property[@name = 'h1']/value"/>
                </h4>
                <div umi:field-name="content" umi:empty="&empty-page-content;">
                    <fx:value-of select=".//property[@name = 'content']/value" disable-output-escaping="yes"/>
                </div>
            </div>
        </div>
        <script type="text/javascript"><![CDATA[
            google.maps.Map.prototype.setCenterWithOffset= function(latlng, offsetX, offsetY) {
                var map = this;
                var ov = new google.maps.OverlayView();
                ov.onAdd = function() {
                    var proj = this.getProjection();
                    var aPoint = proj.fromLatLngToContainerPixel(latlng);
                    aPoint.x = aPoint.x+offsetX;
                    aPoint.y = aPoint.y+offsetY;
                    map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
                };
                ov.draw = function() {};
                ov.setMap(this);
            };
            var point = new google.maps.LatLng(]]><fx:value-of select="concat($org-prop[@name = 'lat']/value,',',$org-prop[@name = 'lng']/value)"/><![CDATA[);
            var myMapOptions = {
                scrollwheel:false,
                zoom: ]]><fx:value-of select="$org-prop[@name = 'zoom']/value"/><![CDATA[,
                center: point,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    position: google.maps.ControlPosition.TOP_RIGHT },
                navigationControl: true,
                navigationControlOptions: {
                    style: google.maps.NavigationControlStyle.SMALL,
                    position: google.maps.ControlPosition.BOTTOM_CENTER},
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_CENTER},
                panControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP},
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById('map'),myMapOptions);
            var image = new google.maps.MarkerImage(
                    ']]><fx:value-of select="$org-prop[@name = 'map_pointer']/value"/><![CDATA[',
                    new google.maps.Size(]]><fx:value-of select="concat($org-prop[@name = 'map_pointer']/value/@width,',',$org-prop[@name = 'map_pointer']/value/@height)"/><![CDATA[),
                    new google.maps.Point(0,0),
                    new google.maps.Point(]]><fx:value-of select="concat($org-prop[@name = 'map_pointer']/value/@width div 2,',',$org-prop[@name = 'map_pointer']/value/@height)"/><![CDATA[)
            );
            var marker = new google.maps.Marker({
                draggable:false,
                raiseOnDrag: false,
                icon: image,
                map: map,
                position: point
            });
            map.setCenterWithOffset(point, -150, -25);
            ]]></script>
        <div style="min-height:500px"></div>
    </fx:template>

    <fx:template match="property[@name = 'video']" priority="1">
        <div class="slide-8">
            <fx:apply-templates select="document(value/page/attribute::*[starts-with(name(),'xlink')])/udata/page"/>
        </div>
    </fx:template>

    <fx:template match="page[basetype/@module = 'videoalbum' and basetype/@method ='video']" priority="1">
        <iframe width="560" height="315" src="//www.youtube.com/embed/{substring-after(string(.//property[@name = 'url']/value),'watch?v=')}" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
    </fx:template>

    <fx:template match="page[basetype/@module = 'videoalbum' and basetype/@method ='video' and .//property[@name = 'photo']]" priority="1">
        <fx:variable name="photo" select=".//property[@name = 'photo']/value"/>
        <img src="{$photo}" width="{$photo/@width}" height="{$photo/@height}"/>
        <iframe class="resize" src="//www.youtube.com/embed/{translate(substring-after(string(.//property[@name = 'url']/value),'watch?v='),'&amp;','?')}" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
    </fx:template>

    <fx:template match="udata" mode="how_work">
        <div class="how-we-work clearfix">
            <div class="wrp clearfix">
                <h1 umi:type-id="{type/@id}" umi:field-name="title">
                    <fx:value-of select="type/@title"/>
                </h1>
                <ul class="clearfix">
                    <fx:for-each select="items/item">
                        <li umi:object-id="{@id}">
                            <fx:if test=".//property[@name = 'class']">
                                <fx:attribute name="class">
                                    <fx:value-of select=".//property[@name = 'class']/value"/>
                                </fx:attribute>
                            </fx:if>
                            <img src="{.//property[@name = 'image']/value}" umi:field-name="image" umi:empty="{$empty-photo}" umi:delete="delete"/>
                            <p class="custom-1" umi:field-name="name">
                                <fx:value-of select="concat(position(),'. ',@name)"/>
                            </p>
                            <p class="custom-2" umi:field-name="descr" umi:empty="{.//property[@name = 'descr']/title}">
                                <fx:value-of select=".//property[@name = 'descr']/value"/>
                            </p>
                        </li>
                    </fx:for-each>
                </ul>
            </div>
        </div>
    </fx:template>

    <fx:template match="property[@name = 'quote']/value/item" priority="1">
        <div class="quote" umi:object-id="{@id}">
            <div class="author clearfix">
                <h1 umi:field-name="name">
                    <fx:value-of select="@name"/>
                </h1>
            </div>
            <div class="txt clearfix">
                <p umi:field-name="text">
                    <fx:value-of select="document(concat('uobject://',@id,'.text'))/udata/property/value" disable-output-escaping="yes"/>
                </p>
            </div>
        </div>
    </fx:template>


</fx:stylesheet>