<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">
<fx:stylesheet version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">

    <fx:variable name="errors"	select="document('udata://system/listErrorMessages')/udata"/>

    <fx:variable name="lang-prefix" select="/result/@pre-lang" />
    <fx:variable name="document-page-id" select="/result/@pageId" />
    <fx:variable name="document-title" select="/result/@title" />

    <fx:variable name="request-uri" select="/result/@request-uri" />
    <fx:variable name="domain" select="/result/@domain" />
    <fx:variable name="is-default" select="/result/page/@is-default"/>

    <fx:variable name="org-prop" select="document('udata://org/getter')//property"/>
    <fx:variable name="empty-photo">
        <fx:choose>
            <fx:when test="$org-prop[@name = 'empty-photo']/value">
                <fx:value-of select="$org-prop[@name = 'empty-photo']/value"/>
            </fx:when>
            <fx:otherwise>&empty-photo;</fx:otherwise>
        </fx:choose>
    </fx:variable>

    <fx:variable name="user-id" select="/result/user/@id" />
    <fx:variable name="user-type" select="/result/user/@type" />

    <fx:variable name="module" select="/result/@module" />
    <fx:variable name="method" select="/result/@method" />

    <fx:param name="p">0</fx:param>
    <fx:param name="catalog" />
    <fx:param name="search_string" />

</fx:stylesheet>