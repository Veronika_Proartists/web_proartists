<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform">

	<fx:template name="format-date">
		<fx:param name="date" />
		<fx:param name="pattern" select="'d.m.Y'" />
		<fx:variable name="uri" select="concat('udata://system/convertDate/', $date, '/(', $pattern, ')')" />
		
		<fx:value-of select="document($uri)/udata" />
	</fx:template>
	
	<fx:template match="property[@type = 'date']">
		<fx:param name="pattern" select="'&default-date-format;'" />
		<fx:call-template name="format-date">
			<fx:with-param name="date" select="value/@unix-timestamp" />
			<fx:with-param name="pattern" select="$pattern" />
		</fx:call-template>
	</fx:template>

</fx:stylesheet>