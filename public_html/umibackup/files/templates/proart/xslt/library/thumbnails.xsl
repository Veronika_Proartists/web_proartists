<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE fx:stylesheet SYSTEM	"ulang://i18n/constants.dtd:file">

<fx:stylesheet	version="1.0" xmlns:fx="http://www.w3.org/1999/XSL/Transform"
                  xmlns:umi="http://www.umi-cms.ru/TR/umi"
                  xmlns:foxi="http://foxinet.ru"
                  xmlns:func="http://exslt.org/functions"
                  extension-element-prefixes="func">

    <func:function name="foxi:thumbnail">
        <fx:param name="src" />
        <fx:param name="width">auto</fx:param>
        <fx:param name="height">auto</fx:param>
        <fx:param name="mode">canvas</fx:param>
        <fx:param name="empty"><fx:value-of select="$empty-photo"/></fx:param>
        <func:result>
            <fx:value-of select="document(concat('udata://custom/makeThumbnailImagic/(.',foxi:if($src,$src,$empty),')/', $width, '/', $height, '/', $mode,'/'))/udata/src"/>
        </func:result>
    </func:function>

	<fx:template name="catalog-thumbnail">
		<fx:param name="element-id" />
		<fx:param name="field-name" />
		<fx:param name="empty" />
		<fx:param name="width">auto</fx:param>
		<fx:param name="height">auto</fx:param>
		<fx:param name="item">1</fx:param>
		<fx:param name="align" />
		<fx:param name="alt" />
		<fx:param name="title" />
		<fx:param name="class" />
		<fx:param name="no-fix" />
        <fx:param name="mode">canvas</fx:param>
		<fx:param name="getSrc" />

		<fx:variable name="property" select="document(concat('upage://', $element-id, '.', $field-name))/udata/property" />
		
		<fx:call-template name="thumbnail">
			<fx:with-param name="width" select="$width" />
			<fx:with-param name="height" select="$height" />
			<fx:with-param name="align" select="$align" />
			<fx:with-param name="item" select="$item"/>
            <fx:with-param name="alt" select="$alt" />
            <fx:with-param name="title" select="$title" />
            <fx:with-param name="class" select="$class" />
            <fx:with-param name="no-fix" select="$no-fix" />
			<fx:with-param name="element-id" select="$element-id" />
			<fx:with-param name="field-name" select="$field-name" />
			<fx:with-param name="empty" select="$empty" />
			<fx:with-param name="mode" select="$mode" />
			<fx:with-param name="getSrc" select="$getSrc" />

			<fx:with-param name="src">
				<fx:choose>
					<fx:when test="$property/value">
						<fx:value-of select="$property/value" />
					</fx:when>
					<fx:otherwise><fx:value-of select="$empty-photo"/></fx:otherwise>
				</fx:choose>
			</fx:with-param>
		</fx:call-template>
	</fx:template>

	<fx:template name="thumbnail">
		<fx:param name="src" />
		<fx:param name="width">auto</fx:param>
		<fx:param name="height">auto</fx:param>
		<fx:param name="empty" />
		<fx:param name="align" />
		<fx:param name="item" />
        <fx:param name="alt" />
        <fx:param name="title" />
        <fx:param name="class" />
        <fx:param name="no-fix" />
		<fx:param name="element-id" />
		<fx:param name="field-name" />
		<fx:param name="getSrc" />
		<fx:param name="mode">canvas</fx:param>
        <fx:variable name="request">
            <fx:choose>
                <fx:when test="$mode = 'default'">
                    <fx:value-of select="concat('udata://system/makeThumbnailFull/(.', $src, ')/', $width, '/', $height, '/void/0/1///100')"/>
                </fx:when>
                <fx:otherwise>
                    <fx:value-of select="concat('udata://custom/makeThumbnailImagic/(.', $src, ')/', $width, '/', $height, '/', $mode,'/')"/>
                </fx:otherwise>
            </fx:choose>
        </fx:variable>
        <fx:apply-templates select="document($request)/udata">
			<fx:with-param name="element-id" select="$element-id" />
			<fx:with-param name="field-name" select="$field-name" />
			<fx:with-param name="empty" select="$empty" />
			<fx:with-param name="align" select="$align" />
			<fx:with-param name="item" select="$item" />
            <fx:with-param name="alt" select="$alt" />
            <fx:with-param name="title" select="$title" />
            <fx:with-param name="class" select="$class" />
            <fx:with-param name="no-fix" select="$no-fix" />
            <fx:with-param name="getSrc" select="$getSrc" />
		</fx:apply-templates>
	</fx:template>
	
	<fx:template match="udata[(@module = 'custom' or @module = 'system') and (@method = 'makeThumbnail' or @method = 'makeThumbnailFull' or @method = 'makeThumbnailImagic')]">
		<fx:param name="element-id" />
		<fx:param name="field-name" />
		<fx:param name="empty" />
		<fx:param name="align" />
		<fx:param name="item" />
        <fx:param name="alt" />
        <fx:param name="title" />
        <fx:param name="class" />
        <fx:param name="no-fix" />
        <fx:param name="getSrc"/>
        <fx:choose>
            <fx:when test="$getSrc">
                <fx:attribute name="{$getSrc}"><fx:value-of select="src"/></fx:attribute>
            </fx:when>
            <fx:otherwise>
                <img src="{src}">
                    <fx:if test="$element-id and $field-name">
                        <fx:attribute name="umi:element-id">
                            <fx:value-of select="$element-id" />
                        </fx:attribute>
                        <fx:attribute name="umi:field-name">
                            <fx:value-of select="$field-name" />
                        </fx:attribute>
                    </fx:if>
                    <fx:if test="$align">
                        <fx:attribute name="align">
                            <fx:value-of select="$align" />
                        </fx:attribute>
                    </fx:if>
                    <fx:if test="$empty">
                        <fx:attribute name="umi:empty">
                            <fx:value-of select="$empty" />
                        </fx:attribute>
                    </fx:if>
                    <fx:if test="$alt">
                        <fx:attribute name="alt">
                            <fx:value-of select="$alt" />
                        </fx:attribute>
                    </fx:if>
                    <fx:if test="$title">
                        <fx:attribute name="title">
                            <fx:value-of select="$title" />
                        </fx:attribute>
                    </fx:if>
                    <fx:if test="$class">
                        <fx:attribute name="class">
                            <fx:value-of select="$class" />
                        </fx:attribute>
                    </fx:if>
                    <fx:if test="not($no-fix)">
                        <fx:attribute name="width">
                            <fx:value-of select="width" />
                        </fx:attribute>
                        <fx:attribute name="height">
                            <fx:value-of select="height" />
                        </fx:attribute>
                    </fx:if>
                    <fx:if test="$item = 1">
                        <fx:attribute name="itemprop">image</fx:attribute>
                    </fx:if>
                </img>
            </fx:otherwise>
        </fx:choose>

	</fx:template>
</fx:stylesheet>