<?php
class photoalbum_custom extends def_module {

    public function albumRandom($path = false, $template = "default", $limit = false, $ignore_paging = false) {
        $curr_page = (int) getRequest('p');
        $per_page = ($limit) ? $limit : $this->per_page;

        if($ignore_paging) $curr_page = 0;
        $element_id = $this->analyzeRequiredPath($path);

        if($element_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $path));
        }

        list($template_block, $template_block_empty, $template_line) = def_module::loadTemplates("photoalbum/".$template, "album_block", "album_block_empty", "album_block_line");
        $block_arr = Array();

        $hierarchy_type_id = umiHierarchyTypesCollection::getInstance()->getTypeByName("photoalbum", "photo")->getId();

        $sel = new umiSelection;
        $sel->addElementType($hierarchy_type_id);

        if($path != KEYWORD_GRAB_ALL) {
            $sel->addHierarchyFilter($element_id);
        }

        $sel->addPermissions();
        $sel->setOrderByRand();
        $sel->addLimit($per_page, $curr_page);

        $result = umiSelectionsParser::runSelection($sel);
        $total = umiSelectionsParser::runSelectionCounts($sel);

        $block_arr['id'] = $block_arr['void:album_id'] = $element_id;

        $lines = Array();
        if($total > 0) {
            foreach($result as $photo_element_id) {
                $line_arr = Array();

                $photo_element = umiHierarchy::getInstance()->getElement($photo_element_id);

                if(!$photo_element) continue;

                $line_arr['attribute:id'] = $photo_element_id;
                $line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($photo_element_id);
                $line_arr['xlink:href'] = "upage://" . $photo_element_id;
                $line_arr['node:name'] = $photo_element->getName();

                $this->pushEditable("photoalbum", "photo", $photo_element_id);
                $lines[] = self::parseTemplate($template_line, $line_arr, $photo_element_id);
            }
        } else {
            return self::parseTemplate($template_block_empty, $block_arr, $element_id);
        }

        $block_arr['subnodes:items'] = $block_arr['void:lines'] = $lines;
        $block_arr['total'] = $total;
        $block_arr['per_page'] = $per_page;
        $block_arr['link'] = umiHierarchy::getInstance()->getPathById($element_id);

        return self::parseTemplate($template_block, $block_arr, $element_id);
    }


};
?>