<?php
class catalog_custom extends def_module {

    public function getObjectsListNotField($path = false,$field = '', $limit = false, $ignore_paging = false, $i_need_deep = 0) {
        if (!$i_need_deep) $i_need_deep = intval(getRequest('param4'));
        if (!$i_need_deep) $i_need_deep = 1;
        $i_need_deep = intval($i_need_deep);
        if ($i_need_deep === -1) $i_need_deep = 100;

        $category_id = $this->analyzeRequiredPath($path);
        if($category_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $path));
        }
        $per_page = ($limit) ? $limit : $this->per_page;
        $curr_page = getRequest('p');
        if($ignore_paging) $curr_page = 0;

        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'object');
        $sel->where('hierarchy')->page($category_id)->childs($i_need_deep);
        $sel->where($field)->isnull(true);
        selectorHelper::detectFilters($sel);

        if($curr_page !== "all") {
            $curr_page = (int) $curr_page;
            $sel->limit($per_page*$curr_page, $per_page);
        }

        $result = $sel->result();
        $total = $sel->length();

        $block_arr = Array();
        $lines = Array();
        for($i = 0; $i < sizeof($result); $i++) {
            $element = &$result[$i];
            $line_arr = Array();
            $line_arr['attribute:id'] = $element->getId();
            $line_arr['attribute:alt_name'] = $element->getAltName();
            $line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element->getId());
            $line_arr['xlink:href'] = "upage://" . $element->getId();
            $line_arr['name'] = $element->getName();
            $line_arr['descr'] = $element->getValue('descr_mini');
            $line_arr['attribute:not_available'] = $element->getValue('not_available');

            $lines[] = $line_arr;
        }

        $block_arr['items']['nodes:item'] = $lines;
        $block_arr['total'] = $total;
        $block_arr['per_page'] = $per_page;
        $block_arr['category_id'] = $category_id;

        return $block_arr;
    }

    public function getObjectsListField($path = false,$field = '') {
        $category_id = $this->analyzeRequiredPath($path);
        if($category_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $path));
        }

        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'object');
        $sel->where('hierarchy')->page($category_id)->childs(1);
        $sel->where($field)->equals(1);

        $result = $sel->result();
        $block_arr = Array();
        $lines = Array();
        for($i = 0; $i < sizeof($result); $i++) {
            $element = &$result[$i];
            $line_arr = Array();
            $line_arr['attribute:id'] = $element->getId();
            $line_arr['attribute:alt_name'] = $element->getAltName();
            $line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element->getId());
            $line_arr['xlink:href'] = "upage://" . $element->getId();
            $line_arr['name'] = $element->getName();
            $line_arr['descr'] = $element->getValue('descr_mini');

            $lines[] = $line_arr;
        }
        $block_arr['items']['nodes:item'] = $lines;

        return $block_arr;
    }

    public function getTypeId($element_id){
        if($element = umiHierarchy::getInstance()->getElement($element_id))
            return $element->getObject()->getTypeId();
    }

    public function getSortPropertyItems($page_id,$property_name){
        if(!$page_id || !$property_name) return false;
        $props = umiHierarchy::getInstance()->getElement($page_id)->getValue($property_name);
        $result = array();
        foreach($props as $prop){
            if(!isset($prop['rel'])) continue;
            $object = umiObjectsCollection::getInstance()->getObject($prop['rel']);
            if(!$object) continue;
            $group = (int) $object->group_number;
            $result['+items'][$group]['+item'][] = $object;
        }
        return $result;
    }

    public function getCategoryOnly($path = false) {
        $category_id = $this->analyzeRequiredPath($path);
        if($category_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $path));
        }

        $sel = new selector('pages');
        $sel->types('hierarchy-type')->name('catalog', 'category');
        $sel->where('hierarchy')->page($category_id)->childs(1);

        $result = $sel->result();
        $block_arr = Array();
        $lines = Array();
        for($i = 0; $i < sizeof($result); $i++) {
            $element = &$result[$i];
            $line_arr = Array();
            $line_arr['attribute:id'] = $element->getId();
            $line_arr['attribute:alt_name'] = $element->getAltName();
            $line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element->getId());
            $line_arr['xlink:href'] = "upage://" . $element->getId();
            $line_arr['name'] = $element->getName();
            $line_arr['descr'] = $element->getValue('descr_mini');

            $lines[] = $line_arr;
        }
        $block_arr['items']['nodes:item'] = $lines;

        return $block_arr;
    }

    public function getObjectsOnly($path = false) {
        $category_id = $this->analyzeRequiredPath($path);
        if($category_id === false && $path != KEYWORD_GRAB_ALL) {
            throw new publicException(getLabel('error-page-does-not-exist', null, $path));
        }
        $category = umiHierarchy::getInstance()->getElement($category_id);
        if($category->getMethod() == 'object'){
            $result = array($category);
        }else{
            $sel = new selector('pages');
            $sel->types('hierarchy-type')->name('catalog', 'object');
            $sel->where('hierarchy')->page($category_id)->childs(1);
            selectorHelper::detectFilters($sel);
            $result = $sel->result();
        }
        $block_arr = Array();
        $lines = Array();
        for($i = 0; $i < sizeof($result); $i++) {
            $element = &$result[$i];
            $line_arr = Array();
            $line_arr['attribute:id'] = $element->getId();
            $line_arr['attribute:alt_name'] = $element->getAltName();
            $line_arr['attribute:art'] = $element->getValue('article');
            $line_arr['attribute:color'] = $element->getValue('color_code');
            $line_arr['attribute:volume'] = $element->getValue('volume');
            $line_arr['attribute:series'] = $element->getValue('series');
            $line_arr['attribute:amount'] = $element->getValue('common_quantity');
            $line_arr['attribute:link'] = umiHierarchy::getInstance()->getPathById($element->getId());
            $line_arr['xlink:href'] = "upage://" . $element->getId();
            $line_arr['name'] = $element->getName();
            //$line_arr['descr'] = $element->getValue('descr_mini');

            $lines[] = $line_arr;
        }
        $block_arr['items']['nodes:item'] = $lines;

        return $block_arr;
    }
};
?>