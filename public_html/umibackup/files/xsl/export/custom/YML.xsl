<?xml version="1.0" encoding="UTF-8"?>
<!--
TODO: // Write here your own templates
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:php="http://php.net/xsl"
	xmlns:udt="http://umi-cms.ru/2007/UData/templates"
	extension-element-prefixes="php"
	exclude-result-prefixes="xsl php udt">


    <xsl:template match="page[basetype/@method = 'object']">
        <xsl:param name="price" select="number(.//property[@name = 'price']/value)" />
        <xsl:if test="$price &gt; 0">
            <offer id="{@id}">
                <xsl:attribute name="available">
                    <xsl:choose>
                        <xsl:when test=".//property[@name = 'not_available']/value"><xsl:text>false</xsl:text></xsl:when>
                        <xsl:otherwise><xsl:text>true</xsl:text></xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <url><xsl:value-of select="concat('http://', /umidump/meta/domain, @link)" /></url>
                <price><xsl:value-of select="$price"/></price>
                <currencyId>
                    <xsl:choose>
                        <xsl:when test="string-length($default-currency)"><xsl:value-of select="$default-currency" /></xsl:when>
                        <xsl:otherwise>RUR</xsl:otherwise>
                    </xsl:choose>
                </currencyId>
                <categoryId><xsl:value-of select="@parentId"/></categoryId>
                <xsl:if test=".//property[@name = 'photo']/value != ''">
                    <picture>
                        <xsl:value-of select="concat('http://', /umidump/meta/domain, .//property[@name = 'photo']/value)"/>
                    </picture>
                </xsl:if>
                <store>false</store>
                <pickup>true</pickup>
                <delivery>true</delivery>
                <local_delivery_cost>250</local_delivery_cost>
                <name><xsl:value-of select="name"/></name>
                <xsl:if test=".//property[@name = 'vendor']/value">
                    <vendor><xsl:value-of select=".//property[@name = 'vendor']/value/item/@name"/></vendor>
                </xsl:if>
                <xsl:if test=".//property[@name = 'article']/value">
                    <vendorCode><xsl:value-of select=".//property[@name = 'article']/value"/></vendorCode>
                </xsl:if>
                <xsl:if test=".//property[@name = 'descr']/value != ''">
                    <xsl:variable name="descr" select="document(concat('udata://custom/stripField/',@id,'/descr'))/udata"/>
                    <description>
                        <xsl:choose>
                            <xsl:when test="string-length($descr) &gt; 512">
                                <xsl:value-of select="normalize-space(substring($descr,0,512))" disable-output-escaping="yes"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space($descr)" disable-output-escaping="yes"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </description>
                </xsl:if>
                <xsl:if test=".//property[@name = 'sales_notes']/value">
                    <sales_notes><xsl:value-of select=".//property[@name = 'sales_notes']/value"/></sales_notes>
                </xsl:if>
                <manufacturer_warranty>false</manufacturer_warranty>
                <xsl:if test=".//property[@name = 'vendor']/value">
                    <country_of_origin>
                        <xsl:variable name="vendor_id" select=".//property[@name = 'vendor']/value/item/@id"/>
                        <xsl:value-of select="document(concat('uobject://',$vendor_id,'.country'))/udata/property/value"/>
                    </country_of_origin>
                </xsl:if>
                <age unit="year">12</age>
                <!--<xsl:variable name="type_id" select="@type-id" />
                <xsl:variable name="page_id" select="@id" />
                <xsl:apply-templates select="../../types/type[@id = $type_id]" mode="filterable">
                    <xsl:with-param name="page_id" select="$page_id" />
                </xsl:apply-templates>-->
            </offer>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
