<meta charset="UTF-8"/>
<?php
include "standalone.php";

$sel = new selector('pages');
$sel->types('hierarchy-type')->name('catalog', 'object');
$sel->where('only')->equals(1);

$result = $sel->result();
$hierarchy = umiHierarchy::getInstance();
$relation = umiImportRelations::getInstance();
for($i = 0; $i < sizeof($result); $i++) {
    $element = &$result[$i];
    echo $element->getId()." - ".$element->getName()."<br/>";

    $import_id = $relation->addNewSource('restruct');
    if($rel_id = $relation->getNewIdRelation($import_id,$element->getId())){
        $new = $hierarchy->getElement($rel_id);
        $new->setValue('descr_mini',$element->getValue('descr_mini'));
        $new->setValue('price',$element->getValue('price'));
        $new->commit();
    }
}
?>